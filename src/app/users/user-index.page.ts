import { ACL } from '../common/util/acl.util';
import { Store } from '@ngrx/store';
import * as reducer from '../common/reducers/reducer';
import { User } from '../common/models/user.model';
import { OnInit, Component, ViewChild } from '@angular/core';
import { ApplicationConstant } from '../common/application-constant';
import { DataTableDirective } from 'angular-datatables';
import { UserEffects } from '../common/effects/users.effect';
import * as users from '../common/actions/users.action';

import { Subject } from 'rxjs';
declare var $: any;

@Component({
  selector: 'cat-page',
  templateUrl: './user-index.html',
})
export class UserIndexComponent implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  acl: ACL;
  /*
  'columnDefs': [
        { 'width': '10%', 'targets': 0 },
        { 'width': '20%', 'targets': 1 },
        { 'width': '20%', 'targets': 2 },
        { 'width': '20%', 'targets': 3 },
        { 'width': '15%', 'targets': 4 },
        { 'width': '10%', 'targets': 5 }
      ],
  */
  dtTrigger: Subject<any> = new Subject();
  dtOptions: DataTables.Settings = {
    'pageLength': 30,
    'language': ApplicationConstant.DATATABLE_GLOBAL_LANGUAGE_CONFIG,
    'autoWidth': true,
    'dom': '<\'row\'<\'col-sm-12\'B>>' +
      '<\'row\'<\'col-sm-6\'l><\'col-sm-6\'f>>' +
      '<\'row\'<\'col-sm-12\'tr>>' +
      '<\'row\'<\'col-sm-5\'i><\'col-sm-7\'p>>',
    'buttons': [
      {
        extend: 'excelHtml5',
        text: '<i class="fa fa-file-excel-o fa-x5"></i> ดาวน์โหลด Excel',
        className: 'btn btn-primary btn-sm glyphicon glyphicon-list-alt pull-right',
        exportOptions:{
          columns:[0,1,2,3,4,5]
        }
      },
      {
        extend: 'print',
        text: '<i class="fa fa-print fa-x5"></i> พิมพ์รายงาน',
        className: 'btn btn-primary btn-sm btn-print glyphicon glyphicon-print pull-right',
        pageSize: 'A4',
        title: 'รายการเอกสาร',
        exportOptions:{
          columns:[0,1,2,3,4,5]
        }
      }
    ]
  };
  public users: Array<User>;
  constructor(
    private userEffects:UserEffects,
    private _store: Store<reducer.State>) {
    this.acl = new ACL();
    this.users = new Array<User>();
    this._store.let(reducer.getUsers).subscribe(usersList => {
      this.users = usersList;
      this.rerender();
      return this.users
    });
    this.userEffects.loadUsers$.filter(
      action => action.type === users.ActionTypes.LOAD_USERS_SUCCESS
    ).subscribe(() => {
      this.rerender()
    });
    this.userEffects.deleteUsers$.filter(
      action => action.type === users.ActionTypes.DELETE_USERS_SUCCESS
    ).subscribe(() => {
      this.rerender()
    });
  }
  rerender(): void {
    if (this.dtElement) {
      if(this.dtElement.dtInstance!=undefined){
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          // Destroy the table first
          dtInstance.destroy();
          // Call the dtTrigger to rerender again
          this.dtTrigger.next();
        });
      }else{
        this.dtTrigger.next();
      }
    }
  }
  ngOnInit() {
    this._store.dispatch(new users.LoadUsersAction({}));
  }

  deleteUser(userId: number) {
    this._store.dispatch(new users.DeleteUserAction(userId));
  }
  role(role: string): boolean {
    return this.acl.hasRole(role);
  }
  isReadOnly(role_list: Array<string>): boolean {
    let result = false;
    let thisobj = this;
    $.each(role_list,function(i,role){ 
      result = result||(thisobj.acl.isReadOnly(role)===false)
    });
    return !result;
  }

  pad(num: number, size: number): string {
    let s = num + '';
    while (s.length < size) {
      s = '0' + s;
    }
    return s;
  }
}
