import { Store } from '@ngrx/store';
import * as reducer from '../common/reducers/reducer';
import { DataTableDirective } from 'angular-datatables';
import { ApplicationConstant } from '../common/application-constant';
import { Component, OnInit, ViewChild } from '@angular/core';
import { UserPermission } from '../common/models/user-permissions.model';
import { UserGroup } from '../common/models/usergroup.model';
import { Role } from '../common/models/role.model';
import * as user_permission from '../common/actions/user-permissions.action';
import * as usergroups from '../common/actions/usergroups.action';
import { UserPermissionEffects } from '../common/effects/user-permissions.effect';

@Component({
  selector: 'cat-page',
  templateUrl: './user-permissions.html'
})

export class UserPermissions implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  dtOptions: DataTables.Settings = {
    // 'columnDefs': [
    //   { 'width': '40%', 'targets': 0 },
    // //   { 'width': '10%', 'targets': 1 },
    // //   { 'width': '10%', 'targets': 2 },
    // //   { 'width': '10%', 'targets': 3 },
    // //   { 'width': '10%', 'targets': 4 },
    // //   { 'width': '10%', 'targets': 5 },
    // ],
    'autoWidth': false,
    'pageLength': 30,
    'language': ApplicationConstant.DATATABLE_GLOBAL_LANGUAGE_CONFIG
  };
  public user_permission: Array<UserPermission>;
  public userGroups: Array<UserGroup>;
  public result:object;
  public modeOptions:Array<object>;
  public modePrintOptions:Array<object>;
  constructor(private _store: Store<reducer.State>,
    private userPermissionEffects: UserPermissionEffects,) {
    this.user_permission = new Array<UserPermission>();
    this.modeOptions = [{id:0,name:"ไม่สามารถใช้งาน"},{id:1,name:"เรียกดูอย่างเดียว"},{id:2,name:"สร้าง,แก้ไข,บันทึก"}];
    this.modePrintOptions = [{id:0,name:"ไม่สามารถใช้งาน"},{id:1,name:"พิมพ์&ดาวน์โหลด"}];
    this._store.let(reducer.getUserGroupsPermission).subscribe(userGroups => {
      this.userGroups = userGroups
      this.genResult();
    });
    this._store.let(reducer.getUserPermissions).subscribe(user_permissionList => {
      this.user_permission = user_permissionList;
      console.log(this.result)
      console.log("sdkfjksdjflkjsdklfj",this.user_permission)
      this.initResult();
    });
    // console.log("userGroups",this.userGroups)
    
      //'ATFMNG07':{1:{'view':true,'edit':false}}
      this.userPermissionEffects.createRole$.filter(
        action => action.type === user_permission.ActionTypes.CREATE_ROLE_SUCCESS)
        .subscribe(() => this.refresh_success());
      this.userPermissionEffects.createRole$.filter(
        action => action.type === user_permission.ActionTypes.CREATE_ROLE_FAIL)
        .subscribe(() => this.refresh());
      this.userPermissionEffects.deleteRole$.filter(
        action => action.type === user_permission.ActionTypes.DELETE_ROLE_SUCCESS)
        .subscribe(() => this.refresh_success());
      this.userPermissionEffects.deleteRole$.filter(
        action => action.type === user_permission.ActionTypes.DELETE_ROLE_FAIL)
        .subscribe(() => this.refresh());
      this.userPermissionEffects.updateRole$.filter(
        action => action.type === user_permission.ActionTypes.UPDATE_ROLE_SUCCESS)
        .subscribe(() => this.refresh_success());
      this.userPermissionEffects.updateRole$.filter(
        action => action.type === user_permission.ActionTypes.UPDATE_ROLE_FAIL)
        .subscribe(() => this.refresh());
   
  }
  genResult(){
    this.result = {}
    const keylist=['ACLMNG','ATFMNG','ATFMNG02','ATFMNG03','ATFMNG04','ATFMNG05','ATFMNG06','ATFMNG07','ATFVIW','ATFVIW02','ATFVIW03','DEPMNG','DOCMNG','DOCMNG02','METDAT','METDAT02','TCKMNG','TSKASM','TSKASM02','TSKRVW','TSKRVW02','TSKAPP','USRMNG','TSKRVW03']
    // ['ACLMNG','ATFAPP','ATFMNG','ATFVIW','DEPMNG','DOCMNG','METDAT','SYSSET','TCKMNG','TSKASM','TSKVIW','TSKRVW','LCNMNG','TSKAPP','USRMNG']
    console.log("result",this.result)
    for (const key of keylist) {
      this.result[key] = {}
      for (const user_group of this.userGroups) {
        // this.result[key][user_group.id]={'view':false,'edit':false}
        this.result[key][user_group.id]={'mode':0}
      }
    }
  }
  initResult(){
    console.log("userGroups",this.userGroups)
    console.log("result",this.result)
    console.log("user_permission",this.user_permission)
    for (const user_permission of this.user_permission) {
      for (const role of user_permission.roles) {
        // let view = false
        // let edit = false
        // if(role.isReadOnly){
        //   view = true
        //   edit = false
        // }else{
        //   view = false
        //   edit = true
        // }
        // if(this.result.hasOwnProperty(role.key)){
        //   this.result[role.key][user_permission.id]={'view':view,'edit':edit}
        // }
        let mode = 0
        if(role.isReadOnly){
          mode = 1
        }else{
          mode = 2
        }
        if(this.result.hasOwnProperty(role.key)){
          this.result[role.key][user_permission.id]={'mode':mode}
        }
      }
    }
    console.log("result",this.result)
  }
  validRole(key,groupId){
    let result = false
    for (const user_permission of this.user_permission) {
      for (const role of user_permission.roles) {
        if((role.key==key)&&(user_permission.id==groupId)){
          result = true
        }
      }
    }
    return result
  }
  refresh_success(){
    this._store.dispatch(new user_permission.LoadRoleAction({}));
  }
  refresh(){
    this.userGroups = new Array<UserGroup>();
    this._store.dispatch(new usergroups.LoadUserGroupPermissionAction({}));
    this._store.dispatch(new user_permission.LoadRoleAction({}));
  }
  createRole(role,groupId,isReadOnly){
    const role_obj = new Role();
    role_obj.role = role
    role_obj.groupId = groupId
    role_obj.isReadOnly = isReadOnly
    return role_obj
  }
  getRole(key,groupId){
    const keylist={'ACLMNG':110,'ATFMNG':220,'ATFMNG02':221,'ATFMNG03':222,'ATFMNG04':223,'ATFMNG05':224,'ATFMNG06':225,'ATFMNG07':226,'ATFVIW':230,'ATFVIW02':231,'ATFVIW03':232,'DEPMNG':310,'DOCMNG':410,'DOCMNG02':411,'METDAT':610,'METDAT02':611,'TCKMNG':810,'TSKASM':910,'TSKASM02':911,'TSKRVW':930,'TSKRVW02':931,'TSKAPP':940,'USRMNG':1010,'TSKRVW03':932}
    return keylist[key]
  }
  getRoleId(key,groupId){
    let result = null;
    for (const user_permission of this.user_permission) {
      for (const role of user_permission.roles) {
        if((role.key==key)&&(user_permission.id==groupId)){
          result = role
        }
      }
    }
    if(result!=null){
      return result.id
    }else{
      return result
    }
  }
  submit(event,key,groupId,mode){
    console.log(event,key,groupId,mode)
    if((event==1)||(event==2)){
      let result = event==1?true:false;
      if(this.validRole(key,groupId)){
        // update
        console.log("update",key,this.getRole(key,groupId),groupId,result)
        let role_obj = this.createRole(this.getRole(key,groupId),groupId,result)
        role_obj.id = this.getRoleId(key,groupId)
        this._store.dispatch(new user_permission.UpdateRoleAction(role_obj));
      }else{
        // create
        console.log("create",key,this.getRole(key,groupId),groupId,result)
        let role_obj = this.createRole(this.getRole(key,groupId),groupId,result)
        console.log(this.userPermissionEffects.createRole$)
        this._store.dispatch(new user_permission.CreateRoleAction(role_obj));
      }
    }else{
      // delete
      console.log("delete",key,groupId,mode,this.getRoleId(key,groupId))  
      this._store.dispatch(new user_permission.DeleteRoleAction(this.getRoleId(key,groupId)));
    }
    
    console.log("change",key,groupId,mode)
  }
  checkPermission(id:number,key:string,type:number){
    console.log(id,key,type)
    return false
  }
  ngOnInit() {
    this.refresh();
    this.userPermissionEffects.deleteRole$.filter(
      action => action.type === user_permission.ActionTypes.DELETE_ROLE_FAIL)
      .subscribe(() => this.refresh());
  }
}
