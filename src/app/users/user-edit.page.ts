import { ACL } from '../common/util/acl.util';
import { UserEffects } from '../common/effects/users.effect';
import { Component, OnInit, ViewChild} from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { User } from '../common/models/user.model';
import { UserGroup } from '../common/models/usergroup.model';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import * as user from '../common/actions/users.action';
import * as reducer from '../common/reducers/reducer';
import * as usergroups from '../common/actions/usergroups.action';
import { SwalComponent } from '@toverux/ngsweetalert2';
declare var $: any;

@Component({
  selector: 'cat-page',
  templateUrl: './user-edit.html'
})

export class UserEditComponent implements OnInit {
  userEditForm: FormGroup;
  @ViewChild('validation_dialog') private validationDialog: SwalComponent;
  private userId: number;
  private user: User;
  public userGroups: Array<UserGroup>;
  public usergroup$: Observable<Array<UserGroup>>;
  acl: ACL;

  constructor(private _route: ActivatedRoute,
    private _store: Store<reducer.State>,
    private fb: FormBuilder,
    private router: Router,
    private userEffects: UserEffects) {
    this.acl = new ACL();
    // handle url string
    this._route.params.subscribe(params => {
      this.userId = +params['id'];
    });
    // dispatch select categoryId
    this._store.dispatch(new user.SelectUserAction(this.userId));
    // initial form
    this.userEditForm = this.fb.group({
      id: ['', [Validators.required]],
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      email: ['', [Validators.required]],
      groupId: ['', [Validators.required]],
      isActive: [false, [Validators.required]]
    });

    this._store.let(reducer.getSelectedUser).subscribe(user => {
      if (user != null) {
        this.user = user;
        this.setFormValue(user);
      }
    });

    // We filter out Id 6 because it belong to auto user - this work aorund will be apply when create also
    this._store.let(reducer.getUserGroups).subscribe(userGroups => this.userGroups = userGroups.filter(group => group.id !== 6));
    this.userEffects.updateUsers$.filter(
      action => action.type === user.ActionTypes.UPDATE_USERS_SUCCESS
    ).subscribe(() => this.redirectToIndex());
  }
  ngOnInit(): void {
    this._store.dispatch(new usergroups.LoadUserGroupAction({}));
  }
  onSubmit(): void {
    if(this.userEditForm.valid) {
      this.user = this.userEditForm.value;
      this._store.dispatch(new user.UpdateUserAction(this.user));
    }else{
      this.validationDialog.show();
    }
  }
  setFormValue(user: User): void {
    this.userEditForm.setValue({
      firstName: user.firstName,
      lastName: user.lastName,
      email: user.email,
      groupId: user.groupId,
      id: user.id,
      isActive:user.isActive
    });
  }
  redirectToIndex(): void {
    this.router.navigate(['/user/user-index']);
  }
  role(role: string): boolean {
    return this.acl.hasRole(role);
  }
  isReadOnly(role_list: Array<string>): boolean {
    let result = false;
    let thisobj = this;
    $.each(role_list,function(i,role){ 
      result = result||(thisobj.acl.isReadOnly(role)===false)
    });
    return !result;
  }
}
