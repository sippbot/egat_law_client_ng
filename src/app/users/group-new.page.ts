import { ACL } from '../common/util/acl.util';
import { UserGroupEffects } from '../common/effects/usergroups.effect';
import { Component, OnInit, Input ,ViewChild } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { UserGroup } from '../common/models/usergroup.model';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { ActivatedRoute } from '@angular/router';
import * as usergroup from '../common/actions/usergroups.action';
import * as reducer from '../common/reducers/reducer';
import { SwalComponent } from '@toverux/ngsweetalert2';
declare var $: any;

@Component({
  selector: 'cat-page',
  templateUrl: './group-new.html'
})

export class UserGroupNewComponent implements OnInit {
  userGroupNewForm: FormGroup;
  @Input()
  @ViewChild('validation_dialog') private validationDialog: SwalComponent;
  private usg: UserGroup;
  acl: ACL;

  constructor(private _store: Store<reducer.State>,
    private fb: FormBuilder,
    private router: Router,
    private userGroupEffects: UserGroupEffects) {
    // initial form
    this.acl = new ACL();
    this.userGroupNewForm = this.fb.group({
      name: ['', [Validators.required]]
    });
    this.userGroupEffects.craeteUserGroup$.filter(
      action => action.type === usergroup.ActionTypes.CREATE_USERGROUPS_SUCCESS
    ).subscribe(() => this.redirectToIndex());

  }
  ngOnInit(): void {
  }
  checkValidation(){
    let pass = true;
    if((this.userGroupNewForm.value.name==undefined)||(this.userGroupNewForm.value.name=="")){
      pass = false
    }
    return pass
  }
  onSubmit(): void {
    this.usg = this.userGroupNewForm.value;
    let validation = this.checkValidation();
    if(!validation){
      this.validationDialog.show();
      return
    }
    this._store.dispatch(new usergroup.CreateUsergroupAction(this.usg));
  }
  redirectToIndex(): void {
    this.router.navigate(['/user/group']);
  }
  role(role: string): boolean {
    return this.acl.hasRole(role);
  }
  isReadOnly(role_list: Array<string>): boolean {
    let result = false;
    let thisobj = this;
    $.each(role_list,function(i,role){ 
      result = result||(thisobj.acl.isReadOnly(role)===false)
    });
    return !result;
  }
}
