import { ACL } from '../common/util/acl.util';
import { Alert } from '../common/models/alert.model';
import { ShowAlertAction } from '../common/actions/alert.action';
import { UserGroupEffects } from '../common/effects/usergroups.effect';
import { Store } from '@ngrx/store';
import * as reducer from '../common/reducers/reducer';
import { UserGroup } from '../common/models/usergroup.model';
import { OnInit, Component, ViewChild } from '@angular/core';
import { ApplicationConstant } from '../common/application-constant';
import { DataTableDirective } from 'angular-datatables';
import * as usergroups from '../common/actions/usergroups.action';

import { Subject } from 'rxjs';
declare var $: any;

@Component({
  selector: 'cat-page',
  templateUrl: './group.html'
})

export class UserGroupComponent implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  acl: ACL;
  /*
      'columnDefs': [
      { 'width': '10%', 'targets': 0 },
      { 'width': '45%', 'targets': 1 },
      { 'width': '45%', 'targets': 2 }
    ],
  */
  dtTrigger: Subject<any> = new Subject();
  dtOptions: DataTables.Settings = {
    'autoWidth': true,
    'pageLength': 30,
    'language': ApplicationConstant.DATATABLE_GLOBAL_LANGUAGE_CONFIG,
    'dom': '<\'row\'<\'col-sm-12\'B>>' +
      '<\'row\'<\'col-sm-6\'l><\'col-sm-6\'f>>' +
      '<\'row\'<\'col-sm-12\'tr>>' +
      '<\'row\'<\'col-sm-5\'i><\'col-sm-7\'p>>',
    'buttons': [
      {
        extend: 'excelHtml5',
        text: '<i class="fa fa-file-excel-o fa-x5"></i> ดาวน์โหลด Excel',
        className: 'btn btn-primary btn-sm glyphicon glyphicon-list-alt pull-right',
        exportOptions:{
          columns:[0,1,2]
        }
      },
      {
        extend: 'print',
        text: '<i class="fa fa-print fa-x5"></i> พิมพ์รายงาน',
        className: 'btn btn-primary btn-sm btn-print glyphicon glyphicon-print pull-right',
        pageSize: 'A4',
        title: 'รายการเอกสาร',
        exportOptions:{
          columns:[0,1,2]
        }
      }
    ]
  };
  public usergroups: Array<UserGroup>;
  constructor(private _store: Store<reducer.State>,
    private userGroupEffects: UserGroupEffects) {
    this.acl = new ACL();
    this._store.let(reducer.getUserGroups).subscribe(x => this.usergroups = x);
    this.userGroupEffects.deleteUserGroup$.filter(
      action => action.type === usergroups.ActionTypes.DELETE_USERGROUPS_SUCCESS
    ).subscribe(() => this.pushSuccessNotification());
    this.userGroupEffects.deleteUserGroup$.filter(
      action => action.type === usergroups.ActionTypes.DELETE_USERGROUPS_FAIL
    ).subscribe(() => this.pushFailNotification());
    this.userGroupEffects.loadUserGroups$.filter(
      action => action.type === usergroups.ActionTypes.LOAD_USERGROUPS_SUCCESS
    ).subscribe(() => this.rerender());
  }
  rerender(): void {
    if (this.dtElement) {
      if(this.dtElement.dtInstance!=undefined){
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          // Destroy the table first
          dtInstance.destroy();
          // Call the dtTrigger to rerender again
          this.dtTrigger.next();
        });
      }else{
        this.dtTrigger.next();
      }
    }
  }
  ngOnInit() {
    this._store.dispatch(new usergroups.LoadUserGroupAction({}));
  }
  deleteUserGroup(groupId: number) {
    
    this._store.dispatch(new usergroups.DeleteUserGroupAction(groupId));
  }
  pushSuccessNotification() {
    this._store.dispatch(new ShowAlertAction(new Alert('ดำเนินการลบกลุ่มผู้ใช้สำเร็จ', 'สำเร็จ!', 'success')));
    this.rerender()
  }
  pushFailNotification() {
    this._store.dispatch(new ShowAlertAction(
      new Alert('ไม่สามารถลบกลุ่มผู้ใช้ได้เนื่องจาก มีผู้ใช้อยู่ในกลุ่มนี้', 'ล้มเหลว!', 'fail')));
  }
  role(role: string): boolean {
    return this.acl.hasRole(role);
  }
  isReadOnly(role_list: Array<string>): boolean {
    let result = false;
    let thisobj = this;
    $.each(role_list,function(i,role){ 
      result = result||(thisobj.acl.isReadOnly(role)===false)
    });
    return !result;
  }
}
