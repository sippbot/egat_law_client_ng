import { SweetAlert2Module } from '@toverux/ngsweetalert2';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';

import { UserIndexComponent } from './user-index.page';
import { UserNewComponent } from './user-new.page';
import { UserEditComponent } from './user-edit.page';

import { UserGroupComponent } from './group.component';
import { UserGroupNewComponent } from './group-new.page';
import { UserGroupEditComponent } from './group-edit.page';

import { UserPermissions } from './user-permissions.page';

import { StoreModule } from '@ngrx/store';
import { reducer } from '../common/reducers/reducer';
import { EffectsModule } from '@ngrx/effects';
import { MetadataService } from '../common/services/metadata.service';
import { UserService } from '../common/services/user.service';
import { UserGroupEffects } from '../common/effects/usergroups.effect';
import { UserEffects } from '../common/effects/users.effect';

import { AuthGuard } from '../guard/auth.guard';

export const routes: Routes = [
  { path: 'user/user-index', component: UserIndexComponent, canActivate: [AuthGuard] },
  { path: 'user/user-new', component: UserNewComponent, canActivate: [AuthGuard] },
  { path: 'user/user-edit/:id', component: UserEditComponent, canActivate: [AuthGuard] },

  { path: 'user/group', component: UserGroupComponent, canActivate: [AuthGuard] },
  { path: 'user/group-new', component: UserGroupNewComponent, canActivate: [AuthGuard] },
  { path: 'user/group-edit/:id', component: UserGroupEditComponent, canActivate: [AuthGuard] },
  { path: 'user/user-permissions', component: UserPermissions, canActivate: [AuthGuard] }

];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    DataTablesModule,
    SweetAlert2Module
    /* Put the reducer as an argument in provideStore */
  ],
  declarations: [
    UserIndexComponent,
    UserNewComponent,
    UserEditComponent,
    UserGroupComponent,
    UserGroupNewComponent,
    UserGroupEditComponent,
    UserPermissions
  ],
  providers: [MetadataService, UserService]
})

export class UsersModule { }
