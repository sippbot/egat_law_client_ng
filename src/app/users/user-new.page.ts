import { ACL } from '../common/util/acl.util';
import { UserEffects } from '../common/effects/users.effect';
import { Component, OnInit, Input ,ViewChild } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { User } from '../common/models/user.model';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import * as user from '../common/actions/users.action';
import * as reducer from '../common/reducers/reducer';
import { UserGroup } from '../common/models/usergroup.model';
import * as usergroups from '../common/actions/usergroups.action';
import { SwalComponent } from '@toverux/ngsweetalert2';
declare var $: any;

@Component({
  selector: 'cat-page',
  templateUrl: './user-new.html'
})

export class UserNewComponent implements OnInit {
  userNewForm: FormGroup;
  @Input()
  @ViewChild('validation_dialog') private validationDialog: SwalComponent;
  private user: User;
  public userGroups: Array<UserGroup>;
  public usergroup$: Observable<Array<UserGroup>>;
  acl: ACL;
  constructor(private _store: Store<reducer.State>,
    private fb: FormBuilder,
    private router: Router,
    private userEffects: UserEffects) {
    this.acl = new ACL();
    // initial form
    this.userNewForm = this.fb.group({
      id: ['', [Validators.required]],
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      email: ['', [Validators.required]],
      groupId: ['', [Validators.required]]
    });

    // We filter out Id 6 because it belong to auto user - this work aorund will be apply when edit user also
    this._store.let(reducer.getUserGroups).subscribe(userGroups => this.userGroups = userGroups.filter(group => group.id !== 6 ));
    this.userEffects.craeteUsers$.filter(
      action => action.type === user.ActionTypes.CREATE_USERS_SUCCESS
    ).subscribe(() => this.redirectToIndex());
  }
  ngOnInit(): void {
    this._store.dispatch(new usergroups.LoadUserGroupAction({}));
  }
  onSubmit(): void {
    if (this.userNewForm.valid) {
      this.user = this.userNewForm.value;
      this._store.dispatch(new user.CreateUserAction(this.user));
    } else {
      this.validationDialog.show();
    }

  }
  redirectToIndex(): void {
    this.router.navigate(['/user/user-index']);
  }
  role(role: string): boolean {
    return this.acl.hasRole(role);
  }
  isReadOnly(role_list: Array<string>): boolean {
    let result = false;
    let thisobj = this;
    $.each(role_list,function(i,role){ 
      result = result||(thisobj.acl.isReadOnly(role)===false)
    });
    return !result;
  }
}
