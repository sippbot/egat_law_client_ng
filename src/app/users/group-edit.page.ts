import { ACL } from '../common/util/acl.util';
import { UserGroupEffects } from '../common/effects/usergroups.effect';
import { Component, OnInit, Input ,ViewChild} from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { UserGroup } from '../common/models/usergroup.model';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { ActivatedRoute } from '@angular/router';
import * as usergroup from '../common/actions/usergroups.action';
import * as reducer from '../common/reducers/reducer';
import { SwalComponent } from '@toverux/ngsweetalert2';

declare var $: any;

@Component({
  selector: 'cat-page',
  templateUrl: './group-edit.html'
})

export class UserGroupEditComponent implements OnInit {
  userGroupEditForm: FormGroup;
  @ViewChild('validation_dialog') private validationDialog: SwalComponent;
  private usergroupId: number;
  private userGroup: UserGroup;
  acl: ACL;

  constructor(private _route: ActivatedRoute,
    private _store: Store<reducer.State>,
    private fb: FormBuilder,
    private router: Router,
    private userGroupEffects: UserGroupEffects) {
    // handle url string
    this.acl = new ACL();
    this._route.params.subscribe(params => {
      this.usergroupId = +params['id'];
    });
    // dispatch select categoryId
    this._store.dispatch(new usergroup.SelectUsergroupAction(this.usergroupId));
    // initial form
    this.userGroupEditForm = this.fb.group({
      id: ['', [Validators.required]],
      name: ['', [Validators.required]],
      isSetPermission: [false,[Validators.required]]
    });

    this._store.let(reducer.getSelectedUsergroup).subscribe(userg => {
      if (userg != null) {
        this.userGroup = userg;
        this.setFormValue(userg);
      }
    });
    this.userGroupEffects.updateUserGroup$.filter(
      action => action.type === usergroup.ActionTypes.UPDATE_USERGROUPS_SUCCESS
    ).subscribe(() => this.redirectToIndex());
  }

  ngOnInit(): void {
  }
  checkValidation(){
    let pass = true;
    if((this.userGroupEditForm.value.name==undefined)||(this.userGroupEditForm.value.name=="")){
      pass = false
    }
    return pass
  }
  onSubmit(): void {
    this.userGroup = this.userGroupEditForm.value;
    let validation = this.checkValidation();
    if(!validation){
      this.validationDialog.show();
      return
    }
    this._store.dispatch(new usergroup.UpdateUsergroupAction(this.userGroup));
  }
  setFormValue(usergroup: UserGroup): void {
    this.userGroupEditForm.setValue(usergroup);
  }
  redirectToIndex(): void {
    this.router.navigate(['/user/group']);
  }

  role(role: string): boolean {
    return this.acl.hasRole(role);
  }
  isReadOnly(role_list: Array<string>): boolean {
    let result = false;
    let thisobj = this;
    $.each(role_list,function(i,role){ 
      result = result||(thisobj.acl.isReadOnly(role)===false)
    });
    return !result;
  }
}
