import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-artifact-view',
  templateUrl: './view.component.html'
})
export class ArtifactViewComponent {
  artifactId: number;

  constructor(private _route: ActivatedRoute) {
    this._route.params.subscribe(params => {
      this.artifactId = +params['id'];
    });
  }

}

