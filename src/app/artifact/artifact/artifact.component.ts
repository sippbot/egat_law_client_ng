import { ArtifactEffects } from '../../common/effects/artifact.effect';
import { Subscription } from 'rxjs';
import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { Category } from '../../common/models/category.model';
import { Lawlevel } from '../../common/models/lawlevel.model';
import { Artifact } from '../../common/models/artifact.model';
import { ArtifactNode } from '../../common/models/artifact-node.model';
import { ArtifactNodeGroup } from '../../common/models/artifact-node-group.model';
import { Department } from '../../common/models/department.model';
import { LicenseType } from '../../common/models/license-type.model';
import { Document } from '../../common/models/document.model';
import { FileData } from '../../common/models/file-data.model';
import { Router, ActivatedRoute } from '@angular/router';
import { ApplicationConstant } from '../../common/application-constant';
import { Http, Headers, RequestOptions } from '@angular/http';
// import { HttpClient,HttpRequest,HttpHeaders  } from '@angular/common/http';
// import { pipe } from 'rxjs/util/pipe';
// import { Observable } from 'rxjs/Observable';
// import { tap } from 'rxjs/operators';
declare var $: any;
declare var jQuery: any;
import { Store } from '@ngrx/store';
import * as reducer from '../../common/reducers/reducer';
import * as artifact from '../../common/actions/artifact.action';
import * as category from '../../common/actions/category.action';
import * as lawlevels from '../../common/actions/lawlevel.action';
import * as department from '../../common/actions/departments.action';
import * as licensetype from '../../common/actions/license-type.action';
import { IMyOptions } from 'mydatepicker-th';
import * as alert from '../../common/actions/alert.action';
import { Alert } from '../../common/models/alert.model';
import { Actions } from '@ngrx/effects';
import { SwalComponent } from '@toverux/ngsweetalert2';
declare var NProgress: any;

@Component({
  selector: 'cat-page',
  templateUrl: './artifact.component.html',
})

export class ArtifactComponent implements OnInit, AfterViewInit {
  categories: Category[];
  lawlevels: Lawlevel[];
  artifact: Artifact;
  @ViewChild('validation_dialog') private validationDialog: SwalComponent;
  artifactNodeGroups: Array<ArtifactNodeGroup>;
  artifactId: number;

  isCreateRequest: boolean;
  isEditMode: boolean;
  createSuccess: Subscription;
  step: number;
  validationError: string;
  createDate: any; // IMyDateModel;
  publishedDate: any; // IMyDateModel;
  effectiveDate: any; // IMyDateModel;

  isHasFileUpload = false;

  placeholder = 'dd/mm/yyyy';
  myDatePickerOptions: IMyOptions = {
    dateFormat: 'dd/mm/yyyy',
    editableDateField: false,
    openSelectorOnInputClick: true
  };

  public options: Object = {
    charCounterCount: true,
    height: 200,
    toolbarButtons: ['bold', 'italic', 'underline', 'paragraphFormat', 'insertTable', 'alert'],
    toolbarButtonsXS: ['bold', 'italic', 'underline', 'paragraphFormat', 'insertTable', 'alert'],
    toolbarButtonsSM: ['bold', 'italic', 'underline', 'paragraphFormat', 'insertTable', 'alert'],
    toolbarButtonsMD: ['bold', 'italic', 'underline', 'paragraphFormat', 'insertTable', 'alert'],
    tableEditButtons: ['tableRows', 'tableColumns', 'tableCells', 'tableCellVerticalAlign', 'tableRemove'],
  };

  // TODO: fix to use internal form property
  private _selectdCategory: number;
  private _selectdLawlevel: number;

  private apiEndPoint: string = ApplicationConstant.APP_BASE_ENDPOINT_URL + '/fileupload';
  count = 0;
  fileDatas: FileData[];
  documents: Document[];

  departments: Department[];

  licensetypes: LicenseType[];

  constructor(private _store: Store<reducer.State>,
    private artifactEffects: ArtifactEffects,
    private _route: ActivatedRoute,
    private router: Router,
    private http: Http) {
    this.artifact = new Artifact();
    this.artifactId = 0;
    this.validationError = "";
    this.artifactNodeGroups = new Array<ArtifactNodeGroup>();
    // this.artifact.nodeGroups = new Array<ArtifactNodeGroup>();
    // this._store.dispatch(new artifact.SelectArtifactAction(0));
    this.documents = [];
    this.fileDatas = [];
    this.step = 0;
    this.isCreateRequest = true;

    this._route.params.subscribe(params => {
      this.artifactId = +params['id'];
      this.isEditMode = true;
      if (this.artifactId > 0) {
        NProgress.start();
        this._store.dispatch(new artifact.SelectOrLoadArtifactAction({ artifactId: this.artifactId, isActive: true }));
      }
    });

    if (this.router.url.indexOf('/artifact/new') >= 0) {
      this.artifactId = 0;
      this.artifact = new Artifact();
    }

    this._store.let(reducer.getSelectedArtifacts).subscribe(
      artifact => {
        if ((artifact != null)) {
          // console.log(artifact,this.artifactId,this.router.url.indexOf('/artifact/new') >= 0,this.isCreateRequest,this.step)
          if ((this.router.url.indexOf('/artifact/new') >= 0) && (this.step == 0)) {
            NProgress.done();
            this.artifactId = 0;
            this.artifact = new Artifact();
            this.isCreateRequest = true;
            this.artifactNodeGroups = new Array<ArtifactNodeGroup>()
            this.documents = [];
            this.fileDatas = [];
            this.setSelect();
          } else {
            NProgress.done();
            this.artifactId = artifact.id;
            this.artifact = artifact;
            this.isCreateRequest = false;
            this.artifactNodeGroups = artifact.nodeGroups.sort((a, b) => a.seq - b.seq);
            this.setDatePicker();
            this.setSelect();
            this.fileDatas = [];
            this.documents = artifact.documents;
            this.setFileData();
          }
          // console.log(artifact,this.artifactId,this.router.url.indexOf('/artifact/new') >= 0,this.isCreateRequest,this.step)
        }
      }
    );

    this._store.let(reducer.getCategories).subscribe(
      cats => {
        this.categories = cats;
        this.setCategorySelect();
      }
    );

    this._store.let(reducer.getLawlevels).subscribe(
      levels => {
        this.lawlevels = levels;
        this.setLevelSelect();
      }
    );

    this._store.let(reducer.getDepartments).subscribe(
      department => this.departments = department
    );

    this._store.let(reducer.getLicenseTypes).subscribe(
      licensetype => this.licensetypes = licensetype
    );
    /*
        this.artifactEffects.craeteArtifact$.filter(
          action => action.type === artifact.ActionTypes.CREATE_ARTIFACT_SUCCESS)
          .subscribe(() => this.redirectToIndex());
    */
    this.artifactEffects.updateArtifact$.filter(
      action => action.type === artifact.ActionTypes.UPDATE_ARTIFACT_SUCCESS)
      .subscribe(() => this.redirectToIndex());
  }

  ngOnInit() {

    this.initUI();
    this._store.dispatch(new category.LoadCategoryAction({}));
    this._store.dispatch(new lawlevels.LoadLawlevelAction({}));
    this._store.dispatch(new department.LoadActiveDepartmentsAction({}));
    this._store.dispatch(new licensetype.LoadLicenseTypeAction({}));
    // this.checkValidation();
    // console.log("ngOnInit")
  }

  get currentArtifact() { return JSON.stringify(this.artifact); }

  setDatePicker() {
    // Effective date
    const effectiveDate = this.stringToDate(this.artifact.effectiveDate, 'dd/MM/yyyy', '/');
    this.effectiveDate = {
      date: {
        year: effectiveDate.getFullYear(),
        month: effectiveDate.getMonth() + 1,
        day: effectiveDate.getDate()
      }
    };

    const createDate = this.stringToDate(this.artifact.createDate, 'dd/MM/yyyy', '/');
    this.createDate = {
      date: {
        year: createDate.getFullYear(),
        month: createDate.getMonth() + 1,
        day: createDate.getDate()
      }
    };

    const publishDate = this.stringToDate(this.artifact.publishedDate, 'dd/MM/yyyy', '/');
    this.publishedDate = {
      date: {
        year: publishDate.getFullYear(),
        month: publishDate.getMonth() + 1,
        day: publishDate.getDate()
      }
    };
  }

  setFileData() {
    this.artifact.documents.forEach(element => {
      const file: FileData = new FileData();
      console.log(element.url)
      file.title = element.title;
      file.href = element.url;
      file.id = element.id;
      this.fileDatas.push(file);
    });
  }

  setSelect() {
    this.setTypeSelect();
    this.setCategorySelect();
    this.setLevelSelect();
  }
  setTypeSelect() {
    if (this.artifact != null) {
      jQuery('.field_type').val(this.artifact.type != undefined ? this.artifact.type : "").trigger('change');
    } else {
      jQuery('.field_type').val("").trigger('change');
    }
  }
  setCategorySelect() {
    if (this.artifact != null) {
      jQuery('.field_category').val(this.artifact.categoryId != undefined ? this.artifact.categoryId : "").trigger('change');
    } else {
      jQuery('.field_category').val("").trigger('change');
    }
  }
  setLevelSelect() {
    if (this.artifact != null) {
      jQuery('.field_lawlevel').val(this.artifact.levelId != undefined ? this.artifact.levelId : "").trigger('change');
    } else {
      jQuery('.field_lawlevel').val("").trigger('change');
    }
  }

  fileChange(event) {
    const fileList: FileList = event.target.files;
    this.isHasFileUpload = true;
    if (fileList.length > 0) {
      this._store.dispatch(new alert.ShowAlertAction(
        new Alert('อัพโหลดไฟล์เข้าสู่ระบบ จำนวน ' + fileList.length + ' ไฟล์', 'กำลังอัพโหลดไฟล์!', 'info')
      ));
    }
    let count = 0
    for (let i = 0; i < fileList.length; i++) {
      const file: File = fileList[i];
      const formData: FormData = new FormData();

      formData.append('uploadFile', file, file.name);
      formData.append('ownerId', localStorage.getItem('userId'));
      formData.append('departmentId', localStorage.getItem('departmentId'));
      // let xxsrfToken = localStorage.getItem('xsrfToken');
      const headers = new Headers();
      headers.append('Accept', 'application/json');
      // headers.append('X-XSRF-TOKEN', xxsrfToken);
      // const req = new HttpRequest('POST',`${this.apiEndPoint}`, formData, {
      //   reportProgress: true
      // });
      // this.httpClient.request(req).pipe(map(res => res))
      // const options = {
      //   headers: new HttpHeaders({
      //     'Accept':  'application/json'
      //   }),
      //   reportProgress:true,

      // }
      // this.httpClient.post(`${this.apiEndPoint}`, formData, options)
      // .map(res => res)
      // .subscribe(
      //   data => this.handleFileUploadSuccess(data),
      //   error => console.log(error.json())
      // );
      // map(res => res),
      // .tap(),
      // .subscribe(
      //   data => this.handleFileUploadSuccess(data),
      //   error => console.log(error.json())
      // );
      // );
      //   map(event => this.getEventMessage(event, file)),
      // .tap(message => this.showProgress(message)),
      //   last(), // return last (completed) message to caller
      //   catchError(this.handleError(file))
      // );
      const options = new RequestOptions({ headers: headers });
      this.http.post(`${this.apiEndPoint}`, formData, options)
        .map(res => res.json())
        // .catch(error => Observable.throw(error))
        .subscribe(
          data => this.handleFileUploadSuccess(data),
          error => console.log(error.json())
        );
      count += 1;
    }
    if (count == fileList.length) {
      $("#addnew-file").val("")
      this.isHasFileUpload = false;
    }
  }

  handleFileUploadSuccess(data: any) {
    const f: FileData = new FileData();
    f.title = data.filename;
    this._store.dispatch(new alert.ShowAlertAction(
      new Alert('อัพโหลดไฟล์ ' + f.title + ' เข้าสู่ระบบ เรียบร้อยแล้ว!', 'สำเร็จแล้ว!', 'success')
    ));
    f.href = data.url//this.fileEndpoint + data.id + '/' //+ data.filename;
    f.id = data.id;
    this.fileDatas.push(f);
    const doc: Document = new Document();
    doc.id = data.id;
    doc.title = f.title;
    this.documents.push(doc);
  }

  deleteAttachment(documentId: number) {
    let aid: number;
    if (this.artifactId) {
      aid = this.artifactId;
    } else {
      aid = 0;
    }

    this._store.dispatch(new artifact.DeleteArtifactAttachmentAction({
      artifactId: aid,
      documentId: documentId
    }));

    this.fileDatas = this.fileDatas.filter(item => item.id !== documentId);
    this.documents = this.documents.filter(doc => doc.id !== documentId);
  }

  ngAfterViewInit() {
    jQuery('.field_category').on(
      'change',
      (e) =>
        this._selectdCategory = jQuery(e.target).val()
    );

    jQuery('.field_lawlevel').on(
      'change',
      (e) => this._selectdLawlevel = jQuery(e.target).val()
    );
  }

  checkValidation() {
    $("#type").removeClass("content-invalid")
    $("#categoryId").parent().removeClass("content-invalid")
    $("#levelId").parent().removeClass("content-invalid")
    $("#title").parent().removeClass("content-invalid")
    $("#source").parent().removeClass("content-invalid")
    $("#createDate").parent().removeClass("content-invalid")
    $("#publishedDate").parent().removeClass("content-invalid")
    $("#effectiveDate").parent().removeClass("content-invalid")
    let pass = true
    let artifact = this.artifact;
    let error = []
    let categoryId = "" + artifact.categoryId
    let levelId = "" + artifact.levelId
    if (artifact.type == undefined) {
      pass = false;
      error.push("เกี่ยวข้องกับกฎหมายในด้าน");
      $("#type").addClass("content-invalid")
    }
    if ((artifact.categoryId == undefined) || (categoryId.length == 0)) {
      pass = false
      error.push("หมวดหมู่กฎหมาย");
      $("#categoryId").parent().addClass("content-invalid")
    }
    if ((artifact.levelId == undefined) || (levelId.length == 0)) {
      pass = false
      error.push("ประเภทของกฎหมาย");
      $("#levelId").parent().addClass("content-invalid")
    }
    if ((artifact.title == undefined) || (artifact.title == "")) {
      pass = false
      error.push("เรื่อง");
      $("#title").parent().addClass("content-invalid")
    }
    if ((artifact.source == undefined) || (artifact.source == "")) {
      pass = false
      error.push("แหล่งที่มาของกฎหมาย");
      $("#source").parent().addClass("content-invalid")
    }
    if ((artifact.createDate == undefined) || (artifact.createDate == "")) {
      pass = false
      error.push("ประกาศ ณ วันที่")
      $("#createDate").parent().addClass("content-invalid")
    }
    if ((artifact.publishedDate == undefined) || (artifact.publishedDate == "")) {
      pass = false
      error.push("วันประกาศในราชกิจจานุเบกษา")
      $("#publishedDate").parent().addClass("content-invalid")
    }
    if ((artifact.effectiveDate == undefined) || (artifact.effectiveDate == "")) {
      pass = false
      error.push("วันที่มีผลบังคับ")
      $("#effectiveDate").parent().addClass("content-invalid")
    }
    if (error.length > 0) {
      this.validationError = "* โปรดระบุข้อมูล : " + error.join(",");
    }
    return pass
  }

  onSubmit(type: string) {
    this.validationError = "";
    this.artifact.status = type;
    this.artifact.categoryId = this._selectdCategory != null ? this._selectdCategory : this.artifact.categoryId;
    this.artifact.levelId = this._selectdLawlevel != null ? this._selectdLawlevel : this.artifact.levelId;
    this.artifact.documents = this.documents != null ? this.documents : this.artifact.documents;
    this.artifact.createDate = this.createDate != undefined ? this.createDate.formatted != null ? this.createDate.formatted : this.artifact.createDate : "";
    this.artifact.effectiveDate = this.effectiveDate != undefined ? this.effectiveDate.formatted != null ? this.effectiveDate.formatted : this.artifact.effectiveDate : "";
    this.artifact.publishedDate = this.publishedDate != undefined ? this.publishedDate.formatted != null ? this.publishedDate.formatted : this.artifact.publishedDate : "";
    let validation = this.checkValidation();
    if (!validation) {
      this.validationDialog.show();
      return false
    }
    if (this.isCreateRequest) {
      this._store.dispatch(new artifact.CreateArtifactAction(this.artifact))

      // this.artifactId=this.artifact.id
    } else {
      this._store.dispatch(new artifact.UpdateArtifactAction(this.artifact));
    }
    // TODO remove this and use subscribe
    // this.metadataService.createArtifact(value).subscribe(r => this.redirectToIndex(r));
  }

  onUpdate(type: string) {
    this.step = 3;
    if (type == "Draft") {
      this.step = 4;
    }
    this.artifact.status = type;
    this.artifact.categoryId = this._selectdCategory != null ? this._selectdCategory : this.artifact.categoryId;
    this.artifact.levelId = this._selectdLawlevel != null ? this._selectdLawlevel : this.artifact.levelId;
    this.artifact.documents = this.documents != null ? this.documents : this.artifact.documents;
    this.artifact.createDate = this.createDate.formatted != null ? this.createDate.formatted : this.artifact.createDate;
    this.artifact.effectiveDate = this.effectiveDate.formatted != null ? this.effectiveDate.formatted : this.artifact.effectiveDate;
    this.artifact.publishedDate = this.publishedDate.formatted != null ? this.publishedDate.formatted : this.artifact.publishedDate;

    this._store.dispatch(new artifact.UpdateArtifactAction(this.artifact));
  }

  private initUI() {
    let thisArtifact = this;
    $(function () {
      $('.select2').select2();
      $('.dropify').dropify();
      $('.next-step-1').on('click', function () {
        let validation = thisArtifact.checkValidation();
        if (!validation) {
          return false
        }
        thisArtifact.step = 1
        $('.step2').parent().parent().addClass('active');
        $('#tab1').removeClass('active');
        $('#tab2').addClass('active');
      });
      $('.next-step-2').on('click', function () {
        thisArtifact.step = 2
        $('.step3').parent().parent().addClass('active');
        $('#tab2').removeClass('active');
        $('#tab3').addClass('active');
      });
      $('.before-step-2').on('click', function () {
        thisArtifact.step = 0
        $('.step2').parent().parent().removeClass('active');
        $('#tab2').removeClass('active');
        $('#tab1').addClass('active');
      });
      $('.before-step-3').on('click', function () {
        thisArtifact.step = 1
        $('.step3').parent().parent().removeClass('active');
        $('#tab3').removeClass('active');
        $('#tab2').addClass('active');
      });
      thisArtifact.setSelect();
    });
  }

  addArtifactNodeGroup() {
    const artifactNodeGroup = new ArtifactNodeGroup();
    artifactNodeGroup.nodes = new Array<ArtifactNode>();
    artifactNodeGroup.isEditMode = true;
    artifactNodeGroup.id = 0;
    this.artifactNodeGroups.push(artifactNodeGroup);
  }

  removeNodeContent(i: number, j: number) {
    this.artifact.nodeGroups[i].nodes.splice(j, 1);
  }

  refreshLicense() {
    this._store.dispatch(new licensetype.LoadLicenseTypeAction({}));
  }

  redirectToIndex(): void {
    console.log(this.step)
    if (this.step === 3) {
      this.step = 0;
      this.router.navigate(['/artifact/list/initial/created']);
    } else if (this.step == 4) {
      this.step = 0;
      this.router.navigate(['/artifact/list/initial/draft']);
    }
  }

  stringToDate(_date: string, _format: string, _delimiter: string): Date {
    const formatLowerCase = _format.toLowerCase();
    const formatItems = formatLowerCase.split(_delimiter);
    const dateItems = _date.split(_delimiter);
    const monthIndex = formatItems.indexOf('mm');
    const dayIndex = formatItems.indexOf('dd');
    const yearIndex = +formatItems.indexOf('yyyy');
    let month = +dateItems[monthIndex];
    month -= 1;
    const formatedDate = new Date(+dateItems[yearIndex] - 543, month, +dateItems[dayIndex]);
    return formatedDate;
  }
}
