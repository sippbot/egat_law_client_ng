import { LicenseType } from '../../common/models/license-type.model';
import { Department } from '../../common/models/department.model';
import { Store } from '@ngrx/store';
import { ArtifactNode } from '../../common/models/artifact-node.model';
import { Component, OnInit, Input, SimpleChanges, OnChanges } from '@angular/core';
import * as reducer from '../../common/reducers/reducer';
import * as artifact from '../../common/actions/artifact.action';
import * as department from '../../common/actions/departments.action';
import * as licensetype from '../../common/actions/license-type.action';
import * as alert from '../../common/actions/alert.action';
import { Alert } from '../../common/models/alert.model';
declare var $: any;
declare var jQuery: any;

@Component({
  selector: 'app-artifact-node',
  templateUrl: './node.component.html'
})
export class NodeComponent implements OnInit, OnChanges {
  @Input() groupId: number;
  @Input() artifactId: number;
  @Input() artifactNode: ArtifactNode;
  // @Input() artifactNodeId: number;

  private _groupId: number;

  selectedArtifactNode: ArtifactNode;

  departments: Array<Department>;
  licensetypes: Array<LicenseType>;

  _selectedDepartment: Set<number> = new Set<number>();
  _selectedDepartment2: Array<any> = new Array();
  _selectedDepartmentArray: number[] = [];

  _selectedLicenseType: Set<number> = new Set<number>();
  _selectedLicenseType2: Array<any> = new Array();
  _selectedLicenseTypeArray: number[] = [];

  public options: Object = {
    charCounterCount: true,
    height: 200,
    toolbarButtons: ['bold', 'italic', 'underline', 'paragraphFormat', 'insertTable', 'alert'],
    toolbarButtonsXS: ['bold', 'italic', 'underline', 'paragraphFormat', 'insertTable', 'alert'],
    toolbarButtonsSM: ['bold', 'italic', 'underline', 'paragraphFormat', 'insertTable', 'alert'],
    toolbarButtonsMD: ['bold', 'italic', 'underline', 'paragraphFormat', 'insertTable', 'alert'],
    tableEditButtons: ['tableRows', 'tableColumns', 'tableCells', 'tableCellVerticalAlign', 'tableRemove'],
  };

  constructor(private _store: Store<reducer.State>) {
    this._store.let(reducer.getDepartments).subscribe(
      department => this.departments = department
    );

    this._store.let(reducer.getLicenseTypes).subscribe(
      licensetype => this.licensetypes = licensetype
    );
  }

  ngOnInit() {
    this.selectedArtifactNode = new ArtifactNode();
    // this.artifactNode.followUpPeriod = 6;
    // this.artifactNode.isAllDepartment = false;
    this._store.dispatch(new department.LoadActiveDepartmentsAction({}));
    this._store.dispatch(new licensetype.LoadLicenseTypeAction({}));

    $(function () {
      $('.lisence').hide();
      $('.lisence-checkbox').change(function () {
        if (this.checked) {
          $('.lisence').slideToggle();
        } else {
          $('.lisence').slideToggle();
        }
      });
    });

    if (this.artifactNode !== undefined && this.artifactNode.isEditMode === true) {
      this.editArtifactNode();
    }
  }

  get currentArtifactNode() { return JSON.stringify(this.selectedArtifactNode); }

  editArtifactNode() {
    $('#modal-addnew').modal('show');
    this.selectedArtifactNode = this.artifactNode;
    if (this.selectedArtifactNode.id > 0) {
      for (let i = 0; i < this.selectedArtifactNode.departments.length; i++) {
        this._selectedDepartment2.push({
          id: this.selectedArtifactNode.departments[i].id,
          name: this.selectedArtifactNode.departments[i].aliasName
        });
        this._selectedDepartment.add(this.selectedArtifactNode.departments[i].id);
      }

      for (let i = 0; i < this.selectedArtifactNode.requiredLicenses.length; i++) {
        this._selectedLicenseType2.push({
          id: this.selectedArtifactNode.requiredLicenses[i].id,
          name: this.selectedArtifactNode.requiredLicenses[i].name
        });
        this._selectedLicenseType.add(this.selectedArtifactNode.requiredLicenses[i].id);
      }

    } else {
      this.selectedArtifactNode.content = '';
      this.selectedArtifactNode.followUpPeriodMonth = 6;

      // an.departments = Object.assign({}, deps);
      this._selectedDepartment = new Set<number>();
      this._selectedDepartment2 = new Array();
      this._selectedDepartmentArray = [];
      if (this.selectedArtifactNode.isRequiredLicense) {
        // Do stuff
        $('.lisence').slideToggle();
        this.selectedArtifactNode.isRequiredLicense = false;
      }
    }
  }

  newArtifactNode(groupId: number) {
    $('#modal-addnew').modal('show');
    this.selectedArtifactNode = new ArtifactNode();
    this._groupId = groupId;
    if (this.selectedArtifactNode.id > 0) {
      for (let i = 0; i < this.selectedArtifactNode.departments.length; i++) {
        this._selectedDepartment2.push({
          id: this.selectedArtifactNode.departments[i].id,
          name: this.selectedArtifactNode.departments[i].aliasName
        });
        this._selectedDepartment.add(this.selectedArtifactNode.departments[i].id);
      }

      for (let i = 0; i < this.selectedArtifactNode.requiredLicenses.length; i++) {
        this._selectedLicenseType2.push({
          id: this.selectedArtifactNode.requiredLicenses[i].id,
          name: this.selectedArtifactNode.requiredLicenses[i].name
        });
        this._selectedLicenseType.add(this.selectedArtifactNode.requiredLicenses[i].id);
      }

    } else {
      this.selectedArtifactNode.content = '';
      this.selectedArtifactNode.followUpPeriodMonth = 6;

      // an.departments = Object.assign({}, deps);
      this._selectedDepartment = new Set<number>();
      this._selectedDepartment2 = new Array();
      this._selectedDepartmentArray = [];
      if (this.selectedArtifactNode.isRequiredLicense) {
        // Do stuff
        $('.lisence').slideToggle();
        this.selectedArtifactNode.isRequiredLicense = false;
      }
    }
  }

  submitArtifactNode() {
    // let groupId: number;
    // const groupId = this.groupId;
    // let artifactNode: ArtifactNode;
    this.selectedArtifactNode.artifactId = this.artifactId;
    this.selectedArtifactNode.groupId = this._groupId;

    // this.selectedArtifactNode = Object.assign({}, this.artifactNode);
    this._selectedDepartmentArray = Array.from(this._selectedDepartment);
    this.selectedArtifactNode.departments = [];
    if (this.selectedArtifactNode.content === undefined) {
      this._store.dispatch(new alert.ShowAlertAction(
        new Alert('ไม่มีรายละเอียดข้อปฏิบัติ โปรดระบุรายละเอียดข้อปฏิบัติ', 'กรุณาตรวจสอบข้อมูล!', 'error')
      ));
      return;
    } else if (this._selectedDepartmentArray.length === 0) {
      this._store.dispatch(new alert.ShowAlertAction(
        new Alert('ไม่มีหน่วยงานใดถูกเลือก  โปรดระบุหน่วยงานที่เกี่ยวข้อง', 'กรุณาตรวจสอบข้อมูล!', 'error')
      ));
      return;
    } else {

      for (const did of this._selectedDepartment2) {
        const dep = new Department();
        dep.id = did.id;
        dep.aliasName = did.name;
        this.selectedArtifactNode.departments.push(dep);
      }

      this._selectedLicenseTypeArray = Array.from(this._selectedLicenseType);
      this.selectedArtifactNode.requiredLicenses = [];
      for (const did of this._selectedLicenseType2) {
        const dep = new LicenseType();
        dep.id = did.id;
        dep.name = did.name;
        this.selectedArtifactNode.requiredLicenses.push(dep);
      }

      // Send Request

      if (this.selectedArtifactNode.id > 0) {
        // Update request
        this._store.dispatch(new artifact.UpdateArtifactNodeAction(this.selectedArtifactNode));
      } else {
        // Create request
        this._store.dispatch(new artifact.CreateArtifactNodeAction(this.selectedArtifactNode));
      }

      // an.departments = Object.assign({}, deps);
      this._selectedLicenseType = new Set<number>();
      this._selectedLicenseType2 = new Array();
      this._selectedLicenseTypeArray = [];

      // TODO send create req to backend here
      // this.artifact.nodeGroups[groupId].nodes.push(an);
      this.selectedArtifactNode.isEditMode = false;
      $('#modal-addnew').modal('hide');
    }
  }


  addDepartment() {
    // add node to the list
    const depIt = jQuery('#law-review-division').val();
    const name = jQuery('#law-review-division option:selected').text();
    // If all department WS will verify this flag and departmentId should be 0
    if (depIt === '0') {
      this.selectedArtifactNode.isAllDepartment = true;
      this._selectedDepartment.forEach(departmentId => this.removeDepartment(departmentId));
      this._selectedDepartment2.push({ id: depIt, name: name });
      this._selectedDepartment.add(depIt);
    }

    if (!this._selectedDepartment.has(depIt) && !this.selectedArtifactNode.isAllDepartment) {
      this._selectedDepartment2.push({ id: depIt, name: name });
      this._selectedDepartment.add(depIt);
    }
  }
  removeDepartment(depIt) {
    if (depIt === '0') {
      this.selectedArtifactNode.isAllDepartment = false;
    }

    // remove node from the list
    if (this._selectedDepartment.has(depIt)) {
      this._selectedDepartment2 = this._selectedDepartment2.filter(obj => obj.id !== depIt);
      this._selectedDepartment.delete(depIt);
    }
  }

  addLicensetype() {
    // add node to the list
    const licenseId = jQuery('#select-license-type').val();
    const name = jQuery('#select-license-type option:selected').text();

    if (!this._selectedLicenseType.has(licenseId)) {
      this._selectedLicenseType2.push({ id: licenseId, name: name });
      this._selectedLicenseType.add(licenseId);
    }
  }

  removeLicensetype(licenseID: number) {
    // remove node from the list
    if (this._selectedLicenseType.has(licenseID)) {
      this._selectedLicenseType2 = this._selectedLicenseType2.filter(obj => obj.id !== licenseID);
      this._selectedLicenseType.delete(licenseID);
    }
  }

  refreshLicense() {

  }

  ngOnChanges(changes: SimpleChanges) {
    // only run when property "data" changed
    if (changes['artifactNode']) {
      if (this.artifactNode !== undefined && this.artifactNode.isEditMode === true) {
        this.editArtifactNode();
      }
    }
  }

}
