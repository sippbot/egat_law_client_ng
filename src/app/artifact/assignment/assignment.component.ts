import { ACL } from '../../common/util/acl.util';
import { Store } from '@ngrx/store';
import { Component, OnInit, Input } from '@angular/core';
import { Artifact } from '../../common/models/artifact.model';
import * as artifact from '../../common/actions/artifact.action';
import * as reducer from '../../common/reducers/reducer';
import { ArtifactEffects } from '../../common/effects/artifact.effect';
import { Router, ActivatedRoute } from '@angular/router';
declare var $: any;

@Component({
  selector: 'app-artifact-assignment',
  templateUrl: './assignment.component.html'
})
export class ArtifactAssignmentComponent implements OnInit{
  @Input() artifactId: number;
  acl:ACL;
  artifact: Artifact;
  type:string;
  constructor(
    private _route: ActivatedRoute,
    private router: Router,
    private artifactEffects: ArtifactEffects,
    private _store: Store<reducer.State>) {
    this.artifact = new Artifact();
    this.acl = new ACL();
    this._route.params.subscribe(params => {
      this.type = params['type'];
    });
    this._store.let(reducer.getSelectedArtifacts).subscribe(
      artifact => {
        if (artifact != null) {
          this.artifact = artifact;
        }
      });
    
    this.artifactEffects.deleteArtifact$.filter(
        action => action.type === artifact.ActionTypes.DELETE_ARTIFACT_SUCCESS)
        .subscribe(() => this.router.navigate(['/artifact/list/followup/approved']));
    
  }
  isReadOnly(role_list: Array<string>): boolean {
    let result = false;
    let thisobj = this;
    $.each(role_list,function(i,role){ 
      result = result||(thisobj.acl.isReadOnly(role)===false)
    });
    return !result;
  }
  ngOnInit() {
    // this._store.dispatch(new artifact.SelectOrLoadArtifactAction({artifactId:this.artifactId,isActive:true}));
  }
  onCancelAll(artifactId){
    this._store.dispatch(new artifact.DeleteArtifactAction(artifactId));
  }
  onCancelNodeGroup(nodeGroupId){
    this._store.dispatch(new artifact.DeleteArtifactNodeGroupAction(nodeGroupId));
    this.artifact.nodeGroups=this.artifact.nodeGroups.filter(g => g.id !== nodeGroupId);
  }
  onCancelNode(nodeGroupId,nodeId){
    this._store.dispatch(new artifact.DeleteArtifactNodeAction(nodeId));
    const _group = this.artifact.nodeGroups.find(g => g.id === nodeGroupId);
    const _node = _group.nodes.filter(g => g.id !== nodeId);
    let tempartifactNodeGroups = this.artifact.nodeGroups
    tempartifactNodeGroups.forEach(function(NodeGroups){
      if(NodeGroups.id==nodeGroupId){
        NodeGroups.nodes = _node
      }
    });
    this.artifact.nodeGroups = tempartifactNodeGroups
  }
/*
  ngOnChanges(changes: SimpleChanges) {
    // only run when property "data" changed
    if (changes['artifactId']) {
      this._store.dispatch(new artifact.SelectOrLoadArtifactAction({artifactId:this.artifactId,isActive:true}));
    }
  }
  */
}
