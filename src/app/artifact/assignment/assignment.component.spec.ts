import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArtifactAssignmentComponent } from './assignment.component';

describe('ArtifactAssignmentComponent', () => {
  let component: ArtifactAssignmentComponent;
  let fixture: ComponentFixture<ArtifactAssignmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArtifactAssignmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtifactAssignmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
