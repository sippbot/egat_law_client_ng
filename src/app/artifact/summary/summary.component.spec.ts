import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArtifactSummaryComponent } from './summary.component';

describe('SummaryComponent', () => {
  let component: ArtifactSummaryComponent;
  let fixture: ComponentFixture<ArtifactSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArtifactSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtifactSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
