import { ACL } from '../../common/util/acl.util';
import { ArtifactEffects } from '../../common/effects/artifact.effect';
import { Actions } from '@ngrx/effects';
import { OnInit, Component, ViewChild } from '@angular/core';
import { ArtifactSummary } from '../../common/models/artifact-summary.model';
import { ApplicationConstant } from '../../common/application-constant';
import { Store } from '@ngrx/store';
import { Router, ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import * as reducer from '../../common/reducers/reducer';
import * as artifact from '../../common/actions/artifact.action';
declare var $: any;

@Component({
  selector: 'app-artifact-summary',
  templateUrl: './summary.component.html',
})

export class ArtifactSummaryComponent implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  ArtifactSummaryEntities: Array<ArtifactSummary>;
  status: string;
  departmentId: number;
  taskAssignmentType: string; // Initial  or FollowUp
  userGroup: string;
  statusText: string;
  acl: ACL;
  dtTrigger: Subject<any> = new Subject();
  dtOptions: DataTables.Settings;
  enableFilter = false;

  constructor(private _route: ActivatedRoute,
    private _store: Store<reducer.State>,
    private artifactEffects: ArtifactEffects) {
    this.acl = new ACL();
    this.departmentId = +localStorage.getItem('departmentId');
    this.userGroup = localStorage.getItem('userGroup');
    ApplicationConstant.print_flag = true;
    this.ArtifactSummaryEntities = new Array<ArtifactSummary>();
    this._route.params.subscribe(params => {
      if (params != null) {
        console.log(params);
        this.ArtifactSummaryEntities = [];
        this.status = params['status'];
        this.taskAssignmentType = params['type'];
        this.dtOptions = {
          'autoWidth': true,
          'pageLength': 30,
          'language': ApplicationConstant.DATATABLE_GLOBAL_LANGUAGE_CONFIG,
          'dom': '<\'row\'<\'col-sm-12\'B>>' +
            '<\'row\'<\'col-sm-6\'l><\'col-sm-6\'f>>' +
            '<\'row\'<\'col-sm-12\'tr>>' +
            '<\'row\'<\'col-sm-5\'i><\'col-sm-7\'p>>',
          'buttons': [
            {
              extend: 'excelHtml5',
              text: '<i class="fa fa-file-excel-o fa-x5"></i> ดาวน์โหลด Excel',
              className: 'btn btn-primary btn-sm glyphicon glyphicon-list-alt pull-right',
              available: function (dt, config) {
                // console.log('ดาวน์โหลด Excel',dt,config,ApplicationConstant.print_flag,this.taskAssignmentType,((this.taskAssignmentType ==='followup') || (this.taskAssignmentType ==='all'))?[0,1,2,3,4,5,6,7]:[0,1,2,3,4,5,6])
                return ApplicationConstant.print_flag;
              },
              exportOptions: {
                columns: ((this.taskAssignmentType === 'followup') || (this.taskAssignmentType === 'all')) ? [0, 1, 2, 3, 4, 5, 6, 7] : [0, 1, 2, 3, 4, 5, 6]
              }
            },
            {
              extend: 'print',
              text: '<i class="fa fa-print fa-x5"></i> พิมพ์รายงาน',
              className: 'btn btn-primary btn-sm btn-print glyphicon glyphicon-print pull-right',
              pageSize: 'A4',
              title: 'รายการเอกสาร',
              available: function (dt, config) {
                // console.log('พิมพ์รายงาน',dt,config,ApplicationConstant.print_flag)
                return ApplicationConstant.print_flag;
              },
              exportOptions: {
                columns: ((this.taskAssignmentType === 'followup') || (this.taskAssignmentType === 'all')) ? [0, 1, 2, 3, 4, 5, 6, 7] : [0, 1, 2, 3, 4, 5, 6]
              }
            },
            {
              text: 'ตัวกรองขั้นสูง',
              className: 'btn btn-primary btn-sm btn-print glyphicon glyphicon-print pull-right',
              action: () => {
                // this.enableFilter = !this.enableFilter;
                $('.form-control-filter').toggle();
                this.enableSearchFilter();
              }
            }
          ]
        };
        this.resolveStatusText();
        if ((this.taskAssignmentType === "initial") && (this.status === "inprogress")) {
          console.log("initial", this.role('ATFMNG05'))
          if (this.role('ATFMNG05')) {
            ApplicationConstant.print_flag = true;
          } else {
            ApplicationConstant.print_flag = false;
          }
        } else if ((this.taskAssignmentType == "followup") && (this.status == "inprogress")) {
          console.log("followup", this.role('ATFVIW'))
          if (this.role('ATFVIW')) {
            ApplicationConstant.print_flag = true;
          } else {
            ApplicationConstant.print_flag = false;
          }
        } else {
          ApplicationConstant.print_flag = true;
        }
        let payload;
        if (this.role('ATFMNG') || this.role('TSKAPP')) {
          payload = {
            status: this.status,
            departmentId: 0,
            taskAssignmentType: this.taskAssignmentType
          };
        } else {
          payload = {
            status: this.status,
            departmentId: this.departmentId,
            taskAssignmentType: this.taskAssignmentType
          };

        }
        this._store.dispatch(new artifact.ClearArtifactSummaryAction({}));
        if (this.status === "Canceled") {
          this._store.dispatch(new artifact.SelectOrLoadArtifactSummaryAction(payload));
        } else {
          this._store.dispatch(new artifact.SelectOrLoadArtifactSummaryAction(payload));
        }

        /*
        const payload = {
          status: this.status,
          departmentId: this.departmentId,
          taskAssignmentType: this.taskAssignmentType
        };
        this._store.dispatch(new artifact.SelectOrLoadArtifactSummaryAction(payload));
        */
      }
    });
    this.artifactEffects.loadArtifactSummary$.filter(
      action => action.type === artifact.ActionTypes.LOAD_ARTIFACTS_SUMMARY_SUCCESS
    ).subscribe(
      () => {
        this._store.dispatch(new artifact.SelectArtifactSummaryAction({
          status: this.status,
          taskAssignmentType: this.taskAssignmentType
        }));
        // this.rerender();
        $('#table-filter-row').toggle();
      }
    );

    this.artifactEffects.deleteArtifact$.filter(
      action => action.type === artifact.ActionTypes.DELETE_ARTIFACT_SUCCESS
    ).subscribe(
      () => { this._store.dispatch(new artifact.LoadArtifactSummaryAction(0)); }
    );

    this.artifactEffects.loadCanceledArtifactSummary$.filter(
      action => action.type === artifact.ActionTypes.LOAD_CANCELED_ARTIFACTS_SUMMARY_SUCCESS
    ).subscribe(
      () => {
        this._store.dispatch(new artifact.SelectCanceledArtifactSummaryAction({
          status: this.status,
          taskAssignmentType: this.taskAssignmentType
        }));
        // this.rerender();
        $('#table-filter-row').toggle();
      }
    );

    this._store.let(reducer.getSelectedArtifactSummary).subscribe(
      artifactSummaryList => {
        // this.ArtifactSummaryEntities = artifactSummaryList;  
        // this.rerender();
        // if(this.status=="inprogress"){
        //   this.ArtifactSummaryEntities=artifactSummaryList.filter(g => (g.taskAssignmentStatus != 'Initial')&&(g.taskAssignmentStatus != 'Created'));
        // }else{
        this.ArtifactSummaryEntities = artifactSummaryList;
        // }
        // return this.ArtifactSummaryEntities;
        $('#table-filter-row').toggle();
      }
    );
  }
  rerender(): void {
    if (this.dtElement) {
      if (this.dtElement.dtInstance !== undefined) {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          // Destroy the table first
          dtInstance.destroy();
          // Call the dtTrigger to rerender again
          this.dtTrigger.next();
        });
      } else {
        this.dtTrigger.next();
      }
    }
  }
  ngOnInit() {

  }

  enableSearchFilter(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.columns().every(function () {
        const that = this;
        $('input', this.header()).on('keyup change', function () {
          if (that.search() !== this['value']) {
            that
              .search(this['value'], true, true)
              .draw();
          }
        });
      });
    });
  }

  role(role: string): boolean {
    return this.acl.hasRole(role);
  }

  isReadOnly(role_list: Array<string>): boolean {
    let result = false;
    const thisobj = this;
    $.each(role_list, function (i, role) {
      result = result || (thisobj.acl.isReadOnly(role) === false)
    });
    return !result;
  }

  refresh() {
    if (this.role('ATFMNG') || this.role('TSKAPP')) {
      this._store.dispatch(new artifact.LoadArtifactSummaryAction(0));
    } else {
      this._store.dispatch(new artifact.LoadArtifactSummaryAction(this.departmentId));
    }
  }
  removeArtifact(artifactId) {
    this._store.dispatch(new artifact.DeleteArtifactAction(artifactId));
    // this.ArtifactSummaryEntities=this.ArtifactSummaryEntities.filter(g => g.id !== artifactId);
  }
  resolveStatusText() {
    console.log(this.status.toLowerCase(), this.taskAssignmentType.toLowerCase())
    if (this.taskAssignmentType.toLowerCase() === 'initial') {
      switch (this.status.toLowerCase()) {
        case 'created':
          this.statusText = 'กฎหมายรอทวนสอบ';
          break;
        case 'draft':
          this.statusText = 'กฎหมายฉบับร่าง';
          break;
        case 'inprogress':
          this.statusText = 'กฎหมายที่อยู่ในระหว่างดำเนินการ';
          break;
        case 'canceled':
          this.statusText = 'กฎหมายที่ยกเลิก';
          break;
        default:
          this.statusText = 'ทะเบียนกฎหมายที่ใช้ในโรงไฟฟ้า';
      }
    } else if (this.taskAssignmentType.toLowerCase() === 'all') {
      switch (this.status.toLowerCase()) {
        case 'approved':
          this.statusText = 'ผลการติดตาม';
          break;
      }
    } else {
      switch (this.status.toLowerCase()) {
        case 'initial':
          this.statusText = 'การติดตามรอบต่อไป';
          break;
        case 'inprogress':
          this.statusText = 'การติดตามที่อยู่ในระหว่างดำเนินการ';
          break;
        case 'overdue':
          this.statusText = 'การติดตามเกินกำหนด';
          break;
        case 'approved':
          this.statusText = 'ทะเบียนกฎหมายที่ใช้ในโรงไฟฟ้า';
          break;
        default:
          this.statusText = 'การติดตาม';
      }
    }

  }
}
