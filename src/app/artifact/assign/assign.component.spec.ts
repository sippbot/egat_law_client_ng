import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArtifactAssignComponent } from './assign.component';

describe('AssignComponent', () => {
  let component: ArtifactAssignComponent;
  let fixture: ComponentFixture<ArtifactAssignComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArtifactAssignComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtifactAssignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
