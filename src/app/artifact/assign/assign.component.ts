import { ShowAlertAction } from '../../common/actions/alert.action';
import { ArtifactEffects } from '../../common/effects/artifact.effect';
import { Artifact } from '../../common/models/artifact.model';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Http } from '@angular/http';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { ActivatedRoute } from '@angular/router';
import * as artifact from '../../common/actions/artifact.action';
import * as reducer from '../../common/reducers/reducer';
import { IMyOptions } from 'mydatepicker-th';
import { Alert } from '../../common/models/alert.model';
import { SwalComponent } from '@toverux/ngsweetalert2';

@Component({
  selector: 'app-artifact-assign',
  templateUrl: './assign.component.html'
})
export class ArtifactAssignComponent implements OnInit {
  artifactId: number;
  selectedArtifact: Artifact;
  date: any;
  duedate: any;//IMyDateModel;
  @ViewChild('validation_dialog') private validationDialog: SwalComponent;
  _duedate: string;
  _nowdate: Date;
  placeholder: string ="dd/mm/yyyy";
  myDatePickerOptions: IMyOptions = {
    dateFormat: 'dd/mm/yyyy',
    editableDateField: false,
    openSelectorOnInputClick: true
  };

  constructor(private _route: ActivatedRoute,
    private _store: Store<reducer.State>,
    private fb: FormBuilder,
    private router: Router,
    private http: Http,
    private artifactEffects: ArtifactEffects) {
    this.selectedArtifact = new Artifact();
    this._route.params.subscribe(params => {
      this.artifactId = +params['id'];
    });
    this._store.let(reducer.getSelectedArtifacts).subscribe(
      artifact => this.selectedArtifact = artifact
    );
    this._nowdate = new Date();
    this._nowdate.setDate(this._nowdate.getDate() + 7);
    this._duedate = this.pad(this._nowdate.getDate())+"/"+this.pad(this._nowdate.getMonth()+1)+"/"+(this._nowdate.getFullYear()+543)
    this.setDate();
    this.duedate.formatted=this.pad(this._nowdate.getDate())+"/"+this.pad(this._nowdate.getMonth()+1)+"/"+(this._nowdate.getFullYear()+543)
    this.artifactEffects.craeteArtifactAssignment$.filter(
      action => action.type === artifact.ActionTypes.CREATE_ARTIFACT_ASSIGNMENT_SUCCESS
    ).subscribe(() => this.redirect());

    this.artifactEffects.craeteArtifactAssignment$.filter(
      action => action.type === artifact.ActionTypes.CREATE_ARTIFACT_ASSIGNMENT_FAIL
    ).subscribe(() => this._store.dispatch(new ShowAlertAction(
      new Alert('ไม่สามารถส่งทวนสอบได้ โปรดลองอีกครั้ง', 'ล้มเหลว!', 'fail'))));
  }
  pad(n):string{
    return (n<10)?("0"+n):n
  }
  stringToDate(_date: string, _format: string, _delimiter: string): Date {
    const formatLowerCase = _format.toLowerCase();
    const formatItems = formatLowerCase.split(_delimiter);
    const dateItems = _date.split(_delimiter);
    const monthIndex = formatItems.indexOf('mm');
    const dayIndex = formatItems.indexOf('dd');
    const yearIndex = +formatItems.indexOf('yyyy');
    let month = +dateItems[monthIndex];
    month -= 1;
    const formatedDate = new Date(+dateItems[yearIndex] - 543, month, +dateItems[dayIndex]);
    return formatedDate;
  }
  hello($event){
    console.log($event)
  }
  setDate(): void {
    // Set today date using the setValue function
    console.log(this._duedate)
    const _duedate = this.stringToDate(this._duedate, 'dd/MM/yyyy', '/');
    console.log(this._duedate)
    this.duedate = {
      date: {
        year: _duedate.getFullYear(),
        month: _duedate.getMonth()+1,
        day: _duedate.getDate()
      },
      formatted:this.pad(_duedate.getDate())+"/"+this.pad(_duedate.getMonth())+"/"+(_duedate.getFullYear())
    };
    // this.duedate = Object.assign({}, this.duedate, {
    //   date: {
    //     year: _duedate.getFullYear(),
    //     month: _duedate.getMonth(),
    //     day: _duedate.getDate()
    //   },
    //   // formatted:this.pad(_duedate.getDate())+"/"+this.pad(_duedate.getMonth())+"/"+(_duedate.getFullYear())
    // });
 }

  ngOnInit() {
    this._store.dispatch(new artifact.SelectOrLoadArtifactAction({artifactId:this.artifactId,isActive:true}));
    
    // this._store.dispatch(new LoadArtifactAction(this.artifactId));
  }
  onSubmit() {
    // Add Sweet alert here to confirm
    const userId = localStorage.getItem('userId');
    console.log(this.duedate)
    if(this.duedate!=null){
      const req = {
        artifactId: this.selectedArtifact.id,
        duedate: this.duedate.formatted,
        assignerId: userId
      };
      console.log(req)
      

      this._store.dispatch(new artifact.CreateArtifactAssignmentAction(req));
    }else{
      this.validationDialog.show();
    }
  }
  redirect() {
    this.router.navigate(['/artifact/list/initial/inprogress']);
  }
}

