import { SafeHtmlPipeModule } from '../safe-html.module';
import { SweetAlert2Module } from '@toverux/ngsweetalert2';
import { DataTablesModule } from 'angular-datatables';
import { MyDatePickerTHModule } from 'mydatepicker-th';
import { FroalaViewModule, FroalaEditorModule } from 'angular-froala-wysiwyg';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../guard/auth.guard';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArtifactComponent } from './artifact/artifact.component';
import { GroupComponent } from './group/group.component';
import { NodeComponent } from './node/node.component';
import { ArtifactDetailComponent } from './detail/detail.component';
import { ArtifactAssignmentComponent } from './assignment/assignment.component';
import { ArtifactAssignComponent } from './assign/assign.component';
import { ArtifactTaskComponent } from './task/task.component';
import { ArtifactSummaryComponent } from './summary/summary.component';
import { ArtifactViewComponent } from './view/view.component';

export const routes: Routes = [
  { path: 'artifact/new', component: ArtifactComponent, canActivate: [AuthGuard] },
  { path: 'artifact/edit/:id', component: ArtifactComponent, canActivate: [AuthGuard] },
  { path: 'artifact/assign/:id', component: ArtifactAssignComponent, canActivate: [AuthGuard] },
  { path: 'artifact/task/:type/:id', component: ArtifactTaskComponent, canActivate: [AuthGuard] },
  { path: 'artifact/task/:type/:id/:departmentId', component: ArtifactTaskComponent, canActivate: [AuthGuard] },
  // { path: 'artifact/list/:status', component: ArtifactSummaryComponent, canActivate: [AuthGuard] },
  { path: 'artifact/list/:type/:status', component: ArtifactSummaryComponent, canActivate: [AuthGuard] },
  { path: 'artifact/view/:type/:id', component: ArtifactViewComponent, canActivate: [AuthGuard] },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    DataTablesModule,
    FroalaEditorModule,
    FroalaViewModule,
    MyDatePickerTHModule,
    SweetAlert2Module,
    SafeHtmlPipeModule
  ],
  declarations: [
    ArtifactComponent,
    GroupComponent,
    NodeComponent,
    ArtifactDetailComponent,
    ArtifactAssignmentComponent,
    ArtifactAssignComponent,
    ArtifactTaskComponent,
    ArtifactSummaryComponent,
    ArtifactViewComponent
  ],
  exports: [ArtifactDetailComponent, ArtifactAssignmentComponent]
})
export class ArtifactModule { }
