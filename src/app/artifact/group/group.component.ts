import { LicenseType } from '../../common/models/license-type.model';
import { Department } from '../../common/models/department.model';
import { Store } from '@ngrx/store';
import { ArtifactNode } from '../../common/models/artifact-node.model';
import { ArtifactNodeGroup } from '../../common/models/artifact-node-group.model';
import { Component, OnInit, Input } from '@angular/core';
import * as department from '../../common/actions/departments.action';
import * as licensetype from '../../common/actions/license-type.action';
import * as reducer from '../../common/reducers/reducer';
import * as artifact from '../../common/actions/artifact.action';
import * as alert from '../../common/actions/alert.action';
import { Alert } from '../../common/models/alert.model';
declare var $: any;
declare var jQuery: any;

@Component({
  selector: 'app-artifact-group',
  templateUrl: './group.component.html'
})
export class GroupComponent implements OnInit {
  @Input() artifactId: number;
  @Input() artifactNodeGroups: Array<ArtifactNodeGroup>;

  selectedArtifactNodeGroup: ArtifactNodeGroup;

  // groupId: number;
  editMode: boolean;
  selectedArtifactNode: ArtifactNode;
  selectedArtifactNodeId: number;

  departments: Array<Department>;
  licensetypes: Array<LicenseType>;

  _selectedDepartment: Set<number> = new Set<number>();
  _selectedDepartment2: Array<any> = new Array();
  _selectedDepartmentArray: number[] = [];

  _selectedLicenseType: Set<number> = new Set<number>();
  _selectedLicenseType2: Array<any> = new Array();
  _selectedLicenseTypeArray: number[] = [];

  public options: Object = {
    charCounterCount: true,
    height: 200,
    toolbarButtons: ['bold', 'italic', 'underline', 'paragraphFormat', 'insertTable', 'fontSize', 'color', 'alert'],
    toolbarButtonsXS: ['bold', 'italic', 'underline', 'paragraphFormat', 'insertTable', 'fontSize', 'color', 'alert'],
    toolbarButtonsSM: ['bold', 'italic', 'underline', 'paragraphFormat', 'insertTable', 'fontSize', 'color', 'alert'],
    toolbarButtonsMD: ['bold', 'italic', 'underline', 'paragraphFormat', 'insertTable', 'fontSize', 'color', 'alert'],
    tableEditButtons: ['tableRows', 'tableColumns', 'tableCells', 'tableCellVerticalAlign', 'tableRemove'],
  };

  constructor(private _store: Store<reducer.State>) {
    this.artifactNodeGroups = new Array<ArtifactNodeGroup>();
    this.selectedArtifactNodeGroup = new ArtifactNodeGroup();
    this.selectedArtifactNode = new ArtifactNode();

    this._store.let(reducer.getArtifactNodeGroups).subscribe(
      artifactNodeGroups => this.artifactNodeGroups = artifactNodeGroups
    );

    this._store.let(reducer.getDepartments).subscribe(
      department => this.departments = department
    );

    this._store.let(reducer.getLicenseTypes).subscribe(
      licensetype => this.licensetypes = licensetype
    );
  }

  ngOnInit() {
    this._store.dispatch(new department.LoadActiveDepartmentsAction({}));
    this._store.dispatch(new licensetype.LoadLicenseTypeAction({}));

    $(function () {
      $('.lisence').hide();
      $('.lisence-checkbox').change(function () {
        if (this.checked) {
          $('.lisence').slideDown();
        } else {
          $('.lisence').slideUp();
        }
      });
    });
  }

  saveArtifactNodeGroup() {
    this.onSubmit();
  }

  removeNodeContent(groupId: number, nodeId: number) {
    // Ask for confirmation
    console.log(groupId + ' ' + nodeId);
    this._store.dispatch(new artifact.DeleteArtifactNodeAction(nodeId));
    const _group = this.artifactNodeGroups.find(g => g.id === groupId);
    const _node = _group.nodes.filter(g => g.id !== nodeId);
    let tempartifactNodeGroups = this.artifactNodeGroups
    tempartifactNodeGroups.forEach(function (NodeGroups) {
      if (NodeGroups.id == groupId) {
        NodeGroups.nodes = _node
      }
    });
    this.artifactNodeGroups = tempartifactNodeGroups
  }

  sendDeleteRequest(i: number) {
    // Send request to backend
  }

  editNodeContent(groupId: number, nodeId: number) {
    const _group = this.artifactNodeGroups.find(g => g.id === groupId);
    const _node = _group.nodes.find(n => n.id === nodeId);

    this.selectedArtifactNode = _node;
    this.selectedArtifactNode.isEditMode = true;

    this.editArtifactNode();
  }

  editArtifactNodeGroup(groupId: number) {
    this.selectedArtifactNodeGroup = this.artifactNodeGroups.find(g => g.id === groupId);
    this.selectedArtifactNodeGroup.isEditMode = true;
  }

  removeArtifactNodeGroup(groupId: number) {
    // this.selectedArtifactNodeGroup = this.artifactNodeGroups.find(g => g.id === groupId);
    // console.log(this.selectedArtifactNodeGroup)
    this._store.dispatch(new artifact.DeleteArtifactNodeGroupAction(groupId));
    this.artifactNodeGroups = this.artifactNodeGroups.filter(g => g.id !== groupId);
  }

  addArtifactNodeGroup() {
    const artifactNodeGroup = new ArtifactNodeGroup();
    artifactNodeGroup.nodes = new Array<ArtifactNode>();
    artifactNodeGroup.isEditMode = true;
    artifactNodeGroup.id = 0;
    this.artifactNodeGroups.push(artifactNodeGroup);

    this.selectedArtifactNodeGroup = artifactNodeGroup;
  }

  onSubmit() {
    // this.editMode = false;
    this.selectedArtifactNodeGroup.artifactId = this.artifactId;
    if (this.selectedArtifactNodeGroup.id > 0) {
      // Update request
      this._store.dispatch(new artifact.UpdateArtifactNodeGroupAction(this.selectedArtifactNodeGroup));
    } else {
      // Create request
      this._store.dispatch(new artifact.CreateArtifactNodeGroupAction(this.selectedArtifactNodeGroup));
    }
  }

  get currentArtifactNode() { return JSON.stringify(this.selectedArtifactNode); }

  editArtifactNode() {
    $('#modal-addnew').modal('show');
    jQuery('#law-review-division').val("default").trigger('change');
    jQuery('#select-license-type').val("default").trigger('change');
    // this.selectedArtifactNode = this.artifactNode;
    this._selectedLicenseType2 = new Array();
    this._selectedLicenseType = new Set<number>();
    this._selectedDepartment2 = new Array();
    this._selectedDepartment = new Set<number>();

    if (this.selectedArtifactNode.id > 0) {
      for (let i = 0; i < this.selectedArtifactNode.departments.length; i++) {
        this._selectedDepartment2.push({
          id: this.selectedArtifactNode.departments[i].id,
          name: this.selectedArtifactNode.departments[i].aliasName
        });
        this._selectedDepartment.add(this.selectedArtifactNode.departments[i].id);
      }

      for (let i = 0; i < this.selectedArtifactNode.requiredLicenses.length; i++) {
        this._selectedLicenseType2.push({
          id: this.selectedArtifactNode.requiredLicenses[i].id,
          name: this.selectedArtifactNode.requiredLicenses[i].name
        });
        this._selectedLicenseType.add(this.selectedArtifactNode.requiredLicenses[i].id);
      }
      if (this.selectedArtifactNode.isRequiredLicense) {
        $('.lisence').slideDown();
      } else {
        $('.lisence').slideUp();
      }

    } else {
      this.selectedArtifactNode.content = '';
      this.selectedArtifactNode.followUpPeriodMonth = 6;
      this.selectedArtifactNode.followUpPeriodYear = 0;

      this._selectedDepartment = new Set<number>();
      this._selectedDepartment2 = new Array();
      this._selectedDepartmentArray = [];
      if (this.selectedArtifactNode.isRequiredLicense) {
        // Do stuff
        $('.lisence').slideDown();
      } else {
        $('.lisence').slideUp();
      }
    }
  }

  newArtifactNode(groupId: number) {
    $('#modal-addnew').modal('show');
    jQuery('#law-review-division').val("default").trigger('change');
    jQuery('#select-license-type').val("default").trigger('change');
    this._selectedLicenseType2 = new Array();
    this._selectedLicenseType = new Set<number>();
    this._selectedDepartment2 = new Array();
    this._selectedDepartment = new Set<number>();
    this.selectedArtifactNode = new ArtifactNode();
    this.selectedArtifactNode.groupId = groupId;
    this.selectedArtifactNode.artifactId = this.artifactId;
    if (this.selectedArtifactNode.id > 0) {
      for (let i = 0; i < this.selectedArtifactNode.departments.length; i++) {
        this._selectedDepartment2.push({
          id: this.selectedArtifactNode.departments[i].id,
          name: this.selectedArtifactNode.departments[i].aliasName
        });
        this._selectedDepartment.add(this.selectedArtifactNode.departments[i].id);
      }

      for (let i = 0; i < this.selectedArtifactNode.requiredLicenses.length; i++) {
        this._selectedLicenseType2.push({
          id: this.selectedArtifactNode.requiredLicenses[i].id,
          name: this.selectedArtifactNode.requiredLicenses[i].name
        });
        this._selectedLicenseType.add(this.selectedArtifactNode.requiredLicenses[i].id);
      }
      if (this.selectedArtifactNode.isRequiredLicense) {
        $('.lisence').slideDown();
      } else {
        $('.lisence').slideUp();
      }

    } else {
      this.selectedArtifactNode.content = '';
      this.selectedArtifactNode.followUpPeriodMonth = 6;
      this.selectedArtifactNode.followUpPeriodYear = 0;

      // an.departments = Object.assign({}, deps);
      this._selectedDepartment = new Set<number>();
      this._selectedDepartment2 = new Array();
      this._selectedDepartmentArray = [];
      if (this.selectedArtifactNode.isRequiredLicense) {
        // Do stuff
        $('.lisence').slideDown();
      } else {
        $('.lisence').slideUp();
      }
    }
  }

  submitArtifactNode() {
    this._selectedDepartmentArray = Array.from(this._selectedDepartment);
    this.selectedArtifactNode.departments = [];
    if (this.selectedArtifactNode.content === undefined) {
      this._store.dispatch(new alert.ShowAlertAction(
        new Alert('ไม่มีรายละเอียดข้อปฏิบัติ โปรดระบุรายละเอียดข้อปฏิบัติ', 'กรุณาตรวจสอบข้อมูล!', 'error')
      ));
      return;
    } else if (this._selectedDepartmentArray.length === 0) {
      this._store.dispatch(new alert.ShowAlertAction(
        new Alert('ไม่มีหน่วยงานใดถูกเลือก  โปรดระบุหน่วยงานที่เกี่ยวข้อง', 'กรุณาตรวจสอบข้อมูล!', 'error')
      ));
      return;
    } else {

      for (const did of this._selectedDepartment2) {
        const dep = new Department();
        dep.id = did.id;
        dep.aliasName = did.name;
        this.selectedArtifactNode.departments.push(dep);
      }
      console.log("this.selectedArtifactNode.departments", this.selectedArtifactNode.departments)

      this._selectedLicenseTypeArray = Array.from(this._selectedLicenseType);
      this.selectedArtifactNode.requiredLicenses = [];
      for (const did of this._selectedLicenseType2) {
        // if(this._selectedLicenseTypeArray.indexOf(did.id)==-1){
        const dep = new LicenseType();
        dep.id = parseInt(did.id);
        dep.name = did.name;
        this.selectedArtifactNode.requiredLicenses.push(dep);
        // }
      }
      console.log("this._selectedLicenseTypeArray", this._selectedLicenseTypeArray)
      console.log("this._selectedLicenseType", this._selectedLicenseType)
      console.log("this._selectedLicenseType2", this._selectedLicenseType2)
      console.log("this.selectedArtifactNode.requiredLicenses", this.selectedArtifactNode.requiredLicenses)

      // Send Request

      if (this.selectedArtifactNode.id > 0) {
        // Update request
        this._store.dispatch(new artifact.UpdateArtifactNodeAction(this.selectedArtifactNode));
      } else {
        // Create request
        this._store.dispatch(new artifact.CreateArtifactNodeAction(this.selectedArtifactNode));
      }

      // an.departments = Object.assign({}, deps);
      this._selectedLicenseType = new Set<number>();
      this._selectedLicenseType2 = new Array();
      this._selectedLicenseTypeArray = [];

      // TODO send create req to backend here
      // this.artifact.nodeGroups[groupId].nodes.push(an);
      this.selectedArtifactNode.isEditMode = false;
      $('#modal-addnew').modal('hide');
    }
  }

  addDepartment() {
    // add node to the list
    const depIt = jQuery('#law-review-division').val();
    const name = jQuery('#law-review-division option:selected').text();
    // If all department WS will verify this flag and departmentId should be 0
    console.log(depIt, name, this._selectedDepartment, this.selectedArtifactNode.isAllDepartment)
    if (depIt === "0") {
      this.selectedArtifactNode.isAllDepartment = true;
      this._selectedDepartment.forEach(departmentId => this.removeDepartment(departmentId));
      this._selectedDepartment2.push({ id: parseInt(depIt), name: name });
      this._selectedDepartment.add(parseInt(depIt));
    }

    if (!this._selectedDepartment.has(parseInt(depIt)) && (depIt !== "0")) {// && !this.selectedArtifactNode.isAllDepartment) {
      if ((depIt != "default") && (!this._selectedDepartment.has(0))) {
        this._selectedDepartment2.push({ id: parseInt(depIt), name: name });
        this._selectedDepartment.add(parseInt(depIt));
      }
    }
    console.log(depIt === '0', !this._selectedDepartment.has(parseInt(depIt)) && (depIt !== "0"))
  }

  removeDepartment(depIt) {
    if (depIt === '0') {
      this.selectedArtifactNode.isAllDepartment = false;
    }

    // remove node from the list
    if (this._selectedDepartment.has(parseInt(depIt))) {
      this._selectedDepartment2 = this._selectedDepartment2.filter(obj => obj.id !== parseInt(depIt));
      this._selectedDepartment.delete(parseInt(depIt));
    }
  }

  addLicensetype() {
    // add node to the list
    const licenseId = jQuery('#select-license-type').val();
    const name = jQuery('#select-license-type option:selected').text();
    console.log(this._selectedLicenseType, licenseId)
    if (licenseId != "default") {
      if (!this._selectedLicenseType.has(parseInt(licenseId))) {
        this._selectedLicenseType2.push({ id: parseInt(licenseId), name: name });
        // this._selectedLicenseType.add(licenseId);
      }
    }
  }

  removeLicensetype(licenseID: number) {
    // remove node from the list
    console.log(this._selectedLicenseType, licenseID)
    // if (this._selectedLicenseType.has(licenseID)) {
    this._selectedLicenseType2 = this._selectedLicenseType2.filter(obj => obj.id !== licenseID);
    if (this._selectedLicenseType.has(licenseID)) {
      this._selectedLicenseType.delete(licenseID);
    }
  }

  refreshLicense() {
    this._store.dispatch(new licensetype.LoadLicenseTypeAction({}));
  }

}
