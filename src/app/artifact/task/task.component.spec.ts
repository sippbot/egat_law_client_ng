import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArtifactTaskComponent } from './task.component';

describe('ArtifactTaskComponent', () => {
  let component: ArtifactTaskComponent;
  let fixture: ComponentFixture<ArtifactTaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArtifactTaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtifactTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
