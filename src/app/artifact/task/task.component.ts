import { ACL } from '../../common/util/acl.util';
import { TaskAssignment } from '../../common/models/task-assignment.model';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { ActivatedRoute } from '@angular/router';
import { Artifact } from '../../common/models/artifact.model';
import * as reducer from '../../common/reducers/reducer';
import * as taskAssigment from '../../common/actions/task-assignment.action';
declare var $: any;
@Component({
  selector: 'app-artifact-task',
  templateUrl: './task.component.html',
})

export class ArtifactTaskComponent implements OnInit {
  artifact: Artifact;
  departmentId: number;
  userId: number;
  artifactId: number;
  title: string;
  acl: ACL;
  taskAssignments: Array<TaskAssignment>;
  userGroup: string;
  mode: string;
  constructor(private _route: ActivatedRoute,
    private _store: Store<reducer.State>) {
    this.acl = new ACL();
    this.title = '';
    this._route.params.subscribe(params => {
      this.departmentId = +params['departmentId'];
      this.mode = params['type'];
      if (!this.departmentId) {
        this.departmentId = +localStorage.getItem('departmentId');
      }
    });

    this._route.params.subscribe(params => {
      this.artifactId = +params['id'];
    });

    this._store.let(reducer.getTaskAssignments).subscribe(
      taskAssignments => {
        if (taskAssignments.length !== 0) {
          this.taskAssignments = taskAssignments;
          this.title = taskAssignments[0].title;
        }
      });
    this.userGroup = localStorage.getItem('userGroup');
  }

  role(role: string): boolean {
    return this.acl.hasRole(role);
  }

  isReadOnly(role_list: Array<string>): boolean {
    let result = false;
    const thisobj = this;
    $.each(role_list, function (i, role) {
      result = result || (thisobj.acl.isReadOnly(role) === false)
    });
    return !result;
  }

  ngOnInit() {
    if (!this.role('TSKRVW') && !this.role('TSKAPP')) {
      this._store.dispatch(new taskAssigment.LoadTaskAssignmentsAction({ departmentId: this.departmentId, artifactId: this.artifactId }));
    } else {
      this._store.dispatch(new taskAssigment.LoadTaskAssignmentsAction({ departmentId: undefined, artifactId: this.artifactId }));
    }
  }
}
