import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArtifactDetailComponent } from './detail.component';

describe('ArtifactDetailComponent', () => {
  let component: ArtifactDetailComponent;
  let fixture: ComponentFixture<ArtifactDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArtifactDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtifactDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
