import { IMap } from '../../common/interfaces/imap.interface';
import { Document } from '../../common/models/document.model';
import { Component, OnInit, Input } from '@angular/core';
import { Store } from '@ngrx/store';
import { Artifact } from '../../common/models/artifact.model';
import * as artifact from '../../common/actions/artifact.action';
import * as lawlevel from '../../common/actions/lawlevel.action';
import * as reducer from '../../common/reducers/reducer';
import { Lawlevel } from '../../common/models/models';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-artifact-detail',
  templateUrl: './detail.component.html'
})
export class ArtifactDetailComponent implements OnInit {
  @Input() artifactId: number;

  artifact: Artifact;
  levelMap: IMap<Lawlevel>;
  type:string;
  constructor(
    private _route: ActivatedRoute,
    private _store: Store<reducer.State>) {
    this.artifact = new Artifact();
    this.levelMap = {};
    this.artifact.documents = new Array<Document>();
    this._route.params.subscribe(params => {
      this.type = params['type'];
    });
    this._store.let(reducer.getSelectedArtifacts).subscribe(
      artifact => {
        if (artifact != null) {
          this.artifact = artifact;
        }
      });
    this._store.let(reducer.getLawlevels).subscribe(
      levels => {
        levels.forEach(item => {
          this.levelMap[item.id] = item;
        });
      }
    );
  }

  ngOnInit() {
    let isActive = true;
    if(this.type=='canceled'){
      isActive = false
    }
    this._store.dispatch(new artifact.SelectOrLoadArtifactAction({artifactId:this.artifactId,isActive:isActive}));
    this._store.dispatch(new lawlevel.LoadLawlevelAction({}));
  }
  /*
    ngOnChanges(changes: SimpleChanges) {
      // only run when property "data" changed
      if (changes['artifactId']) {
        this._store.dispatch(new artifact.SelectOrLoadArtifactAction({artifactId:this.artifactId,isActive:true}));
      }
    }
    */
}
