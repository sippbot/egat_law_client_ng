import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import * as reducer from './common/reducers/reducer';
import * as artifact from './common/actions/artifact.action';

@Component({
  selector: 'app-root',
  templateUrl: './app.html'
})
export class AppComponent{

  constructor(private _store: Store<reducer.State>) {
  }
}
