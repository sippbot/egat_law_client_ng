import { ACL } from '../../common/util/acl.util';
import { TicketEffects } from '../../common/effects/ticket.effect';
import { Store } from '@ngrx/store';
import { Ticket } from '../../common/models/ticket.model';
import { Component, OnInit } from '@angular/core';
import * as reducer from '../../common/reducers/reducer';
import * as ticket from '../../common/actions/ticket.action';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
declare var $: any;

export interface CloseTicketModel {
  ticketId: number;
  taskAssignmentId: number;
}

@Component({
  selector: 'app-close',
  templateUrl: './close.component.html'
})
export class TicketCloseComponent extends DialogComponent<CloseTicketModel, boolean> implements CloseTicketModel, OnInit {
  ticketId: number;
  taskAssignmentId: number;
  userId: number;
  // ticketId: number;
  actionDetail: string;
  ticket: Ticket;
  isConfirm: boolean;
  heightModal:number;
  acl: ACL;

  constructor(public dialogService: DialogService,
    private _store: Store<reducer.State>,
    private ticketEffects: TicketEffects) {
    super(dialogService);
    this.ticket = new Ticket();
    this.acl = new ACL();
    this._store.let(reducer.getSelectedTicket).subscribe(
      ticket => {
        if (ticket != null) {
          this.ticket = ticket;
        }
      }
    );
    this.ticketEffects.closeTicket$.filter(
      action => action.type === ticket.ActionTypes.CLOSE_TICKET_SUCCESS
    ).subscribe(() => {
      this.closeModal()
      location.reload();
    });

    this.userId = +localStorage.getItem('userId');
  }
  ngOnInit() {
    this.heightModal = window.innerHeight-290;
    console.log(this.heightModal)
    this._store.dispatch(new ticket.SelectOrLoadTicketAction(this.ticketId));
    
    if(this.isReadOnly(['TCKMNG'])==false){
      $(function () {
        $('.modal-footer').hide();
        $('.checkbox').change(function () {
          if (this.checked) {
            // Do stuff
            $('.modal-footer').slideToggle();

          } else {
            $('.modal-footer').slideToggle();
          }
        });
      });
    }
  }
  closeModal(){
    $(".modal-backdrop").remove();
    $(".modal.fade").remove();
    $("body").removeClass("modal-open")
  }
  onSubmit() {

    const request = {
      taskAssignmentId: this.taskAssignmentId,
      ticketId: this.ticketId,
      userId: this.userId,
      actionDetail: this.actionDetail
    };

    this._store.dispatch(new ticket.CloseTicketAction(request));
  }
  role(role: string): boolean {
    return this.acl.hasRole(role);
  }
  isReadOnly(role_list: Array<string>): boolean {
    let result = false;
    let thisobj = this;
    $.each(role_list,function(i,role){ 
      result = result||(thisobj.acl.isReadOnly(role)===false)
    });
    return !result;
  }
}
