import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TicketCloseComponent } from './close.component';

describe('CloseComponent', () => {
  let component: TicketCloseComponent;
  let fixture: ComponentFixture<TicketCloseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TicketCloseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TicketCloseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
