import { SafeHtmlPipeModule } from '../safe-html.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TicketIndex } from './index.page';
import { MetadataService } from '../common/services/metadata.service';
import { UserService } from '../common/services/user.service';
import { AuthGuard } from '../guard/auth.guard';
import { DetailComponent } from './detail/detail.component';
import { TicketCloseComponent } from './close/close.component';
import { SummaryComponent } from './summary/summary.component';

export const routes: Routes = [
  { path: 'ticket/index', component: TicketIndex, canActivate: [AuthGuard] },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    DataTablesModule,
    SafeHtmlPipeModule
    /* Put the reducer as an argument in provideStore */

  ],
  declarations: [
    TicketIndex,
    DetailComponent,
    TicketCloseComponent,
    SummaryComponent,
  ],
  entryComponents: [
    TicketCloseComponent
  ],
  exports: [
    TicketCloseComponent
  ],

  providers: [MetadataService, UserService]

})

export class TicketModule { }
