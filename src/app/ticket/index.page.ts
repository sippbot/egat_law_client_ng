import { Ticket } from '../common/models/ticket.model';
import { Store } from '@ngrx/store';
import * as reducer from '../common/reducers/reducer';
import { OnInit, Component, ViewChild } from '@angular/core';
import { ApplicationConstant } from '../common/application-constant';
import { DataTableDirective } from 'angular-datatables';
import * as ticket from '../common/actions/ticket.action';
import { TicketEffects } from '../common/effects/ticket.effect';
import { Subject } from 'rxjs';
import { ACL } from '../common/util/acl.util';

@Component({
  selector: 'cat-page',
  templateUrl: './index.html',
})
export class TicketIndex implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  tickets: Array<Ticket>;
  acl: ACL;
  departmentId: number;
  dtTrigger: Subject<any> = new Subject();
  dtOptions: DataTables.Settings = {
    'autoWidth': true,
    'pageLength': 30,
    'language': ApplicationConstant.DATATABLE_GLOBAL_LANGUAGE_CONFIG,
    'dom': '<\'row\'<\'col-sm-12\'B>>' +
      '<\'row\'<\'col-sm-6\'l><\'col-sm-6\'f>>' +
      '<\'row\'<\'col-sm-12\'tr>>' +
      '<\'row\'<\'col-sm-5\'i><\'col-sm-7\'p>>',
    'buttons': [
      {
        extend: 'excelHtml5',
        text: '<i class="fa fa-file-excel-o fa-x5"></i> ดาวน์โหลด Excel',
        className: 'btn btn-primary btn-sm glyphicon glyphicon-list-alt pull-right',
        exportOptions: {
          columns: [0, 1, 2, 3, 4, 5, 6, 7, 8]
        }
      },
      {
        extend: 'print',
        text: '<i class="fa fa-print fa-x5"></i> พิมพ์รายงาน',
        className: 'btn btn-primary btn-sm btn-print glyphicon glyphicon-print pull-right',
        pageSize: 'A4',
        title: 'รายการเอกสาร',
        exportOptions: {
          columns: [0, 1, 2, 3, 4, 5, 6, 7, 8]
        }
      }
    ]
  };

  constructor(
    private ticketEffects: TicketEffects,
    private _store: Store<reducer.State>) {
    this.acl = new ACL();
    this.departmentId = +localStorage.getItem('departmentId');
    this._store.let(reducer.getTickets).subscribe(
      _tickets => {
        this.tickets = _tickets;
        this.rerender();
        return this.tickets;
      }
    );
    this.ticketEffects.loadTickets$.filter(
      action => action.type === ticket.ActionTypes.LOAD_TICKETS_SUCCESS
    ).subscribe(() => {
      this.rerender();
    });
  }
  ngOnInit() {
    let payload: any;
    if (this.role('ATFMNG')) {
      payload = {
        departmentId: 0,
        status: 'Submitted'
      };
    } else {
      payload = {
        departmentId: this.departmentId,
        status: 'Submitted'
      };
    }
    this._store.dispatch(new ticket.LoadTicketsAction(payload));
  }
  role(role: string): boolean {
    return this.acl.hasRole(role);
  }
  rerender(): void {
    if (this.dtElement) {
      if (this.dtElement.dtInstance !== undefined) {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          // Destroy the table first
          dtInstance.destroy();
          // Call the dtTrigger to rerender again
          this.dtTrigger.next();
        });
      } else {
        this.dtTrigger.next();
      }
    }
  }
}
