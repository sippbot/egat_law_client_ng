import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskReviewComponent } from './review.component';

describe('TaskReviewComponent', () => {
  let component: TaskReviewComponent;
  let fixture: ComponentFixture<TaskReviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaskReviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskReviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
