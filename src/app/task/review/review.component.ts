import { Router } from '@angular/router';
import { ACL } from '../../common/util/acl.util';
import { TaskAssignmentEffects } from '../../common/effects/task-assignment.effect';
import { TaskAssessment } from '../../common/models/task-assessment.model';
import { AssessmentReview } from '../../common/models/assessment-review.model';
import { Store } from '@ngrx/store';
import { Component, OnInit, Input } from '@angular/core';
import { Alert } from '../../common/models/alert.model';
import { ShowAlertAction } from '../../common/actions/alert.action';
declare var $: any;
import * as reducer from '../../common/reducers/reducer';
import * as taskAssignment from '../../common/actions/task-assignment.action';

@Component({
  selector: 'app-task-review',
  templateUrl: './review.component.html'
})
export class TaskReviewComponent implements OnInit {
  assessmentReview: AssessmentReview;
  isConfirm = false;
  reviewComment = '';
  recentTaskAssessment: TaskAssessment;
  @Input() department: string;
  @Input() taskAssessments: Array<TaskAssessment>;
  @Input() taskAssignmentId: number;
  @Input() userId: number;
  @Input() artifactId: number;
  acl: ACL;

  constructor(private router: Router,
    private _store: Store<reducer.State>,
    private taskAssignmentEffects: TaskAssignmentEffects) {
    this.acl = new ACL();
    this.assessmentReview = new AssessmentReview();
    this.taskAssignmentEffects.createTaskAssessmentReview$.filter(
      action => action.type === taskAssignment.ActionTypes.CREATE_TASKASSESSMENT_REVIEW_SUCCESS
    ).subscribe(() => {
      this.closeModal();
      this.returnToMiddle();
      this._store.dispatch(new ShowAlertAction(new Alert('บันทึกการตรวจสอบแล้ว', 'สำเร็จ!', 'success')));
    });

    this.taskAssignmentEffects.createTaskAssessmentReview$.filter(
      action => action.type === taskAssignment.ActionTypes.CREATE_TASKASSESSMENT_REVIEW_FAIL
    ).subscribe(() => {
      this.closeModal();
      this._store.dispatch(new ShowAlertAction(new Alert('ลองเข้าสูระบบ และประเมินการตรวจสอบอีกครั้ง', 'เกิดข้อผิดพลาด!', 'fail')));
    });
  }

  onSubmit() {
    this.assessmentReview.reviewComment = this.reviewComment;
    this.assessmentReview.isConfirm = this.isConfirm;
    this.assessmentReview.reviewerId = this.userId;
    this.assessmentReview.taskAssessmentId = this.getAssessmentId();
    this.assessmentReview.taskAssignmentId = this.taskAssignmentId;
    this._store.dispatch(new taskAssignment.CreateTaskAssessmentReviewAction(this.assessmentReview));
  }

  role(role: string): boolean {
    return this.acl.hasRole(role);
  }

  onReject() {
    this.assessmentReview.reviewComment = this.reviewComment;
    this.assessmentReview.isConfirm = this.isConfirm;
    this.assessmentReview.reviewerId = this.userId;
    this.assessmentReview.taskAssessmentId = this.getAssessmentId();
    this.assessmentReview.taskAssignmentId = this.taskAssignmentId;
    this._store.dispatch(new taskAssignment.CreateTaskAssessmentReviewAction(this.assessmentReview));
  }

  reloadRecentTaskAssessment(): void {
    if (this.taskAssessments.length === 0) {
      return null;
    }
    this.recentTaskAssessment = this.taskAssessments[0];
    for (const item of this.taskAssessments) {
      if (this.recentTaskAssessment.id < item.id) {
        this.recentTaskAssessment = item;
      }
    }
  }

  getAssessmentId(): number {
    this.reloadRecentTaskAssessment();
    return this.recentTaskAssessment.id;
  }

  onClickCheckEvaluate(): void {
    this.reloadRecentTaskAssessment();
    this.isConfirm = false;
    $('#reviewModal').modal('show');
  }

  ngOnInit() {
    /*
    this.formSuccess = this.actions$
      .ofType(taskAssignment.ActionTypes.CREATE_TASKASSESSMENT_REVIEW_SUCCESS)
      .subscribe(() => {
        this._store.dispatch(new ShowAlertAction(new Alert('บันทึกการตรวจสอบแล้ว', 'สำเร็จ!', 'success')));
        this.closeModal();
      });

    this.formError = this.actions$
      .ofType(taskAssignment.ActionTypes.CREATE_TASKASSESSMENT_REVIEW_FAIL)
      .subscribe(() => {
        this._store.dispatch(new ShowAlertAction(new Alert('ลองเข้าสูระบบ และประเมินการตรวจสอบอีกครั้ง', 'เกิดข้อผิดพลาด!', 'fail')));
        this.closeModal();
      });
*/
    this.reloadRecentTaskAssessment();
    $(function () {
      $('.modal-footer .checkbox').change(function () {
        if (this.checked) {
          $(this).val('check');
          $('.alert.alert-danger').hide();
          $('.confirm').attr('disabled', false);

        } else {
          $(this).val('');
          $('.confirm').attr('disabled', 'disabled');
        }
      });
    });
  }
  returnToMiddle() {
    this.router.navigate(['/artifact/task/initial/' + this.artifactId]);
  }
  closeModal() {
    $(function () {
      $('#reviewModal').modal('hide');
      $(".modal-backdrop").remove();
      $("body").removeClass("modal-open")
      // this.returnToMiddle();
    });
  }
}

