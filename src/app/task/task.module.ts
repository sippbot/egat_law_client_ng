import { SafeHtmlPipeModule } from '../safe-html.module';
import { SweetAlert2Module } from '@toverux/ngsweetalert2';
import { DocumentsModule } from '../documents/documents.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { ArtifactService } from '../common/services/artifact.service';
import { TaskAssignmentComponent } from './assignment/assignment.component';
import { TaskAssessmentComponent } from './assessment/assessment.component';
import { TaskCommentComponent } from './comment/comment.component';
import { DocumentComponent } from './document/document.component';
import { TaskReviewComponent } from './review/review.component';
import { AuthGuard } from '../guard/auth.guard';
import { TaskAssignmentSummaryComponent } from './summary/summary.component';
import { TaskApprovalComponent } from './approval/approval.component';
import { ArtifactModule } from '../artifact/artifact.module';
import { TicketModule } from '../ticket/ticket.module';

export const routes: Routes = [
  { path: 'task/:type/:id', component: TaskAssignmentComponent, canActivate: [AuthGuard] },
  { path: 'task/summary/:type/:id', component: TaskAssignmentSummaryComponent, canActivate: [AuthGuard] },
  { path: 'task/approve/:type/:id', component: TaskApprovalComponent, canActivate: [AuthGuard] }
  // { path: 'retention/plan', component: RetentionPlan, canActivate: [AuthGuard] },
  // { path: 'retention/addnew-plan/:lawId', component: RetentionNewPlan, canActivate: [AuthGuard] },
  // { path: 'retention/all', component: RetentionAll, canActivate: [AuthGuard] },
  // { path: 'retention/all-overdue', component: RetentionAllOverDue, canActivate: [AuthGuard] },
  // { path: 'retention/find-document', component: RetentionFindDocument, canActivate: [AuthGuard] },
  // { path: 'retention/results', component: RetentionResults, canActivate: [AuthGuard] },
  // { path: 'retention/evaluate-practice/:id', component: RetentionEvaluatePractice, canActivate: [AuthGuard] },
  // { path: 'retention/check-evaluate/:id', component: RetentionCheckEvaluate, canActivate: [AuthGuard] },
  // { path: 'retention/approve-evaluate/:id', component: RetentionApproveEvaluate, canActivate: [AuthGuard] },
  // { path: 'retention/law-index', component: retentionLawsIndex, canActivate: [AuthGuard] },
  // { path: 'retention/law-index-followup', component: retentionLawsIndexFollowup, canActivate: [AuthGuard] },
  // { path: 'retention/view/:id', component: TrackingView, canActivate: [AuthGuard] },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    FroalaEditorModule,
    FroalaViewModule,
    DocumentsModule,
    ArtifactModule,
    TicketModule,
    SweetAlert2Module,
    SafeHtmlPipeModule
  ],
  declarations: [
    TaskAssignmentComponent,
    TaskAssessmentComponent,
    TaskCommentComponent,
    DocumentComponent,
    TaskReviewComponent,
    TaskAssignmentSummaryComponent,
    TaskApprovalComponent
  ],
  providers: [
    ArtifactService,
    AuthGuard
  ]
})
export class TaskModule { }
