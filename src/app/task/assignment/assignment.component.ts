import { Document } from '../../common/models/document.model';
import { ACL } from '../../common/util/acl.util';
import { AttachmentComponent } from '../../documents/attachment/attachment.component';
import { LicenseUploadComponent } from '../../license/upload/upload.component';
import { TicketCloseComponent } from '../../ticket/close/close.component';
import { DialogService } from 'ng2-bootstrap-modal';
import { TaskAssessment } from '../../common/models/task-assessment.model';
import { TaskAssignment } from '../../common/models/task-assignment.model';
import { OnInit, Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ApplicationConstant } from '../../common/application-constant';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { Alert } from '../../common/models/alert.model';
import { ShowAlertAction } from '../../common/actions/alert.action';
import { TaskAssignmentEffects } from '../../common/effects/task-assignment.effect';
import { DocumentUploadComponent } from '../../documents/upload/upload.component';
import * as reducer from '../../common/reducers/reducer';
import * as taskAssignment from '../../common/actions/task-assignment.action';
import { BsModalService } from 'ngx-bootstrap/modal';
declare var $: any;

@Component({
  selector: 'cat-page',
  templateUrl: './assignment.component.html',
})

export class TaskAssignmentComponent implements OnInit {
  selectTask: any;
  taskAssignmentId: number;
  departmentId: number;
  taskAssignment: TaskAssignment;
  selectedDepartmentName: string;
  userId: number;
  taskAssessments: Array<TaskAssessment>;
  documents: Array<Document>;
  acl: ACL;
  fileEndpoint: string = ApplicationConstant.APP_BASE_ENDPOINT_URL + '/document/';
  mode:string;
  constructor(private _route: ActivatedRoute,
    private _store: Store<reducer.State>,
    private fb: FormBuilder,
    private router: Router,
    public dialogService: DialogService,
    private modalService: BsModalService,
    private taskAssignmentEffects: TaskAssignmentEffects) {
    this.taskAssignment = new TaskAssignment();
    this.acl = new ACL();
    this._route.params.subscribe(params => {
      this.taskAssignmentId = +params['id'];
      this.mode = params['type']
    });
    this._store.dispatch(new taskAssignment.ClearSelectedTaskAssignmentction({}))
    this._store.let(reducer.getSelectedTaskAssignment).subscribe(
      taskAssignment => {
        if (taskAssignment != null) {
          console.log(taskAssignment,taskAssignment.artifactId)
          this.taskAssignment = taskAssignment;
        }
      });

    this._store.let(reducer.getTaskAssessments).subscribe(
      taskAssessments => this.taskAssessments = taskAssessments
    );

    this._store.let(reducer.getTasAssignmentDocuments).subscribe(
      documents => this.documents = documents
    );

    this.departmentId = +localStorage.getItem('departmentId');
    this.userId = +localStorage.getItem('userId');

    this.taskAssignmentEffects.deleteTaskAssignmentAttachment$.filter(
      action => action.type === taskAssignment.ActionTypes.DELETE_TASKASSIGNMENT_ATTACHMENT_SUCCESS
    ).subscribe(() => {
      this._store.dispatch(new ShowAlertAction(new Alert('ลบไฟล์เรียบร้อยแล้ว', 'ลบไฟล์สำเร็จ!', 'success')))
    });
  }

  ngOnInit() {
    this._store.dispatch(new taskAssignment.LoadTaskAssignmentFullDetailAction({ taskAssignmentId: this.taskAssignmentId }));

    $(function () {

      $('.alert.alert-danger').hide();
      $('.confirm').attr('disabled', 'disabled');

      $('#button-comment').click(function () {
        $('html, body').animate({
          scrollTop: $('#comment_div').offset().top - 80
        }, 700);
      });

      $('#table-search-document').DataTable({
        responsive: true
      });

      $('#table-search-license').DataTable({
        responsive: true
      });

      $('.dropify').dropify();

      $('.document-type').hide();
      $('.dropdown-menu a').click(function () {
        $('.document-type').hide();
        $($(this).attr('rel')).show();
      });

      $('.close-addfile').click(function () {
        $('.document-type').hide();
      });

      $('.table-all-document').on('click', '.icmn-cross', function () {
        $(this).parent().parent().remove();
      });

      $('.readmore-toggle a').click(function () {
      });
    });
  }

  deleteAttachment(documentId: number) {
    this._store.dispatch(new taskAssignment.DeleteTaskAssignmentAttachmentAction({
      taskAssignmentId: this.taskAssignmentId,
      documentId: documentId
    }));
  }

  closeTicket(_ticketId: number, _taskAssignmentId: number) {
    
    const disposable = this.dialogService.addDialog(TicketCloseComponent, {
      taskAssignmentId: _taskAssignmentId,
      ticketId: _ticketId
    }).subscribe((isSuccess) => {
      // We get dialog result
      if (isSuccess) {
        // alert('success');
      } else {
        // alert('declined');
      }
    });
    
  }
  uploadDocument(){
    const initialState = {
      taskAssignmentId: this.taskAssignmentId,
      userId: this.userId,
      mode: 'upload_document'
    };
    this.modalService.show(DocumentUploadComponent, {initialState});
  }
  uploadLicense() {
    const initialState = {
      taskAssignmentId: this.taskAssignmentId,
      userId: this.userId,
      mode: 'upload_license'
    };
    this.modalService.show(LicenseUploadComponent, {initialState});
    /*
    const disposable = this.dialogService.addDialog(LicenseUploadComponent, {
      taskAssignmentId: this.taskAssignmentId,
      userId: this.userId
    }).subscribe((isSuccess) => {
      // We get dialog result
      if (isSuccess) {
        // alert('success');
      } else {
        // alert('declined');
      }
    });
    */
  }

  attachDocument() {
    const initialState = {
      taskAssignmentId: this.taskAssignmentId,
      userId: this.userId,
      mode: 'documents'
    };
    this.modalService.show(AttachmentComponent, {initialState});
    /*
    const disposable = this.dialogService.addDialog(AttachmentComponent, {
      taskAssignmentId: this.taskAssignmentId,
      userId: this.userId,
      mode: 'documents'
    }).subscribe((isSuccess) => {
      // We get dialog result
      if (isSuccess) {
        // this.dialogService.removeDialog()
      } else {
        // alert('declined');
      }
    });
    */
    
  }

  attachLicense() {
    const initialState = {
      taskAssignmentId: this.taskAssignmentId,
      userId: this.userId,
      mode: 'license'
    };
    this.modalService.show(AttachmentComponent, {initialState});
    /*
    const disposable = this.dialogService.addDialog(AttachmentComponent, {
      taskAssignmentId: this.taskAssignmentId,
      userId: this.userId,
      mode: 'license'
    }).subscribe((isSuccess) => {
      // We get dialog result
      if (isSuccess) {
        // alert('success');
      } else {
        // alert('declined');
      }
    });
    */
  }

  redirectToIinsndex() {
    this.router.navigate(['/retention/all']);
  }
  role(role: string): boolean {
    return this.acl.hasRole(role);
  }
  isReadOnly(role_list: Array<string>): boolean {
    let result = false;
    let thisobj = this;
    $.each(role_list,function(i,role){ 
      result = result||(thisobj.acl.isReadOnly(role)===false)
    });
    return !result;
  }
}
