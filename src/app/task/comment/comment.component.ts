import { Store } from '@ngrx/store';
import { TaskComment } from '../../common/models/task-assignment-comment.model';
import { Component, Input } from '@angular/core';
import * as reducer from '../../common/reducers/reducer';
import * as taskAssignment from '../../common/actions/task-assignment.action';

@Component({
  selector: 'app-task-comment',
  templateUrl: './comment.component.html'
})
export class TaskCommentComponent {

  @Input() userId: number;
  @Input() taskAssignmentId: number;
  comments: Array<TaskComment>;

  content: string;
  constructor(private _store: Store<reducer.State>) {
    this._store.let(reducer.getTaskComments).subscribe(
      comments => this.comments = comments
    );
  }

  onSubmit() {
    const req = new TaskComment();
    req.content = this.content;
    req.taskAssignmentId = this.taskAssignmentId;
    req.creatorId = this.userId;

    this._store.dispatch(new taskAssignment.CreateTaskCommentAction(req));

    // Reset value
    this.content = '';
  }
}
