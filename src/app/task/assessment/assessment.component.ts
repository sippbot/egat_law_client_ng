import { Router } from '@angular/router';
import { ACL } from '../../common/util/acl.util';
import { TaskAssignmentEffects } from '../../common/effects/task-assignment.effect';
import { Alert } from '../../common/models/alert.model';
import { ShowAlertAction } from '../../common/actions/alert.action';
import { Store } from '@ngrx/store';
import { AssessmentInfo } from '../../common/models/assessment-info.model';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';

import * as taskAssignment from '../../common/actions/task-assignment.action';
import * as reducer from '../../common/reducers/reducer';
declare var $: any;
declare var jQuery: any;

@Component({
  selector: 'app-task-assessment',
  templateUrl: './assessment.component.html'
})
export class TaskAssessmentComponent implements OnInit {
  // artifactId: number;
  taskAssessmentForm: FormGroup;
  assessmentInfo: AssessmentInfo;
  @Input() departmentId: number;
  @Input() taskAssignmentId: number;
  @Input() userId: number;
  @Input() artifactId: number;
  acl: ACL;

  constructor(private fb: FormBuilder,
    private _store: Store<reducer.State>,
    private router: Router,
    private taskAssignmentEffects: TaskAssignmentEffects) {
    // initial form
    
    this.taskAssessmentForm = this.fb.group({
      isApplicable: ['', [Validators.required]],
      isCompliant: ['', [Validators.required]],
      detail: ['', [Validators.required]]
    });
    // this._route.params.subscribe(params => {
    //   this.artifactId = +params['id'];
    // });
    this.acl = new ACL();
    this.taskAssignmentEffects.createTaskAssessment$.filter(
      action => action.type === taskAssignment.ActionTypes.CREATE_TASKASSESSMENT_SUCCESS
    ).subscribe(() => {
      jQuery('#isApplicable').val("").trigger('change');
      this.closeModal();
      this.returnToMiddle();
      this._store.dispatch(new ShowAlertAction(new Alert('บันทึกการประเมินแล้ว', 'สำเร็จ!', 'success')));
    });

    this.taskAssignmentEffects.createTaskAssessment$.filter(
      action => action.type === taskAssignment.ActionTypes.CREATE_TASKASSESSMENT_FAIL
    ).subscribe(() => {
      jQuery('#isApplicable').val("").trigger('change');
      this.closeModal();
      this._store.dispatch(new ShowAlertAction(new Alert('ลองเข้าสูระบบ แล้วประเมินอีกครั้ง', 'เกิดข้อผิดพลาด!', 'fail')));
    });
  }

  ngOnInit() {
    /*
    this.formSuccess = this.actions$
      .ofType(taskAssignment.ActionTypes.CREATE_TASKASSESSMENT_SUCCESS)
      .subscribe(() => {
        this._store.dispatch(new ShowAlertAction(new Alert('บันทึกการประเมินแล้ว', 'สำเร็จ!', 'success')));
        this.closeModal();
      });

    this.formError = this.actions$
      .ofType(taskAssignment.ActionTypes.CREATE_TASKASSESSMENT_FAIL)
      .subscribe(() => {
        this._store.dispatch(new ShowAlertAction(new Alert('ลองเข้าสูระบบ แล้วประเมินอีกครั้ง', 'เกิดข้อผิดพลาด!', 'fail')));
        this.closeModal();
      });
      */
  }

  role(role: string): boolean {
    return this.acl.hasRole(role);
  }

  onSubmit() {
    console.log(this.departmentId,'sdfsdf')
    this.assessmentInfo = this.taskAssessmentForm.value;
    this.assessmentInfo.isApplicable = this.taskAssessmentForm.value.isApplicable=="true"?true:false;
    this.assessmentInfo.isCompliant = this.taskAssessmentForm.value.isCompliant=="true"?true:false;
    this.assessmentInfo.departmentId = this.departmentId;
    this.assessmentInfo.taskAssignmentId = this.taskAssignmentId;
    this.assessmentInfo.creatorId = this.userId;
    this._store.dispatch(new taskAssignment.CreateTaskAssessmentAction(this.assessmentInfo));
  }
  returnToMiddle(){
    this.router.navigate(['/artifact/task/initial/'+this.artifactId]);
  }
  closeModal() {
    $(function () {
      $('#assessmentModal').modal('hide');
      $(".modal-backdrop").remove();
      $("body").removeClass("modal-open")
    });
  }
  /*
    ngOnDestroy() {
      this.formError.unsubscribe();
      this.formSuccess.unsubscribe();
    }
  */
}
