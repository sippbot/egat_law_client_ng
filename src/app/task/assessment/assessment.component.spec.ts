import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskAssessmentComponent } from './assessment.component';

describe('TaskAssessmentComponent', () => {
  let component: TaskAssessmentComponent;
  let fixture: ComponentFixture<TaskAssessmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaskAssessmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskAssessmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
