import { ACL } from '../../common/util/acl.util';
import { TaskAssignmentSummary } from '../../common/models/task-assignment-summary.model';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { ActivatedRoute } from '@angular/router';
import { ArtifactSummary } from '../../common/models/artifact-summary.model';
import { Artifact } from '../../common/models/artifact.model';
import * as reducer from '../../common/reducers/reducer';
import * as taskAssignment from '../../common/actions/task-assignment.action';
import { TaskAssignmentEffects } from '../../common/effects/task-assignment.effect';
import { Alert } from '../../common/models/alert.model';
import { ShowAlertAction } from '../../common/actions/alert.action';
import { SwalComponent } from '@toverux/ngsweetalert2';
declare var $: any;
declare var jQuery: any;

@Component({
  selector: 'cat-page',
  templateUrl: './approval.component.html',

})
export class TaskApprovalComponent implements OnInit {
  artifact: Artifact;
  artifactReviewAssignment: any;
  selectedArtifact: ArtifactSummary;
  SelectedArtifactReviewAssignment: any;
  SelectedSubArtifactReviewAssessment: any;
  proposedTaskAssignmentSummaries: Array<TaskAssignmentSummary>;
  inprogressTaskAssignmentSummaries: Array<TaskAssignmentSummary>;
  ArtifactReviewAssignment: any;
  departmentId: number;
  userId: number;
  artifactId: number;
  mode: string;
  taskGroupId: number;
  artifactReviewAssessmentForm: FormGroup;
  @ViewChild('validation_dialog') private validationDialog: SwalComponent;
  law: any;
  acl: ACL;
  _selectedDepartment: number[] = [];

  constructor(private _route: ActivatedRoute,
    private _store: Store<reducer.State>,
    private router: Router,
    private taskAssignmentEffects: TaskAssignmentEffects) {
    this.acl = new ACL();
    this.law = new Artifact();
    this.artifact = new Artifact();
    this.proposedTaskAssignmentSummaries = new Array<TaskAssignmentSummary>();
    this.inprogressTaskAssignmentSummaries = new Array<TaskAssignmentSummary>();
    this._route.params.subscribe(params => {
      this.artifactId = +params['id'];
      this.mode = params['type'];
      if (this.artifactId > 0) {
        // this._store.dispatch(new artifact.SelectOrLoadArtifactAction({artifactId:this.artifactId,isActive:true}));
      }
      this._store.dispatch(new taskAssignment.LoadTaskAssignmentSummaryAction(this.artifactId));

    });

    this._store.let(reducer.getSelectedArtifacts).subscribe(art => {
      if (art != null) {
        this.law = art;
        console.log(this.law)
      }
    });

    this._store.let(reducer.getTaskAssignmentSummary).subscribe(taskSummary => {
      if (taskSummary.length !== 0) {
        this.proposedTaskAssignmentSummaries = taskSummary.filter(
          t => (t.isApplicable == true && t.isCompliant == true) || ( t.isApplicable == false ) && (t.status === 'Proposed')// applicable = true ,iscomplain=true,.status === 'Proposed'
        );
        this.inprogressTaskAssignmentSummaries = taskSummary.filter(
          t => (t.isApplicable == true) && (t.isCompliant == false) && (t.status === 'Proposed') //iscomplain=false,.status === 'Proposed'
        );
      }
      if (this.proposedTaskAssignmentSummaries.length !== 0) {
        this.taskGroupId = this.proposedTaskAssignmentSummaries[0].taskGroupId;
      }
    });
    this.taskAssignmentEffects.createTaskAssessmentApproval$.filter(
      action => action.type === taskAssignment.ActionTypes.CREATE_TASKASSIGNMENT_APPROVAL_SUCCESS
    ).subscribe(() => {
      this.closeModal();
      this._store.dispatch(new taskAssignment.LoadTaskAssignmentSummaryAction(this.artifactId));
      this._store.dispatch(new ShowAlertAction(new Alert('อนุมัติเสร็จสิ้น', 'สำเร็จ!', 'success')));
      this.router.navigate(["/artifact/list/" + this.mode + "/inprogress"])
    });
  }

  ngOnInit() {
    console.log(this.artifactId)
    // this._store.dispatch(new artifact.SelectApproveArtifactAssessmentAction(this.artifactId));
    // this._store.dispatch(new taskAssignment.LoadTaskAssignmentSummaryAction(this.artifactId));

    $('.btn-present-executive').attr('disabled', 'disabled');

    // DO NOT USE; SEE BELOW
    jQuery('.table-checkbox').click(function () {
      var atLeastOneIsChecked = false;
      jQuery('.table-checkbox').each(function () {
        if (jQuery(this).is(':checked')) {
          jQuery('yes');
        }
      });

      // Do something with atLeastOneIsChecked
    });
  }

  onSubmit() {
    const signature = jQuery('#signature').val();
    if ((signature != "") && (signature != undefined)) {
      let approverId;
      approverId = localStorage.getItem('userId');
      // TODO handle login if userId is null

      const departmentList = this.proposedTaskAssignmentSummaries.map(({ departmentId }) => departmentId);
      const req = {
        approverId: approverId,
        artifactId: this.artifactId,
        departments: departmentList,
        signature: signature
      };
      this._store.dispatch(new taskAssignment.CreateTaskAssignmentApprovalAction(req));
    } else {
      this.validationDialog.show();
    }
  }

  /*
  ngOnReview(depIt: number) { // ตรวจสอบผลการประเมิน

    this._store.let(reducer.getSelectedSubArtifactReviewAssessment).subscribe(art => {
      if (art !== null) {
        this.SelectedSubArtifactReviewAssessment = art;
      }
    });

    this._store.dispatch(new artifact.SelectSubArtifactReviewAssessmentAction(depIt));

    console.log(this.SelectedSubArtifactReviewAssessment);

  }
  */
  closeModal() {
    $(function () {
      // $('.close').click();
      $('#modal-approve').modal('hide');
      $(".modal-backdrop").remove();
      $("body").removeClass("modal-open")
    });
  }
  role(role: string): boolean {
    return this.acl.hasRole(role);
  }
  isReadOnly(role_list: Array<string>): boolean {
    let result = false;

    let thisobj = this;
    $.each(role_list, function (i, role) {
      result = result || (thisobj.acl.isReadOnly(role) === false)
    });
    return !result;
  }
}
