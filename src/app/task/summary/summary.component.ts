import { ACL } from '../../common/util/acl.util';
import { TaskAssignmentSummary } from '../../common/models/task-assignment-summary.model';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { DataTableDirective } from 'angular-datatables';
import { ActivatedRoute } from '@angular/router';
import { ApplicationConstant } from '../../common/application-constant';
import { Artifact } from '../../common/models/artifact.model';
import * as reducer from '../../common/reducers/reducer';
import * as artifact from '../../common/actions/artifact.action';
import * as taskAssignment from '../../common/actions/task-assignment.action';
import { TaskAssignmentEffects } from '../../common/effects/task-assignment.effect';
import { Alert } from '../../common/models/alert.model';
import { ShowAlertAction } from '../../common/actions/alert.action';

import { Subject } from 'rxjs';
declare var $: any;

@Component({
  selector: 'cat-page',
  templateUrl: './summary.component.html'
})

export class TaskAssignmentSummaryComponent implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  artifact: Artifact;
  departmentId: number;
  userId: number;
  artifactId: number;
  // allCheckBox:boolean;
  _selectedDepartment: number[] = [];

  taskAssignmentSummary: Array<TaskAssignmentSummary>;
  acl: ACL;
  dtOptions: DataTables.Settings = {
    'columnDefs': [
      { 'width': '5%', 'targets': 0 },
      { 'width': '15%', 'targets': 1 },
      { 'width': '15%', 'targets': 2 },
      { 'width': '25%', 'targets': 3 },
      { 'width': '20%', 'targets': 4 },
      { 'width': '20%', 'targets': 5 },
    ],
    'language': ApplicationConstant.DATATABLE_GLOBAL_LANGUAGE_CONFIG
  };
  // We use this trigger because fetching the list of persons can be quite long,
  // thus we ensure the data is fetched before rendering
  dtTrigger: Subject<any> = new Subject();
  mode: string;
  constructor(private _route: ActivatedRoute,
    private _store: Store<reducer.State>,
    private fb: FormBuilder,
    private router: Router,
    private taskAssignmentEffects: TaskAssignmentEffects
  ) {
    this.acl = new ACL();
    // this.allCheckBox = false;
    this._route.params.subscribe(params => {
      this.artifactId = +params['id'];
      this.mode = params['type'];
    });

    this._store.let(reducer.getTaskAssignmentSummary).subscribe(
      taskAssignmentSummary => this.taskAssignmentSummary = taskAssignmentSummary
    );
    this.taskAssignmentEffects.createTaskAssignmentProposal$.filter(
      action => action.type === taskAssignment.ActionTypes.CREATE_TASKASSIGNMENT_PROPOSAL_SUCCESS
    ).subscribe(() => {
      this.closeModal();
      this._store.dispatch(new taskAssignment.LoadTaskAssignmentSummaryAction(this.artifactId));
      this._store.dispatch(new ShowAlertAction(new Alert('นำเสนอผู้แทนฝ่ายบริหารอนุมัติเสร็จสิ้น', 'สำเร็จ!', 'success')));
      this.router.navigate(["/artifact/list/" + this.mode + "/inprogress"])
    });

  }

  ngOnInit() {

    this._store.dispatch(new artifact.SelectOrLoadArtifactAction({ artifactId: this.artifactId, isActive: true }));

    // this._store.dispatch(new artifact.SelectArtifactReviewAssessmentAction(this.artifactId));

    this._store.dispatch(new taskAssignment.LoadTaskAssignmentSummaryAction(this.artifactId));

    $(function () {


      $('.alert.alert-danger').hide();

      $('.confirm').attr('disabled', 'disabled');

      $('.modal-footer .checkbox').change(function () {
        if (this.checked) {
          $(this).val('check');
          $('.alert.alert-danger').hide();
          $('.confirm').attr('disabled', false);

        } else {
          $(this).val('');
          $('.confirm').attr('disabled', 'disabled');
        }
      });

      $('.btn-present-executive').attr('disabled', 'disabled');

      $('.btn-present-executive').click(function () {
        let count = 1;

        $('#modal-present-executive ul').html('');
        $('#review-artifact-assesment-table .checkbox').each(function () {
          const value = $(this).parent().parent().find('#get-departmentName').val();
          if ($(this).is(':checked') === true) {
            const content = '<li>' + value + '</li>';
            $('#modal-present-executive ul').append(content);
            count++;
          }
        });
      });
    });
  }

  onSubmit(): void {
    // if(this.allCheckBox){
    const _selectedDepartment2 = [];
    $('#review-artifact-assesment-table .checkbox').each(function () {
      if ($(this).is(':checked') === true) {
        const depIt = $(this).val();
        if ((_selectedDepartment2.indexOf(depIt) == -1) && ($(this).prop('disabled') == false)) {
          _selectedDepartment2.push(depIt);
        }
      }

    });

    let userId;
    userId = localStorage.getItem('userId');
    this._selectedDepartment = _selectedDepartment2;

    const req = {
      artifactId: this.artifactId,
      departments: this._selectedDepartment,
      creatorId: userId
    };
    console.log(req)
    this._store.dispatch(new taskAssignment.CreateTaskAssignmentProposalAction(req));

    this.closeModal();
    // }
  }

  rerender(): void {
    if (this.dtElement) {
      if (this.dtElement.dtInstance != undefined) {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          // Destroy the table first
          dtInstance.destroy();
          // Call the dtTrigger to rerender again
          this.dtTrigger.next();
        });
      } else {
        this.dtTrigger.next();
      }
    }
  }

  closeModal() {
    $(function () {
      $('.close').click();
      // $('.modal').modal('hide');
    });
  }

  onCheckboxCheck(e) {
    let departmentId = e.target.value
    console.log(e, departmentId)
    if ($(e.target).prop("checked")) {
      $('#review-artifact-assesment-table .checkbox').each(function () {
        if ($(this).is(':checked') === false) {
          const depIt = $(this).val();
          console.log(depIt, departmentId, (departmentId == depIt) && ($(this).prop('disabled') == false))
          if ((departmentId == depIt) && ($(this).prop('disabled') == false)) {
            $(this).prop('checked', true);
          }
        }
      });
    } else {
      $('#review-artifact-assesment-table .checkbox').each(function () {
        if ($(this).is(':checked') === true) {
          const depIt = $(this).val();
          console.log(depIt, departmentId, (departmentId == depIt) && ($(this).prop('disabled') == false))
          if ((departmentId == depIt) && ($(this).prop('disabled') == false)) {
            $(this).prop('checked', false);
          }
        }
      });
    }
    let check = 0;
    let all_check = true
    $('#review-artifact-assesment-table .table-checkbox').each(function () {
      if ($(this).is(':checked') === true) {
        check++;
      } else {
        if ($(this).prop('disabled') == false) {
          all_check = false
        }
      }
    });



    if (all_check) {
      $(".all_checkbox").prop('checked', true);
    } else {
      $(".all_checkbox").prop('checked', false);
    }

    if (check === 0) {
      $('.btn-present-executive').attr('disabled', 'disabled');
    } else {
      $('.btn-present-executive').attr('disabled', false);
    }
    this.checkReadyToPost()
    // if(all_check){
    //   $('.btn-present-executive').attr('disabled', false);
    // } else {
    //   $('.btn-present-executive').attr('disabled', 'disabled');
    // }

  }
  checkReadyToPost() {
    let check_list = []
    $('#review-artifact-assesment-table .table-checkbox').each(function () {
      if ($(this).prop('checked')) {
        if (check_list.indexOf($(this).val()) == -1) {
          check_list.push($(this).val());
        }
      }
    });
    console.log("check_list", check_list)
    var error = false
    $.each(check_list, function (i, item) {
      $('#review-artifact-assesment-table .table-checkbox').each(function () {
        // console.log($(this).val())
        if (item == $(this).val()) {
          // console.log(item,$(this).is(":checked")) 
          if (($(this).is(":checked") == false) && ($(this).prop('disabled') == false)) {
            console.log(11)
            error = true
          } else {
            if (($(this).is(":checked") == false) && ($(this).attr('status') != 'Confirmed')) {
              if ($(this).attr('status') != 'Ticketed') {
                console.log(22, $(this).attr('status'))
                error = true
              }

            }
          }
        }
      });
      console.log(item, error)
    });

    if (error) {
      $('.btn-present-executive').attr('disabled', 'disabled');
    } else {
      if (check_list.length > 0) { $('.btn-present-executive').attr('disabled', false); }

    }
  }
  onAllCheckboxCheck() {
    if ($(".all_checkbox").is(":checked")) {
      $('#review-artifact-assesment-table .table-checkbox').each(function () {
        if ($(this).prop('disabled') == false) {
          $(this).prop('checked', true);
          $('.btn-present-executive').attr('disabled', false);
        }
      });
    } else {
      $('#review-artifact-assesment-table .table-checkbox').each(function () {
        $(this).prop('checked', false);
        $('.btn-present-executive').attr('disabled', 'disabled');
      });
    }
    this.checkReadyToPost()
  }
  role(role: string): boolean {
    return this.acl.hasRole(role);
  }
  isReadOnly(role_list: Array<string>): boolean {
    let result = false;
    let thisobj = this;
    $.each(role_list, function (i, role) {
      result = result || (thisobj.acl.isReadOnly(role) === false)
    });
    return !result;
  }

}
