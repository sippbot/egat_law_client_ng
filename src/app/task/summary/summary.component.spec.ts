import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskAssignmentSummaryComponent } from './summary.component';

describe('TaskAssignmentSummaryComponent', () => {
  let component: TaskAssignmentSummaryComponent;
  let fixture: ComponentFixture<TaskAssignmentSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaskAssignmentSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskAssignmentSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
