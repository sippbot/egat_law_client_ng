import { ArtifactModule } from "./artifact/artifact.module";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import {
  NavigationEnd,
  NavigationStart,
  Router,
  RouterModule
} from "@angular/router";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { FroalaEditorModule, FroalaViewModule } from "angular-froala-wysiwyg";

import { routing } from "./app.routing";
import { StoreModule } from "@ngrx/store";

import { AppComponent } from "./app.component";
import { TopBarComponent } from "./components/top-bar/top-bar.component";
import { MenuLeftComponent } from "./components/menu-left/menu-left.component";
import { MenuRightComponent } from "./components/menu-right/menu-right.component";
import { FooterComponent } from "./components/footer/footer.component";
import { AlertComponent } from "./components/alert/alert.component";
import { ProfileComponent } from "./components/profile/profile.component";

import { GeneralinfoModule } from "./generalinfo/generalinfo.module";
import { DocumentsModule } from "./documents/documents.module";
import { LawsModule } from "./laws/laws.module";
import { UsersModule } from "./users/users.module";
import { LoginModule } from "./login/login.module";
import { HomeModule } from "./home/home.module";
import { NotificationModule } from "./notification/notification.module";
import { TicketModule } from "./ticket/ticket.module";
import { LicenseModule } from "./license/license.module";
import { TaskModule } from "./task/task.module";
import { EffectsModule } from "@ngrx/effects";
import { Ng2IziToastModule } from "ng2-izitoast"; // notification animation
import { SweetAlert2Module } from "@toverux/ngsweetalert2"; // Sweet-Alert
import { BootstrapModalModule } from "ng2-bootstrap-modal"; // ng2-bootstrap-modal remove this due to it not fully support animation
import { ModalModule } from "ngx-bootstrap/modal";

// services
import { DepartmentService } from "./common/services/department.service";
import { UserService } from "./common/services/user.service";
import { MetadataService } from "./common/services/metadata.service";
import { ArtifactService } from "./common/services/artifact.service";
import { DocumentService } from "./common/services/document.service";
import { LicenseTypeService } from "./common/services/license-type.service";
import { PostService } from "./common/services/post.service";
import { TaskAssignmentService } from "./common/services/task-assignment.service";
import { TicketService } from "./common/services/ticket.service";
// Effect

import { UserPermissionEffects } from "./common/effects/user-permissions.effect";
import { UserGroupEffects } from "./common/effects/usergroups.effect";
import { UserEffects } from "./common/effects/users.effect";
import { DocumentEffects } from "./common/effects/document.effect";
import { DepartmentEffects } from "./common/effects/department.effect";
import { LawlevelEffects } from "./common/effects/lawlevel.effect";
import { CategoryEffects } from "./common/effects/category.effect";
import { ArtifactEffects } from "./common/effects/artifact.effect";
import { LicensetypeEffects } from "./common/effects/license-type.effect";
import { PostEffects } from "./common/effects/post.effect";
import { TaskAssignmentEffects } from "./common/effects/task-assignment.effect";
import { TicketEffects } from "./common/effects/ticket.effect";
import { LicenseEffects } from "./common/effects/license.effect";
import { NotificationEffects } from "./common/effects/notification.effect";
import { reducer } from "./common/reducers/reducer";
import { LicenseService } from "./common/services/license.service";
import { NotificationService } from "./common/services/notification.service";

declare var NProgress: any;

@NgModule({
  declarations: [
    AppComponent,
    TopBarComponent,
    MenuLeftComponent,
    MenuRightComponent,
    FooterComponent,
    AlertComponent,
    ProfileComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    RouterModule,
    FroalaEditorModule,
    FroalaViewModule,

    StoreModule.provideStore(reducer),
    EffectsModule.run(UserPermissionEffects),
    EffectsModule.run(UserGroupEffects),
    EffectsModule.run(UserEffects),
    EffectsModule.run(DocumentEffects),
    EffectsModule.run(DepartmentEffects),
    EffectsModule.run(LawlevelEffects),
    EffectsModule.run(CategoryEffects),
    EffectsModule.run(ArtifactEffects),
    EffectsModule.run(LicensetypeEffects),
    EffectsModule.run(PostEffects),
    EffectsModule.run(TaskAssignmentEffects),
    EffectsModule.run(TicketEffects),
    EffectsModule.run(LicenseEffects),
    EffectsModule.run(NotificationEffects),

    NgbModule.forRoot(),
    SweetAlert2Module.forRoot(),
    routing,

    GeneralinfoModule,
    DocumentsModule,
    LawsModule,
    UsersModule,
    LoginModule,
    HomeModule,
    NotificationModule,
    TicketModule,
    LicenseModule,
    TaskModule,
    ArtifactModule,
    Ng2IziToastModule,
    BootstrapModalModule.forRoot({ container: document.body }),
    // BootstrapModalModule
    ModalModule.forRoot()
  ],
  entryComponents: [ProfileComponent],
  providers: [
    DepartmentService,
    MetadataService,
    ArtifactService,
    UserService,
    DocumentService,
    LicenseTypeService,
    PostService,
    TaskAssignmentService,
    TicketService,
    LicenseService,
    NotificationService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(private router: Router) {
    router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        NProgress.start();
        // console.log("NProgress start");
      }

      if (event instanceof NavigationEnd) {
        setTimeout(function() {
          NProgress.done();
          // console.log("NProgress done or timeout");
        }, 200);
      }
    });
  }
}
