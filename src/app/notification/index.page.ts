import { MailNotification } from '../common/models/mail-notification.model';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import * as reducer from '../common/reducers/reducer';
import * as notification from '../common/actions/notification.action';
import { NotificationEffects } from '../common/effects/notification.effect';

@Component({
  selector: 'cat-page',
  templateUrl: './index.html',
})

export class NotificationComponent implements OnInit {
  notificationEntities: Array<MailNotification>;
  userId: number;
  constructor(private _store: Store<reducer.State>) {
    this.userId = +localStorage.getItem('userId');
    this._store.let(reducer.getNotifications).subscribe(
      notifications => {
        this.notificationEntities = notifications.filter(noti => noti.createDate !== '');
      }
    );
  }

  ngOnInit() {
    this._store.dispatch(new notification.LoadNotificationAction(this.userId));
  }

  actionPath(fullPath: string): string {
    if (fullPath !== null) {
      const fl = fullPath.split('#');
      if (fl.length >= 2) {
        return fl[1];
      }
    }
    return fullPath;
  }
}
