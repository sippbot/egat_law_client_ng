import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { NotificationComponent } from './index.page';
import { AuthGuard } from '../guard/auth.guard';

export const routes: Routes = [
  { path: 'notification', component: NotificationComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    NotificationComponent
  ]
})

export class NotificationModule { }