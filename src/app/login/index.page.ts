import { ShowAlertAction } from '../common/actions/alert.action';
import { Store } from '@ngrx/store';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../common/services/authentication.service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import * as reducer from '../common/reducers/reducer';
import { Alert } from '../common/models/alert.model';

@Component({
  selector: 'cat-page',
  templateUrl: './index.html',
})
export class LoginIndexComponent implements OnInit {
  loginForm: FormGroup;
  model: any = {};
  loading = false;
  error = '';
  returnUrl: string;
  constructor(private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private fb: FormBuilder,
    private _store: Store<reducer.State>) { }
  login() {
    this.loading = true;
    this.authenticationService.login(this.loginForm.get('username').value, this.loginForm.get('password').value)
      .subscribe(result => {
        if (result === true) {
          // login successful
          this.router.navigateByUrl(this.returnUrl);
        } else {
          // login failed
          this._store.dispatch(new ShowAlertAction(new Alert('เลขประจำตัวผู้ใช้หรือรหัสผ่าน ไม่ถูกต้อง', 'โปรดลองใหม่!', 'fail')));

          this.error = 'Username or password is incorrect';
          this.loading = false;
        }
      });
  }
  ngOnInit() {
    this.authenticationService.logout();
    this.loginForm = this.fb.group({
      'username': ['', Validators.required],
      'password': ['', Validators.required]
    });
    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/home';
  }
}
