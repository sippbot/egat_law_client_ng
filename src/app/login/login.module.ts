import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { Routes, RouterModule }  from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthenticationService } from '../common/services/authentication.service';
import { LoginIndexComponent } from './index.page';

export const routes: Routes = [
  { path: 'login/index', component: LoginIndexComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule
  ],
  declarations: [
    LoginIndexComponent
  ],
  providers:[
    AuthenticationService
  ]
})

export class LoginModule { }
