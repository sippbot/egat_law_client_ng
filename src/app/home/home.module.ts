import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { Routes, RouterModule }  from '@angular/router';

import { HomeIndex } from './index.page';
import { AuthGuard } from '../guard/auth.guard';
export const routes: Routes = [
  { path: 'home', component: HomeIndex ,canActivate: [AuthGuard]  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    HomeIndex
  ]
})

export class HomeModule { }
