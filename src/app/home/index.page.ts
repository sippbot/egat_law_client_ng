import { ACL } from '../common/util/acl.util';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'cat-page',
  templateUrl: './index.html',
})

export class HomeIndex implements OnInit {
  // userGroup: string;
  acl: ACL;
  constructor() {
    this.acl = new ACL();
  }
  ngOnInit() {
    // this.userGroup = localStorage.getItem('userGroup');
  }

  role(role: string): boolean {
    return this.acl.hasRole(role);
  }
}
