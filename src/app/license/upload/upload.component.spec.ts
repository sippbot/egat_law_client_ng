import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LicenseUploadComponent } from './upload.component';

describe('LicenseUploadComponent', () => {
  let component: LicenseUploadComponent;
  let fixture: ComponentFixture<LicenseUploadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LicenseUploadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LicenseUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
