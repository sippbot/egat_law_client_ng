import { LicenseEffects } from '../../common/effects/license.effect';
import { LicenseType } from '../../common/models/license-type.model';
import { License } from '../../common/models/license.model';
import { ShowAlertAction } from '../../common/actions/alert.action';
import { ApplicationConstant } from '../../common/application-constant';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Store } from '@ngrx/store';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Document } from '../../common/models/document.model';
import { IMyOptions } from 'mydatepicker-th';
import * as reducer from '../../common/reducers/reducer';
import * as license from '../../common/actions/license.action';
import * as licenseType from '../../common/actions/license-type.action';
import { Actions } from '@ngrx/effects';
import { TaskAssignmentEffects } from '../../common/effects/task-assignment.effect';
import * as taskAssignment from '../../common/actions/task-assignment.action';
import { Alert } from '../../common/models/alert.model';

declare var NProgress: any;

export interface LicenseUploadModel {
  taskAssignmentId: number;
  licenseId: number;
  userId: number;
}

@Component({
  selector: 'app-license-upload',
  templateUrl: './upload.component.html'
})
export class LicenseUploadComponent extends DialogComponent<LicenseUploadModel, boolean> implements LicenseUploadModel, OnInit, OnDestroy {
  taskAssignmentId: number;
  licenseId: number;
  userId: number;
  issueDate: any; // IMyDateModel;
  expireDate: any; // IMyDateModel;
  dueDate: any; // IMyDateModel;
  documents: Document[];
  isHasFileUpload = false;
  isPublic = false;
  private apiEndPoint: string = ApplicationConstant.APP_BASE_ENDPOINT_URL + '/fileupload';
  // private fileEndpoint: string = ApplicationConstant.APP_BASE_ENDPOINT_URL + '/document/';
  count = 0;
  // fileDatas: FileData[];

  fileName: string;
  isReadyToSubmit = false;

  placeholder = 'dd/mm/yyyy';
  myDatePickerOptions: IMyOptions = {
    dateFormat: 'dd/mm/yyyy',
    editableDateField: false,
    openSelectorOnInputClick: true
  };

  licensTypes: Array<LicenseType>;
  selectedLicense: License;
  userGroup: string;

  constructor(public dialogService: DialogService,
    private _store: Store<reducer.State>,
    private http: Http,
    private licenseEffects: LicenseEffects,
    private taskAssignmentEffects: TaskAssignmentEffects) {
    super(dialogService);
    console.log('dialog');
    console.log(this.licenseId);
    this.documents = [];
    this.fileName = '';
    this.licensTypes = new Array<LicenseType>();
    this.selectedLicense = new License();
    this.userGroup = localStorage.getItem('userGroup');
    this._store.let(reducer.getLicenseTypes).subscribe(
      licenseTypeList => {
        if (licenseTypeList.length > 0) {
          this.licensTypes = licenseTypeList;
        }
      }
    );
    this.taskAssignmentEffects.createTaskAttachment$.filter(
      action => action.type === taskAssignment.ActionTypes.CREATE_TASKASSIGNMENT_ATTACHMENT_SUCCESS
    ).subscribe(() => {
      this._store.dispatch(new ShowAlertAction(new Alert('บันทึกไฟล์ของท่านเรียบร้อยแล้ว', 'อัพโหลดสำเร็จ!', 'success')));
      this.closeModal();
    });
    this.licenseEffects.craeteLicense$.filter(
      action => action.type === license.ActionTypes.CREATE_LICENSE_SUCCESS 
    ).subscribe(
      () => {
        this.closeModal();
        if (this.taskAssignmentId != 0) {  
          this._store.dispatch(new taskAssignment.LoadTaskAssignmentFullDetailAction({ taskAssignmentId: this.taskAssignmentId }));
        }
        // this._store.dispatch(new licenseType.LoadLicenseTypeAction({}));
      }
    );

    //todo add logic to load license info if license id exist


  }

  ngOnInit() {
    // Following line is a tricky to enable scrollable modal
    document.getElementsByTagName('body')[0].classList.add('modal-open');
    // Load license type
    this._store.dispatch(new licenseType.LoadLicenseTypeAction({}));
  }
  ngOnDestroy() {
    // Following line is a tricky to enable scrollable modal
    document.getElementsByTagName('body')[0].classList.remove('modal-open');
  }

  fileChange(event) {
    const fileList: FileList = event.target.files;
    this.isHasFileUpload = true;
    for (let i = 0; i < fileList.length; i++) {
      const file: File = fileList[i];
      const formData: FormData = new FormData();
      if (this.fileName === '') {
        this.fileName = file.name;
      }
      formData.append('uploadFile', file, file.name);
      formData.append('ownerId', localStorage.getItem('userId'));
      formData.append('departmentId', localStorage.getItem('departmentId'));
      // let xxsrfToken = localStorage.getItem('xsrfToken');
      const headers = new Headers();
      headers.append('Accept', 'application/json');
      // headers.append('X-XSRF-TOKEN', xxsrfToken);
      const options = new RequestOptions({ headers: headers });
      NProgress.start();
      this.http.post(`${this.apiEndPoint}`, formData, options)
        .map(res => res.json())
        // .catch(error => Observable.throw(error))
        .subscribe(
        data => this.handleFileUploadSuccess(data),
        error => console.log(error.json())
        );
    }
  }

  handleFileUploadSuccess(data: any) {
    NProgress.done();
    this.isReadyToSubmit = true;
    const doc: Document = new Document();
    doc.id = data.id;
    doc.title = data.filename;
    this.documents.push(doc);
  }

  onSubmit() {
    const doc = this.documents.pop();
    this.selectedLicense.documentId = doc.id;
    this.selectedLicense.dueDate = this.dueDate.formatted;
    this.selectedLicense.expireDate = this.expireDate.formatted;
    this.selectedLicense.issueDate = this.issueDate.formatted;
    this.selectedLicense.taskAssignmentId = this.taskAssignmentId;
    this.selectedLicense.userId = this.userId;
    this.selectedLicense.isPublic = this.isPublic;
    if(this.licenseId === undefined){
      this._store.dispatch(new license.CreateLicenseAction(this.selectedLicense));
    }else{
      this.selectedLicense.id = this.licenseId;
      this._store.dispatch(new license.RenewLicenseAction(this.selectedLicense));
    }
  }

  closeModal() {
    $(function () {
      // $('.close').click();
      $(".modal-backdrop").remove();
      $(".modal.fade").remove();
      $("body").removeClass("modal-open")
      // $('.modal').modal('hide');
    });
  }
}

