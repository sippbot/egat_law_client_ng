import { Store } from '@ngrx/store';
import { ApplicationConstant } from '../../common/application-constant';
import { License } from '../../common/models/license.model';
import { Component, OnInit, Input, SimpleChanges, OnChanges } from '@angular/core';
import * as reducer from '../../common/reducers/reducer';
import * as license from '../../common/actions/license.action';
import { isNgTemplate } from '@angular/compiler';
// import { BsModalService } from 'ngx-bootstrap/modal';
// import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
  selector: 'app-license-list',
  templateUrl: './list.component.html'
})
export class ListComponent implements OnInit, OnChanges {
  @Input() status: string;
  @Input() alert: string;
  @Input() licenseType: string;

  licenses: Array<License>;
  licensesFiltered: Array<License>;
  userId: number;
  userGroup: string;

  dtOptions: DataTables.Settings = {
    'pageLength': 30,
    'language': ApplicationConstant.DATATABLE_GLOBAL_LANGUAGE_CONFIG,
    'autoWidth': true,
    'dom': '<\'row\'<\'col-sm-12\'B>>' +
      '<\'row\'<\'col-sm-6\'l><\'col-sm-6\'f>>' +
      '<\'row\'<\'col-sm-12\'tr>>' +
      '<\'row\'<\'col-sm-5\'i><\'col-sm-7\'p>>',
    'buttons': [
      {
        extend: 'excelHtml5',
        text: '<i class="fa fa-file-excel-o fa-x5"></i> ดาวน์โหลด Excel',
        className: 'btn btn-primary btn-sm glyphicon glyphicon-list-alt pull-right',
        exportOptions: {
          columns: [0, 1, 2, 3, 4, 5, 6, 7]
        }
      },
      {
        extend: 'print',
        text: '<i class="fa fa-print fa-x5"></i> พิมพ์รายงาน',
        className: 'btn btn-primary btn-sm btn-print glyphicon glyphicon-print pull-right',
        pageSize: 'A4',
        title: 'รายการเอกสาร',
        exportOptions: {
          columns: [0, 1, 2, 3, 4, 5, 6, 7]
        }
      }
    ]
  };
  constructor(
    // public bsModalRef: BsModalRef,
    private _store: Store<reducer.State>) {
    this.userId = +localStorage.getItem('userId');
    this.userGroup = localStorage.getItem('userGroup');

    this._store.let(reducer.getLicenses).subscribe(licenses => {
      this.licenses = licenses;
      this.licensesFiltered = licenses;
      this.updateDataList();
    });
  }

  ngOnInit() {
    if (this.userGroup === '2' || this.userGroup === '5') {
      this._store.dispatch(new license.LoadLicensesAction(0));
    } else {
      this._store.dispatch(new license.LoadLicensesAction(this.userId));
    }
  }

  ngOnChanges(changeRecord: SimpleChanges) {
    this.updateDataList();
  }

  updateDataList() {
    // case alert all/ status all
    if (this.alert === 'all' && this.status === 'all') {
      if (this.licenseType !== 'all') {
        this.licensesFiltered = this.licenses.filter(item => item.licenseType.category === this.licenseType);
      } else {
        this.licensesFiltered = this.licenses;
      }
      // case alert all/ status specific value
    } else if (this.alert === 'all' && this.status !== 'all') {
      if (this.licenseType !== 'all') {
        this.licensesFiltered = this.licenses.filter(item => item.status === this.status && item.licenseType.category === this.licenseType);
      } else {
        this.licensesFiltered = this.licenses.filter(item => item.status === this.status);
      }
      // case alert specific value/ status all
    } else if (this.alert !== 'all' && this.status === 'all') {
      if (this.licenseType !== undefined && this.licenseType !== 'all') {
        this.licensesFiltered = this.licenses.filter(item => item.alert === this.alert && item.licenseType.category === this.licenseType);
      } else {
        this.licensesFiltered = this.licenses.filter(item => item.alert === this.alert);
      }
      // case alert and status has specific value
    } else if (this.alert !== 'all' && this.status !== 'all') {
      if (this.licenseType !== undefined && this.licenseType !== 'all') {
        this.licensesFiltered = this.licenses.filter(item => item.status === this.status && item.alert === this.alert && item.licenseType.category === this.licenseType);
      } else {
        this.licensesFiltered = this.licenses.filter(item => item.status === this.status && item.alert === this.alert);
      }
    }
    this.licensesFiltered = this.licensesFiltered.filter(item => !item.isOverride);
  }
}
