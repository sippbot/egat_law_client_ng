import { ACL } from '../../common/util/acl.util';
import { Store } from '@ngrx/store';
import * as reducer from '../../common/reducers/reducer';
import { LicenseType } from '../../common/models/license-type.model';
import { AfterViewInit, OnInit, OnDestroy, Component, ViewChild } from '@angular/core';
import { ApplicationConstant } from '../../common/application-constant';
import { DataTableDirective } from 'angular-datatables';
import * as licensetype from '../../common/actions/license-type.action';
declare var $: any;

@Component({
  selector: 'cat-page',
  templateUrl: './typelist.component.html',
})
export class LicenseTypeListComponent implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  licensetypes: Array<LicenseType>;
  /*
      'columnDefs': [
        { 'width': '5%', 'targets': 0 },
        { 'width': '30%', 'targets': 1 },
        { 'width': '10%', 'targets': 2 },
        { 'width': '10%', 'targets': 3 },
        { 'width': '20%', 'targets': 4 },
        { 'width': '15%', 'targets': 5 },
        { 'width': '10%', 'targets': 6 }
      ],
  */
  dtOptions: DataTables.Settings = {
    'autoWidth': true,
    'pageLength': 30,
    'language': ApplicationConstant.DATATABLE_GLOBAL_LANGUAGE_CONFIG,
    'dom': '<\'row\'<\'col-sm-12\'B>>' +
      '<\'row\'<\'col-sm-6\'l><\'col-sm-6\'f>>' +
      '<\'row\'<\'col-sm-12\'tr>>' +
      '<\'row\'<\'col-sm-5\'i><\'col-sm-7\'p>>',
    'buttons': [
      {
        extend: 'excelHtml5',
        text: '<i class="fa fa-file-excel-o fa-x5"></i> ดาวน์โหลด Excel',
        className: 'btn btn-primary btn-sm glyphicon glyphicon-list-alt pull-right',
        exportOptions:{
          columns:[0,1,2,3,4,5]
        }
      },
      {
        extend: 'print',
        text: '<i class="fa fa-print fa-x5"></i> พิมพ์รายงาน',
        className: 'btn btn-primary btn-sm btn-print glyphicon glyphicon-print pull-right',
        pageSize: 'A4',
        title: 'รายการเอกสาร',
        exportOptions:{
          columns:[0,1,2,3,4,5]
        }
      }
    ]
  };
  acl: ACL;

  constructor(private _store: Store<reducer.State>) {
    this.acl = new ACL();
    this.licensetypes = new Array<LicenseType>();
    this._store.let(reducer.getLicenseTypes).subscribe(
      licensetype => this.licensetypes = licensetype
    );
  }
  ngOnInit() {
    this._store.dispatch(new licensetype.LoadLicenseTypeAction({}));
    $('.modal-footer').hide();
    $('.checkbox').change(function () {
      if (this.checked) {
        // Do stuff
        $('.modal-footer').slideToggle();

      } else {
        $('.modal-footer').slideToggle();
      }
    });
  }
  deleteLicenseType(licenseTypeId: number) {
    this._store.dispatch(new licensetype.DeleteLicenseTypeAction(licenseTypeId));
  }
  role(role: string): boolean {
    return this.acl.hasRole(role);
  }
  isReadOnly(role_list: Array<string>): boolean {
    let result = false;
    let thisobj = this;
    $.each(role_list,function(i,role){ 
      result = result||(thisobj.acl.isReadOnly(role)===false)
    });
    return !result;
  }
}
