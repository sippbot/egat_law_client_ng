import { LicenseHistoryComponent } from './history/history.component';
import { SweetAlert2Module } from '@toverux/ngsweetalert2';
import { MyDatePickerTHModule } from 'mydatepicker-th';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { LicenseTypeListComponent } from './typelist/typelist.component';
import { LicenseTypeComponent } from './type/type.component';
import { MetadataService } from '../common/services/metadata.service';
import { UserService } from '../common/services/user.service';
import { AuthGuard } from '../guard/auth.guard';
import { LicenseUploadComponent } from './upload/upload.component';
import { LicenseViewComponent } from './view/view.component';
import { ListComponent } from './list/list.component';

export const routes: Routes = [
  { path: 'license/type/index', component: LicenseTypeListComponent, canActivate: [AuthGuard] },
  { path: 'license/type/new', component: LicenseTypeComponent, canActivate: [AuthGuard] },
  { path: 'license/type/view/:id', component: LicenseTypeComponent, canActivate: [AuthGuard] },
  { path: 'license/type/edit/:id', component: LicenseTypeComponent, canActivate: [AuthGuard] },
  { path: 'license/view', component: LicenseViewComponent, canActivate: [AuthGuard] },
  { path: 'license/view/:id', component: LicenseViewComponent, canActivate: [AuthGuard] },
  { path: 'license/history/:id', component: LicenseHistoryComponent, canActivate: [AuthGuard]},
  { path: 'license/history/:id/:historyId', component: LicenseHistoryComponent, canActivate: [AuthGuard]}
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    DataTablesModule,
    MyDatePickerTHModule,
    SweetAlert2Module
    /* Put the reducer as an argument in provideStore */
  ],
  declarations: [
    LicenseTypeListComponent,
    LicenseTypeComponent,
    LicenseUploadComponent,
    LicenseViewComponent,
    ListComponent,
    LicenseHistoryComponent
  ],
  entryComponents: [
    LicenseUploadComponent
  ],
  exports: [
    LicenseUploadComponent
  ],
  providers: [MetadataService, UserService]
})

export class LicenseModule { }
