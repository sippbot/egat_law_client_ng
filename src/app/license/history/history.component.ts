import { Store } from '@ngrx/store';
import { ApplicationConstant } from '../../common/application-constant';
import { License } from '../../common/models/license.model';
import { Component, OnInit, SimpleChanges, OnChanges } from '@angular/core';
import * as reducer from '../../common/reducers/reducer';
import * as license from '../../common/actions/license.action';
import { DialogService } from 'ng2-bootstrap-modal';
import { LicenseUploadComponent } from '../upload/upload.component';
import { ActivatedRoute } from '../../../../node_modules/@angular/router';
import { ACL } from '../../common/util/acl.util';

@Component({
  selector: 'app-license-history',
  templateUrl: './history.component.html'
})
export class LicenseHistoryComponent implements OnInit, OnChanges {
  historyId: number;
  licenseId: number;
  licenses: Array<License>;
  licensesFiltered: Array<License>;
  userId: number;
  userGroup: string;
  acl: ACL;

  dtOptions: DataTables.Settings = {
    'pageLength': 30,
    'language': ApplicationConstant.DATATABLE_GLOBAL_LANGUAGE_CONFIG,
    'autoWidth': true,
    'dom': '<\'row\'<\'col-sm-12\'B>>' +
      '<\'row\'<\'col-sm-6\'l><\'col-sm-6\'f>>' +
      '<\'row\'<\'col-sm-12\'tr>>' +
      '<\'row\'<\'col-sm-5\'i><\'col-sm-7\'p>>',
    'buttons': [
      {
        extend: 'excelHtml5',
        text: '<i class="fa fa-file-excel-o fa-x5"></i> ดาวน์โหลด Excel',
        className: 'btn btn-primary btn-sm glyphicon glyphicon-list-alt pull-right',
        exportOptions: {
          columns: [0, 1, 2, 3, 4, 5, 6, 7]
        }
      },
      {
        extend: 'print',
        text: '<i class="fa fa-print fa-x5"></i> พิมพ์รายงาน',
        className: 'btn btn-primary btn-sm btn-print glyphicon glyphicon-print pull-right',
        pageSize: 'A4',
        title: 'รายการเอกสาร',
        exportOptions: {
          columns: [0, 1, 2, 3, 4, 5, 6, 7]
        }
      }
    ]
  };
  constructor(
    // public bsModalRef: BsModalRef,
    private dialogService: DialogService,
    private _store: Store<reducer.State>,
    private _route: ActivatedRoute) {
    this.acl = new ACL();
    this._route.params.subscribe(params => {
      this.licenseId = +params['id'];
      this.historyId = +params['historyId'];
    });
    this.userId = +localStorage.getItem('userId');
    this.userGroup = localStorage.getItem('userGroup');

    this._store.let(reducer.getLicenses).subscribe(licenses => {
      this.licenses = licenses;
      this.licensesFiltered = licenses;
      this.updateDataList();
    });
  }

  ngOnInit() {
    if (this.userGroup === '2' || this.userGroup === '5') {
      this._store.dispatch(new license.LoadLicensesAction(0));
    } else {
      this._store.dispatch(new license.LoadLicensesAction(this.userId));
    }
  }

  ngOnChanges(changeRecord: SimpleChanges) {
    this.updateDataList();
  }

  updateDataList() {
    if(this.historyId){
      this.licensesFiltered = this.licenses.filter(item => item.licenseHistoryId === this.historyId);
    }else{
      this.licensesFiltered = this.licenses.filter(item => item.id === this.licenseId);
    }
  }

  renewLicense() {
    console.log(this.licenseId);
    const disposable = this.dialogService.addDialog(LicenseUploadComponent, {
      taskAssignmentId: 0,
      licenseId: this.licenseId,
      userId: this.userId
    }).subscribe((isSuccess) => {
      // We get dialog result
      if (isSuccess) {
        // alert('success');
      } else {
        // alert('declined');
      }
    });
  }

  role(role: string): boolean {
    return this.acl.hasRole(role);
  }
  
  isReadOnly(role_list: Array<string>): boolean {
    let result = false;
    let thisobj = this;
    $.each(role_list, function (i, role) {
      result = result || (thisobj.acl.isReadOnly(role) === false)
    });
    return !result;
  }
}
