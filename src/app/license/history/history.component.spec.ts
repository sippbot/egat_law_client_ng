import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LicenseHistoryComponent } from './history.component';

describe('ListComponent', () => {
  let component: LicenseHistoryComponent;
  let fixture: ComponentFixture<LicenseHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LicenseHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LicenseHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
