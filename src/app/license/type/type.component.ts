import { ShowAlertAction } from '../../common/actions/alert.action';
import { LicensetypeEffects } from '../../common/effects/license-type.effect';
import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { LicenseType } from '../../common/models/license-type.model';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { ActivatedRoute } from '@angular/router';
import * as licensetype from '../../common/actions/license-type.action';
import * as reducer from '../../common/reducers/reducer';
import { Alert } from '../../common/models/alert.model';

declare var $: any;
declare var jQuery: any;
@Component({
  selector: 'cat-page',
  templateUrl: './type.component.html'
})

export class LicenseTypeComponent implements OnInit {
  departmentNewForm: FormGroup;
  private licenseId: number;
  licenseType: LicenseType;
  mode: string;

  constructor(private _route: ActivatedRoute,
    private _store: Store<reducer.State>,
    private router: Router,
    private licenseTypeEffects: LicensetypeEffects) {
    this._store.let(reducer.getSelectedLicenseType).subscribe(
      licenseType => {
        if (licenseType != null) {
          this.licenseType = licenseType;
        }
      });
    this._route.params.subscribe(params => {
      if (params != null) {
        this.licenseId = +params['id'];
        this._store.dispatch(new licensetype.SelectLicenseTypeAction(this.licenseId));
      }
    });
    if (this.router.url.indexOf('/license/type/edit') >= 0) {
      this.mode = 'EDIT';
    } else if (this.router.url.indexOf('/license/type/new') >= 0) {
      this.mode = 'ADD';
      this.licenseType = new LicenseType();
    } else {
      this.mode = 'VIEW';
    }

    this.licenseTypeEffects.craeteLicensetype$.filter(
      action => action.type === licensetype.ActionTypes.CREATE_LICENSETYPE_SUCCESS
    ).subscribe(() => this.redirectToIndex());

    this.licenseTypeEffects.craeteLicensetype$.filter(
      action => action.type === licensetype.ActionTypes.CREATE_LICENSETYPE_FAIL
    ).subscribe(() => this._store.dispatch(new ShowAlertAction(
      new Alert('ไม่สามารถบันทึกใบอนุญาตที่กฎหมายกำหนดได้ โปรดลองใหม่อีกครั้ง', 'ล้มเหลว!', 'fail'))));

    this.licenseTypeEffects.updateLicensetype$.filter(
      action => action.type === licensetype.ActionTypes.UPDATE_LICENSETYPE_SUCCESS
    ).subscribe(() => this.redirectToIndex());

    this.licenseTypeEffects.updateLicensetype$.filter(
      action => action.type === licensetype.ActionTypes.UPDATE_LICENSETYPE_FAIL
    ).subscribe(() => this._store.dispatch(new ShowAlertAction(
      new Alert('ไม่สามารถบันทึกการแก้ไขใบอนุญาตที่กฎหมายกำหนดได้ โปรดลองใหม่อีกครั้ง', 'ล้มเหลว!', 'fail'))));

  }
  ngOnInit() {
  }

  setupSelect() {
    jQuery('.field_license-type').val(this.licenseType.category).trigger('change');
  }

  get currentLicenseType() { return JSON.stringify(this.licenseType); }

  addLicenseType() {
    if (this.mode === 'ADD') {
      // Edit mode
      this._store.dispatch(new licensetype.CreateLicenseTypeAction(this.licenseType));

    } else {
      this._store.dispatch(new licensetype.UpdateLicenseTypeAction(this.licenseType));
    }
  }

  redirectToIndex(): void {
    this.router.navigate(['/license/type/index']);
  }
}
