import { ACL } from '../../common/util/acl.util';
import { DataTableDirective } from 'angular-datatables';
import { ApplicationConstant } from '../../common/application-constant';
import { Store } from '@ngrx/store';
import { License } from '../../common/models/license.model';
import { DialogService } from 'ng2-bootstrap-modal';
import { LicenseUploadComponent } from '../upload/upload.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import * as reducer from '../../common/reducers/reducer';
declare var $: any;

@Component({
  selector: 'cat-page',
  templateUrl: './view.component.html'
})
export class LicenseViewComponent implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  userId: number;
  userGroup: string;
  licenses: Array<License>;
  licensesExpired: Array<License>;
  licensesAlerted: Array<License>;
  acl: ACL;
  /*
  'columnDefs': [
      { 'width': '20%', 'targets': 0 },
      { 'width': '10%', 'targets': 1 },
      { 'width': '10%', 'targets': 2 },
      { 'width': '10%', 'targets': 3 },
      { 'width': '10%', 'targets': 4 },
      { 'width': '10%', 'targets': 5 },
      { 'width': '15%', 'targets': 6 },
      { 'width': '10%', 'targets': 7 },
      { 'width': '5%', 'targets': 8 }
    ],
  */
  dtOptions: DataTables.Settings = {
    'pageLength': 30,
    'language': ApplicationConstant.DATATABLE_GLOBAL_LANGUAGE_CONFIG,
    'autoWidth': true,
    'dom': '<\'row\'<\'col-sm-12\'B>>' +
      '<\'row\'<\'col-sm-6\'l><\'col-sm-6\'f>>' +
      '<\'row\'<\'col-sm-12\'tr>>' +
      '<\'row\'<\'col-sm-5\'i><\'col-sm-7\'p>>',
    'buttons': [
      {
        extend: 'excelHtml5',
        text: '<i class="fa fa-file-excel-o fa-x5"></i> ดาวน์โหลด Excel',
        className: 'btn btn-primary btn-sm glyphicon glyphicon-list-alt pull-right'
      },
      {
        extend: 'print',
        text: '<i class="fa fa-print fa-x5"></i> พิมพ์รายงาน',
        className: 'btn btn-primary btn-sm btn-print glyphicon glyphicon-print pull-right',
        pageSize: 'A4',
        title: 'รายการเอกสาร'
      }
    ]
  };

  constructor(private _store: Store<reducer.State>,
    private dialogService: DialogService) {
    this.acl = new ACL();
    this.userId = +localStorage.getItem('userId');
  }
  ngOnInit() {

  }

  uploadLicense() {
    const disposable = this.dialogService.addDialog(LicenseUploadComponent, {
      taskAssignmentId: 0,
      userId: this.userId
    }).subscribe((isSuccess) => {
      // We get dialog result
      if (isSuccess) {
        // alert('success');
      } else {
        // alert('declined');
      }
    });
  }
  role(role: string): boolean {
    return this.acl.hasRole(role);
  }
  isReadOnly(role_list: Array<string>): boolean {
    let result = false;
    let thisobj = this;
    $.each(role_list,function(i,role){ 
      result = result||(thisobj.acl.isReadOnly(role)===false)
    });
    return !result;
  }
}
