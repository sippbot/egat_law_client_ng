import { TaskAssessment } from './task-assessment.model';
import { TrackingPlan } from './tracking-plan.model';
/*
* Is schedule task
*/
export class Task {
  id: number;
  trackingId: number;
  departmentName: string;
  departmentId: string;
  dueDate: Date;
  status: string;
  status_text: string;
  assessment: TaskAssessment;
  tracking: TrackingPlan;
}
