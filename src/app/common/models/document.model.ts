export class Document {
  id: number;
  title: string;
  type: string;
  fileId: number;
  url: string;
  createDate: string;
  owner: string;
}
