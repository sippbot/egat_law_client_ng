export class TaskAssignmentSummary{
    departmentId: number;
    department: string;
    numberOfTotalTask: number;
    numberOfInprogressTask: number;
    isApplicable: boolean;
    isCompliant: boolean;
    isApplicableText: string;
    isCompliantText: string;
    status: string;
    summaryResult: string;
    taskGroupId: number;
}
