import { Document } from './document.model';
export class TaskApproval {
  approverId: number;
  artifactId:number;
  documents: Array<Document>;
  signature: string;

}
