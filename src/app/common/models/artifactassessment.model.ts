export class ArtifactReviewAssessment {
  assignmentId: number;
  artifactReviewId: number;
  artifactId: number;
  artifactTitle: string;
  departmentId: number;
  departmentName: string;
  status: string;
  status_text: string;
}
