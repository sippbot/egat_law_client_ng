export class Post {
    id: number;
    postName: string;
    isPrimary: boolean;
    departmentId: number;
    userId: number;
    user: string;
    isActive: boolean;
}

