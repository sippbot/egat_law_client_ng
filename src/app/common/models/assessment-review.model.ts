export class AssessmentReview {
    taskAssignmentId: number;
    taskAssessmentId: number;
    reviewerId: number;
    isConfirm: boolean;
    reviewComment: string;
}
