export class Alert {
  message: string;
  status: string;
  title: string;

  constructor(message: string, title: string, status: string) {
    this.message = message;
    this.title = title;
    this.status = status;
  }
}
