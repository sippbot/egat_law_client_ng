export class AssessmentInfo {
    taskAssignmentId: number;
    departmentId: number;
    creatorId: number;
    isApplicable: boolean;
    isCompliant: boolean;
    detail: string;
}
