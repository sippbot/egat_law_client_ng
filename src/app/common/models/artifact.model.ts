import { Category } from './category.model';
import { Lawlevel } from './lawlevel.model';
import { ArtifactNode } from './artifact-node.model';
import { Document } from './document.model';
import { ArtifactNodeGroup } from './artifact-node-group.model';
export class Artifact {
  id: number;
  type: number;
  typeText: string;
  code: string;
  category: Category;
  categoryId: number;
  level: Lawlevel;
  levelId: number;
  title: string;
  status: string;
  createDate: string;
  publishedDate: string;
  effectiveDate: string;
  approvedDate: string;
  lastModified?: string;
  note: string;
  isActive:boolean;
  introduction: string;
  source: string;
  documents: Document[];
  nodes: ArtifactNode[];
  nodeGroups: ArtifactNodeGroup[];

  constructor() {
    this.nodeGroups = new Array<ArtifactNodeGroup>();
    this.documents = new Array<Document>();
  }
}

export const enum ArtifactStatus {
  Draft,
  Created,
  Inprogress,
  Approved,
  Canceled
}
