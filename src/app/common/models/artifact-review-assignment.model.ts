export class ArtifactReviewAssignment {
  artifactId: number;
  duedate: Date;
  departmentIds: number[];
}
