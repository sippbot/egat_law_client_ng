export class ArtifactSummary {
  id: string;
  code: string;
  category: string;
  level: string;
  title: string;
  status: string;
  statusText: string;
  taskAssignmentStatus: string;
  taskAssignmentType: string;
  publishedDate: string;
  effectiveDate: string;
  dueDate:string;
}
