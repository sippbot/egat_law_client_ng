import { TaskAssessmentReview } from './task-assessment-review.model';
import { TaskAssessmentHistory } from './task-assessment-history.model';
export class TaskAssessment {
  id: number;
  taskAssignmentId: number;
  isApplicable: boolean;
  isCompliant: boolean;
  isApplicableText: string;
  isCompliantText: string;
  detail: string;
  reviewComment: string;
  status: string;
  statusText: string;
  createdDate: Date;
  reviewDate: Date;
  creator: string;
  reviewer: string;
}

