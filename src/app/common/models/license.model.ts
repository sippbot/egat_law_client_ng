import { User } from './user.model';
import { Document } from './document.model';
import { LicenseType } from './license-type.model';

export class License {
    id: number;
    licenseNumber: string;
    holderName: string;
    issueDate: string;
    expireDate: string;
    dueDate: string;
    status: string;
    alert: string;

    licenseTypeId: number;
    licenseType: LicenseType;

    documentId: number;
    document: Document;

    taskAssignmentId: number;
    userId: number;

    isPublic: boolean;
    isOverride: boolean;
    licenseHistoryId: number;
}
