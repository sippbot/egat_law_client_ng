import { Post } from './post.model';

export class Department {
  id: number;
  name: string;
  aliasName: string;
  email: string;
  posts: Array<Post>;
  isActive: boolean;
}

