export class Category {
  id: number;
  name: string;
  aliasName: string;
  externalId: number;
}
