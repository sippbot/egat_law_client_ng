import { Ticket } from './ticket.model';
import { ArtifactNode } from './artifact-node.model';
import { Artifact } from './artifact.model';
import { TaskAssessment } from './task-assessment.model';
import { TaskComment } from './task-assignment-comment.model';
import { Document } from './document.model';
export class TaskAssignment {
  id: number;
  departmentId: number;
  artifactId:number;
  assignDate: string;
  dueDate: string;
  type: string;
  status: string;
  statusText: string;
  artifactNode: any;
  artifactNodeId: number;
  isApplicable: boolean;
  isCompliant: boolean;
  isApplicableText: string;
  isCompliantText: string;
  department: string;
  assessments: Array<TaskAssessment>;
  comments: Array<TaskComment>;
  artifact: any;
  documents: Array<Document>;
  description: string;
  progress: number;
  title: string;
  group: string;
  tickets: Array<Ticket>;

  constructor() {
    this.assessments = new Array<TaskAssessment>();
    this.comments = new Array<TaskComment>();
    this.documents = new Array<Document>();
    this.artifact = new Artifact();
    this.artifactNode = new ArtifactNode();
    this.tickets = new Array<Ticket>();
  }
}
