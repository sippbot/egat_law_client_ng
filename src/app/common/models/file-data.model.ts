export class FileData {
  id: number;
  title: string;
  createDate: string;
  authorId: number;
  href: string;
  type: string;
  extension: string;
  size: number;
  owner: string;
}
