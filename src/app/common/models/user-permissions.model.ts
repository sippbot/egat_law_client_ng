import { Role } from './role.model';
export class UserPermission {
    id: number;
    name: string;
    isSetPermission: boolean;
    roles:Array<Role>
  }
  