export class User {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  isActive: boolean;
  post: string;
  groupName: string;
  groupId: number;
  group: string;
}
