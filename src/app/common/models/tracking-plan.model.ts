export class TrackingPlan {
  id?: number;
  artifactId: number;
  artifactTitle?: string;
  artifactCode?: number;
  effectiveDate: Date;
  nextDueDate?: Date;
  content?: string;
  departmentIds: number[];
  retentionPeriod: number;
  status?: string;
  status_text?: string;
}
