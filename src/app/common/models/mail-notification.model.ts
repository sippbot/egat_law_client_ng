export class MailNotification {
    id: number;
    recipientId: number;
    recipientName: string;
    recipient: string;
    subject: string;
    message: string;
    action: string;
    buttonLabel: string;
    buttonLink: string;
    excerpt: string;
    createDate: string;
    sendDate: string;
}
