export class Role {
    id: number;
    role: number;
    groupId: number;
    key: string;
    isReadOnly: boolean;
  }
  