export class Ticket {
    id: number;
    title: string;
    description: string;
    iso: string;
    createDate: string;
    closeDate: string;
    status: string;
    statusText: string;
    ticketExternalId: number;
    taskAssignmentId: number;
    departmentId: number;
    department: string;
    ownerId: number;
    owner: string;
    closeById: number;
    closeBy: string;
}
