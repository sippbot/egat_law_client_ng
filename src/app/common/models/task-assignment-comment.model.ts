export class TaskComment {
    id: number;
    createDate: string;
    content: string;
    creatorId: number;
    creator: string;
    taskAssignmentId: number;
}
