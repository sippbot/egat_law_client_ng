import { LicenseType } from './license-type.model';
import { Department } from './department.model';
export class ArtifactNode {
  id: number;
  seq: number;
  content: string;
  description: string;
  followUpPeriodMonth: number;
  followUpPeriodYear: number;
  isRequiredLicense: boolean;
  isAllDepartment: boolean;
  requiredLicenses: LicenseType[];
  departments: Department[];
  artifactId: number;
  groupId: number;
  title: string;
  order: number;

  isEditMode: boolean;

  constructor() {
    this.isEditMode = false;
  }
}
