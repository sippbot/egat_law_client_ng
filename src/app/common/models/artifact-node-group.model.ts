import { ArtifactNode } from './artifact-node.model';
export class ArtifactNodeGroup {
  id: number;
  content: string;
  seq: number;
  nodes: ArtifactNode[];
  artifactId: number;

  isEditMode: boolean;

  constructor() {
    this.isEditMode = false;
    this.nodes = new Array<ArtifactNode>();
  }
}
