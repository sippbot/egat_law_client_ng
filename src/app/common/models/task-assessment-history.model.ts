export class TaskAssessmentHistory {
  id: number;
  assessmentId: number;
  detail: string;
  creator: string;
  createDate: Date;
}
