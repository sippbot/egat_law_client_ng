export class LicenseType {
  id: number;
  category: string;
  categoryText: string;
  name: string;
  aliasName: string;
  issuer: string;
  frequencyMonth: number;
  frequencyYear: number;
}
