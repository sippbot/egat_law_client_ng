import {Action} from '@ngrx/store';
import {Department} from '../models/department.model';

export const ActionTypes = {
    REMOVE_DEPARTMENT: 'Remove an Department',

    LOAD_DEPARTMENT: 'Load an Department',
    LOAD_DEPARTMENT_SUCCESS: 'Load an Department success',
    LOAD_DEPARTMENT_FAIL: 'Load an Department fail',

    LOAD_DEPARTMENTS: 'Load an Departments',
    LOAD_DEPARTMENTS_SUCCESS: 'Load an Departments success',
    LOAD_DEPARTMENTS_FAIL: 'Load an Departments fail',

    LOAD_ACTIVE_DEPARTMENTS: 'Load an active Departments',
    LOAD_ACTIVE_DEPARTMENTS_SUCCESS: 'Load an active Departments success',
    LOAD_ACTIVE_DEPARTMENTS_FAIL: 'Load an active Departments fail',

    SELECT_DEPARTMENT: 'Select a Department',

    SELECT_OR_LOAD_DEPARTMENT: 'Select or load a Department',

    UPDATE_DEPARTMENT: 'Update a Department',
    UPDATE_DEPARTMENT_SUCCESS: 'Update a Department success',
    UPDATE_DEPARTMENT_FAIL: 'Update a Department fail',

    CREATE_DEPARTMENT: 'Craete a Department',
    CREATE_DEPARTMENT_SUCCESS: 'Craete a Department success',
    CREATE_DEPARTMENT_FAIL: 'Craete a Department fail',

    DELETE_DEPARTMENT: 'Delete a Department',
    DELETE_DEPARTMENT_SUCCESS: 'Delete a Department success',
    DELETE_DEPARTMENT_FAIL: 'Delete a Department fail'
};


export class RemoveDepartmentAction implements Action {
    type = ActionTypes.REMOVE_DEPARTMENT;

    constructor(public payload: Department) {
    }
}

export class SelectOrLoadDepartmentAction implements Action {
    type = ActionTypes.SELECT_OR_LOAD_DEPARTMENT;
    constructor(public payload: any) { }
  }

  export class LoadDepartmentAction implements Action {
    type = ActionTypes.LOAD_DEPARTMENT;

    constructor(public payload: number) {
    }
}

export class LoadDepartmentSuccessAction implements Action {
    type = ActionTypes.LOAD_DEPARTMENT_SUCCESS;

    constructor(public payload: Department) {
    }
}

export class LoadDepartmentFailAction implements Action {
    type = ActionTypes.LOAD_DEPARTMENT_FAIL;

    constructor(public payload: any) {
    }
}

export class LoadDepartmentsAction implements Action {
    type = ActionTypes.LOAD_DEPARTMENTS;

    constructor(public payload: any) {
    }
}

export class LoadDepartmentsSuccessAction implements Action {
    type = ActionTypes.LOAD_DEPARTMENTS_SUCCESS;

    constructor(public payload: Array<Department>) {
    }
}

export class LoadDepartmentsFailAction implements Action {
    type = ActionTypes.LOAD_DEPARTMENTS_FAIL;

    constructor(public payload: any) {
    }
}

export class LoadActiveDepartmentsAction implements Action {
    type = ActionTypes.LOAD_ACTIVE_DEPARTMENTS;

    constructor(public payload: any) {
    }
}

export class LoadActiveDepartmentsSuccessAction implements Action {
    type = ActionTypes.LOAD_ACTIVE_DEPARTMENTS_SUCCESS;

    constructor(public payload: Array<Department>) {
    }
}

export class LoadActiveDepartmentsFailAction implements Action {
    type = ActionTypes.LOAD_ACTIVE_DEPARTMENTS_FAIL;

    constructor(public payload: any) {
    }
}

export class SelectDepartmentAction implements Action {
    type = ActionTypes.SELECT_DEPARTMENT;

    constructor(public payload: number) {
    } // payload is id, so type is number
}

export class UpdateDepartmentAction implements Action {
    type = ActionTypes.UPDATE_DEPARTMENT;

    constructor(public payload: Department) {
    }
}

export class UpdateDepartmentSuccessAction implements Action {
    type = ActionTypes.UPDATE_DEPARTMENT_SUCCESS;

    constructor(public payload: Department) {
    }
}

export class UpdateDepartmentFailAction implements Action {
    type = ActionTypes.UPDATE_DEPARTMENT_FAIL;

    constructor(public payload: any) {
    }
}

export class CreateDepartmentAction implements Action {
    type = ActionTypes.CREATE_DEPARTMENT;

    constructor(public payload: Department) {
    }
}

export class CreateDepartmentSuccessAction implements Action {
    type = ActionTypes.CREATE_DEPARTMENT_SUCCESS;

    constructor(public payload: Department) {
    }
}

export class CreateDepartmentFailAction implements Action {
    type = ActionTypes.CREATE_DEPARTMENT_FAIL;

    constructor(public payload: any) {
    }
}

export class DeleteDepartmentAction implements Action {
    type = ActionTypes.DELETE_DEPARTMENT;

    constructor(public payload: number) {
    }
}

export class DeleteDepartmentSuccessAction implements Action {
    type = ActionTypes.DELETE_DEPARTMENT_SUCCESS;

    constructor(public payload: number) {
    }
}

export class DeleteDepartmentFailAction implements Action {
    type = ActionTypes.DELETE_DEPARTMENT_FAIL;

    constructor(public payload: any) {
    }
}


export type Actions =
    RemoveDepartmentAction |
    LoadDepartmentAction |
    LoadDepartmentSuccessAction |
    LoadDepartmentFailAction |
    LoadDepartmentsAction |
    LoadDepartmentsSuccessAction |
    LoadDepartmentsFailAction |
    LoadActiveDepartmentsAction |
    LoadActiveDepartmentsSuccessAction |
    LoadActiveDepartmentsFailAction |
    SelectDepartmentAction |
    SelectOrLoadDepartmentAction |
    UpdateDepartmentAction |
    UpdateDepartmentSuccessAction |
    UpdateDepartmentFailAction |
    CreateDepartmentAction |
    CreateDepartmentSuccessAction |
    CreateDepartmentFailAction |
    DeleteDepartmentAction |
    DeleteDepartmentSuccessAction |
    DeleteDepartmentFailAction ;
