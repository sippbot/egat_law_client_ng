import {Action} from '@ngrx/store';
import { UserGroup } from '../models/usergroup.model';

export const ActionTypes = {
  ADD_USERGROUP: 'Add an user group',
  REMOVE_USERGROUP: 'Remove an user group',
  LOAD_USERGROUPS: 'Load an user group',
  LOAD_USERGROUPS_SUCCESS: 'Load an user group success',
  LOAD_USERGROUPS_FAIL: 'Load an user group fail'
};

export class AddUserGroupAction implements Action {
  type = ActionTypes.ADD_USERGROUP;
  constructor(public payload: UserGroup) { }
}

export class RemoveUserGroupAction implements Action {
  type = ActionTypes.REMOVE_USERGROUP;
  constructor(public payload: UserGroup) { }
}

export class LoadUserGroupAction implements Action {
  type = ActionTypes.LOAD_USERGROUPS;
  constructor(public payload: any) { }
}

export class LoadUserGroupSuccessAction implements Action {
  type = ActionTypes.LOAD_USERGROUPS_SUCCESS;
  constructor(public payload: Array<UserGroup>) { }
}

export class LoadUserGroupFailAction implements Action {
  type = ActionTypes.LOAD_USERGROUPS_FAIL;
  constructor(public payload: any) { }
}

export type Actions = AddUserGroupAction |
  RemoveUserGroupAction |
  LoadUserGroupAction |
  LoadUserGroupSuccessAction |
  LoadUserGroupFailAction
