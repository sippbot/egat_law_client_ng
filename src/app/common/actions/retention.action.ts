import { Action } from '@ngrx/store';
import { Retention } from '../models/retention.model';

export const ActionTypes = {
  LOAD_RETENTION_ARTIFACT_NEW: 'Load a Retention Artifact New',
  LOAD_RETENTION_ARTIFACT_NEW_SUCCESS: 'Load a Retention Artifact New success',
  LOAD_RETENTION_ARTIFACT_NEW_FAIL: 'Load a Retention Artifact New fail',

  LOAD_RETENTION_ARTIFACT_SUCCESS: 'Load a Retention Artifact  success',
  LOAD_RETENTION_ARTIFACT_FAIL: 'Load a Retention Artifact  fail',

  LOAD_RETENTION_ARTIFACT_FOLLOWUP: 'Load a Retention Artifact Follow Up',
  LOAD_RETENTION_ARTIFACT_FOLLOWUP_SUCCESS: 'Load a Retention Artifact Follow Up success',
  LOAD_RETENTION_ARTIFACT_FOLLOWUP_FAIL: 'Load a Retention Artifact Follow Up fail',

  SELECT_RETENTION_ARTIFACT: 'Select Retention Artifact',

  CREATE_TRACKING_PLAN: 'Craete a tracking plan',
  CREATE_TRACKING_PLAN_SUCCESS: 'Craete a tracking plan success',
  CREATE_TRACKING_PLAN_FAIL: 'Craete a tracking plan fail',

  LOAD_TRACKING_PLAN: 'Load a tracking plan',
  LOAD_TRACKING_PLAN_SUCCESS: 'Load  a tracking plan success',
  LOAD_TRACKING_PLAN_FAIL: 'Load a tracking plan fail',

  LOAD_ALL_TASK_DUE: 'Load a All task due',
  LOAD_ALL_TASK_DUE_SUCCESS: 'Load a All task due success',
  LOAD_ALL_TASK_DUE_FAIL: 'Load a All task due fail',

  LOAD_ALL_TASK_OVER_DUE: 'Load a All task over due',
  LOAD_ALL_TASK_OVER_DUE_SUCCESS: 'Load a All task over due success',
  LOAD_ALL_TASK_OVER_DUE_FAIL: 'Load a All task over due fail',

  SELECT_TASK: 'Select Task',
  LOAD_SELECT_TASK_SUCCESS: 'Load Select Task Succsess',
  LOAD_SELECT_TASK_FAIL: 'Load Select Task Fail',

  LOAD_DEPARTMENT_PLAN: 'Load Department plan',
  LOAD_DEPARTMENT_PLAN_SUCCESS: 'Load Department plan Success',
  LOAD_DEPARTMENT_PLAN_FAIL: 'Load Department plan fail',

  SELECT_TRACKING_PLAN: 'Select Tracking plan',
  LOAD_SELECT_TRACKING_PLAN_SUCCESS: 'Load Select Tracking plan Succsess',
  LOAD_SELECT_TRACKING_PLAN_FAIL: 'Load Select Tracking plan Fail',

  CREATE_CHECK_EVALUATE: 'Craete a check evaluate',
  CREATE_CHECK_EVALUATE_SUCCESS: 'Craete a check evaluate success',
  CREATE_CHECK_EVALUATE_FAIL: 'Craete a check evaluate fail',

  CREATE_ARTIFACTREVIEWKNOWLEDGE: 'Craete a retention review knowledge',
  CREATE_ARTIFACTREVIEWKNOWLEDGE_SUCCESS: 'Craete a retention review knowledge success',
  CREATE_ARTIFACTREVIEWKNOWLEDGE_FAIL: 'Craete a retention review knowledge fail',

  CREATE_APPROVEARTIFACTASSIGNMENT: 'Craete  a approve retention assignment',
  CREATE_APPROVEARTIFACTASSIGNMENT_SUCCESS: 'Craete  a retention artifact assignment success',
  CREATE_APPROVEARTIFACTASSIGNMENT_FAIL: 'Craete a approve retention assignment fail',

  SUBMIT_APPROVE_RETENTION: 'Submit retention approve',
  SUBMIT_APPROVE_RETENTION_SUCCESS: 'Submit retention approve success',
  SUBMIT_APPROVE_RETENTION_FAIL: 'Submit retention approve fail',

  SELECT_PLAN_ASSESSMENT: 'Select plan assessment',
  LOAD_SELECT_PLAN_ASSESSMENT_SUCCESS: 'Load plan assessment Succsess',
  LOAD_SELECT_PLAN_ASSESSMENT_FAIL: 'Load plan assessment Fail',

  SELECT_SUB_TASK_ASSESSMENT: 'Select sub task plan assessment',
  LOAD_SELECT_SUB_TASK_ASSESSMENT_SUCCESS: 'Load sub task plan assessment Succsess',
  LOAD_SELECT_SUB_TASK_ASSESSMENT_FAIL: 'Load sub task plan assessment Fail',

  LOAD_RETENTION_RESULT: 'Load retention result',
  LOAD_RETENTION_RESULT_SUCCESS: 'Load retention result Success',
  LOAD_RETENTION_RESULT_FAIL: 'Load retention result fail',

  SELECT_APPROVE_RETENTION_ASSESSMENT: 'Select Approve Retention assessment',
  LOAD_APPROVE_RETENTION_ASSESSMENT_SUCCESS: 'Load Approve Retention assessment Succsess',
  LOAD_APPROVE_RETENTION_ASSESSMENT_FAIL: 'Load Approve Retention assessmentFail',


};

/**** LOAD LOAD_RETENTION_ARTIFACT_NEW *****/

export class LoadRetentionArtifactNewAction implements Action {
  type = ActionTypes.LOAD_RETENTION_ARTIFACT_NEW;
  constructor(public payload: any) { }
}

export class LoadRetentionArtifactNewSuccessAction implements Action {
  type = ActionTypes.LOAD_RETENTION_ARTIFACT_NEW_SUCCESS;
  constructor(public payload: Array<Retention>) {
    console.log(payload);
  }
}

export class LoadRetentionArtifactNewFailAction implements Action {
  type = ActionTypes.LOAD_RETENTION_ARTIFACT_NEW_FAIL;
  constructor(public payload: any) { }
}

/**** LOAD LOAD_RETENTION_ARTIFACT_FOLLOWUP *****/

export class LoadRetentionArtifactFollowUpAction implements Action {
  type = ActionTypes.LOAD_RETENTION_ARTIFACT_FOLLOWUP;
  constructor(public payload: any) { }
}

export class LoadRetentionArtifactFollowUpSuccessAction implements Action {
  type = ActionTypes.LOAD_RETENTION_ARTIFACT_FOLLOWUP_SUCCESS;
  constructor(public payload: Array<Retention>) {
    console.log(payload);
  }
}

export class LoadRetentionArtifactFollowUpFailAction implements Action {
  type = ActionTypes.LOAD_RETENTION_ARTIFACT_FOLLOWUP_FAIL;
  constructor(public payload: any) { }
}

/**** SELECT RETENTION ARTIFACT ****/
export class SelectRetentionArtifactAction implements Action {
  type = ActionTypes.SELECT_RETENTION_ARTIFACT;
  constructor(public payload: number) { } // payload is id, so type is number
}

export class LoadRetentionArtifactSuccessAction implements Action {
  type = ActionTypes.LOAD_RETENTION_ARTIFACT_SUCCESS;
  constructor(public payload: any) {
    console.log(payload);
  }
}

export class LoadRetentionArtifactFailAction implements Action {
  type = ActionTypes.LOAD_RETENTION_ARTIFACT_FAIL;
  constructor(public payload: any) { }
}


/*** CREATE TRACKING PLAN ******/
export class CreateTrackingPlanAction implements Action {
  type = ActionTypes.CREATE_TRACKING_PLAN;
  constructor(public payload: any) { }
}

export class CreateTrackingPlanSuccessAction implements Action {
  type = ActionTypes.CREATE_TRACKING_PLAN_SUCCESS;
  constructor(public payload: any) { }
}

export class CreateTrackingPlanFailAction implements Action {
  type = ActionTypes.CREATE_TRACKING_PLAN_FAIL;
  constructor(public payload: any) { }
}


/**** LOAD LoadTrackingPlanAction *****/

export class LoadTrackingPlanAction implements Action {
  type = ActionTypes.LOAD_TRACKING_PLAN;
  constructor(public payload: any) { }
}

export class LoadTrackingPlanSuccessAction implements Action {
  type = ActionTypes.LOAD_TRACKING_PLAN_SUCCESS;
  constructor(public payload: Array<any>) {
    console.log(payload);
  }
}

export class LoadTrackingPlanFailAction implements Action {
  type = ActionTypes.LOAD_TRACKING_PLAN_FAIL;
  constructor(public payload: any) { }
}


/**** LOAD GET ALL TASK *****/

export class LoadAllTaskDueAction implements Action {
  type = ActionTypes.LOAD_ALL_TASK_DUE;
  constructor(public payload: any) { }
}

export class LoadAllTaskDueSuccessAction implements Action {
  type = ActionTypes.LOAD_ALL_TASK_DUE_SUCCESS;
  constructor(public payload: Array<Retention>) {
    console.log(payload);
  }
}

export class LoadAllTaskDueFailAction implements Action {
  type = ActionTypes.LOAD_ALL_TASK_DUE_FAIL;
  constructor(public payload: any) { }
}


export class LoadAllTaskOverDueAction implements Action {
  type = ActionTypes.LOAD_ALL_TASK_OVER_DUE;
  constructor(public payload: any) { }
}

export class LoadAllTaskOverDueSuccessAction implements Action {
  type = ActionTypes.LOAD_ALL_TASK_OVER_DUE_SUCCESS;
  constructor(public payload: Array<Retention>) {
    console.log(payload);
  }
}

export class LoadAllTaskOverDueFailAction implements Action {
  type = ActionTypes.LOAD_ALL_TASK_OVER_DUE_FAIL;
  constructor(public payload: any) { }
}

/**** SELECT TASK ******/

export class SelectTaskAction implements Action {
  type = ActionTypes.SELECT_TASK;
  constructor(public payload: number) { } // payload is id, so type is number
}

export class LoadSelectTaskSuccessAction implements Action {
  type = ActionTypes.LOAD_SELECT_TASK_SUCCESS;
  constructor(public payload: Array<any>) {
    console.log(payload);
  }
}

export class LoadSelectTaskFailAction implements Action {
  type = ActionTypes.LOAD_SELECT_TASK_FAIL;
  constructor(public payload: any) { }
}



/**** LOAD DEPARTMENT ******/
export class LoadDepartPlanAction implements Action {
  type = ActionTypes.LOAD_DEPARTMENT_PLAN;
  constructor(public payload: any) { }
}

export class LoadDepartPlanSuccessAction implements Action {
  type = ActionTypes.LOAD_DEPARTMENT_PLAN_SUCCESS;
  constructor(public payload: Array<any>) {
    console.log(payload);
  }
}

export class LoadDepartPlanFailAction implements Action {
  type = ActionTypes.LOAD_DEPARTMENT_PLAN_FAIL;
  constructor(public payload: any) { }
}



/**** SELECT TRACKING PLAN  ******/

export class SelectViewTrackingAction implements Action {
  type = ActionTypes.SELECT_TRACKING_PLAN;
  constructor(public payload: number) { } // payload is id, so type is number
}

export class LoadSelectViewTrackingSuccessAction implements Action {
  type = ActionTypes.LOAD_SELECT_TRACKING_PLAN_SUCCESS;
  constructor(public payload: Array<any>) {
    console.log(payload);
  }
}

export class LoadSelectViewTrackingFailAction implements Action {
  type = ActionTypes.LOAD_SELECT_TRACKING_PLAN_FAIL;
  constructor(public payload: any) { }
}


/*** CREATE CHECK EVALUATE ******/
export class CreateCheckEvaluateAction implements Action {
  type = ActionTypes.CREATE_CHECK_EVALUATE;
  constructor(public payload: any) { }
}

export class CreateCheckEvaluateSuccessAction implements Action {
  type = ActionTypes.CREATE_CHECK_EVALUATE_SUCCESS;
  constructor(public payload: any) { }
}

export class CreateCheckEvaluateFailAction implements Action {
  type = ActionTypes.CREATE_CHECK_EVALUATE_FAIL;
  constructor(public payload: any) { }
}


/*** CREATE CreateArtifactReviewKnowledgeAction ******/
export class CreateArtifactReviewKnowledgeAction implements Action {
  type = ActionTypes.CREATE_ARTIFACTREVIEWKNOWLEDGE;
  constructor(public payload: any) { }
}

export class CreateArtifactReviewKnowledgeSuccessAction implements Action {
  type = ActionTypes.CREATE_ARTIFACTREVIEWKNOWLEDGE_SUCCESS;
  constructor(public payload: any) { }
}

export class CreateArtifactReviewKnowledgeFailAction implements Action {
  type = ActionTypes.CREATE_ARTIFACTREVIEWKNOWLEDGE_FAIL;
  constructor(public payload: any) { }
}

/****** CREATE ApproveArtifactAssignment ********/

export class CreateApproveArtifactAssignmentAction implements Action {
  type = ActionTypes.CREATE_APPROVEARTIFACTASSIGNMENT;
  constructor(public payload: any) { }
}

export class CreateApproveArtifactAssignmentSuccessAction implements Action {
  type = ActionTypes.CREATE_APPROVEARTIFACTASSIGNMENT_SUCCESS;
  constructor(public payload: any) { }
}

export class CreateApproveArtifactAssignmentFailAction implements Action {
  type = ActionTypes.CREATE_APPROVEARTIFACTASSIGNMENT_FAIL;
  constructor(public payload: any) { }
}


/***** SubmitApprove ******/

export class SubmitApproveRetentionAction implements Action {
  type = ActionTypes.SUBMIT_APPROVE_RETENTION;
  constructor(public payload: any) { }
}

export class SubmitApproveRetentionSuccessAction implements Action {
  type = ActionTypes.SUBMIT_APPROVE_RETENTION_SUCCESS;
  constructor(public payload: any) { }
}

export class SubmitApproveRetentionFailAction implements Action {
  type = ActionTypes.SUBMIT_APPROVE_RETENTION_FAIL;
  constructor(public payload: any) { }
}


/**** SELECT PLAN_ASSESSMENT ******/

export class SelectPlanAssesmentAction implements Action {
  type = ActionTypes.SELECT_PLAN_ASSESSMENT;
  constructor(public payload: number) { } // payload is id, so type is number
}

export class LoadSelectPlanAssesmentSuccessAction implements Action {
  type = ActionTypes.LOAD_SELECT_PLAN_ASSESSMENT_SUCCESS;
  constructor(public payload: Array<any>) {
    console.log(payload);
  }
}

export class LoadSelectPlanAssesmentFailAction implements Action {
  type = ActionTypes.LOAD_SELECT_PLAN_ASSESSMENT_FAIL;
  constructor(public payload: any) { }
}

/**** SELECT SUB TASK PLAN_ASSESSMENT ******/

export class SelectedSubTaskAssessmentAction implements Action {
  type = ActionTypes.SELECT_SUB_TASK_ASSESSMENT;
  constructor(public payload: number) { } // payload is id, so type is number
}

export class LoadSelectedSubTaskAssessmentSuccessAction implements Action {
  type = ActionTypes.LOAD_SELECT_SUB_TASK_ASSESSMENT_SUCCESS;
  constructor(public payload: any) {
    console.log(payload);
  }
}

export class LoadSelectedSubTaskAssessmentFailAction implements Action {
  type = ActionTypes.LOAD_SELECT_SUB_TASK_ASSESSMENT_FAIL;
  constructor(public payload: any) { }
}


/***** load retention result *********/
/**** LOAD LOAD_RETENTION_ARTIFACT_NEW *****/

export class LoadRetentionResultAction implements Action {
  type = ActionTypes.LOAD_RETENTION_RESULT;
  constructor(public payload: any) { }
}

export class LoadRetentionResultSuccessAction implements Action {
  type = ActionTypes.LOAD_RETENTION_RESULT_SUCCESS;
  constructor(public payload: any) {
    console.log(payload);
  }
}

export class LoadRetentionResultNewFailAction implements Action {
  type = ActionTypes.LOAD_RETENTION_RESULT_FAIL;
  constructor(public payload: any) { }
}


/**** SELECT APPROVE RETENTION ASSESSMENT ******/

export class SelectApproveRetentionAssessmentAction implements Action {
  type = ActionTypes.SELECT_APPROVE_RETENTION_ASSESSMENT;
  constructor(public payload: number) { } // payload is id, so type is number
}

export class LoadApproveRetentionAssessmentSuccessAction implements Action {
  type = ActionTypes.LOAD_APPROVE_RETENTION_ASSESSMENT_SUCCESS;
  constructor(public payload: Array<any>) {
    console.log(payload);
  }
}

export class LoadApproveRetentionAssessmentFailAction implements Action {
  type = ActionTypes.LOAD_APPROVE_RETENTION_ASSESSMENT_FAIL;
  constructor(public payload: any) { }
}




export type Actions =
  SelectApproveRetentionAssessmentAction |
  LoadApproveRetentionAssessmentSuccessAction |
  LoadApproveRetentionAssessmentFailAction |

  LoadRetentionArtifactNewAction |
  LoadRetentionArtifactNewSuccessAction |
  LoadRetentionArtifactNewFailAction |

  LoadRetentionArtifactFollowUpAction |
  LoadRetentionArtifactFollowUpSuccessAction |
  LoadRetentionArtifactFollowUpFailAction |

  LoadRetentionArtifactSuccessAction |
  LoadRetentionArtifactFailAction |

  CreateTrackingPlanAction |
  CreateTrackingPlanFailAction |
  CreateTrackingPlanSuccessAction |

  SelectRetentionArtifactAction |

  LoadTrackingPlanAction |
  LoadTrackingPlanSuccessAction |
  LoadTrackingPlanFailAction |

  LoadAllTaskDueAction |
  LoadAllTaskDueSuccessAction |
  LoadAllTaskDueFailAction |

  LoadAllTaskOverDueAction |
  LoadAllTaskOverDueSuccessAction |
  LoadAllTaskOverDueFailAction |

  SelectTaskAction |
  LoadDepartPlanSuccessAction |
  LoadSelectTaskFailAction |


  LoadDepartPlanAction |
  LoadDepartPlanSuccessAction |
  LoadDepartPlanFailAction |

  SelectViewTrackingAction |
  LoadSelectViewTrackingSuccessAction |
  LoadSelectViewTrackingFailAction |

  CreateCheckEvaluateAction |
  CreateCheckEvaluateSuccessAction |
  CreateCheckEvaluateFailAction |

  CreateArtifactReviewKnowledgeAction |
  CreateArtifactReviewKnowledgeFailAction |
  CreateArtifactReviewKnowledgeSuccessAction |

  CreateApproveArtifactAssignmentAction |
  CreateApproveArtifactAssignmentSuccessAction |
  CreateApproveArtifactAssignmentFailAction |

  SubmitApproveRetentionAction |
  SubmitApproveRetentionFailAction |
  SubmitApproveRetentionSuccessAction |

  SelectPlanAssesmentAction |

  LoadSelectPlanAssesmentSuccessAction |
  LoadSelectPlanAssesmentFailAction |

  LoadSelectedSubTaskAssessmentFailAction |
  LoadSelectedSubTaskAssessmentSuccessAction |
  SelectedSubTaskAssessmentAction |

  LoadRetentionResultAction |
  LoadRetentionResultSuccessAction |
  LoadRetentionResultNewFailAction

  ;
