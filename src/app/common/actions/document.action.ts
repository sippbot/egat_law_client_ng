import { Action } from '@ngrx/store';
import { Document } from '../models/document.model';

export const ActionTypes = {
  ADD_DOCUMENT: 'Add an document',
  REMOVE_DOCUMENT: 'Remove an document',

  UPDATE_DOCUMENT: 'Update an document',
  UPDATE_DOCUMENT_SUCCESS: 'Update an document success',
  UPDATE_DOCUMENT_FAIL: 'Update an document fail',

  LOAD_DOCUMENT: 'Load an document',
  LOAD_DOCUMENT_SUCCESS: 'Load an document success',
  LOAD_DOCUMENT_FAIL: 'Load an document fail'
};

export class AddDocumentAction implements Action {
  type = ActionTypes.ADD_DOCUMENT;
  constructor(public payload: any) { }
}

export class RemoveDocumentAction implements Action {
  type = ActionTypes.REMOVE_DOCUMENT;
  constructor(public payload: Document) { }
}

export class LoadDocumentAction implements Action {
  type = ActionTypes.LOAD_DOCUMENT;
  constructor(public payload: number) { }
}

export class LoadDocumentSuccessAction implements Action {
  type = ActionTypes.LOAD_DOCUMENT_SUCCESS;
  constructor(public payload: Array<Document>) { }
}

export class LoadDocumentFailAction implements Action {
  type = ActionTypes.LOAD_DOCUMENT_FAIL;
  constructor(public payload: any) { }
}

export class UpdateDocumentAction implements Action {
  type = ActionTypes.UPDATE_DOCUMENT;
  constructor(public payload: any) { }
}

export class UpdateDocumentSuccessAction implements Action {
  type = ActionTypes.UPDATE_DOCUMENT_SUCCESS;
  constructor(public payload: Document) { }
}

export class UpdateDocumentFailAction implements Action {
  type = ActionTypes.UPDATE_DOCUMENT_FAIL;
  constructor(public payload: any) { }
}

export type Actions = AddDocumentAction |
  RemoveDocumentAction |
  LoadDocumentAction |
  LoadDocumentSuccessAction |
  LoadDocumentFailAction|
  UpdateDocumentAction|
  UpdateDocumentSuccessAction|
  UpdateDocumentFailAction;
