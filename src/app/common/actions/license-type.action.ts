import { Action } from '@ngrx/store';
import { LicenseType } from '../models/license-type.model';

export const ActionTypes = {
  LOAD_LICENSETYPE: 'Load an LicenseType',
  LOAD_LICENSETYPE_SUCCESS: 'Load an LicenseType success',
  LOAD_LICENSETYPE_FAIL: 'Load an LicenseType fail',

  CREATE_LICENSETYPE: 'Craete a LicenseType',
  CREATE_LICENSETYPE_SUCCESS: 'Craete a LicenseType success',
  CREATE_LICENSETYPE_FAIL: 'Craete a LicenseType fail',

  UPDATE_LICENSETYPE: 'Update a LicenseType',
  UPDATE_LICENSETYPE_SUCCESS: 'Update a LicenseType success',
  UPDATE_LICENSETYPE_FAIL: 'Update a LicenseType fail',

  DELETE_LICENSETYPE: 'Delete a LicenseType',
  DELETE_LICENSETYPE_SUCCESS: 'Delete a LicenseType success',
  DELETE_LICENSETYPE_FAIL: 'Delete a LicenseType fail',

  SELECT_LICENSETYPE: 'Select a LicenseType'
};

export class LoadLicenseTypeAction implements Action {
  type = ActionTypes.LOAD_LICENSETYPE;
  constructor(public payload: any) { }
}

export class LoadLicenseTypeSuccessAction implements Action {
  type = ActionTypes.LOAD_LICENSETYPE_SUCCESS;

  constructor(public payload: LicenseType[]) { }
}

export class LoadLicenseTypeFailAction implements Action {
  type = ActionTypes.LOAD_LICENSETYPE_FAIL;
  constructor(public payload: any) { }
}

export class CreateLicenseTypeAction implements Action {
  type = ActionTypes.CREATE_LICENSETYPE;
  constructor(public payload: LicenseType) { }
}

export class CreateLicenseTypeSuccessAction implements Action {
  type = ActionTypes.CREATE_LICENSETYPE_SUCCESS;
  constructor(public payload: LicenseType) { }
}

export class CreateLicenseTypeFailAction implements Action {
  type = ActionTypes.CREATE_LICENSETYPE_FAIL;
  constructor(public payload: any) { }
}

export class UpdateLicenseTypeAction implements Action {
  type = ActionTypes.UPDATE_LICENSETYPE;
  constructor(public payload: LicenseType) { }
}

export class UpdateLicenseTypeSuccessAction implements Action {
  type = ActionTypes.UPDATE_LICENSETYPE_SUCCESS;
  constructor(public payload: LicenseType) { }
}

export class UpdateLicenseTypeFailAction implements Action {
  type = ActionTypes.UPDATE_LICENSETYPE_FAIL;
  constructor(public payload: any) { }
}

export class DeleteLicenseTypeAction implements Action {
  type = ActionTypes.DELETE_LICENSETYPE;
  constructor(public payload: number) { }
}

export class DeleteLicenseTypeSuccessAction implements Action {
  type = ActionTypes.DELETE_LICENSETYPE_SUCCESS;
  constructor(public payload: number) { }
}

export class DeleteLicenseTypeFailAction implements Action {
  type = ActionTypes.DELETE_LICENSETYPE_FAIL;
  constructor(public payload: any) { }
}

export class SelectLicenseTypeAction implements Action {
  type = ActionTypes.SELECT_LICENSETYPE;
  constructor(public payload: any) { }
}

export type Actions =
  LoadLicenseTypeAction |
  LoadLicenseTypeSuccessAction |
  LoadLicenseTypeFailAction |
  CreateLicenseTypeAction |
  CreateLicenseTypeSuccessAction |
  CreateLicenseTypeFailAction |
  UpdateLicenseTypeAction |
  UpdateLicenseTypeSuccessAction |
  UpdateLicenseTypeFailAction |
  DeleteLicenseTypeAction |
  DeleteLicenseTypeSuccessAction |
  DeleteLicenseTypeFailAction |
  SelectLicenseTypeAction;
