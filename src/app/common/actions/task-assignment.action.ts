import { Document } from '../models/models';
import { TaskComment } from '../models/task-assignment-comment.model';
import { TaskAssignmentSummary } from '../models/task-assignment-summary.model';
import { AssessmentReview } from '../models/assessment-review.model';
import { AssessmentInfo } from '../models/assessment-info.model';
import { TaskAssignment } from '../models/task-assignment.model';
import { Action } from '@ngrx/store';

export const ActionTypes = {
  LOAD_TASKASSIGNMENTS: 'Load TaskAssignment list',
  LOAD_TASKASSIGNMENTS_SUCCESS: 'Load TaskAssignment list success',
  LOAD_TASKASSIGNMENTS_FAIL: 'Load TaskAssignment list fail',

  SELECT_TASKASSIGNMENT: 'Select a TaskAssignment',

  LOAD_TASKASSIGNMENT_FULLDETAIL: 'Load TaskAssignment',
  LOAD_TASKASSIGNMENT_FULLDETAIL_SUCCESS: 'Load TaskAssignment success',
  LOAD_TASKASSIGNMENT_FULLDETAIL_FAIL: 'Load TaskAssignment fail',

  CREATE_TASKASSESSMENT: 'Create TaskAssessment',
  CREATE_TASKASSESSMENT_SUCCESS: 'Create TaskAssessment success',
  CREATE_TASKASSESSMENT_FAIL: 'Create TaskAssessment fail',

  LOAD_TASKASSESSMENT: 'Load TaskAssessment',
  LOAD_TASKASSESSMENT_SUCCESS: 'Loade TaskAssessment success',
  LOAD_TASKASSESSMENT_FAIL: 'Loade TaskAssessment fail',

  CREATE_TASKASSESSMENT_REVIEW: 'Review TaskAssessment',
  CREATE_TASKASSESSMENT_REVIEW_SUCCESS: 'Review TaskAssessment success',
  CREATE_TASKASSESSMENT_REVIEW_FAIL: 'Review TaskAssessment fail',

  LOAD_TASKASSIGNMENT_SUMMARY: 'Load TaskAssignmentSummary',
  LOAD_TASKASSIGNMENT_SUMMARY_SUCCESS: 'Loade TaskAssignmentSummary success',
  LOAD_TASKASSIGNMENT_SUMMARY_FAIL: 'Loade TaskAssignmentSummary fail',

  CREATE_TASKASSIGNMENT_PROPOSAL: 'Create TaskAssignment proposal',
  CREATE_TASKASSIGNMENT_PROPOSAL_SUCCESS: 'Create TaskAssignment proposal success',
  CREATE_TASKASSIGNMENT_PROPOSAL_FAIL: 'Create TaskAssignment proposal fail',

  CREATE_TASKASSIGNMENT_APPROVAL: 'Create TaskAssignment approval',
  CREATE_TASKASSIGNMENT_APPROVAL_SUCCESS: 'Create TaskAssignment approval success',
  CREATE_TASKASSIGNMENT_APPROVAL_FAIL: 'Create TaskAssignment approval fail',

  CREATE_TASKCOMMENT: 'Create TaskComment',
  CREATE_TASKCOMMENT_SUCCESS: 'Create TaskComment success',
  CREATE_TASKCOMMENT_FAIL: 'Create TaskComment fail',

  CREATE_TASKASSIGNMENT_ATTACHMENT: 'Create TaskAssignmentAttachment',
  CREATE_TASKASSIGNMENT_ATTACHMENT_SUCCESS: 'Create TaskAssignmentAttachment success',
  CREATE_TASKASSIGNMENT_ATTACHMENT_FAIL: 'Create TaskAssignmentAttachment fail',

  DELETE_TASKASSIGNMENT_ATTACHMENT: 'Delete TaskAssignmentAttachment',
  DELETE_TASKASSIGNMENT_ATTACHMENT_SUCCESS: 'Delete TaskAssignmentAttachment success',
  DELETE_TASKASSIGNMENT_ATTACHMENT_FAIL: 'Delete TaskAssignmentAttachment fail',
  
  CLEAR_SELECT_TASKASSIGMENTS:'Clear selected TaskAssignment'
};

export class ClearSelectedTaskAssignmentction implements Action {
  type = ActionTypes.CLEAR_SELECT_TASKASSIGMENTS;
  constructor(public payload: any) { }
}

export class LoadTaskAssignmentsAction implements Action {
  type = ActionTypes.LOAD_TASKASSIGNMENTS;
  constructor(public payload: any) { }
}

export class LoadTaskAssignmentsSuccessAction implements Action {
  type = ActionTypes.LOAD_TASKASSIGNMENTS_SUCCESS;
  constructor(public payload: Array<TaskAssignment>) { }
}

export class LoadTaskAssignmentsFailAction implements Action {
  type = ActionTypes.LOAD_TASKASSIGNMENTS_FAIL;
  constructor(public payload: any) { }
}

export class SelectTaskAssignmentAction implements Action {
  type = ActionTypes.SELECT_TASKASSIGNMENT;
  constructor(public payload: any) { }
}

export class LoadTaskAssignmentFullDetailAction implements Action {
  type = ActionTypes.LOAD_TASKASSIGNMENT_FULLDETAIL;
  constructor(public payload: any) { }
}

export class LoadTaskAssignmentFullDetailSuccessAction implements Action {
  type = ActionTypes.LOAD_TASKASSIGNMENT_FULLDETAIL_SUCCESS;
  constructor(public payload: TaskAssignment) { }
}

export class LoadTaskAssignmentFullDetailFailAction implements Action {
  type = ActionTypes.LOAD_TASKASSIGNMENT_FULLDETAIL_FAIL;
  constructor(public payload: any) { }
}

export class CreateTaskAssessmentAction implements Action {
  type = ActionTypes.CREATE_TASKASSESSMENT;
  constructor(public payload: AssessmentInfo) { }
}

export class CreateTaskAssessmentSuccessAction implements Action {
  type = ActionTypes.CREATE_TASKASSESSMENT_SUCCESS;
  constructor(public payload: any) { }
}

export class CreateTaskAssessmentFailAction implements Action {
  type = ActionTypes.CREATE_TASKASSESSMENT_FAIL;
  constructor(public payload: any) { }
}

export class CreateTaskAssessmentReviewAction implements Action {
  type = ActionTypes.CREATE_TASKASSESSMENT_REVIEW;
  constructor(public payload: AssessmentReview) { }
}

export class CreateTaskAssessmentReviewSuccessAction implements Action {
  type = ActionTypes.CREATE_TASKASSESSMENT_REVIEW_SUCCESS;
  constructor(public payload: any) { }
}

export class CreateTaskAssessmentReviewFailAction implements Action {
  type = ActionTypes.CREATE_TASKASSESSMENT_REVIEW_FAIL;
  constructor(public payload: any) { }
}

export class LoadTaskAssignmentSummaryAction implements Action {
  type = ActionTypes.LOAD_TASKASSIGNMENT_SUMMARY;
  constructor(public payload: any) { }
}

export class LoadTaskAssignmentSummarySuccessAction implements Action {
  type = ActionTypes.LOAD_TASKASSIGNMENT_SUMMARY_SUCCESS;
  constructor(public payload: Array<TaskAssignmentSummary>) { }
}

export class LoadTaskAssignmentSummaryFailAction implements Action {
  type = ActionTypes.LOAD_TASKASSIGNMENT_SUMMARY_FAIL;
  constructor(public payload: any) { }
}

export class CreateTaskAssignmentProposalAction implements Action {
  type = ActionTypes.CREATE_TASKASSIGNMENT_PROPOSAL;
  constructor(public payload: any) { }
}

export class CreateTaskAssignmentProposalSuccessAction implements Action {
  type = ActionTypes.CREATE_TASKASSIGNMENT_PROPOSAL_SUCCESS;
  constructor(public payload: any) { }
}

export class CreateTaskAssignmentProposalFailAction implements Action {
  type = ActionTypes.CREATE_TASKASSIGNMENT_PROPOSAL_FAIL;
  constructor(public payload: any) { }
}

export class CreateTaskAssignmentApprovalAction implements Action {
  type = ActionTypes.CREATE_TASKASSIGNMENT_APPROVAL;
  constructor(public payload: any) { }
}

export class CreateTaskAssignmentApprovalSuccessAction implements Action {
  type = ActionTypes.CREATE_TASKASSIGNMENT_APPROVAL_SUCCESS;
  constructor(public payload: any) { }
}

export class CreateTaskAssignmentApprovalFailAction implements Action {
  type = ActionTypes.CREATE_TASKASSIGNMENT_APPROVAL_FAIL;
  constructor(public payload: any) { }
}

export class CreateTaskCommentAction implements Action {
  type = ActionTypes.CREATE_TASKCOMMENT;
  constructor(public payload: TaskComment) { }
}

export class CreateTaskCommentSuccessAction implements Action {
  type = ActionTypes.CREATE_TASKCOMMENT_SUCCESS;
  constructor(public payload: TaskComment) { }
}

export class CreateTaskCommentFailAction implements Action {
  type = ActionTypes.CREATE_TASKCOMMENT_FAIL;
  constructor(public payload: any) { }
}

export class CreateTaskAssignmentAttachmentAction implements Action {
  type = ActionTypes.CREATE_TASKASSIGNMENT_ATTACHMENT;
  constructor(public payload: any) { }
}

export class CreateTaskAssignmentAttachmentSuccessAction implements Action {
  type = ActionTypes.CREATE_TASKASSIGNMENT_ATTACHMENT_SUCCESS;
  constructor(public payload: Document) { }
}

export class CreateTaskAssignmentAttachmentFailAction implements Action {
  type = ActionTypes.CREATE_TASKASSIGNMENT_ATTACHMENT_FAIL;
  constructor(public payload: any) { }
}

export class DeleteTaskAssignmentAttachmentAction implements Action {
  type = ActionTypes.DELETE_TASKASSIGNMENT_ATTACHMENT;
  constructor(public payload: any) { }
}

export class DeleteTaskAssignmentAttachmentSuccessAction implements Action {
  type = ActionTypes.DELETE_TASKASSIGNMENT_ATTACHMENT_SUCCESS;
  constructor(public payload: number) { }
}

export class DeleteTaskAssignmentAttachmentFailAction implements Action {
  type = ActionTypes.DELETE_TASKASSIGNMENT_ATTACHMENT_FAIL;
  constructor(public payload: any) { }
}

export type Actions =
  LoadTaskAssignmentsAction |
  LoadTaskAssignmentsSuccessAction |
  LoadTaskAssignmentsFailAction |
  SelectTaskAssignmentAction |
  LoadTaskAssignmentFullDetailAction |
  LoadTaskAssignmentFullDetailSuccessAction |
  LoadTaskAssignmentFullDetailFailAction |
  CreateTaskAssessmentAction |
  CreateTaskAssessmentSuccessAction |
  CreateTaskAssessmentFailAction |
  CreateTaskAssessmentReviewAction |
  CreateTaskAssessmentReviewSuccessAction |
  CreateTaskAssessmentReviewFailAction |
  LoadTaskAssignmentSummaryAction |
  LoadTaskAssignmentSummarySuccessAction |
  LoadTaskAssignmentSummaryFailAction |
  CreateTaskAssignmentProposalAction |
  CreateTaskAssignmentProposalSuccessAction |
  CreateTaskAssignmentProposalFailAction |
  CreateTaskAssignmentApprovalAction |
  CreateTaskAssignmentApprovalSuccessAction |
  CreateTaskAssignmentApprovalFailAction |
  CreateTaskCommentAction |
  CreateTaskCommentSuccessAction |
  CreateTaskCommentFailAction|
  CreateTaskAssignmentAttachmentAction|
  CreateTaskAssignmentAttachmentSuccessAction|
  CreateTaskAssignmentAttachmentFailAction|
  DeleteTaskAssignmentAttachmentAction|
  DeleteTaskAssignmentAttachmentSuccessAction|
  DeleteTaskAssignmentAttachmentFailAction;
