import { Action } from '@ngrx/store';
import { UserPermission } from '../models/user-permissions.model';
import { Role } from '../models/role.model';

export const ActionTypes = {
  LOAD_ROLE: 'Load an role',
  LOAD_ROLE_SUCCESS: 'Load an role success',
  LOAD_ROLE_FAIL: 'Load an role fail',
  UPDATE_ROLE: 'Update a role',
  UPDATE_ROLE_SUCCESS: 'Update a role success',
  UPDATE_ROLE_FAIL: 'Update a role fail',
  CREATE_ROLE: 'Create a role',
  CREATE_ROLE_SUCCESS: 'Create a role success',
  CREATE_ROLE_FAIL: 'Create a role fail',
  DELETE_ROLE: 'Delete a role',
  DELETE_ROLE_SUCCESS: 'Delete a role success',
  DELETE_ROLE_FAIL: 'Delete a role fail',
};


export class LoadRoleAction implements Action {
  type = ActionTypes.LOAD_ROLE;
  constructor(public payload: any) { }
}

export class LoadRoleSuccessAction implements Action {
  type = ActionTypes.LOAD_ROLE_SUCCESS;
  constructor(public payload: Array<UserPermission>) { }
}

export class LoadRoleFailAction implements Action {
  type = ActionTypes.LOAD_ROLE_FAIL;
  constructor(public payload: any) { }
}

export class UpdateRoleAction implements Action {
  type = ActionTypes.UPDATE_ROLE;
  constructor(public payload: Role) { }
}

export class UpdateRoleSuccessAction implements Action {
  type = ActionTypes.UPDATE_ROLE_SUCCESS;
  constructor(public payload: Role) { }
}

export class UpdateRoleFailAction implements Action {
  type = ActionTypes.UPDATE_ROLE_FAIL;
  constructor(public payload: any) { }
}

export class CreateRoleAction implements Action {
  type = ActionTypes.CREATE_ROLE;
  constructor(public payload: Role) { }
}

export class CreateRoleSuccessAction implements Action {
  type = ActionTypes.CREATE_ROLE_SUCCESS;
  constructor(public payload: Role) { }
}

export class CreateRoleFailAction implements Action {
  type = ActionTypes.CREATE_ROLE_FAIL;
  constructor(public payload: any) { }
}

export class DeleteRoleAction implements Action {
  type = ActionTypes.DELETE_ROLE;
  constructor(public payload: number) { }
}

export class DeleteRoleSuccessAction implements Action {
  type = ActionTypes.DELETE_ROLE_SUCCESS;
  constructor(public payload: number) { }
}

export class DeleteRoleFailAction implements Action {
  type = ActionTypes.DELETE_ROLE_FAIL;
  constructor(public payload: any) { }
}


export type Actions =
  LoadRoleAction |
  LoadRoleSuccessAction |
  LoadRoleFailAction |
  UpdateRoleAction |
  UpdateRoleSuccessAction |
  UpdateRoleFailAction |
  CreateRoleAction |
  CreateRoleSuccessAction |
  CreateRoleFailAction |
  DeleteRoleAction |
  DeleteRoleSuccessAction |
  DeleteRoleFailAction 
