import { Action } from '@ngrx/store';
import { User } from '../models/user.model';

export const ActionTypes = {
    REMOVE_USER: 'Remove user',

    LOAD_USERS: 'Load users',
    LOAD_USERS_SUCCESS: 'Load users success',
    LOAD_USERS_FAIL: 'Load users fail',

    LOAD_USER_LOGIN: 'Load an user',
    LOAD_USER_LOGIN_SUCCESS: 'Load an user success',
    LOAD_USER_LOGIN_FAIL: 'Load an user fail',

    SELECT_USERS: 'Select a users',

    UPDATE_USERS: 'Update a users',
    UPDATE_USERS_SUCCESS: 'Update a users success',
    UPDATE_USERS_FAIL: 'Update a users fail',

    CREATE_USERS: 'Craete a users',
    CREATE_USERS_SUCCESS: 'Craete a users success',
    CREATE_USERS_FAIL: 'Craete a users fail',

    DELETE_USERS: 'Delete a users',
    DELETE_USERS_SUCCESS: 'Delete a users success',
    DELETE_USERS_FAIL: 'Delete a users fail'
};


export class RemoveUserAction implements Action {
    type = ActionTypes.REMOVE_USER;
    constructor(public payload: User) {
    }
}

export class LoadUsersAction implements Action {
    type = ActionTypes.LOAD_USERS;
    constructor(public payload: any) {
    }
}

export class LoadUsersSuccessAction implements Action {
    type = ActionTypes.LOAD_USERS_SUCCESS;
    constructor(public payload: Array<User>) {
    }
}

export class LoadUsersFailAction implements Action {
    type = ActionTypes.LOAD_USERS_FAIL;
    constructor(public payload: any) {
    }
}

export class LoadUserLoginAction implements Action {
    type = ActionTypes.LOAD_USER_LOGIN;
    constructor(public payload: number) {
    }
}

export class LoadUserLoginSuccessAction implements Action {
    type = ActionTypes.LOAD_USER_LOGIN_SUCCESS;
    constructor(public payload: User) {
    }
}

export class LoadUserLoginFailAction implements Action {
    type = ActionTypes.LOAD_USER_LOGIN_FAIL;
    constructor(public payload: any) {
    }
}

export class SelectUserAction implements Action {
    type = ActionTypes.SELECT_USERS;
    constructor(public payload: number) {
    } // payload is id, so type is number
}

export class UpdateUserAction implements Action {
    type = ActionTypes.UPDATE_USERS;
    constructor(public payload: User) {
    }
}

export class UpdateUserSuccessAction implements Action {
    type = ActionTypes.UPDATE_USERS_SUCCESS;
    constructor(public payload: User) {
    }
}

export class UpdateUserFailAction implements Action {
    type = ActionTypes.UPDATE_USERS_FAIL;
    constructor(public payload: any) {
    }
}

export class CreateUserAction implements Action {
    type = ActionTypes.CREATE_USERS;
    constructor(public payload: User) {
    }
}

export class CreateUserSuccessAction implements Action {
    type = ActionTypes.CREATE_USERS_SUCCESS;
    constructor(public payload: User) {
    }
}

export class CreateUserFailAction implements Action {
    type = ActionTypes.CREATE_USERS_FAIL;
    constructor(public payload: any) {
    }
}

export class DeleteUserAction implements Action {
    type = ActionTypes.DELETE_USERS;
    constructor(public payload: number) {
    }
}

export class DeleteUserSuccessAction implements Action {
    type = ActionTypes.DELETE_USERS_SUCCESS;
    constructor(public payload: number) {
    }
}

export class DeleteUserFailAction implements Action {
    type = ActionTypes.DELETE_USERS_FAIL;
    constructor(public payload: any) {
    }
}

export type Actions =
    RemoveUserAction |
    LoadUsersAction |
    LoadUsersSuccessAction |
    LoadUsersFailAction |
    SelectUserAction |
    UpdateUserAction |
    UpdateUserSuccessAction |
    UpdateUserFailAction |
    CreateUserAction |
    CreateUserSuccessAction |
    CreateUserFailAction|
    LoadUserLoginAction|
    LoadUserLoginSuccessAction|
    LoadUserLoginFailAction|
    DeleteUserAction|
    DeleteUserSuccessAction|
    DeleteUserFailAction;
