import {Action} from '@ngrx/store';

export const LayoutActionTypes = {
  //Right sidenav actions
  OPEN_RIGHT_EDITFORM: '[Layout] Open RightSidenav',
  CLOSE_RIGHT_EDITFORM: '[Layout] Close RightSidenav',
};
export class OpenRightEditFormAction implements Action {
  type = LayoutActionTypes.OPEN_RIGHT_EDITFORM; constructor() { }
}

export class CloseRightEditFormAction implements Action {
  type = LayoutActionTypes.CLOSE_RIGHT_EDITFORM; constructor() { }
}

export type LayoutActions = OpenRightEditFormAction | CloseRightEditFormAction;
