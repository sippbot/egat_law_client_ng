import {Action} from '@ngrx/store';
import {Alert} from '../models/alert.model';

export const ActionTypes = {
    ADD_ALERT: 'Show an alert'
};

export class ShowAlertAction implements Action {
    type = ActionTypes.ADD_ALERT;

    // เอาค่าของ payload ที่ส่งมาจากแต่ละ component ไปเซ็ตค่าให้กับ Alert model
    constructor(public payload: Alert) {
    }
}

export type Actions = ShowAlertAction;
