import { Action } from '@ngrx/store';
import { UserGroup } from '../models/usergroup.model';

export const ActionTypes = {
  ADD_USERGROUP: 'Add an user group',
  REMOVE_USERGROUP: 'Remove an user group',
  LOAD_USERGROUPS: 'Load an user group',
  LOAD_USERGROUPS_SUCCESS: 'Load an user group success',
  LOAD_USERGROUPS_FAIL: 'Load an user group fail',
  LOAD_USERGROUPS_PERMISSION: 'Load an user group permission',
  LOAD_USERGROUPS_PERMISSION_SUCCESS: 'Load an user group permission success',
  LOAD_USERGROUPS_PERMISSION_FAIL: 'Load an user group permission fail',
  SELECT_USERGROUPS: 'Select a user group',
  UPDATE_USERGROUPS: 'Update a user group',
  UPDATE_USERGROUPS_SUCCESS: 'Update a user group success',
  UPDATE_USERGROUPS_FAIL: 'Update a user group fail',
  CREATE_USERGROUPS: 'Craete a user group',
  CREATE_USERGROUPS_SUCCESS: 'Craete a user group success',
  CREATE_USERGROUPS_FAIL: 'Craete a user group fail',
  DELETE_USERGROUPS: 'Delete user group',
  DELETE_USERGROUPS_SUCCESS: 'Delete user group success',
  DELETE_USERGROUPS_FAIL: 'Delete user group fail',
};

export class RemoveUserGroupAction implements Action {
  type = ActionTypes.REMOVE_USERGROUP;
  constructor(public payload: UserGroup) { }
}

export class LoadUserGroupAction implements Action {
  type = ActionTypes.LOAD_USERGROUPS;
  constructor(public payload: any) { }
}

export class LoadUserGroupSuccessAction implements Action {
  type = ActionTypes.LOAD_USERGROUPS_SUCCESS;
  constructor(public payload: Array<UserGroup>) { }
}

export class LoadUserGroupFailAction implements Action {
  type = ActionTypes.LOAD_USERGROUPS_FAIL;
  constructor(public payload: any) { }
}

export class LoadUserGroupPermissionAction implements Action {
  type = ActionTypes.LOAD_USERGROUPS_PERMISSION;
  constructor(public payload: any) { }
}

export class LoadUserGroupPermissionSuccessAction implements Action {
  type = ActionTypes.LOAD_USERGROUPS_PERMISSION_SUCCESS;
  constructor(public payload: Array<UserGroup>) { }
}

export class LoadUserGroupPermissionFailAction implements Action {
  type = ActionTypes.LOAD_USERGROUPS_PERMISSION_FAIL;
  constructor(public payload: any) { }
}

export class SelectUsergroupAction implements Action {
  type = ActionTypes.SELECT_USERGROUPS;
  constructor(public payload: number) { } // payload is id, so type is number
}

export class UpdateUsergroupAction implements Action {
  type = ActionTypes.UPDATE_USERGROUPS;
  constructor(public payload: UserGroup) { }
}

export class UpdateUsergroupSuccessAction implements Action {
  type = ActionTypes.UPDATE_USERGROUPS_SUCCESS;
  constructor(public payload: UserGroup) { }
}

export class UpdateUsergroupFailAction implements Action {
  type = ActionTypes.UPDATE_USERGROUPS_FAIL;
  constructor(public payload: any) { }
}

export class CreateUsergroupAction implements Action {
  type = ActionTypes.CREATE_USERGROUPS;
  constructor(public payload: UserGroup) { }
}

export class CreateUsergroupSuccessAction implements Action {
  type = ActionTypes.CREATE_USERGROUPS_SUCCESS;
  constructor(public payload: UserGroup) { }
}

export class CreateUsergroupFailAction implements Action {
  type = ActionTypes.CREATE_USERGROUPS_FAIL;
  constructor(public payload: any) { }
}

export class DeleteUserGroupAction implements Action {
  type = ActionTypes.DELETE_USERGROUPS;
  constructor(public payload: number) { }
}

export class DeleteUserGroupSuccessAction implements Action {
  type = ActionTypes.DELETE_USERGROUPS_SUCCESS;
  constructor(public payload: number) { }
}

export class DeleteUserGroupFailAction implements Action {
  type = ActionTypes.DELETE_USERGROUPS_FAIL;
  constructor(public payload: any) { }
}

export type Actions =
  RemoveUserGroupAction |
  LoadUserGroupAction |
  LoadUserGroupSuccessAction |
  LoadUserGroupFailAction |
  LoadUserGroupPermissionAction |
  LoadUserGroupPermissionSuccessAction |
  LoadUserGroupPermissionFailAction |
  SelectUsergroupAction |
  UpdateUsergroupAction |
  UpdateUsergroupSuccessAction |
  UpdateUsergroupFailAction |
  CreateUsergroupAction |
  CreateUsergroupSuccessAction |
  CreateUsergroupFailAction |
  DeleteUserGroupAction |
  DeleteUserGroupSuccessAction |
  DeleteUserGroupFailAction;
