import { ArtifactNodeGroup } from '../models/artifact-node-group.model';
import { Action } from '@ngrx/store';
import { ArtifactNode } from '../models/artifact-node.model';
import { ArtifactSummary } from '../models/artifact-summary.model';
import { Artifact } from '../models/artifact.model';
import { ArtifactReviewAssignment } from '../models/artifact-review-assignment.model';

export const ActionTypes = {
  REMOVE_ARTIFACT: 'Remove a artifact',

  LOAD_ARTIFACTS: 'Load a artifact',
  LOAD_ARTIFACTS_SUCCESS: 'Load a artifact success',
  LOAD_ARTIFACTS_FAIL: 'Load a artifact fail',

  LOAD_ARTIFACTS_SUMMARY: 'Load a artifact summary',
  LOAD_ARTIFACTS_SUMMARY_SUCCESS: 'Load a artifact summary success',
  LOAD_ARTIFACTS_SUMMARY_FAIL: 'Load a artifact summary fail',

  LOAD_CANCELED_ARTIFACTS_SUMMARY: 'Load a canceled artifact summary',
  LOAD_CANCELED_ARTIFACTS_SUMMARY_SUCCESS: 'Load a canceled artifact summary success',
  LOAD_CANCELED_ARTIFACTS_SUMMARY_FAIL: 'Load a canceled artifact summary fail',


  SELECT_ARTIFACT: 'Select a artifact',
  SELECT_ARTIFACT_NODE: 'Select an artifact node',
  SELECT_ARTIFACT_NODE_GROUP: 'Select an artifact node group',

  SELECT_OR_LOAD_ARTIFACT: 'Select or load a artifact',
  SELECT_OR_LOAD_ARTIFACT_SUMMARY: 'Select or load a artifact summary',

  SELECT_ARTIFACT_SUMMARY: 'Select an artifact summary',
  SELECT_CANCELED_ARTIFACT_SUMMARY: 'Select an canceled artifact summary',

  UPDATE_ARTIFACT: 'Update a artifact',
  UPDATE_ARTIFACT_SUCCESS: 'Update a artifact success',
  UPDATE_ARTIFACT_FAIL: 'Update a artifact fail',

  CREATE_ARTIFACT: 'Craete a artifact',
  CREATE_ARTIFACT_SUCCESS: 'Craete a artifact success',
  CREATE_ARTIFACT_FAIL: 'Craete a artifact fail',

  DELETE_ARTIFACT: 'Delete a artifact',
  DELETE_ARTIFACT_SUCCESS: 'Delete a artifact success',
  DELETE_ARTIFACT_FAIL: 'Delete a artifact fail',

  UPDATE_ARTIFACT_NODE: 'Update a artifact node',
  UPDATE_ARTIFACT_NODE_SUCCESS: 'Update a artifact node success',
  UPDATE_ARTIFACT_NODE_FAIL: 'Update a artifact node fail',

  CREATE_ARTIFACT_NODE: 'Craete a artifact node',
  CREATE_ARTIFACT_NODE_SUCCESS: 'Craete a artifact node success',
  CREATE_ARTIFACT_NODE_FAIL: 'Craete a artifact node fail',

  UPDATE_ARTIFACT_NODE_GROUP: 'Update a artifact node group',
  UPDATE_ARTIFACT_NODE_GROUP_SUCCESS: 'Update a artifact node group success',
  UPDATE_ARTIFACT_NODE_GROUP_FAIL: 'Update a artifact node group fail',

  CREATE_ARTIFACT_NODE_GROUP: 'Craete a artifact node group',
  CREATE_ARTIFACT_NODE_GROUP_SUCCESS: 'Craete a artifact node group success',
  CREATE_ARTIFACT_NODE_GROUP_FAIL: 'Craete a artifact node group fail',

  DELETE_ARTIFACT_NODE_GROUP: 'Delete a artifact node group',
  DELETE_ARTIFACT_NODE_GROUP_SUCCESS: 'Delete a artifact node group success',
  DELETE_ARTIFACT_NODE_GROUP_FAIL: 'Delete a artifact node group fail',

  DELETE_ARTIFACT_NODE: 'Delete a artifact node',
  DELETE_ARTIFACT_NODE_SUCCESS: 'Delete a artifact node success',
  DELETE_ARTIFACT_NODE_FAIL: 'Delete a artifact node fail',

  CREATE_ARTIFACT_ASSIGNMENT: 'Craete a artifact assignment',
  CREATE_ARTIFACT_ASSIGNMENT_SUCCESS: 'Craete a artifact assignment success',
  CREATE_ARTIFACT_ASSIGNMENT_FAIL: 'Craete a artifact assignment fail',

  DELETE_ARTIFACT_ATTACHMENT: 'Delete a artifact attachment',
  DELETE_ARTIFACT_ATTACHMENT_SUCCESS: 'Delete a artifact attachment success',
  DELETE_ARTIFACT_ATTACHMENT_FAIL: 'Delete a artifact attachment fail',

  CLEAR_ARTIFACT_SUMMARY: 'Clear artifact summary'
};

export class ClearArtifactSummaryAction implements Action {
  type = ActionTypes.CLEAR_ARTIFACT_SUMMARY;
  constructor(public payload: any) { }
}

export class RemoveArtifactAction implements Action {
  type = ActionTypes.REMOVE_ARTIFACT;
  constructor(public payload: Artifact) { }
}

export class LoadArtifactAction implements Action {
  type = ActionTypes.LOAD_ARTIFACTS;
  constructor(public payload: any) { }
}

export class LoadArtifactSuccessAction implements Action {
  type = ActionTypes.LOAD_ARTIFACTS_SUCCESS;
  constructor(public payload: Artifact) { }
}

export class LoadArtifactFailAction implements Action {
  type = ActionTypes.LOAD_ARTIFACTS_FAIL;
  constructor(public payload: any) { }
}
export class LoadCanceledArtifactSummaryAction implements Action {
  type = ActionTypes.LOAD_CANCELED_ARTIFACTS_SUMMARY;
  constructor(public payload: number) { }
}
export class LoadCanceledArtifactSummarySuccessAction implements Action {
  type = ActionTypes.LOAD_CANCELED_ARTIFACTS_SUMMARY_SUCCESS;
  constructor(public payload: Array<ArtifactSummary>) { }
}

export class LoadCanceledArtifactSummaryFailAction implements Action {
  type = ActionTypes.LOAD_CANCELED_ARTIFACTS_SUMMARY_FAIL;
  constructor(public payload: any) { }
}

export class LoadArtifactSummaryAction implements Action {
  type = ActionTypes.LOAD_ARTIFACTS_SUMMARY;
  constructor(public payload: number) { }
}

export class LoadArtifactSummarySuccessAction implements Action {
  type = ActionTypes.LOAD_ARTIFACTS_SUMMARY_SUCCESS;
  constructor(public payload: Array<ArtifactSummary>) { }
}

export class LoadArtifactSummaryFailAction implements Action {
  type = ActionTypes.LOAD_ARTIFACTS_SUMMARY_FAIL;
  constructor(public payload: any) { }
}

export class SelectArtifactAction implements Action {
  type = ActionTypes.SELECT_ARTIFACT;
  constructor(public payload: number) { } // payload is id, so type is number
}

export class SelectOrLoadArtifactAction implements Action {
  type = ActionTypes.SELECT_OR_LOAD_ARTIFACT;
  constructor(public payload: any) { }
}

export class SelectOrLoadArtifactSummaryAction implements Action {
  type = ActionTypes.SELECT_OR_LOAD_ARTIFACT_SUMMARY;
  constructor(public payload: any) { }
}

export class SelectArtifactSummaryAction implements Action {
  type = ActionTypes.SELECT_ARTIFACT_SUMMARY;
  constructor(public payload: any) { } 
}
export class SelectCanceledArtifactSummaryAction implements Action {
  type = ActionTypes.SELECT_CANCELED_ARTIFACT_SUMMARY;
  constructor(public payload: any) { } 
}


export class UpdateArtifactAction implements Action {
  type = ActionTypes.UPDATE_ARTIFACT;
  constructor(public payload: Artifact) { }
}

export class UpdateArtifactSuccessAction implements Action {
  type = ActionTypes.UPDATE_ARTIFACT_SUCCESS;
  constructor(public payload: Artifact) { }
}

export class UpdateArtifactFailAction implements Action {
  type = ActionTypes.UPDATE_ARTIFACT_FAIL;
  constructor(public payload: any) { }
}

export class CreateArtifactAction implements Action {
  type = ActionTypes.CREATE_ARTIFACT;
  constructor(public payload: Artifact) { }
}

export class CreateArtifactSuccessAction implements Action {
  type = ActionTypes.CREATE_ARTIFACT_SUCCESS;
  constructor(public payload: Artifact) { }
}

export class CreateArtifactFailAction implements Action {
  type = ActionTypes.CREATE_ARTIFACT_FAIL;
  constructor(public payload: any) { }
}

export class DeleteArtifactAction implements Action {
  type = ActionTypes.DELETE_ARTIFACT;
  constructor(public payload: Artifact) { }
}

export class DeleteArtifactSuccessAction implements Action {
  type = ActionTypes.DELETE_ARTIFACT_SUCCESS;
  constructor(public payload: Artifact) { }
}

export class DeleteArtifactFailAction implements Action {
  type = ActionTypes.DELETE_ARTIFACT_FAIL;
  constructor(public payload: any) { }
}

export class UpdateArtifactNodeAction implements Action {
  type = ActionTypes.UPDATE_ARTIFACT_NODE;
  constructor(public payload: ArtifactNode) { }
}

export class UpdateArtifactNodeSuccessAction implements Action {
  type = ActionTypes.UPDATE_ARTIFACT_NODE_SUCCESS;
  constructor(public payload: Artifact) { }
}

export class UpdateArtifactNodeFailAction implements Action {
  type = ActionTypes.UPDATE_ARTIFACT_NODE_FAIL;
  constructor(public payload: any) { }
}

export class CreateArtifactNodeAction implements Action {
  type = ActionTypes.CREATE_ARTIFACT_NODE;
  constructor(public payload: ArtifactNode) { }
}

export class CreateArtifactNodeSuccessAction implements Action {
  type = ActionTypes.CREATE_ARTIFACT_NODE_SUCCESS;
  constructor(public payload: Artifact) { }
}

export class CreateArtifactNodeFailAction implements Action {
  type = ActionTypes.CREATE_ARTIFACT_NODE_FAIL;
  constructor(public payload: any) { }
}

export class UpdateArtifactNodeGroupAction implements Action {
  type = ActionTypes.UPDATE_ARTIFACT_NODE_GROUP;
  constructor(public payload: ArtifactNodeGroup) { }
}

export class UpdateArtifactNodeGroupSuccessAction implements Action {
  type = ActionTypes.UPDATE_ARTIFACT_NODE_GROUP_SUCCESS;
  constructor(public payload: Artifact) { }
}

export class UpdateArtifactNodeGroupFailAction implements Action {
  type = ActionTypes.UPDATE_ARTIFACT_NODE_GROUP_FAIL;
  constructor(public payload: any) { }
}

export class CreateArtifactNodeGroupAction implements Action {
  type = ActionTypes.CREATE_ARTIFACT_NODE_GROUP;
  constructor(public payload: ArtifactNodeGroup) { }
}

export class CreateArtifactNodeGroupSuccessAction implements Action {
  type = ActionTypes.CREATE_ARTIFACT_NODE_GROUP_SUCCESS;
  constructor(public payload: Artifact) { }
}

export class CreateArtifactNodeGroupFailAction implements Action {
  type = ActionTypes.CREATE_ARTIFACT_NODE_GROUP_FAIL;
  constructor(public payload: any) { }
}

export class DeleteArtifactNodeAction implements Action {
  type = ActionTypes.DELETE_ARTIFACT_NODE;
  constructor(public payload: any) { }
}

export class DeleteArtifactNodeSuccessAction implements Action {
  type = ActionTypes.DELETE_ARTIFACT_NODE_SUCCESS;
  constructor(public payload: any) { }
}

export class DeleteArtifactNodeFailAction implements Action {
  type = ActionTypes.DELETE_ARTIFACT_NODE_FAIL;
  constructor(public payload: any) { }
}

export class DeleteArtifactNodeGroupAction implements Action {
  type = ActionTypes.DELETE_ARTIFACT_NODE_GROUP;
  constructor(public payload: any) { }
}

export class DeleteArtifactNodeGroupSuccessAction implements Action {
  type = ActionTypes.DELETE_ARTIFACT_NODE_GROUP_SUCCESS;
  constructor(public payload: any) { }
}

export class DeleteArtifactNodeGroupFailAction implements Action {
  type = ActionTypes.DELETE_ARTIFACT_NODE_GROUP_FAIL;
  constructor(public payload: any) { }
}

export class CreateArtifactAssignmentAction implements Action {
  type = ActionTypes.CREATE_ARTIFACT_ASSIGNMENT;
  constructor(public payload: any) { }
}

export class CreateArtifactAssignmentSuccessAction implements Action {
  type = ActionTypes.CREATE_ARTIFACT_ASSIGNMENT_SUCCESS;
  constructor(public payload: any) { }
}

export class CreateArtifactAssignmentFailAction implements Action {
  type = ActionTypes.CREATE_ARTIFACT_ASSIGNMENT_FAIL;
  constructor(public payload: any) { }
}

export class DeleteArtifactAttachmentAction implements Action {
  type = ActionTypes.DELETE_ARTIFACT_ATTACHMENT;
  constructor(public payload: any) { }
}

export class DeleteArtifactAttachmentSuccessAction implements Action {
  type = ActionTypes.DELETE_ARTIFACT_ATTACHMENT_SUCCESS;
  constructor(public payload: any) { }
}

export class DeleteArtifactAttachmentFailAction implements Action {
  type = ActionTypes.DELETE_ARTIFACT_ATTACHMENT_FAIL;
  constructor(public payload: any) { }
}

export type Actions = 
  RemoveArtifactAction |
  LoadArtifactAction |
  LoadArtifactSuccessAction |
  LoadArtifactFailAction |
  CreateArtifactAssignmentAction |
  CreateArtifactAssignmentFailAction |
  CreateArtifactAssignmentSuccessAction |
  SelectArtifactAction |
  UpdateArtifactAction |
  UpdateArtifactSuccessAction |
  UpdateArtifactFailAction |
  CreateArtifactAction |
  CreateArtifactSuccessAction |
  CreateArtifactFailAction |
  DeleteArtifactAction |
  DeleteArtifactSuccessAction |
  DeleteArtifactFailAction |
  UpdateArtifactNodeAction |
  UpdateArtifactNodeSuccessAction |
  UpdateArtifactNodeFailAction |
  CreateArtifactNodeAction |
  CreateArtifactNodeSuccessAction |
  CreateArtifactNodeFailAction |
  UpdateArtifactNodeGroupAction |
  UpdateArtifactNodeGroupSuccessAction |
  UpdateArtifactNodeGroupFailAction |
  CreateArtifactNodeGroupAction |
  CreateArtifactNodeGroupSuccessAction |
  CreateArtifactNodeGroupFailAction|
  DeleteArtifactNodeGroupAction |
  DeleteArtifactNodeGroupSuccessAction |
  DeleteArtifactNodeGroupFailAction|
  DeleteArtifactNodeAction |
  DeleteArtifactNodeSuccessAction |
  DeleteArtifactNodeFailAction|
  DeleteArtifactAttachmentAction|
  DeleteArtifactAttachmentSuccessAction|
  DeleteArtifactAttachmentFailAction;
