import { Action } from '@ngrx/store';
import { Ticket } from '../models/ticket.model';

export const ActionTypes = {
  CLOSE_TICKET: 'Close ticket',
  CLOSE_TICKET_SUCCESS: 'Close ticket success',
  CLOSE_TICKET_FAIL: 'Close ticket fail',

  LOAD_TICKETS: 'Load ticket',
  LOAD_TICKETS_SUCCESS: 'Load ticket success',
  LOAD_TICKETS_FAIL: 'Load ticket fail',

  LOAD_TICKET_ENTRY: 'Load ticket entry',
  LOAD_TICKET_ENTRY_SUCCESS: 'Load ticket entry success',
  LOAD_TICKET_ENTRY_FAIL: 'Load ticket entry fail',

  SELECT_TICKET: 'Select ticket',
  SELECT_TICKET_SUCCESS: 'Select ticket success',
  SELECT_TICKET_FAIL: 'Select ticket fail',

  SELECT_OR_LOAD_TICKET: 'Select or load ticket',
  SELECT_OR_LOAD_TICKET_SUCCESS: 'Select or load ticket success',
  SELECT_OR_LOAD_TICKET_FAIL: 'Select or load ticket fail',
};

export class CloseTicketAction implements Action {
  type = ActionTypes.CLOSE_TICKET;
  constructor(public payload: any) { }
}

export class CloseTicketSuccessAction implements Action {
  type = ActionTypes.CLOSE_TICKET_SUCCESS;
  constructor(public payload: Ticket) { }
}

export class CloseTicketFailAction implements Action {
  type = ActionTypes.CLOSE_TICKET_FAIL;
  constructor(public payload: any) { }
}

export class LoadTicketsAction implements Action {
  type = ActionTypes.LOAD_TICKETS;
  constructor(public payload: any) { }
}

export class LoadTicketsSuccessAction implements Action {
  type = ActionTypes.LOAD_TICKETS_SUCCESS;
  constructor(public payload: Array<Ticket>) { } // payload is id, so type is number
}

export class LoadTicketsFailAction implements Action {
  type = ActionTypes.LOAD_TICKETS_FAIL;
  constructor(public payload: any) { }
}

export class LoadTicketEntryAction implements Action {
  type = ActionTypes.LOAD_TICKET_ENTRY;
  constructor(public payload: any) { }
}

export class LoadTicketEntrySuccessAction implements Action {
  type = ActionTypes.LOAD_TICKET_ENTRY_SUCCESS;
  constructor(public payload: Ticket) { } // payload is id, so type is number
}

export class LoadTicketEntryFailAction implements Action {
  type = ActionTypes.LOAD_TICKET_ENTRY_FAIL;
  constructor(public payload: any) { }
}

export class SelectTicketAction implements Action {
  type = ActionTypes.SELECT_TICKET;
  constructor(public payload: number) { }
}

export class SelectTicketSuccessAction implements Action {
  type = ActionTypes.SELECT_TICKET_SUCCESS;
  constructor(public payload: Ticket) { }
}

export class SelectTicketFailAction implements Action {
  type = ActionTypes.SELECT_TICKET_FAIL;
  constructor(public payload: any) { }
}

export class SelectOrLoadTicketAction implements Action {
  type = ActionTypes.SELECT_OR_LOAD_TICKET;
  constructor(public payload: number) { }
}

export class SelectOrLoadTicketSuccessAction implements Action {
  type = ActionTypes.SELECT_OR_LOAD_TICKET_SUCCESS;
  constructor(public payload: Ticket) { }
}

export class SelectOrLoadTicketFailAction implements Action {
  type = ActionTypes.SELECT_OR_LOAD_TICKET_FAIL;
  constructor(public payload: any) { }
}

export type Actions =
  CloseTicketAction |
  CloseTicketSuccessAction |
  CloseTicketFailAction |
  LoadTicketsAction |
  LoadTicketsSuccessAction |
  LoadTicketsFailAction |
  SelectTicketAction |
  SelectTicketSuccessAction |
  SelectTicketFailAction|
  SelectOrLoadTicketAction|
  SelectOrLoadTicketSuccessAction|
  SelectOrLoadTicketFailAction;
