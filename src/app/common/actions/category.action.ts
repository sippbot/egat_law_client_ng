import { Action } from '@ngrx/store';
import { Category } from '../models/category.model';

export const ActionTypes = {
  REMOVE_CATEGORY: 'Remove a category',
  LOAD_CATEGORIES: 'Load a category',
  LOAD_CATEGORIES_SUCCESS: 'Load a category success',
  LOAD_CATEGORIES_FAIL: 'Load a category fail',
  SELECT_CATEGORY: 'Select a category',
  UPDATE_CATEGORY: 'Update a category',
  UPDATE_CATEGORY_SUCCESS: 'Update a category success',
  UPDATE_CATEGORY_FAIL: 'Update a category fail',
  CREATE_CATEGORY: 'Craete a category',
  CREATE_CATEGORY_SUCCESS: 'Craete a category success',
  CREATE_CATEGORY_FAIL: 'Craete a category fail',

  DELETE_CATEGORY: 'Delete category',
  DELETE_CATEGORY_SUCCESS: 'Delete category success',
  DELETE_CATEGORY_FAIL: 'Delete category fail',
};

export class RemoveCategoryAction implements Action {
  type = ActionTypes.REMOVE_CATEGORY;
  constructor(public payload: Category) { }
}

export class LoadCategoryAction implements Action {
  type = ActionTypes.LOAD_CATEGORIES;
  constructor(public payload: any) { }
}

export class LoadCategorySuccessAction implements Action {
  type = ActionTypes.LOAD_CATEGORIES_SUCCESS;
  constructor(public payload: Array<Category>) {
    console.log(payload);
  }
}

export class LoadCategoryFailAction implements Action {
  type = ActionTypes.LOAD_CATEGORIES_FAIL;
  constructor(public payload: any) { }
}

export class SelectCategoryAction implements Action {
  type = ActionTypes.SELECT_CATEGORY;
  constructor(public payload: number) { } // payload is id, so type is number
}

export class UpdateCategoryAction implements Action {
  type = ActionTypes.UPDATE_CATEGORY;
  constructor(public payload: Category) { }
}

export class UpdateCategorySuccessAction implements Action {
  type = ActionTypes.UPDATE_CATEGORY_SUCCESS;
  constructor(public payload: Category) { }
}

export class UpdateCategoryFailAction implements Action {
  type = ActionTypes.UPDATE_CATEGORY_FAIL;
  constructor(public payload: any) { }
}

export class CreateCategoryAction implements Action {
  type = ActionTypes.CREATE_CATEGORY;
  constructor(public payload: Category) { }
}

export class CreateCategorySuccessAction implements Action {
  type = ActionTypes.CREATE_CATEGORY_SUCCESS;
  constructor(public payload: Category) { }
}

export class CreateCategoryFailAction implements Action {
  type = ActionTypes.CREATE_CATEGORY_FAIL;
  constructor(public payload: any) { }
}

export class DeleteCategoryAction implements Action {
  type = ActionTypes.DELETE_CATEGORY;
  constructor(public payload: number) { }
}

export class DeleteCategorySuccessAction implements Action {
  type = ActionTypes.DELETE_CATEGORY_SUCCESS;
  constructor(public payload: number) { }
}

export class DeleteCategoryFailAction implements Action {
  type = ActionTypes.DELETE_CATEGORY_FAIL;
  constructor(public payload: any) { }
}

export type Actions = RemoveCategoryAction |
  LoadCategoryAction |
  LoadCategorySuccessAction |
  LoadCategoryFailAction |
  SelectCategoryAction |
  UpdateCategoryAction |
  UpdateCategorySuccessAction |
  UpdateCategoryFailAction |
  CreateCategoryAction |
  CreateCategorySuccessAction |
  CreateCategoryFailAction|
  DeleteCategoryAction|
  DeleteCategorySuccessAction|
  DeleteCategoryFailAction;
