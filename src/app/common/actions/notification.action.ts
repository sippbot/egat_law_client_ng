import { MailNotification } from '../models/mail-notification.model';
import { License } from '../models/license.model';
import { Action } from '@ngrx/store';

export const ActionTypes = {
    LOAD_NOTIFICATION: 'Load notification list',
    LOAD_NOTIFICATION_SUCCESS: 'Load notification list success',
    LOAD_NOTIFICATION_FAIL: 'Load notification list fail',

    RESEND_NOTIFICATION: 'Resend notification',
    RESEND_NOTIFICATION_SUCCESS: 'Resend notification success',
    RESEND_NOTIFICATION_FAIL: 'Resend notification fail',
};

export class LoadNotificationAction implements Action {
    type = ActionTypes.LOAD_NOTIFICATION;
    constructor(public payload: number) { }
}

export class LoadNotificationSuccessAction implements Action {
    type = ActionTypes.LOAD_NOTIFICATION_SUCCESS;

    constructor(public payload: MailNotification[]) { }
}

export class LoadNotificationFailAction implements Action {
    type = ActionTypes.LOAD_NOTIFICATION_FAIL;
    constructor(public payload: any) { }
}

export class ResendNotificationAction implements Action {
    type = ActionTypes.RESEND_NOTIFICATION;
    constructor(public payload: number) { }
}

export class ResendNotificationSuccessAction implements Action {
    type = ActionTypes.RESEND_NOTIFICATION_SUCCESS;
    constructor(public payload: number) { }
}

export class ResendNotificationFailAction implements Action {
    type = ActionTypes.RESEND_NOTIFICATION_FAIL;
    constructor(public payload: any) { }
}

export type Actions =
    LoadNotificationAction |
    LoadNotificationSuccessAction |
    LoadNotificationFailAction |
    ResendNotificationAction |
    ResendNotificationSuccessAction |
    ResendNotificationFailAction;