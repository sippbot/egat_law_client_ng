import {Action} from '@ngrx/store';
import {Post} from '../models/post.model';
import {Department} from '../models/department.model';

export const ActionTypes = {
    SELECT_POST: 'Select a User by department id',

    LOAD_POST: 'Load a Post data by department id',
    LOAD_POST_SUCCESS: 'Load a Post data  success',
    LOAD_POST_FAIL: 'Load a Post data  fail',

    CREATE_POST: 'Create an user in department list',
    CREATE_POST_SUCCESS: 'Create an user in department list success',
    CREATE_POST_FAIL: 'Create an user in department list fail',

    REMOVE_POST: 'Remove an user in department list',
    REMOVE_POST_SUCCESS: 'Remove an user in department list success',
    REMOVE_POST_FAIL: 'Remove an user in department list fail',
};

// This action use to get user by department id for update or delete
export class SelectPostAction implements Action {
    type = ActionTypes.SELECT_POST;

    constructor(public payload: number) {
    } // payload is department id, so type is number
}

// This action use to get post data by department id
export class LoadPostAction implements Action {
    type = ActionTypes.LOAD_POST;

    constructor(public payload: number) {
    } // payload is department id, so type is number
}

export class LoadPostSuccessAction implements Action {
    type = ActionTypes.LOAD_POST_SUCCESS;

    constructor(public payload: Array<Post>) {
    } // payload is department id, so type is number
}

export class LoadPostFailAction implements Action {
    type = ActionTypes.LOAD_POST_FAIL;

    constructor(public payload: any) {
    } // payload is department id, so type is number
}

// This action use to post user
export class CreatePostAction implements Action {
    type = ActionTypes.CREATE_POST;

    constructor(public payload: Post) {
    }
}

export class CreatePostSuccessAction implements Action {
    type = ActionTypes.CREATE_POST_SUCCESS;

    constructor(public payload: Post) {
    }
}

export class CreatePostFailAction implements Action {
    type = ActionTypes.CREATE_POST_FAIL;

    constructor(public payload: any) {
    }
}

// This action use to remove post user
export class RemovePostAction implements Action {
    type = ActionTypes.REMOVE_POST;

    constructor(public payload: Post) {
    }
}

export class RemovePostSuccessAction implements Action {
    type = ActionTypes.REMOVE_POST_SUCCESS;

    constructor(public payload: Post) {
    }
}

export class RemovePostFailAction implements Action {
    type = ActionTypes.REMOVE_POST_FAIL;

    constructor(public payload: any) {
    }
}

export type Actions =
    SelectPostAction |
    LoadPostAction |
    LoadPostSuccessAction |
    LoadPostFailAction |
    CreatePostAction |
    CreatePostSuccessAction |
    CreatePostFailAction |
    RemovePostAction |
    RemovePostSuccessAction |
    RemovePostFailAction;
