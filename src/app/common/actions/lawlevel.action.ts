import { Action } from '@ngrx/store';
import { Lawlevel } from '../models/lawlevel.model';

export const ActionTypes = {
  REMOVE_LAWLEVEL: 'Remove an Law Level',
  LOAD_LAWLEVEL: 'Load an Law Level',
  LOAD_LAWLEVEL_SUCCESS: 'Load an Law Level success',
  LOAD_LAWLEVEL_FAIL: 'Load an Law Level fail',
  SELECT_LAWLEVEL: 'Select a Law Level',
  UPDATE_LAWLEVEL: 'Update a Law Level',
  UPDATE_LAWLEVEL_SUCCESS: 'Update a Law Level success',
  UPDATE_LAWLEVEL_FAIL: 'Update a Law Level fail',
  CREATE_LAWLEVEL: 'Craete a Law Level',
  CREATE_LAWLEVEL_SUCCESS: 'Craete a Law Level success',
  CREATE_LAWLEVEL_FAIL: 'Craete a Law Level fail',
  DELETE_LAWLEVEL: 'Delete a Law Level',
  DELETE_LAWLEVEL_SUCCESS: 'Delete a Law Level success',
  DELETE_LAWLEVEL_FAIL: 'Delete a Law Level fail'
};

export class RemoveLawlevelAction implements Action {
  type = ActionTypes.REMOVE_LAWLEVEL;
  constructor(public payload: Lawlevel) { }
}

export class LoadLawlevelAction implements Action {
  type = ActionTypes.LOAD_LAWLEVEL;
  constructor(public payload: any) { }
}

export class LoadLawlevelSuccessAction implements Action {
  type = ActionTypes.LOAD_LAWLEVEL_SUCCESS;
  constructor(public payload: Array<Lawlevel>) { }
}

export class LoadLawlevelFailAction implements Action {
  type = ActionTypes.LOAD_LAWLEVEL_FAIL;
  constructor(public payload: any) { }
}

export class SelectLawlevelAction implements Action {
  type = ActionTypes.SELECT_LAWLEVEL;
  constructor(public payload: number) { } // payload is id, so type is number
}

export class UpdateLawlevelAction implements Action {
  type = ActionTypes.UPDATE_LAWLEVEL;
  constructor(public payload: Lawlevel) { }
}

export class UpdateLawlevelSuccessAction implements Action {

  type = ActionTypes.UPDATE_LAWLEVEL_SUCCESS;
  constructor(public payload: Lawlevel) { }
}

export class UpdateLawlevelFailAction implements Action {
  type = ActionTypes.UPDATE_LAWLEVEL_FAIL;
  constructor(public payload: any) { }
}

export class CreateLawlevelAction implements Action {
  type = ActionTypes.CREATE_LAWLEVEL;
  constructor(public payload: Lawlevel) { }
}

export class CreateLawlevelSuccessAction implements Action {
  type = ActionTypes.CREATE_LAWLEVEL_SUCCESS;
  constructor(public payload: Lawlevel) { }
}

export class CreateLawlevelFailAction implements Action {
  type = ActionTypes.CREATE_LAWLEVEL_FAIL;
  constructor(public payload: any) { }
}

export class DeleteLawlevelAction implements Action {
  type = ActionTypes.DELETE_LAWLEVEL;
  constructor(public payload: number) { }
}

export class DeleteLawlevelSuccessAction implements Action {
  type = ActionTypes.DELETE_LAWLEVEL_SUCCESS;
  constructor(public payload: number) { }
}

export class DeleteLawlevelFailAction implements Action {
  type = ActionTypes.DELETE_LAWLEVEL_FAIL;
  constructor(public payload: any) { }
}

export type Actions =
  RemoveLawlevelAction |
  LoadLawlevelAction |
  LoadLawlevelSuccessAction |
  LoadLawlevelFailAction |
  SelectLawlevelAction |
  UpdateLawlevelAction |
  UpdateLawlevelSuccessAction |
  UpdateLawlevelFailAction |
  CreateLawlevelAction |
  CreateLawlevelSuccessAction |
  CreateLawlevelFailAction |
  DeleteLawlevelAction |
  DeleteLawlevelSuccessAction |
  DeleteLawlevelFailAction;

