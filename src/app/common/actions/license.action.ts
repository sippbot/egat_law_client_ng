import { License } from '../models/license.model';
import { Action } from '@ngrx/store';

export const ActionTypes = {
  LOAD_LICENSES: 'Load an License',
  LOAD_LICENSES_SUCCESS: 'Load an License success',
  LOAD_LICENSES_FAIL: 'Load an License fail',

  CREATE_LICENSE: 'Craete a License',
  CREATE_LICENSE_SUCCESS: 'Craete a License success',
  CREATE_LICENSE_FAIL: 'Craete a License fail',

  RENEW_LICENSE: 'Renew a License',
  RENEW_LICENSE_SUCCESS: 'Renew a License success',
  RENEW_LICENSE_FAIL: 'Renew a License fail',

  SELECT_LICENSE: 'Select a License'
};

export class LoadLicensesAction implements Action {
  type = ActionTypes.LOAD_LICENSES;
  constructor(public payload: number) { }
}

export class LoadLicensesSuccessAction implements Action {
  type = ActionTypes.LOAD_LICENSES_SUCCESS;

  constructor(public payload: License[]) { }
}

export class LoadLicensesFailAction implements Action {
  type = ActionTypes.LOAD_LICENSES_FAIL;
  constructor(public payload: any) { }
}

export class CreateLicenseAction implements Action {
  type = ActionTypes.CREATE_LICENSE;
  constructor(public payload: License) { }
}

export class CreateLicenseSuccessAction implements Action {
  type = ActionTypes.CREATE_LICENSE_SUCCESS;
  constructor(public payload: License) { }
}

export class CreateLicenseFailAction implements Action {
  type = ActionTypes.CREATE_LICENSE_FAIL;
  constructor(public payload: any) { }
}

export class RenewLicenseAction implements Action {
  type = ActionTypes.RENEW_LICENSE;
  constructor(public payload: License) { }
}

export class RenewLicenseSuccessAction implements Action {
  type = ActionTypes.RENEW_LICENSE_SUCCESS;
  constructor(public payload: License) { }
}

export class RenewLicenseFailAction implements Action {
  type = ActionTypes.RENEW_LICENSE_FAIL;
  constructor(public payload: any) { }
}

export class SelectLicenseAction implements Action {
  type = ActionTypes.SELECT_LICENSE;
  constructor(public payload: any) { }
}

export type Actions =
  LoadLicensesAction |
  LoadLicensesSuccessAction |
  LoadLicensesFailAction |
  CreateLicenseAction |
  CreateLicenseSuccessAction |
  CreateLicenseFailAction |
  RenewLicenseAction |
  RenewLicenseSuccessAction |
  RenewLicenseFailAction |
  SelectLicenseAction;
