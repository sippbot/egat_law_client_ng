import '@ngrx/core/add/operator/select';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/let';
import { Observable } from 'rxjs/Observable';
import * as users from '../actions/users.action';
import { User } from '../models/user.model';

/* From a simple array ( [] ), the state becomes a object where the array is contained withing the entities property */
export interface State {
  userEntities: Array<User>;
  selectedUser: User;
  currentUser: User;
};

const initialState: State = {
  userEntities: [],
  selectedUser: null,
  currentUser: null
};
/* Instead of using a constant of type ActionReducer, the function is directly exported */
export function reducer(state = initialState, action: users.Actions): State {
  console.log(action);
  switch (action.type) {
    case users.ActionTypes.REMOVE_USER:
      return Object.assign({}, state, {
        userEntities: state.userEntities.filter(user =>
          user.id !== action.payload.id)
      });

    case users.ActionTypes.LOAD_USER_LOGIN_SUCCESS:
      return Object.assign({}, state, {
        currentUser: action.payload
      });

    case users.ActionTypes.LOAD_USERS_SUCCESS:
      return Object.assign({}, state, {
        userEntities: action.payload
      });

    case users.ActionTypes.SELECT_USERS:
      const selected = state.userEntities.find(item => { return item.id === action.payload; });
      console.log(selected);
      return Object.assign({}, state, {
        selectedUser: selected
      });

    case users.ActionTypes.UPDATE_USERS_SUCCESS:
      return Object.assign({}, state, {
        userEntities: state.userEntities.map(item => {
          return item.id === action.payload.id ? Object.assign({}, item, action.payload) : item;
        }),
        selectedUser: null
      });

    case users.ActionTypes.CREATE_USERS_SUCCESS:
      const user: User = action.payload;
      /* Because the state is now an object instead of an array, the return statements of the reducer have to be adapted. */
      return Object.assign({}, state, {
        userEntities: [...state.userEntities, user],
        selectedUser: null
      });

    case users.ActionTypes.DELETE_USERS:
      return Object.assign({}, state, {
        userEntities: state.userEntities.filter(
          group => group.id !== action.payload),
        selectedUser: null
      });

    default:
      return state;
  }
};

export function getUserEntities(state$: Observable<State>) {
  return state$.select(s => s.userEntities);
}

export function getSelectedUser(state$: Observable<State>) {
  return state$.select(s => s.selectedUser);
}

export function getLoginUser(state$: Observable<State>) {
  return state$.select(s => s.currentUser);
}
