import { ActionTypes } from '../actions/post.action';
import '@ngrx/core/add/operator/select';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/let';
import { Observable } from 'rxjs/Observable';
import * as user_permission from '../actions/user-permissions.action';
import { UserPermission } from '../models/user-permissions.model';
import { Role } from '../models/role.model';

/* From a simple array ( [] ), the state becomes a object where the array is contained withing the entities property */
export interface State {
  roleEntities: Array<Role>;
  selectedRole: Role;
};

const initialState: State = {
  roleEntities: [],
  selectedRole: null
};
/* Instead of using a constant of type ActionReducer, the function is directly exported */
export function reducer(state = initialState, action: user_permission.Actions): State {
  console.log(action);
  switch (action.type) {

    case user_permission.ActionTypes.UPDATE_ROLE_SUCCESS:
      return Object.assign({}, state, {
        roleEntities: state.roleEntities.map(item => {
          return item.id === action.payload.id ? Object.assign({}, item, action.payload) : item;
        }),
        selectedRole: null
      });

    case user_permission.ActionTypes.CREATE_ROLE_SUCCESS:
      const role: Role = action.payload;
      /* Because the state is now an object instead of an array, the return statements of the reducer have to be adapted. */
      return {
        roleEntities: [...state.roleEntities, role],
        selectedRole: null
      };

    case user_permission.ActionTypes.DELETE_ROLE_SUCCESS:
      return Object.assign({}, state, {
        roleEntities: state.roleEntities.filter(
          role => role.id !== action.payload),
          selectedRole: null
      });

    default:
      return state;
  }
};


export function getRoleEntities(state$: Observable<State>) {
  return state$.select(s => s.roleEntities);
}

export function getSelectedRole(state$: Observable<State>) {
  return state$.select(s => s.selectedRole);
}
