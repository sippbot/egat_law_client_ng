import { Document } from '../models/document.model';
import { Observable } from 'rxjs';
import { TaskComment } from '../models/task-assignment-comment.model';
import { TaskAssignmentSummary } from '../models/task-assignment-summary.model';
import { TaskAssessment } from '../models/task-assessment.model';
import { TaskAssignment } from '../models/task-assignment.model';
import '@ngrx/core/add/operator/select';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/let';
import * as taskAssignments from '../actions/task-assignment.action';

/* From a simple array ( [] ), the state becomes a object where the array is contained withing the entities property */
export interface State {
    taskAssignmentEntities: Array<TaskAssignment>;
    selectedTaskAssignment: TaskAssignment;
    taskAssessmentEntities: Array<TaskAssessment>;
    taskAssignmentSummaryEntities: Array<TaskAssignmentSummary>;
    taskCommentEntities: Array<TaskComment>;
    taskAssignmentDocumentEntities: Array<Document>;
    // selectedDepartmentId: number;
    // SelectedArtifactId: number;
};

const initialState: State = {
    taskAssignmentEntities: [],
    selectedTaskAssignment: null,
    taskAssessmentEntities: [],
    taskAssignmentSummaryEntities: [],
    taskCommentEntities: [],
    taskAssignmentDocumentEntities: []
    // selectedDepartmentId: 0,
    // SelectedArtifactId: 0
};


export function reducer(state = initialState, action: taskAssignments.Actions): State {
    console.log(action)
    switch (action.type) {
        case taskAssignments.ActionTypes.LOAD_TASKASSIGNMENTS_SUCCESS:
            return Object.assign({}, state, {
                taskAssignmentEntities: action.payload
            });
            
        case taskAssignments.ActionTypes.CLEAR_SELECT_TASKASSIGMENTS:
            return Object.assign({}, state, {
                selectedTaskAssignment: null,
                taskAssessmentEntities: [],
                taskCommentEntities: [],
                taskAssignmentDocumentEntities: []
            });

        case taskAssignments.ActionTypes.LOAD_TASKASSIGNMENT_FULLDETAIL_SUCCESS:
            return Object.assign({}, state, {
                selectedTaskAssignment: action.payload,
                taskAssessmentEntities: action.payload.assessments,
                taskCommentEntities: action.payload.comments,
                taskAssignmentDocumentEntities: action.payload.documents
            });

        case taskAssignments.ActionTypes.CREATE_TASKASSESSMENT_SUCCESS:
            return Object.assign({}, state, {
                taskAssessmentEntities: action.payload
            });

        case taskAssignments.ActionTypes.CREATE_TASKASSESSMENT_REVIEW_SUCCESS:
            return Object.assign({}, state, {
                taskAssessmentEntities: action.payload
            });

        case taskAssignments.ActionTypes.CREATE_TASKASSIGNMENT_PROPOSAL_SUCCESS:
            return Object.assign({}, state, {
                taskAssignmentSummaryEntities: action.payload
            });

        case taskAssignments.ActionTypes.CREATE_TASKASSIGNMENT_APPROVAL_SUCCESS:
            return Object.assign({}, state, {
                taskAssignmentSummaryEntities: action.payload
            });

        case taskAssignments.ActionTypes.LOAD_TASKASSIGNMENT_SUMMARY_SUCCESS:
            return Object.assign({}, state, {
                taskAssignmentSummaryEntities: action.payload
            });

        case taskAssignments.ActionTypes.CREATE_TASKCOMMENT_SUCCESS:
            return Object.assign({}, state, {
                taskCommentEntities: [...state.taskCommentEntities, action.payload]
            });

        case taskAssignments.ActionTypes.CREATE_TASKASSIGNMENT_ATTACHMENT_SUCCESS:
            return Object.assign({}, state, {
                taskAssignmentDocumentEntities: [...state.taskAssignmentDocumentEntities, action.payload]
            });

        case taskAssignments.ActionTypes.DELETE_TASKASSIGNMENT_ATTACHMENT_SUCCESS:
            const documents = state.taskAssignmentDocumentEntities.filter(item => item.id !== action.payload);
            return Object.assign({}, state, {
                taskAssignmentDocumentEntities: documents
            });

        default:
            return state;
    }
};

export function getTaskAssignmentEntities(state$: Observable<State>) {
    return state$.select(s => s.taskAssignmentEntities);
}

export function getSelectedTaskAssignment(state$: Observable<State>) {
    return state$.select(s => s.selectedTaskAssignment);
}

export function getTaskAssessmentEntities(state$: Observable<State>) {
    return state$.select(s => s.taskAssessmentEntities);
}

export function getTaskAssessmentSummaryEntities(state$: Observable<State>) {
    return state$.select(s => s.taskAssignmentSummaryEntities);
}

export function getTaskCommentEntities(state$: Observable<State>) {
    return state$.select(s => s.taskCommentEntities);
}

export function getTaskAssignmentDocuments(state$: Observable<State>) {
    return state$.select(s => s.taskAssignmentDocumentEntities);
}

