import '@ngrx/core/add/operator/select';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/let';
import { Observable } from 'rxjs/Observable';
import * as lawlevel from '../actions/lawlevel.action';
import { Lawlevel } from '../models/lawlevel.model';
/* From a simple array ( [] ), the state becomes a object where the array is contained withing the entities property */

export interface State {
  lawlevelEntities: Array<Lawlevel>;
  selectedLawlevel: Lawlevel;
};

const initialState: State = {
  lawlevelEntities: [],
  selectedLawlevel: null
};

/* Instead of using a constant of type ActionReducer, the function is directly exported */
export function reducer(state = initialState, action: lawlevel.Actions): State {
  switch (action.type) {
    case lawlevel.ActionTypes.REMOVE_LAWLEVEL:
      return Object.assign({}, state, {
        lawlevelEntities: state.lawlevelEntities.filter(x =>
          x.id !== action.payload.id)
      });
    case lawlevel.ActionTypes.LOAD_LAWLEVEL_SUCCESS:
      return Object.assign({}, state, {
        lawlevelEntities: action.payload
      });
    case lawlevel.ActionTypes.SELECT_LAWLEVEL:
      const selected = state.lawlevelEntities.find(item => { return item.id === action.payload; });
      console.log(selected);
      return Object.assign({}, state, {
        selectedLawlevel: selected
      });
    case lawlevel.ActionTypes.UPDATE_LAWLEVEL_SUCCESS:
      return Object.assign({}, state, {
        categoryEntities: state.lawlevelEntities.map(item => {
          return item.id === action.payload.id ? Object.assign({}, item, action.payload) : item;
        }),
        selectedLawlevel: null
      });
    case lawlevel.ActionTypes.CREATE_LAWLEVEL_SUCCESS:
      const _lawlevel: Lawlevel = action.payload;
      /* Because the state is now an object instead of an array, the return statements of the reducer have to be adapted. */
      return {
        lawlevelEntities: [...state.lawlevelEntities, _lawlevel],
        selectedLawlevel: null
      };
    case lawlevel.ActionTypes.DELETE_LAWLEVEL_SUCCESS:
      return Object.assign({}, state, {
        lawlevelEntities: state.lawlevelEntities.filter(
          level => level.id !== action.payload),
        selectedLawlevel: null
      });
    default:
      return state;
  }
};
export function getLawlevelEntities(state$: Observable<State>) {
  return state$.select(s => s.lawlevelEntities);
}
export function getSelectedLawlevel(state$: Observable<State>) {
  return state$.select(s => s.selectedLawlevel);
}
