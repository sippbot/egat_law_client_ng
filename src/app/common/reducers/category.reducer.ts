import '@ngrx/core/add/operator/select';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/let';
import { Observable } from 'rxjs/Observable';
import * as category from '../actions/category.action';
import { Category } from "../models/category.model";

/* From a simple array ( [] ), the state becomes a object where the array is contained withing the entities property */
export interface State {
  categoryEntities: Array<Category>,
  selectedCategory: Category
};

const initialState: State = {
  categoryEntities: [],
  selectedCategory: null
};
/* Instead of using a constant of type ActionReducer, the function is directly exported */
export function reducer(state = initialState, action: category.Actions): State {
  console.log(action);
  switch (action.type) {

    case category.ActionTypes.REMOVE_CATEGORY:
      return Object.assign({}, state, {
        categoryEntities: state.categoryEntities.filter(category =>
          category.id !== action.payload.id)
      });
    case category.ActionTypes.LOAD_CATEGORIES_SUCCESS:
      console.log(action.payload);
      return Object.assign({}, state, {
        categoryEntities: action.payload
      });
    case category.ActionTypes.SELECT_CATEGORY:
      const selected = state.categoryEntities.find(item => { return item.id === action.payload; });
      console.log(selected);
      return Object.assign({}, state, {
        selectedCategory: selected
      });
    case category.ActionTypes.DELETE_CATEGORY_SUCCESS:
      return Object.assign({}, state, {
        categoryEntities: state.categoryEntities.filter(
          category => category.id !== action.payload),
        selectedCategory: null
      });
    case category.ActionTypes.UPDATE_CATEGORY_SUCCESS:
      return Object.assign({}, state, {
        categoryEntities: state.categoryEntities.map(item => {
          return item.id === action.payload.id ? Object.assign({}, item, action.payload) : item;
        }),
        selectedCategory: null
      });
    case category.ActionTypes.CREATE_CATEGORY_SUCCESS:
      const _category: Category = action.payload;
      /* Because the state is now an object instead of an array, the return statements of the reducer have to be adapted. */
      return {
        categoryEntities: [...state.categoryEntities, _category],
        selectedCategory: null
      };
    default: return state;
  }
};

export function getCategoryEntities(state$: Observable<State>) {
  return state$.select(s => s.categoryEntities);
}

export function getSelectedCategory(state$: Observable<State>) {
  return state$.select(s => s.selectedCategory);
}
