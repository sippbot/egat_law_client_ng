import { TaskAssignmentSummary } from '../models/task-assignment-summary.model';
import '@ngrx/core/add/operator/select';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/let';
import { Observable } from 'rxjs/Observable';
import * as artifactAction from '../actions/artifact.action';
import { Artifact } from '../models/artifact.model';
import { ArtifactNode } from '../models/artifact-node.model';
import { ArtifactSummary } from '../models/artifact-summary.model';
import { ArtifactNodeGroup } from '../models/artifact-node-group.model';

export interface State {
  selectedArtifact: Artifact;
  selectedArtifactNode: ArtifactNode;
  selectedArtifactNodeGroup: ArtifactNodeGroup;

  artifactEntities: Array<Artifact>;
  artifactNodeEntities: Array<ArtifactNode>;
  artifactNodeGroupEntities: Array<ArtifactNodeGroup>;

  artifactSummaryEntities: Array<ArtifactSummary>;
  followupArtifactSummaryEntities: Array<ArtifactSummary>;
  selectedArtifactSummary: Array<ArtifactSummary>;
};

const initialState: State = {
  selectedArtifact: null,
  selectedArtifactNode: null,
  selectedArtifactNodeGroup: null,

  artifactEntities: [],
  artifactNodeEntities: [],
  artifactNodeGroupEntities: [],

  artifactSummaryEntities: [],
  followupArtifactSummaryEntities: [],
  selectedArtifactSummary: []
};

/* Instead of using a constant of type ActionReducer, the function is directly exported */
export function reducer(state = initialState, action: artifactAction.Actions): State {
  console.log(action);
  switch (action.type) {
    case artifactAction.ActionTypes.LOAD_ARTIFACTS_SUMMARY_SUCCESS:
      return Object.assign({}, state, {
        artifactSummaryEntities: action.payload
      });
    
    case artifactAction.ActionTypes.LOAD_CANCELED_ARTIFACTS_SUMMARY_SUCCESS:
      return Object.assign({}, state, {
        artifactSummaryEntities: action.payload
      });

    case artifactAction.ActionTypes.LOAD_ARTIFACTS_SUCCESS:
      return Object.assign({}, state, {
        selectedArtifact: action.payload,
        artifactEntities: [...state.artifactEntities, action.payload]
      });

    case artifactAction.ActionTypes.SELECT_ARTIFACT:
      const selectedArtifact = state.artifactEntities.find(item => item.id === action.payload);
      return Object.assign({}, state, {
        selectedArtifact: selectedArtifact,
        artifactNodeGroupEntities: selectedArtifact.nodeGroups
      });

    case artifactAction.ActionTypes.SELECT_ARTIFACT_NODE_GROUP:
      const selectedArtifactNodeGroup = state.artifactNodeGroupEntities.find(item => item.id === action.payload);
      return Object.assign({}, state, {
        selectedArtifactNodeGroup: selectedArtifactNodeGroup,
        artifactNodeEntities: selectedArtifactNodeGroup.nodes
      });
    /*
        case artifactAction.ActionTypes.SELECT_ARTIFACT_NODE:
          const selectedArtifactNode = state.artifactNodeEntities.find(item => { return item.id === action.payload; });
          return Object.assign({}, state, {
            selectedArtifactNode: selectedArtifactNode
          });
    */
    case artifactAction.ActionTypes.DELETE_ARTIFACT_SUCCESS:
      return Object.assign({}, state, {
        artifactSummaryEntities: state.artifactSummaryEntities.filter(
          item => item.id !== action.payload),
          selectedArtifact: null
      });
    case artifactAction.ActionTypes.DELETE_ARTIFACT_ATTACHMENT_SUCCESS:
          
      const _selectedArtifact = state.selectedArtifact;
      if(state.selectedArtifact!=null){
        _selectedArtifact.documents = state.selectedArtifact.documents.filter(d => d.id === action.payload);
      }
      return Object.assign({}, state, {
        selectedArtifact: _selectedArtifact
      });
      
    case artifactAction.ActionTypes.CLEAR_ARTIFACT_SUMMARY:

      let _selectedClearArtifactSummary: ArtifactSummary[];
      _selectedClearArtifactSummary = []
      return Object.assign({}, state, {
        selectedArtifactSummary: _selectedClearArtifactSummary
      });

    case artifactAction.ActionTypes.SELECT_CANCELED_ARTIFACT_SUMMARY:

      let _selectedCanceledArtifactSummary: ArtifactSummary[];
      _selectedCanceledArtifactSummary = state.artifactSummaryEntities
      return Object.assign({}, state, {
        selectedArtifactSummary: _selectedCanceledArtifactSummary
      });

    case artifactAction.ActionTypes.SELECT_ARTIFACT_SUMMARY:

      let _selectedArtifactSummary: ArtifactSummary[];
      /*
  if(action.payload.taskAssignmentType.toLowerCase() === 'all'){
    _selectedArtifactSummary = state.artifactSummaryEntities.filter(item =>
      item.status.toLowerCase() === 'approved' &&
      item.taskAssignmentType.toLowerCase() === action.payload.taskAssignmentType.toLowerCase());
  } else if (action.payload.status.toLowerCase() === 'inprogress') {
    _selectedArtifactSummary = state.artifactSummaryEntities.filter(item =>
      (item.status.toLowerCase() === 'assigned' ||
        item.status.toLowerCase() === 'assessed' ||
        item.status.toLowerCase() === 'confirmed' ||
        item.status.toLowerCase() === 'proposed') &&
      item.taskAssignmentType.toLowerCase() === action.payload.taskAssignmentType.toLowerCase());
  } else {
    _selectedArtifactSummary = state.artifactSummaryEntities.filter(
      item => item.status.toLowerCase() === action.payload.status.toLowerCase() &&
        item.taskAssignmentType.toLowerCase() === action.payload.taskAssignmentType.toLowerCase()
    );
  }
*/
      if (action.payload.taskAssignmentType.toLowerCase() === 'all') {
        _selectedArtifactSummary = state.artifactSummaryEntities.filter(item =>
          item.status.toLowerCase() === 'approved');
      } else if (action.payload.taskAssignmentType.toLowerCase() === 'initial') {
        if (action.payload.status.toLowerCase() === 'inprogress') {
          _selectedArtifactSummary = state.artifactSummaryEntities.filter(item =>
            (item.status.toLowerCase() === 'assigned' ||
              item.status.toLowerCase() === 'assessed' ||
              item.status.toLowerCase() === 'confirmed' ||
              item.status.toLowerCase() === 'proposed' ||
              // add following logic to bypass approved to inprogress
              item.status.toLowerCase() === 'approved') &&
            item.taskAssignmentType.toLowerCase() === action.payload.taskAssignmentType.toLowerCase());
        } else {
          _selectedArtifactSummary = state.artifactSummaryEntities.filter(
            item => item.status.toLowerCase() === action.payload.status.toLowerCase() &&
              item.taskAssignmentType.toLowerCase() === action.payload.taskAssignmentType.toLowerCase()
          );
        }
      } else {
        if (action.payload.status.toLowerCase() === 'inprogress') {
          _selectedArtifactSummary = state.artifactSummaryEntities.filter(item =>
            (item.taskAssignmentStatus.toLowerCase() === 'assigned' ||
              item.taskAssignmentStatus.toLowerCase() === 'assessed' ||
              item.taskAssignmentStatus.toLowerCase() === 'confirmed' ||
              item.taskAssignmentStatus.toLowerCase() === 'proposed' ||
              item.taskAssignmentStatus.toLowerCase() === 'approved' ||
              item.taskAssignmentStatus.toLowerCase() === 'ticketed') && 
            (item.status.toLowerCase() === 'approved') &&
            item.taskAssignmentType.toLowerCase() === action.payload.taskAssignmentType.toLowerCase());
        } else if(action.payload.status.toLowerCase() === 'initial'){
          _selectedArtifactSummary = state.artifactSummaryEntities.filter(item =>
            (item.taskAssignmentStatus.toLowerCase() === 'initial' ||
              item.taskAssignmentStatus.toLowerCase() === 'created') &&
              (item.status.toLowerCase() === 'approved') &&
            item.taskAssignmentType.toLowerCase() === action.payload.taskAssignmentType.toLowerCase());
          
        }else if(action.payload.status.toLowerCase() === 'overdue'){
          _selectedArtifactSummary = state.artifactSummaryEntities.filter(item =>
            (item.taskAssignmentStatus.toLowerCase() === 'ticketed') &&
            (item.status.toLowerCase() === 'approved') &&
            item.taskAssignmentType.toLowerCase() === action.payload.taskAssignmentType.toLowerCase());
          
        }else if(action.payload.status.toLowerCase() === 'approved'){
          _selectedArtifactSummary = state.artifactSummaryEntities.filter(item =>
            (item.status.toLowerCase() === action.payload.status.toLowerCase()) &&
            (item.status.toLowerCase() === 'approved') &&
            item.taskAssignmentType.toLowerCase() === action.payload.taskAssignmentType.toLowerCase());
        }else{
          _selectedArtifactSummary = state.artifactSummaryEntities.filter(item =>
            (item.status.toLowerCase() === action.payload.status.toLowerCase()) &&
            (item.status.toLowerCase() === 'approved') &&
            item.taskAssignmentType.toLowerCase() === action.payload.taskAssignmentType.toLowerCase());
        }
        //   _selectedArtifactSummary = state.artifactSummaryEntities.filter(
        //     item => item.status.toLowerCase() === action.payload.status.toLowerCase() &&
        //       item.taskAssignmentType.toLowerCase() === action.payload.taskAssignmentType.toLowerCase()
        //   );
        // }
        // if (action.payload.status.toLowerCase() === 'initial' || action.payload.status.toLowerCase() === 'inprogress') {
        //   _selectedArtifactSummary = state.artifactSummaryEntities.filter(item =>
        //     item.status.toLowerCase() === 'approved' &&
        //     item.taskAssignmentType.toLowerCase() === action.payload.taskAssignmentType.toLowerCase());
        // } else {
        //   _selectedArtifactSummary = state.artifactSummaryEntities.filter(item =>
        //     item.status.toLowerCase() === action.payload.status.toLowerCase() &&
        //     item.taskAssignmentType.toLowerCase() === action.payload.taskAssignmentType.toLowerCase());
        // }
      }

      return Object.assign({}, state, {
        selectedArtifactSummary: _selectedArtifactSummary
      });

    /*
    case artifactAction.ActionTypes.UPDATE_ARTIFACT_SUCCESS:
      return Object.assign({}, state, {
        artifactEntities: state.artifactEntities.map(item => {
          return item.code === action.payload.id ? Object.assign({}, item, action.payload) : item;
        }),
        selectedArtifact: null
      });
    */
    case artifactAction.ActionTypes.CREATE_ARTIFACT_ASSIGNMENT_SUCCESS:
      return Object.assign({}, state, {
        artifactSummaryEntities: state.artifactSummaryEntities.map(item => {
          return item.id === action.payload.id ? Object.assign({}, item, action.payload) : item;
        })
      });

    case artifactAction.ActionTypes.CREATE_ARTIFACT_SUCCESS:
    case artifactAction.ActionTypes.CREATE_ARTIFACT_NODE_SUCCESS:
    case artifactAction.ActionTypes.CREATE_ARTIFACT_NODE_GROUP_SUCCESS:
    case artifactAction.ActionTypes.UPDATE_ARTIFACT_SUCCESS:
    case artifactAction.ActionTypes.UPDATE_ARTIFACT_NODE_SUCCESS:
    case artifactAction.ActionTypes.UPDATE_ARTIFACT_NODE_GROUP_SUCCESS:
      return Object.assign({}, state, {
        selectedArtifact: action.payload,
        artifactEntities: state.artifactEntities.map(item => {
          return item.id === action.payload.id ? Object.assign({}, item, action.payload) : item;
        })
      });

    default:
      return state;
  }
};

export function getArtifactEntities(state$: Observable<State>) {
  return state$.select(s => s.artifactEntities);
}

export function getArtifactNodeGroupEntities(state$: Observable<State>) {
  return state$.select(s => s.artifactNodeGroupEntities);
}

export function getArtifactNodeEntities(state$: Observable<State>) {
  return state$.select(s => s.artifactNodeEntities);
}

export function getArtifactSummaryEntities(state$: Observable<State>) {
  return state$.select(s => s.artifactSummaryEntities);
}
/*
export function getArtifactReviewAssesmentEntities(state$: Observable<State>) {
  return state$.select(s => s.artifactReviewAssignmentEntities);
}

export function getApproveArtifactsAssesmentEntities(state$: Observable<State>): Observable<TaskAssignmentSummary[]> {
  return state$.select(s => s.approveArtifactAssessmentEntities);
}

export function getArtifactsAssesmentEntities(state$: Observable<State>) {
  return state$.select(s => s.ArtifactAssessmentEntities);
}
*/

export function getSelectedArtifact(state$: Observable<State>): Observable<Artifact> {
  return state$.select(s => s.selectedArtifact);
}

export function getSelectedArtifactNode(state$: Observable<State>): Observable<ArtifactNode> {
  return state$.select(s => s.selectedArtifactNode);
}

export function getSelectedArtifactNodeGroup(state$: Observable<State>): Observable<ArtifactNodeGroup> {
  return state$.select(s => s.selectedArtifactNodeGroup);
}

export function getSelectedArtifactSummary(state$: Observable<State>): Observable<ArtifactSummary[]> {
  return state$.select(s => s.selectedArtifactSummary);
}
