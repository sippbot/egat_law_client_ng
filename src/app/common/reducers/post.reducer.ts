import '@ngrx/core/add/operator/select';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/let';
import {Observable} from 'rxjs/Observable';
import * as posts from '../actions/post.action';
import {Post} from '../models/post.model';
import {Department} from '../models/department.model';

/* From a simple array ( [] ), the state becomes a object where the array is contained withing the entities property */
export interface State {
    postEntities: Array<Post>;
    selectedPost: Post;
    selectedDepartment: Department;
}

const initialState: State = {
    postEntities: [],
    selectedPost: null,
    selectedDepartment: null
};

/* Instead of using a constant of type ActionReducer, the function is directly exported */
export function reducer(state = initialState, action: posts.Actions): State {
    console.log(action, state);
    switch (action.type) {
        case posts.ActionTypes.SELECT_POST: {
            let selected = state.postEntities.find(item => {
                return item.id === action.payload;
            });
            return Object.assign({}, state, {
                selectedPost: selected
            });
        }
        case posts.ActionTypes.LOAD_POST: {
            return state;
        }
        case posts.ActionTypes.LOAD_POST_SUCCESS: {
            return Object.assign({}, state, {
                postEntities: action.payload
            });
        }
        case posts.ActionTypes.CREATE_POST: {
            return state;
        }
        case posts.ActionTypes.CREATE_POST_SUCCESS: {
            const post: Post = action.payload;
            console.log("post",post)
            /* Because the state is now an object instead of an array, the return statements of the reducer have to be adapted. */
            return {
                postEntities: [...state.postEntities, post],
                selectedPost: null,
                selectedDepartment: null
            };
        }
        case posts.ActionTypes.REMOVE_POST: {
            return state;
        }
        case posts.ActionTypes.REMOVE_POST_SUCCESS: {
            /* Because the state is now an object instead of an array, the return statements of the reducer have to be adapted. */
            return Object.assign({}, state, {
                postEntities: state.postEntities.filter(x =>
                    x.id !== action.payload)
            });
        }
        default:
            return state;
    }
}

export function getPostEntities(state$: Observable<State>) {
    return state$.select(s => s.postEntities);
}

export function getSelectedPost(state$: Observable<State>) {
    return state$.select(s => s.selectedPost);
}
