import '@ngrx/core/add/operator/select';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/let';
import { Observable } from 'rxjs/Observable';
import * as ticket from '../actions/ticket.action';
import { Ticket } from '../models/ticket.model';

/* From a simple array ( [] ), the state becomes a object where the array is contained withing the entities property */
export interface State {
    ticketEntities: Array<Ticket>;
    selectedTicket: Ticket;
};

const initialState: State = {
    ticketEntities: [],
    selectedTicket: null
};
/* Instead of using a constant of type ActionReducer, the function is directly exported */
export function reducer(state = initialState, action: ticket.Actions): State {
    console.log(action);
    switch (action.type) {
        case ticket.ActionTypes.LOAD_TICKETS_SUCCESS:
            return Object.assign({}, state, {
                ticketEntities: action.payload
            });

        case ticket.ActionTypes.LOAD_TICKET_ENTRY_SUCCESS:
            return Object.assign({}, state, {
                selectedTicket: action.payload,
                ticketEntities: [...state.ticketEntities, action.payload]
            });

        case ticket.ActionTypes.CLOSE_TICKET_SUCCESS:
            return Object.assign({}, state, {
                ticketEntities: state.ticketEntities.map(item => {
                    return item.id === action.payload.id ? Object.assign({}, item, action.payload) : item;
                }),
                selectedUsergroup: null
            });

        case ticket.ActionTypes.SELECT_TICKET:
            const selected = state.ticketEntities.find(item => { return item.id === action.payload; });
            return Object.assign({}, state, {
                selectedTicket: selected
            });

        default:
            return state;
    }
};

export function getTicketEnities(state$: Observable<State>) {
    return state$.select(s => s.ticketEntities);
}

export function getSelectedTicketElement(state$: Observable<State>) {
    return state$.select(s => s.selectedTicket);
}
