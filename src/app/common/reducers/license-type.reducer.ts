import '@ngrx/core/add/operator/select';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/let';
import { Observable } from 'rxjs/Observable';
import * as licensetypes from '../actions/license-type.action';
import { LicenseType } from '../models/license-type.model';
/* From a simple array ( [] ), the state becomes a object where the array is contained withing the entities property */
export interface State {
  licensetypeEntities: Array<LicenseType>;
  selectedLicensetype: LicenseType;
};

const initialState: State = {
  licensetypeEntities: [],
  selectedLicensetype: null
};

/* Instead of using a constant of type ActionReducer, the function is directly exported */
export function reducer(state = initialState, action: licensetypes.Actions): State {
  switch (action.type) {
    case licensetypes.ActionTypes.SELECT_LICENSETYPE:
      const selectedLicensetype = state.licensetypeEntities.find(item => { return item.id === action.payload; });
      return Object.assign({}, state, {
        selectedLicensetype: selectedLicensetype
      });

    case licensetypes.ActionTypes.LOAD_LICENSETYPE_SUCCESS:
      return Object.assign({}, state, {
        licensetypeEntities: action.payload
      });

    case licensetypes.ActionTypes.CREATE_LICENSETYPE_SUCCESS:
      const licensetype: LicenseType = action.payload;
      /* Because the state is now an object instead of an array, the return statements of the reducer have to be adapted. */
      return {
        licensetypeEntities: [...state.licensetypeEntities, licensetype],
        selectedLicensetype: null
      };

    case licensetypes.ActionTypes.UPDATE_LICENSETYPE_SUCCESS:
      return Object.assign({}, state, {
        licensetypeEntities: state.licensetypeEntities.map(item => {
          return item.id === action.payload.id ? Object.assign({}, item, action.payload) : item;
        }),
        selectedLicensetype: null
      });

    case licensetypes.ActionTypes.DELETE_LICENSETYPE_SUCCESS:
      return Object.assign({}, state, {
        licensetypeEntities: state.licensetypeEntities.filter(
          group => group.id !== action.payload),
        selectedLicensetype: null
      });

    default:
      return state;
  }
};

export function getLicensetypeEntities(state$: Observable<State>) {
  return state$.select(s => s.licensetypeEntities);
}

export function getSelectedLicensetype(state$: Observable<State>) {
  return state$.select(s => s.selectedLicensetype);
}
