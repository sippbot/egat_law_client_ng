import * as layout from '../actions/layout.action';
export interface State {
  rightEditFormOpened: boolean;
}

const initialState: State = {
  rightEditFormOpened: false
};

export function reducer(state = initialState, action: layout.LayoutActions): State {
  switch (action.type) {
    case layout.LayoutActionTypes.CLOSE_RIGHT_EDITFORM: {
      return Object.assign({}, state, { rightEditFormOpened: false });
    }
    case layout.LayoutActionTypes.OPEN_RIGHT_EDITFORM: {
      return Object.assign({}, state, { rightEditFormOpened: true });
    }
    default: return state;
  }
}
export const getrightEditFormState = (state: State) => state.rightEditFormOpened;
