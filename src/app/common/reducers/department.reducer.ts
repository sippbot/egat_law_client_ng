import '@ngrx/core/add/operator/select';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/let';
import {Observable} from 'rxjs/Observable';
import * as department from '../actions/departments.action';
import {Department} from '../models/department.model';

/* From a simple array ( [] ), the state becomes a object where the array is contained withing the entities property */
export interface State {
    departmentEntities: Array<Department>;
    selectedDepartment: Department;
}

const initialState: State = {
    departmentEntities: [],
    selectedDepartment: null
};

/* Instead of using a constant of type ActionReducer, the function is directly exported */
export function reducer(state = initialState, action: department.Actions): State {
    switch (action.type) {
        case department.ActionTypes.REMOVE_DEPARTMENT: {
            return Object.assign({}, state, {
                departmentEntities: state.departmentEntities.filter(x =>
                    x.id !== action.payload.id)
            });
        }
        case department.ActionTypes.LOAD_DEPARTMENT_SUCCESS:{
            return Object.assign({}, state, {
                selectedDepartment: action.payload,
                departmentEntities: [...state.departmentEntities, action.payload]
            });
        }
        case department.ActionTypes.LOAD_DEPARTMENTS: {
            return state;
        }
        case department.ActionTypes.LOAD_DEPARTMENTS_SUCCESS: {
            return Object.assign({}, state, {
                departmentEntities: action.payload
            });
        }
        case department.ActionTypes.LOAD_ACTIVE_DEPARTMENTS: {
            return state;
        }
        case department.ActionTypes.LOAD_ACTIVE_DEPARTMENTS_SUCCESS: {
            return Object.assign({}, state, {
                departmentEntities: action.payload
            });
        }
        case department.ActionTypes.SELECT_DEPARTMENT: {
            let selected = state.departmentEntities.find(item => {
                return item.id === action.payload;
            });
            return Object.assign({}, state, {
                selectedDepartment: selected
            });
        }
        case department.ActionTypes.UPDATE_DEPARTMENT: {
            return state;
        }
        case department.ActionTypes.UPDATE_DEPARTMENT_SUCCESS: {
            console.log(action.payload);
            return Object.assign({}, state, {
                departmentEntities: state.departmentEntities.map(item => {
                    return item.id === action.payload.id ? Object.assign({}, item, action.payload) : item;
                }),
                selectedDepartment: null
            });
        }
        case department.ActionTypes.CREATE_DEPARTMENT: {
            return state;
        }
        case department.ActionTypes.CREATE_DEPARTMENT_SUCCESS: {
            const department: Department = action.payload;
            /* Because the state is now an object instead of an array, the return statements of the reducer have to be adapted. */
            return {
                departmentEntities: [...state.departmentEntities, department],
                selectedDepartment: null
            };
        }
        default:
            return state;
    }
}

export function getDepartmentEntities(state$: Observable<State>) {
    return state$.select(s => s.departmentEntities);
}

export function getSelectedDepartment(state$: Observable<State>) {
    return state$.select(s => s.selectedDepartment);
}
