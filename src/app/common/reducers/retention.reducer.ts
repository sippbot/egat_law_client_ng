import '@ngrx/core/add/operator/select';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/let';
import { Observable } from 'rxjs/Observable';
import * as retention from '../actions/retention.action';
import {Retention} from "../models/retention.model";
/* From a simple array ( [] ), the state becomes a object where the array is contained withing the entities property */
export interface State {
  retentionArtifactEntities: Array<any>
  selectedRetentionArtifact: any,
  taskassessment: any,
  departmentEntities: Array<any>,
  selectTask: any,
  selectTrackingPlan: any,
  selectedtaskassessment: any,
  approveRetentionAssessment: any
};

const initialState: State = {
  retentionArtifactEntities: [],
  selectedRetentionArtifact: null,
  departmentEntities: [],
  selectTask: null,
  selectTrackingPlan: null,
  taskassessment: null,
  selectedtaskassessment: null,
  approveRetentionAssessment: null
};

/* Instead of using a constant of type ActionReducer, the function is directly exported */
export function reducer(state = initialState, action: retention.Actions): State {
  switch (action.type) {

    case retention.ActionTypes.LOAD_RETENTION_ARTIFACT_NEW: {
      return state;
    }
    case retention.ActionTypes.LOAD_RETENTION_ARTIFACT_NEW_SUCCESS: {
      console.log(action.payload);
      return Object.assign({}, state, {
        retentionArtifactEntities: action.payload,
      });
    }

    case retention.ActionTypes.LOAD_RETENTION_ARTIFACT_FOLLOWUP: {
      return state;
    }
    case retention.ActionTypes.LOAD_RETENTION_ARTIFACT_FOLLOWUP_SUCCESS: {
      console.log(action.payload);
      return Object.assign({}, state, {
        retentionArtifactEntities: action.payload,
      });
    }

    case retention.ActionTypes.SELECT_RETENTION_ARTIFACT: {
      let selected = state.retentionArtifactEntities.find(item => { return item.id === action.payload; });
      console.log(selected);
      return Object.assign({}, state, {
        selectedRetentionArtifact: selected
      });

    }

    case retention.ActionTypes.LOAD_RETENTION_ARTIFACT_SUCCESS: {
      let selected = state.retentionArtifactEntities.find(item => { return item.id === action.payload; });
      console.log(selected);
      return Object.assign({}, state, {
        selectedRetentionArtifact: selected
      });

    }


    case retention.ActionTypes.LOAD_TRACKING_PLAN: {
      return state;
    }
    case retention.ActionTypes.LOAD_TRACKING_PLAN_SUCCESS: {
      console.log(action.payload);
      return Object.assign({}, state, {
        retentionArtifactEntities: action.payload,
      });
    }


    case retention.ActionTypes.LOAD_ALL_TASK_DUE: {
      return state;
    }
    case retention.ActionTypes.LOAD_ALL_TASK_DUE_SUCCESS: {
      console.log(action.payload);
      return Object.assign({}, state, {
        retentionArtifactEntities: action.payload,
      });
    }


    case retention.ActionTypes.LOAD_ALL_TASK_OVER_DUE: {
      return state;
    }
    case retention.ActionTypes.LOAD_ALL_TASK_OVER_DUE_SUCCESS: {
      console.log(action.payload);
      return Object.assign({}, state, {
        retentionArtifactEntities: action.payload,
      });
    }


    case retention.ActionTypes.LOAD_DEPARTMENT_PLAN: {
      return state;
    }
    case retention.ActionTypes.LOAD_DEPARTMENT_PLAN_SUCCESS: {
      console.log(action.payload);
      return Object.assign({}, state, {
        departmentEntities: action.payload,
      });
    }



    case retention.ActionTypes.LOAD_SELECT_TASK_SUCCESS: {
      console.log(action.payload);
      return Object.assign({}, state, {
        selectTask: action.payload,
      });
    }


    case retention.ActionTypes.SELECT_TRACKING_PLAN: {
      return state;
    }
    case retention.ActionTypes.LOAD_SELECT_TRACKING_PLAN_SUCCESS: {
      console.log(action.payload);
      return Object.assign({}, state, {
        selectTrackingPlan: action.payload,
      });
    }


    case retention.ActionTypes.SELECT_PLAN_ASSESSMENT: {
      return state;
    }
    case retention.ActionTypes.LOAD_SELECT_PLAN_ASSESSMENT_SUCCESS: {
      console.log(action.payload);
      return Object.assign({}, state, {
        taskassessment: action.payload,
      });
    }

    case retention.ActionTypes.SELECT_SUB_TASK_ASSESSMENT: {
      return state;
    }
    case retention.ActionTypes.LOAD_SELECT_SUB_TASK_ASSESSMENT_SUCCESS: {
      console.log(action.payload);
      return Object.assign({}, state, {
        selectedtaskassessment: action.payload,
      });
    }


    case retention.ActionTypes.LOAD_RETENTION_RESULT: {
      return state;
    }
    case retention.ActionTypes.LOAD_RETENTION_RESULT_SUCCESS: {
      console.log(action.payload);
      return Object.assign({}, state, {
        retentionArtifactEntities: action.payload,
      });
    }



    case retention.ActionTypes.SELECT_APPROVE_RETENTION_ASSESSMENT: {
      return state;
    }
    case retention.ActionTypes.LOAD_APPROVE_RETENTION_ASSESSMENT_SUCCESS: {
      console.log(action.payload);
      return Object.assign({}, state, {
        approveRetentionAssessment: action.payload,
      });
    }




    default: return state;
  }

};


export function getRetentionArtifactsNewEntities(state$: Observable<State>) {
  return state$.select(s => s.retentionArtifactEntities);
}

export function getRetentionArtifactsFollowUpEntities(state$: Observable<State>) {
  return state$.select(s => s.retentionArtifactEntities);
}

export function getRetentionArtifactEntities(state$: Observable<State>) {
  return state$.select(s => s.retentionArtifactEntities);
}

export function getSelectedRetentionArtifacts(state$: Observable<State>) {
  console.log("getSelectedRetentionArtifacts");
  return state$.select(s => s.selectedRetentionArtifact);
}

export function getTrackingPlanEntities(state$: Observable<State>) {
  return state$.select(s => s.retentionArtifactEntities);
}

export function getAllTaskDueEntities(state$: Observable<State>) {
  return state$.select(s => s.retentionArtifactEntities);
}

export function getAllTaskOverDueEntities(state$: Observable<State>) {
  return state$.select(s => s.retentionArtifactEntities);
}

export function getDepartmentPlanEntities(state$: Observable<State>) {
  return state$.select(s => s.departmentEntities);
}


export function getSelectTasksEntities(state$: Observable<State>) {
  return state$.select(s => s.selectTask);
}


export function getSelectTrackingPlanEntities(state$: Observable<State>) {
  return state$.select(s => s.selectTrackingPlan);
}

export function getPlanAssesmentsEntities(state$: Observable<State>) {
  return state$.select(s => s.taskassessment);
}

export function getSelectedSubTaskAssessmentEntities(state$: Observable<State>) {
  return state$.select(s => s.selectedtaskassessment);
}

export function getRetentionResultsEntities(state$: Observable<State>) {
  return state$.select(s => s.retentionArtifactEntities);
}

export function getApproveRetentionAssesmentEntities(state$: Observable<State>) {
  return state$.select(s => s.approveRetentionAssessment);
}
