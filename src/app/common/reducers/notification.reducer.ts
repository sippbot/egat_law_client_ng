import { MailNotification } from '../models/mail-notification.model';
import '@ngrx/core/add/operator/select';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/let';
import { Observable } from 'rxjs/Observable';
import * as notification from '../actions/notification.action';
/* From a simple array ( [] ), the state becomes a object where the array is contained withing the entities property */
export interface State {
    notificationEntities: Array<MailNotification>;
    selectedNotification: MailNotification;
};

const initialState: State = {
    notificationEntities: [],
    selectedNotification: null
};

/* Instead of using a constant of type ActionReducer, the function is directly exported */
export function reducer(state = initialState, action: notification.Actions): State {
    switch (action.type) {
        case notification.ActionTypes.LOAD_NOTIFICATION_SUCCESS:
            return Object.assign({}, state, {
                notificationEntities: action.payload
            });

        default:
            return state;
    }
};

export function getNotificationEntities(state$: Observable<State>) {
    return state$.select(s => s.notificationEntities);
}
