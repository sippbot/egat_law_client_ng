import '@ngrx/core/add/operator/select';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/let';
import { Observable } from 'rxjs/Observable';
import * as license from '../actions/license.action';
import { License } from '../models/license.model';
/* From a simple array ( [] ), the state becomes a object where the array is contained withing the entities property */
export interface State {
  licenseEntities: Array<License>;
  selectedLicense: License;
};

const initialState: State = {
  licenseEntities: [],
  selectedLicense: null
};

/* Instead of using a constant of type ActionReducer, the function is directly exported */
export function reducer(state = initialState, action: license.Actions): State {
  switch (action.type) {
    case license.ActionTypes.SELECT_LICENSE:
    const selectedLicense = state.licenseEntities.find(item => { return item.id === action.payload; });
      return Object.assign({}, state, {
        selectedLicense: selectedLicense
      });

    case license.ActionTypes.LOAD_LICENSES_SUCCESS:
      return Object.assign({}, state, {
        licenseEntities: action.payload
      });

    case license.ActionTypes.CREATE_LICENSE_SUCCESS:
      const licensetype: License = action.payload;
      return {
        licenseEntities: [...state.licenseEntities, licensetype],
        selectedLicense: null
      };

    case license.ActionTypes.RENEW_LICENSE_SUCCESS:
      const licenserenew: License = action.payload;
      return {
        licenseEntities: [...state.licenseEntities, licenserenew],
        selectedLicense: null
      };

    default:
      return state;
  }
};

export function getLicenseEntities(state$: Observable<State>) {
  return state$.select(s => s.licenseEntities);
}

export function getSelectedLicense(state$: Observable<State>) {
  return state$.select(s => s.selectedLicense);
}
