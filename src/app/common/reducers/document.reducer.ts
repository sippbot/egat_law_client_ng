import '@ngrx/core/add/operator/select';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/let';
import { Observable } from 'rxjs/Observable';
import * as documents from '../actions/document.action';
import { Document } from '../models/document.model';

/* From a simple array ( [] ), the state becomes a object where the array is contained withing the entities property */
export interface State {
  documentEntities: Array<Document>;
};

const initialState: State = {
  documentEntities: []
};
/* Instead of using a constant of type ActionReducer, the function is directly exported */
export function reducer(state = initialState, action: documents.Actions): State {
  console.log(action);
  switch (action.type) {
    case documents.ActionTypes.UPDATE_DOCUMENT_SUCCESS:
    case documents.ActionTypes.ADD_DOCUMENT:
      return {
        documentEntities: [...state.documentEntities, action.payload]
      };

    case documents.ActionTypes.REMOVE_DOCUMENT:
      return Object.assign({}, state, {
        documentEntities: state.documentEntities.filter(document =>
          document.id !== action.payload.id)
      });

    case documents.ActionTypes.LOAD_DOCUMENT_SUCCESS:
      // console.log(action.payload);
      return Object.assign({}, state, {
        documentEntities: action.payload
      });

    default:
      return state;
  }
};

export function getDocumentEntities(state$: Observable<State>) {
  return state$.select(s => s.documentEntities);
}
