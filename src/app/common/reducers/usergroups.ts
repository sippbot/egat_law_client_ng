import { ActionTypes } from '../actions/post.action';
import '@ngrx/core/add/operator/select';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/let';
import { Observable } from 'rxjs/Observable';
import * as usergroups from '../actions/usergroups.action';
import { UserGroup } from '../models/usergroup.model';

/* From a simple array ( [] ), the state becomes a object where the array is contained withing the entities property */
export interface State {
  usergroupEntities: Array<UserGroup>;
  selectedUsergroup: UserGroup;
};

const initialState: State = {
  usergroupEntities: [],
  selectedUsergroup: null
};
/* Instead of using a constant of type ActionReducer, the function is directly exported */
export function reducer(state = initialState, action: usergroups.Actions): State {
  console.log(action);
  switch (action.type) {
    case usergroups.ActionTypes.REMOVE_USERGROUP:
      return Object.assign({}, state, {
        usergroupEntities: state.usergroupEntities.filter(group =>
          group.id !== action.payload.id)
      });

    case usergroups.ActionTypes.LOAD_USERGROUPS_SUCCESS:
      console.log(action.payload);
      return Object.assign({}, state, {
        usergroupEntities: action.payload
      });
    
    case usergroups.ActionTypes.LOAD_USERGROUPS_PERMISSION_SUCCESS:
      console.log(action.payload);
      return Object.assign({}, state, {
        usergroupEntities: action.payload
      });

    case usergroups.ActionTypes.SELECT_USERGROUPS:
      const selected = state.usergroupEntities.find(item => { return item.id === action.payload; });
      return Object.assign({}, state, {
        selectedUsergroup: selected
      });

    case usergroups.ActionTypes.UPDATE_USERGROUPS_SUCCESS:
      return Object.assign({}, state, {
        usergroupEntities: state.usergroupEntities.map(item => {
          return item.id === action.payload.id ? Object.assign({}, item, action.payload) : item;
        }),
        selectedUsergroup: null
      });

    case usergroups.ActionTypes.CREATE_USERGROUPS_SUCCESS:
      const usergroup: UserGroup = action.payload;
      /* Because the state is now an object instead of an array, the return statements of the reducer have to be adapted. */
      return {
        usergroupEntities: [...state.usergroupEntities, usergroup],
        selectedUsergroup: null
      };

    case usergroups.ActionTypes.DELETE_USERGROUPS_SUCCESS:
      return Object.assign({}, state, {
        usergroupEntities: state.usergroupEntities.filter(
          group => group.id !== action.payload),
        selectedUsergroup: null
      });

    default:
      return state;
  }
};

export function getUsergroupEntities(state$: Observable<State>) {
  return state$.select(s => s.usergroupEntities);
}

export function getSelectedUsergroup(state$: Observable<State>) {
  return state$.select(s => s.selectedUsergroup);
}
