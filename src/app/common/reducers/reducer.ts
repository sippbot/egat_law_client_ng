// META Reducer
import { ActionReducer, combineReducers } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { compose } from '@ngrx/core';
import { combineLatest } from 'rxjs/observable/combineLatest';
/* Import each module of your state. This way, you can access its reducer
function and state interface as a property. */

/* The top-level interface of the state is simply a map of all the inner states. */
import * as usergroups from './usergroups';
import * as departments from './department.reducer';
import * as users from './users';
import * as documents from './document.reducer';
import * as lawlevels from './lawlevel.reducer';
import * as categories from './category.reducer';
import * as artifacts from './artifact.reducer';
import * as retentions from './retention.reducer';
import * as licensetypes from './license-type.reducer';
import * as licenses from './license.reducer';
import * as alerts from './alert.reducer';
import * as post from './post.reducer';
import * as taskAssignment from './task-assignment.reducer';
import * as ticket from './ticket.reducer';
import * as userpermission from './user-permissions.reducer';
import * as notification from './notification.reducer';
import * as role from './role.reducer';

// 1. register here
export interface State {
    usergroups: usergroups.State;
    departments: departments.State;
    users: users.State;
    documents: documents.State;
    lawlevels: lawlevels.State;
    categories: categories.State;
    artifacts: artifacts.State;
    retentions: retentions.State;
    licensetypes: licensetypes.State;
    licenses: licenses.State;
    alerts: alerts.State;
    post: post.State;
    taskAssignment: taskAssignment.State;
    ticket: ticket.State;
    userpermission: userpermission.State;
    role: role.State;
    notification: notification.State;
}

// 2. register here
/* The reducers variable represents the map of all the reducer function that is used in the Meta Reducer */
const reducers = {
    usergroups: usergroups.reducer,
    departments: departments.reducer,
    users: users.reducer,
    documents: documents.reducer,
    lawlevels: lawlevels.reducer,
    categories: categories.reducer,
    artifacts: artifacts.reducer,
    retentions: retentions.reducer,
    licensetypes: licensetypes.reducer,
    licenses: licenses.reducer,
    alerts: alerts.reducer,
    post: post.reducer,
    taskAssignment: taskAssignment.reducer,
    ticket: ticket.reducer,
    userpermission: userpermission.reducer,
    role: role.reducer,
    notification: notification.reducer
};

/* Using combineReducers to create the Meta Reducer and export it from the module.
The exported Meta Reducer will be used as an argument in provideStore() in the application's root module. */
const combinedReducer: ActionReducer<State> = combineReducers(reducers);

export function reducer(state: any, action: any) {
    return combinedReducer(state, action);
}

// How to get latest value of each state
/***** USER GROUP *****/
export function getUserGroup(state$: Observable<State>) {
    return state$.select(state => state.usergroups);
}
export function getUserGroupPermission(state$: Observable<State>) {
    return state$.select(state => state.usergroups);
}
export const getUserGroups = compose(usergroups.getUsergroupEntities, getUserGroup);
export const getUserGroupsPermission = compose(usergroups.getUsergroupEntities, getUserGroupPermission);

/***** USER *****/
export function getUser(state$: Observable<State>) {
    return state$.select(state => state.users);
}
export const getUsers = compose(users.getUserEntities, getUser);
export const getLoginUser = compose(users.getLoginUser, getUser);

/***** DOCUMENT *****/
export function getDocument(state$: Observable<State>) {
    return state$.select(state => state.documents);
}
export const getDocuments = compose(documents.getDocumentEntities, getDocument);

/***** DEPARTMENT *****/
export function getDepartment(state$: Observable<State>) {
    return state$.select(state => state.departments);
}
export const getDepartments = compose(departments.getDepartmentEntities, getDepartment);

/***** Post user *****/
export function getPost(state$: Observable<State>) {
    return state$.select(state => state.post);
}
export const getPostById = compose(post.getPostEntities, getPost);

/***** LAWLEVEL *****/
export function getLawlevel(state$: Observable<State>) {
    return state$.select(state => state.lawlevels);
}
export const getLawlevels = compose(lawlevels.getLawlevelEntities, getLawlevel);

export function getCategory(state$: Observable<State>) {
    return state$.select(state => state.categories);
}
export const getCategories = compose(categories.getCategoryEntities, getCategory);

/***** LAW Artifact *****/
export function getArtifact(state$: Observable<State>) {
    return state$.select(state => state.artifacts);
}

export function getArtifactReviewAssesment(state$: Observable<State>) {
    return state$.select(state => state.artifacts);
}

/***** retention *****/
export function getRetentionArtifact(state$: Observable<State>) {
    return state$.select(state => state.retentions);
}

/***** LICENSETYPE *****/
export function getLicenseType(state$: Observable<State>) {
    return state$.select(state => state.licensetypes);
}

export function getLicense(state$: Observable<State>) {
    return state$.select(state => state.licenses);
}

export function getTaskAssignment(state$: Observable<State>) {
    return state$.select(state => state.taskAssignment);
}

export function getTicket(state$: Observable<State>) {
    return state$.select(state => state.ticket);
}
export function getUserPermission(state$: Observable<State>) {
    return state$.select(state => state.userpermission);
}
export function getAlert(state$: Observable<State>) {
    return state$.select(state => state.alerts);
}
export function getNotification(state$: Observable<State>){
    return state$.select(state => state.notification);
}

export const getLicenseTypes = compose(licensetypes.getLicensetypeEntities, getLicenseType);
export const getSelectedLicenseType = compose(licensetypes.getSelectedLicensetype, getLicenseType);

export const getLicenses = compose(licenses.getLicenseEntities, getLicense);
export const getSelectedLicense = compose(licenses.getSelectedLicense, getLicense);

export const getArtifacts = compose(artifacts.getArtifactEntities, getArtifact);
export const getArtifactNodeGroups = compose(artifacts.getArtifactNodeGroupEntities, getArtifact);
export const getArtifactNodes = compose(artifacts.getArtifactNodeEntities, getArtifact);
export const getSelectedArtifacts = compose(artifacts.getSelectedArtifact, getArtifact);
export const getSelectedArtifactNodeGroups = compose(artifacts.getSelectedArtifactNodeGroup, getArtifact);
export const getSelectedArtifactNodes = compose(artifacts.getSelectedArtifactNode, getArtifact);

export const getArtifactSummary = compose(artifacts.getArtifactSummaryEntities, getArtifact);
export const getSelectedArtifactSummary = compose(artifacts.getSelectedArtifactSummary, getArtifact);

export const getSelectedCategory = compose(categories.getSelectedCategory, getCategory);
export const getSelectedDepartment = compose(departments.getSelectedDepartment, getDepartment);
export const getSelectedLawlevel = compose(lawlevels.getSelectedLawlevel, getLawlevel);
export const getSelectedUser = compose(users.getSelectedUser, getUser);
export const getSelectedUsergroup = compose(usergroups.getSelectedUsergroup, getUserGroup);

/***** retention *****/
export const getRetentionArtifactsNew = compose(retentions.getRetentionArtifactsNewEntities, getRetentionArtifact);
export const getRetentionArtifactsFollowUp = compose(retentions.getRetentionArtifactsFollowUpEntities, getRetentionArtifact);
export const getRetentionArtifacts = compose(retentions.getRetentionArtifactEntities, getRetentionArtifact);
export const getSelectedRetentionArtifact = compose(retentions.getSelectedRetentionArtifacts, getRetentionArtifact);
export const getTrackingPlans = compose(retentions.getTrackingPlanEntities, getRetentionArtifact);
export const getAllTasksDue = compose(retentions.getAllTaskDueEntities, getRetentionArtifact);
export const getAllTasksOverDue = compose(retentions.getAllTaskOverDueEntities, getRetentionArtifact);
export const getDepartmentPlan = compose(retentions.getDepartmentPlanEntities, getRetentionArtifact);
export const getSelectTasks = compose(retentions.getSelectTasksEntities, getRetentionArtifact);
export const getSelectTrackingPlan = compose(retentions.getSelectTrackingPlanEntities, getRetentionArtifact);
export const getPlanAssesments = compose(retentions.getPlanAssesmentsEntities, getRetentionArtifact);
export const getSelectedSubTaskAssessment = compose(retentions.getSelectedSubTaskAssessmentEntities, getRetentionArtifact);
export const getRetentionResults = compose(retentions.getRetentionResultsEntities, getRetentionArtifact);
export const getApproveRetentionAssesments = compose(retentions.getApproveRetentionAssesmentEntities, getRetentionArtifact);

/*********** Alert ***********/
export const getAlerts = compose(alerts.getAlerts, getAlert);

/*********** TaskAssigment *************/
export const getTaskAssignments = compose(taskAssignment.getTaskAssignmentEntities, getTaskAssignment);
export const getSelectedTaskAssignment = compose(taskAssignment.getSelectedTaskAssignment, getTaskAssignment);
export const getTaskAssessments = compose(taskAssignment.getTaskAssessmentEntities, getTaskAssignment);
export const getTaskAssignmentSummary = compose(taskAssignment.getTaskAssessmentSummaryEntities, getTaskAssignment);
export const getTaskComments = compose(taskAssignment.getTaskCommentEntities, getTaskAssignment);
export const getTasAssignmentDocuments = compose(taskAssignment.getTaskAssignmentDocuments, getTaskAssignment);

/************ Ticket **************/
export const getTickets = compose(ticket.getTicketEnities, getTicket);
export const getSelectedTicket = compose(ticket.getSelectedTicketElement, getTicket);

/************ UserPermission **************/
export const getUserPermissions = compose(userpermission.getUserpermissionEntities, getUserPermission);

/************ Notification ****************/
export const getNotifications = compose(notification.getNotificationEntities, getNotification);
