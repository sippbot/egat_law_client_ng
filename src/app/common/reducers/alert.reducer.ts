import '@ngrx/core/add/operator/select';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/let';
import {Observable} from 'rxjs/Observable';
import * as alert from '../actions/alert.action';
import {Alert} from '../models/alert.model';

/* From a simple array ( [] ), the state becomes a object where the array is contained withing the entities property */
export interface State {
    alerts: Array<Alert>;
}

const initialState: State = {
    alerts: []
};

/* Instead of using a constant of type ActionReducer, the function is directly exported */
export function reducer(state = initialState, action: alert.Actions): State {
    console.log(action);
    switch (action.type) {
        case alert.ActionTypes.ADD_ALERT: {
            // console.log(action.payload)
            // state.alerts.push(action.payload);
            // console.log(state.alerts)
            // return state;
            return Object.assign({}, state, {
                alerts: [action.payload]
              });
        }
        default: return state;
    }
}

export function getAlerts(state$: Observable<State>) {
    return state$.select(s => s.alerts);
}
