import '@ngrx/core/add/operator/select';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/let';
import { Observable } from 'rxjs/Observable';
import * as user_permission from '../actions/user-permissions.action';
import { UserPermission } from '../models/user-permissions.model';

/* From a simple array ( [] ), the state becomes a object where the array is contained withing the entities property */
export interface State {
  userpermissionEntities: Array<UserPermission>;
  selectedUserpermission: UserPermission;
}

const initialState: State = {
  userpermissionEntities: [],
  selectedUserpermission: null,
}
/* Instead of using a constant of type ActionReducer, the function is directly exported */
export function reducer(state = initialState, action: user_permission.Actions): State {
  console.log(action);
  switch (action.type) {

    case user_permission.ActionTypes.LOAD_ROLE_SUCCESS:
      console.log(action.payload);
      return Object.assign({}, state, {
        userpermissionEntities: action.payload
      });

    default:
      return state;
  }
};

export function getUserpermissionEntities(state$: Observable<State>) {
  return state$.select(s => s.userpermissionEntities);
}

