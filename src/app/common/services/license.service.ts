import { License } from '../models/license.model';
import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { ApplicationConstant } from '../application-constant';

@Injectable()
export class LicenseService {
    baseUrl: string = ApplicationConstant.APP_BASE_ENDPOINT_URL;
    constructor(private http: Http) { }

    private getHeaders() {
        // I included these headers because otherwise FireFox
        // will request text/html instead of application/json
        const headers = new Headers();
        headers.append('Accept', 'application/json');
        return headers;
    }

    getAllLicense(userId: number): Observable<any> {
        return this.http
            .get(`${this.baseUrl}/license/user/${userId}`, { headers: this.getHeaders() })
            .map(response => this.extractData(response))
            .catch(this.handleError);
    }

    createLicense(license: License): Observable<License> {
        return this.http
            .post(`${this.baseUrl}/license`, license, { headers: this.getHeaders() })
            .map(response => this.extractData(response))
            .catch(this.handleError);
    }

    renewLicense(license: License): Observable<License> {
        return this.http
        .put(`${this.baseUrl}/license/`+ license.id, license, { headers: this.getHeaders() })
        .map(response => this.extractData(response))
        .catch(this.handleError);
    }

    private extractData(response: Response) {
        if (response.status < 200 || response.status >= 300) {
            throw new Error('Bad response status: ' + response.status);
        }
        try{
            return response.json();
        }catch(e){
            return {}
        }
    }

    private handleError(errorResponse: Response) {
        const body = errorResponse.json();
        const message = body.message ?
            body.message :
            (errorResponse.statusText || 'unknown error');
        return Observable.throw(message);
    }
}

