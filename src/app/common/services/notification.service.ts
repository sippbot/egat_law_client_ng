import { MailNotification } from '../models/mail-notification.model';
import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { ApplicationConstant } from '../application-constant';

@Injectable()
export class NotificationService {
    baseUrl: string = ApplicationConstant.APP_BASE_ENDPOINT_URL;
    constructor(private http: Http) { }
    private getHeaders() {
        // I included these headers because otherwise FireFox
        // will request text/html instead of application/json
        const headers = new Headers();
        headers.append('Accept', 'application/json');
        return headers;
    }

    getNotifications(userId: number): Observable<MailNotification[]> {
        return this.http
            .get(`${this.baseUrl}/notification?userId=${userId}`, { headers: this.getHeaders() })
            .map(response => this.extractData(response))
            .catch(this.handleError);
    }

    resendNotification(messageId: number): Observable<number>{
        return this.http
        .post(`${this.baseUrl}/notification/resend`, { 'messageId': messageId }, { headers: this.getHeaders() })
        .map(response => this.extractData(response))
        .catch(this.handleError);
    }

    private extractData(response: Response) {
        if (response.status < 200 || response.status >= 300) {
            throw new Error('Bad response status: ' + response.status);
        }
        try {
            return response.json();
        } catch (e) {
            return {}
        }
    }

    private extractDataBlock(blockName: string, response: Response) {
        if (response.status < 200 || response.status >= 300) {
            console.log('Erro : Bad response');
            throw new Error('Bad response status: ' + response.status);
        }
        return response.json()[blockName];
    }

    private handleError(errorResponse: Response) {
        const body = errorResponse.json();
        const message = body.message ?
            body.message :
            (errorResponse.statusText || 'unknown error');
        return Observable.throw(message);
    }
}