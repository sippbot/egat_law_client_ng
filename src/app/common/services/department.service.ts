import { Injectable } from '@angular/core';
import { Department } from '../models/department.model';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ApplicationConstant } from '../application-constant';
import 'rxjs/add/operator/map';

@Injectable()
export class DepartmentService {
  baseUrl: string = ApplicationConstant.APP_BASE_ENDPOINT_URL;
  constructor(private http: Http) { }

  private getHeaders() {
    // I included these headers because otherwise FireFox
    // will request text/html instead of application/json
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    return headers;
  }

  // ----------------------------------------------------
  //         new version
  // ----------------------------------------------------
  
  getAllDepartment(): Observable<any> {
    return this.http
      .get(`${this.baseUrl}/department`, { headers: this.getHeaders() })
      .map(response => this.extractData(response))
      .catch(this.handleError);
  }
  getAllActiveDepartment(): Observable<any> {
    return this.http
      .get(`${this.baseUrl}/department?isactive=true`, { headers: this.getHeaders() })
      .map(response => this.extractData(response))
      .catch(this.handleError);
  }
  
  getDepartment(id: number): Observable<Department> {
    const url = `${this.baseUrl}/department/${id}`;
    return this.http
      .get(url, { headers: this.getHeaders() })
      .map(response => this.extractData(response))
      .catch(this.handleError);
  }

  updateDepartment(department: Department): Observable<Department> {
    const url = `${this.baseUrl}/department/${department.id}`;
    return this.http
      .put(url, department, { headers: this.getHeaders() }) // because it update objct value so we use PUT method
      .map(response => this.extractData(response))
      .catch(this.handleError);
  }

  createDepartment(department: Department): Observable<Department> {
    return this.http
      .post(`${this.baseUrl}/department`, department, { headers: this.getHeaders() })
      .map(response => this.extractData(response))
      .catch(this.handleError);
  }

  deleteDepartment(departmentId: number): Observable<number> {
    return this.http
      .delete(`${this.baseUrl}/department/${departmentId}`, { headers: this.getHeaders() })
      .map(response => this.extractData(response))
      .catch(this.handleError);
  }

  private extractData(response: Response) {
    if (response.status < 200 || response.status >= 300) {
      throw new Error('Bad response status: ' + response.status);
    }
    try{
      return response.json();
    }catch(e){
      return {}
    }
  }

  private handleError(errorResponse: Response) {
    let body = errorResponse.json();
    let message = body.message ?
      body.message :
      (errorResponse.statusText || 'unknown error');
    return Observable.throw(message);
  }


}

/* function mapDepartments(response: Response): Department[] {
  // The response of the API has a results
  // property with the actual results
  return response.json().map(toDepartment)
}

function toDepartment(r: any): Department {
  let dep = <Department>({
    id: r.id,
    name: r.name,
    aliasName: r.aliasName,
    email: r.email,
    posts: [{ post: r.posts[0].post, id: r.posts[0].id }]
  });
  console.log('Parsed person:', dep);
  return dep;
} */



//
// [{"id":9520020,"name":"วิศวกรควบคุมความปลอดภัย","aliasName":"วคภม.","email":"9524040@egat.go.th","posts":[{"post":"นายสมชาย ใจดี (294357)","id":1}]},
//{"id":9524040,"name":"สำนักสารสนเทศโรงไฟฟ้าแม่เมาะ","aliasName":"สท-ฟม.","email":"9520020@egat.go.th","posts":[{"post":"นายสมภพ สามารถ (346659)","id":2
