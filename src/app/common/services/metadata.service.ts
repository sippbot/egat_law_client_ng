import { Injectable } from '@angular/core';
import { Department } from '../models/department.model';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { Category } from '../models/category.model';
import { Lawlevel } from '../models/lawlevel.model';
import { UserGroup } from '../models/usergroup.model';
import { UserPermission } from '../models/user-permissions.model';
import { Role } from '../models/role.model';
import { ApplicationConstant } from '../application-constant';

@Injectable()
export class MetadataService {
  baseUrl: string = ApplicationConstant.APP_BASE_ENDPOINT_URL;
  constructor(private http: Http) { }
  private getHeaders() {
    // I included these headers because otherwise FireFox
    // will request text/html instead of application/json
    const headers = new Headers();
    headers.append('Accept', 'application/json');
    return headers;
  }
  getMetadata(): Observable<any> {
    return this.http
      .get(`${this.baseUrl}/artifactmetadata`, { headers: this.getHeaders() })
      .map(response => this.extractData(response))
      .catch(this.handleError);
  }
  updateCategory(cat: Category): Observable<Category> {
    console.log('updateCategory');
    const id = cat.id;
    const url = `${this.baseUrl}/artifactcategory/${id}`;
    return this.http
      .put(url, cat, { headers: this.getHeaders() }) // because it update objct value so we use PUT method
      .map(response => this.extractData(response))
      .catch(this.handleError);
  }

  updateLawlevel(level: Lawlevel): Observable<Lawlevel> {
    const id = level.id;
    const url = `${this.baseUrl}/artifactlevel/${id}`;
    return this.http
      .put(url, level, { headers: this.getHeaders() }) // because it update objct value so we use PUT method
      .map(response => this.extractData(response))
      .catch(this.handleError);
  }
  getUserGroup(): Observable<any> {
    return this.http
      .get(`${this.baseUrl}/usergroup`, { headers: this.getHeaders() })
      .map(response => this.extractData(response))
      .catch(this.handleError);
  }
  getUserGroupPermission(): Observable<any> {
    return this.http
      .get(`${this.baseUrl}/usergroup?issetpermission=true`, { headers: this.getHeaders() })
      .map(response => this.extractData(response))
      .catch(this.handleError);
  }
  getAllCategory(): Observable<Category[]> {
    const metadata$ = this.http
      .get(`${this.baseUrl}/artifactmetadata`, { headers: this.getHeaders() })
      .map(response => this.extractDataBlock('categories', response));
    return metadata$;
  }
  getAllLevel(): Observable<Lawlevel[]> {
    const metadata$ = this.http
      .get(`${this.baseUrl}/artifactmetadata`, { headers: this.getHeaders() })
      .map(response => this.extractDataBlock('levels', response));
    return metadata$;
  }
  createCategory(category: Category): Observable<Category> {
    return this.http
      .post(`${this.baseUrl}/artifactcategory`, category, { headers: this.getHeaders() })
      .map(response => this.extractData(response))
      .catch(this.handleError);
  }
  createLawlevel(lawlevel: Lawlevel): Observable<Lawlevel> {
    return this.http
      .post(`${this.baseUrl}/artifactlevel`, lawlevel, { headers: this.getHeaders() })
      .map(response => this.extractData(response))
      .catch(this.handleError);
  }
  updateUserGroup(usg: UserGroup): Observable<UserGroup> {
    const id = usg.id;
    const url = `${this.baseUrl}/usergroup/${id}`;
    return this.http
      .put(url, usg, { headers: this.getHeaders() }) // because it update objct value so we use PUT method
      .map(response => this.extractData(response))
      .catch(this.handleError);
  }
  createUserGroup(usergroup: UserGroup): Observable<UserGroup> {
    return this.http
      .post(`${this.baseUrl}/usergroup`, usergroup, { headers: this.getHeaders() })
      .map(response => this.extractData(response))
      .catch(this.handleError);
  }
  deleteUserGroup(groupId: number): Observable<number> {
    const url = `${this.baseUrl}/usergroup/${groupId}`;
    return this.http
      .delete(url, { headers: this.getHeaders() })
      .map(response => this.extractData(response))
      .catch(this.handleError);
  }

  deleteCategory(categoryId: number): Observable<number> {
    const url = `${this.baseUrl}/artifactcategory/${categoryId}`;
    return this.http
      .delete(url, { headers: this.getHeaders() })
      .map(response => this.extractData(response))
      .catch(this.handleError);
  }

  deleteLevel(levelId: number): Observable<number> {
    const url = `${this.baseUrl}/artifactlevel/${levelId}`;
    return this.http
      .delete(url, { headers: this.getHeaders() })
      .map(response => this.extractData(response))
      .catch(this.handleError);
  }

  getRole(): Observable<any> {
    return this.http
      .get(`${this.baseUrl}/role`, { headers: this.getHeaders() })
      .map(response => this.extractData(response))
      .catch(this.handleError);
  }
  updateRole(role: Role): Observable<Role> {
    const id = role.id;
    const url = `${this.baseUrl}/role/${id}`;
    return this.http
      .put(url, role, { headers: this.getHeaders() }) // because it update objct value so we use PUT method
      .map(response => this.extractData(response))
      .catch(this.handleError);
  }
  createRole(role: Role): Observable<Role> {
    return this.http
      .post(`${this.baseUrl}/role`, role, { headers: this.getHeaders() })
      .map(response => this.extractData(response))
      .catch(this.handleError);
  }
  deleteRole(roleId: number): Observable<number> {
    const url = `${this.baseUrl}/role/${roleId}`;
    return this.http
      .delete(url, { headers: this.getHeaders() })
      .map(response => this.extractData(response))
      .catch(this.handleError);
  }

  private extractData(response: Response) {
    if (response.status < 200 || response.status >= 300) {
      throw new Error('Bad response status: ' + response.status);
    }
    try{
      return response.json();
    }catch(e){
      return {}
    }
  }
  private extractDataBlock(blockName: string, response: Response) {
    if (response.status < 200 || response.status >= 300) {
      console.log('Erro : Bad response');
      throw new Error('Bad response status: ' + response.status);
    }
    return response.json()[blockName];
  }
  private handleError(errorResponse: Response) {
    const body = errorResponse.json();
    const message = body.message ?
      body.message :
      (errorResponse.statusText || 'unknown error');
    return Observable.throw(message);
  }
}
// TODO: should move all external-class method into correspnding class
function mapCategory(response: Response): Category[] {
  // console.log("categories count ======>" + response.json().categories.length)
  return response.json().categories.map(toCategory);
}
function mapLawlevel(response: Response): Lawlevel[] {
  return response.json().levels.map(toLevel);
}
function toCategory(r: any): Category {
  const cat = <Category>({
    id: r.id,
    name: r.name,
    aliasName: r.aliasName
  });
  return cat;
}
function toLevel(r: any): Lawlevel {
  const level = <Lawlevel>({
    id: r.id,
    name: r.name,
    aliasName: r.aliasName
  });
  return level;
}
function handleSuccess(response: Response): Category {
  if (response.ok) {
    const cat = <Category>({
      id: response.json().id,
      name: response.json().name,
      aliasName: response.json().aliasName
    });
    return cat;
  }
}
