import { ArtifactNode } from '../models/artifact-node.model';
import { ArtifactNodeGroup } from '../models/artifact-node-group.model';
import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { ArtifactSummary } from '../models/artifact-summary.model';
import { ApplicationConstant } from '../application-constant';
import { Artifact } from '../models/artifact.model';

@Injectable()
export class ArtifactService {
  baseUrl: string = ApplicationConstant.APP_BASE_ENDPOINT_URL;
  constructor(private http: Http) { }

  ///*** get approve artifact ****///
  getArtifactSummaryByStatus(status: string): Observable<ArtifactSummary[]> {
    const url = `${this.baseUrl}/artifactsummary?status=${status}`;
    const metadata$ = this.http
      .get(url, { headers: this.getHeaders() })
      .map(response => this.extractData(response));
    return metadata$;
  }

  getCanceledArtifactSummary(departmentId: number): Observable<ArtifactSummary[]> {
    const url = `${this.baseUrl}/cancellaw?departmentId=${departmentId}`;
    const metadata$ = this.http
      .get(url, { headers: this.getHeaders() })
      .map(response => this.extractData(response));
    return metadata$;
  }
  ///*** get approve artifact ****///
  getArtifactSummary(departmentId: number): Observable<ArtifactSummary[]> {
    const url = `${this.baseUrl}/artifactsummary?departmentId=${departmentId}`;
    const metadata$ = this.http
      .get(url, { headers: this.getHeaders() })
      .map(response => this.extractData(response));
    return metadata$;
  }

  ///*** get view artifact ****///
  getArtifact(id: number,isactive:boolean): Observable<Artifact> {
    const url = `${this.baseUrl}/artifact/${id}?isactive=${isactive}`;
    const artifact$ = this.http
      .get(url, { headers: this.getHeaders() })
      .map(response => this.extractData(response));
    return artifact$;
  }

  createArtifact(artifact: Artifact): Observable<Artifact> {
    const artifact$ = this.http
      .post(`${this.baseUrl}/artifact/create`, artifact, { headers: this.getHeaders() })
      .map(response => this.extractData(response));
    return artifact$;
  }

  updateArtifact(id: number, artifact: Artifact): Observable<Artifact> {
    const url = `${this.baseUrl}/artifact/${id}`;
    const artifact$ = this.http
      .put(url, artifact, { headers: this.getHeaders() })
      .map(response => this.extractData(response));
    return artifact$;
  }
  deleteArtifact(artifactId: number): Observable<Artifact> {
    const artifact$ = this.http
      .delete(`${this.baseUrl}/artifact/${artifactId}`, { headers: this.getHeaders() })
      .map(response => this.extractData(response));
    return artifact$;
  }

  createArtifactNodeGroup(artifactNodeGroup: ArtifactNodeGroup): Observable<Artifact> {
    const artifact$ = this.http
      .post(`${this.baseUrl}/artifact/group`, artifactNodeGroup, { headers: this.getHeaders() })
      .map(response => this.extractData(response));
    return artifact$;
  }

  deleteArtifactNodeGroup(artifactNodeGroupId: number): Observable<Artifact> {
    const artifact$ = this.http
      .delete(`${this.baseUrl}/artifact/group/${artifactNodeGroupId}`, { headers: this.getHeaders() })
      .map(response => this.extractData(response));
    return artifact$;
  }

  updateArtifactNodeGroup(id: number, artifactNodeGroup: ArtifactNodeGroup): Observable<Artifact> {
    const url = `${this.baseUrl}/artifact/group/${id}`;
    const artifact$ = this.http
      .put(url, artifactNodeGroup, { headers: this.getHeaders() })
      .map(response => this.extractData(response));
    return artifact$;
  }

  createArtifactNode(artifactNode: ArtifactNode): Observable<Artifact> {
    const artifact$ = this.http
      .post(`${this.baseUrl}/artifact/node`, artifactNode, { headers: this.getHeaders() })
      .map(response => this.extractData(response));
    return artifact$;
  }

  deleteArtifactNode(artifactNodeId: number): Observable<Artifact> {
    const artifact$ = this.http
      .delete(`${this.baseUrl}/artifact/node/${artifactNodeId}`, { headers: this.getHeaders() })
      .map(response => this.extractData(response));
    return artifact$;
  }

  updateArtifactNode(id: number, artifactNode: ArtifactNode): Observable<Artifact> {
    const url = `${this.baseUrl}/artifact/node/${id}`;
    const artifact$ = this.http
      .put(url, artifactNode, { headers: this.getHeaders() })
      .map(response => this.extractData(response));
    return artifact$;
  }

  createArtifactAssignment(artifactAssignmentInfo: any): Observable<any> {
    const artifact$ = this.http
      .post(`${this.baseUrl}/artifactassignment`, artifactAssignmentInfo, { headers: this.getHeaders() })
      .map(response => this.extractData(response));
    return artifact$;
  }

  createArtifactReviewAssignment(artifactreviewassessment: any): Observable<any> {
    const artifact$ = this.http
      .post(`${this.baseUrl}/artifactreviewassessment`, artifactreviewassessment, { headers: this.getHeaders() })
      .map(response => this.extractData(response));
    return artifact$;
  }

  deleteArtifactAttachment(artifactId: number, documentId: number): Observable<any> {
    const artifact$ = this.http
      .delete(`${this.baseUrl}/artifact/attachment/${artifactId}/${documentId}`, { headers: this.getHeaders() })
      .map(response => this.extractData(response));
    return artifact$;
  }

  // ****** review-artifact-assesment
  getArtifactSummaryById(id: number): Observable<any> {
    const url = `${this.baseUrl}/artifactsummary/${id}`;
    const artifact$ = this.http
      .get(url, { headers: this.getHeaders() })
      .map(response => this.extractData(response));
    return artifact$;
  }

  private extractData(response: Response) {
    if (response.status < 200 || response.status >= 300) {
      throw new Error('Bad response status: ' + response.status);
    }
    try{
      return response.json();
    }catch(e){
      return {}
    }
  }

  private extractDataBlock(blockName: string, response: Response) {
    if (response.status < 200 || response.status >= 300) {
      console.log('Erro : Bad response');
      throw new Error('Bad response status: ' + response.status);
    }
    return response.json()[blockName];
  }

  private handleError(errorResponse: Response) {
    const body = errorResponse.json();
    const message = body.message ?
      body.message :
      (errorResponse.statusText || 'unknown error');
    return Observable.throw(message);
  }

  private onSuccess(response: Response) {
    if (response.ok) {
      const art = <ArtifactSummary>({
        code: response.json().code,
        title: response.json().title,
        category: '',
        level: '',
        publishedDate: response.json().publishedDate,
        effectiveDate: response.json().effectiveDate
      });
      return art;
    }
  }

  private getHeaders() {
    const headers = new Headers();
    headers.append('Accept', 'application/json');
    // headers.append('Accept-Encoding', 'gzip');
    return headers;
  }
}

// TODO: should move all external-class method into correspnding class
function mapArtifactSummary(response: Response): ArtifactSummary[] {
  return response.json().map(toArtifactSummary);
}

function toArtifactSummary(r: any): ArtifactSummary {
  const ats = <ArtifactSummary>({
    code: r.code,
    category: r.category,
    level: r.level,
    title: r.title,
    status: r.status,
    publishedDate: r.publishedDate,
    effectiveDate: r.effectiveDate
  });
  return ats;
}
