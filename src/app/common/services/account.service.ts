import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'
import {ApplicationConstant} from '../application-constant';

import { AuthenticationService } from './authentication.service';
import { Account } from '../models/account.model';

@Injectable()
export class AccountService {
    constructor(
        private http: Http,
        private authenticationService: AuthenticationService) {
    }
    baseUrl: string = ApplicationConstant.APP_BASE_ENDPOINT_URL;
    getUsers(): Observable<Account[]> {
        // add authorization header with jwt token
        let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
        let options = new RequestOptions({ headers: headers });

        // get users from api //TODO end point should be proper one
        return this.http.get(`${this.baseUrl}/api/user`, options)
            .map((response: Response) => response.json());
    }
}
