import { Document } from '../models/document.model';
import { TaskAssignmentSummary } from '../models/task-assignment-summary.model';
import { AssessmentReview } from '../models/assessment-review.model';
import { AssessmentInfo } from '../models/assessment-info.model';
import { TaskAssignment } from '../models/task-assignment.model';
import { TaskApproval } from '../models/task-approval.model';
import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { ApplicationConstant } from '../application-constant';

@Injectable()
export class TaskAssignmentService {
  baseUrl: string = ApplicationConstant.APP_BASE_ENDPOINT_URL;
  constructor(private http: Http) { }

  private getHeaders() {
    // I included these headers because otherwise FireFox
    // will request text/html instead of application/json
    const headers = new Headers();
    headers.append('Accept', 'application/json');
    return headers;
  }

  getInprogressTaskAssignment(departmentId: number, artifactId: number): Observable<TaskAssignment[]> {
    let url = `${this.baseUrl}/taskassignment?departmentId=${departmentId}&artifactId=${artifactId}`;
    if(departmentId==undefined){
      url =  `${this.baseUrl}/taskassignment?artifactId=${artifactId}`;
    }
    
    return this.http
      .get(url, { headers: this.getHeaders() })
      .map(response => this.extractData(response))
      .catch(this.handleError);
  }

  getTaskAssignmentById(taskAssginmentId: number): Observable<TaskAssignment> {
    const url = `${this.baseUrl}/taskassignment/${taskAssginmentId}`;
    return this.http
      .get(url, { headers: this.getHeaders() })
      .map(response => this.extractData(response))
      .catch(this.handleError);
  }

  getTaskAssignmentSummaryByArtifactId(taskAssginmentId: number): Observable<TaskAssignmentSummary[]> {
    const url = `${this.baseUrl}/taskassignmentsummary/${taskAssginmentId}`;
    return this.http
      .get(url, { headers: this.getHeaders() })
      .map(response => this.extractData(response))
      .catch(this.handleError);
  }

  createTaskAssessment(taskAssessmentInfo: AssessmentInfo): Observable<any> {

    
    let params = {
      taskAssignmentId : taskAssessmentInfo.taskAssignmentId,
      departmentId : taskAssessmentInfo.departmentId,
      creatorId : taskAssessmentInfo.creatorId,
      isApplicable : taskAssessmentInfo.isApplicable
    }
    let isApplicable = taskAssessmentInfo.isApplicable
    // console.log(isApplicable,typeof(isApplicable))
    if(isApplicable!=true){
      // params['detail'] = ""
      // params['isCompliant'] = null
    }else{
      params['detail'] =  taskAssessmentInfo.detail
      params['isCompliant'] =  taskAssessmentInfo.isCompliant
    }
    const url = `${this.baseUrl}/taskassessment/create`;
    return this.http
      .post(url, params, { headers: this.getHeaders() })
      .map(response => this.extractData(response))
      .catch(this.handleError);
  }

  createTaskAssessmentReview(taskAssessmentReview: AssessmentReview): Observable<any> {
    const url = `${this.baseUrl}/taskassessment/review`;
    return this.http
      .post(url, taskAssessmentReview, { headers: this.getHeaders() })
      .map(response => this.extractData(response))
      .catch(this.handleError);
  }

  createTaskAssignmentProposal(taskAssignmentProposal: any): Observable<any> {
    const url = `${this.baseUrl}/taskassignment/proposal`;
    return this.http
      .post(url, taskAssignmentProposal, { headers: this.getHeaders() })
      .map(response => this.extractData(response))
      .catch(this.handleError);
  }

  createTaskComment(taskComment: any): any {
    const url = `${this.baseUrl}/taskassignment/comment`;
    return this.http
      .post(url, taskComment, { headers: this.getHeaders() })
      .map(response => this.extractData(response))
      .catch(this.handleError);
  }

  createTaskAttachment(taskComment: any): Observable<Document> {
    const url = `${this.baseUrl}/taskassignment/attachment`;
    return this.http
      .post(url, taskComment, { headers: this.getHeaders() })
      .map(response => this.extractData(response))
      .catch(this.handleError);
  }

  deleteTaskAttachment(taskAssignmentId: number, documentId: number): Observable<number> {
    const url = `${this.baseUrl}/taskassignment/attachment/${taskAssignmentId}/${documentId}`;
    return this.http
      .delete(url, { headers: this.getHeaders() })
      .map(response => this.extractData(response))
      .catch(this.handleError);
  }

  approveTaskAssignmentProposal(taskAssignmentApproval: TaskApproval): Observable<any> {
    console.log("send")
    const url = `${this.baseUrl}/taskassignment/approve`;
    return this.http
      .post(url, taskAssignmentApproval, { headers: this.getHeaders() })
      .map(response => this.extractData(response))
      .catch(this.handleError);
  }

  private extractData(response: Response) {
    if (response.status < 200 || response.status >= 300) {
      throw new Error('Bad response status: ' + response.status);
    }
    try{
      console.log(response,"1",response.json())
      return response.json();
    }catch(e){
      console.log(response,"2",{})
      return {}
    }
  }

  private handleError(errorResponse: Response) {
    console.log("error",errorResponse)
    const body = errorResponse.json();
    const message = body.message ?
      body.message :
      (errorResponse.statusText || 'unknown error');
    return Observable.throw(message);
  }

}
