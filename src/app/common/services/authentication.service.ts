import { Store } from '@ngrx/store';
import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { ApplicationConstant } from '../application-constant';
import * as reducer from '../reducers/reducer';
import * as user from '../actions/users.action';

@Injectable()
export class AuthenticationService {
  public token: string;
  baseUrl: string = ApplicationConstant.APP_LOGIN_ENDPOINT_URL;

  constructor(private http: Http, private _store: Store<reducer.State>) {
    // set token if saved in local storage
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.token = currentUser && currentUser.token;
  }

  login(username: string, password: string): Observable<boolean> {
    // TODO chang back to stringfy json when ready
    const formData: FormData = new FormData();
    formData.append('username', username);
    formData.append('password', password);

    // return this.http.post(`${this.baseUrl}/login`, { username: username, password: password }, { headers: this.getHeaders() })
    return this.http.post(`${this.baseUrl}/login`, formData, { headers: this.getHeaders() })
      .map((response: Response) => {
        if (response.json().statusCode > 400) {
          return false;
        }
        // login successful if there's a jwt token in the response
        const token = response.json() && response.json().token;
        const returnUsername = response.json() && response.json().username;
        const departmentId = response.json() && response.json().departmentId;
        const userId = response.json() && response.json().userId;
        const roles = response.json() && response.json().roles.keyList;
        const roles_privilege = response.json() && response.json().roles.keyPrivileges;
        // const userGroup = response.json() && response.json().userGroup;
        // let xsrfToken = response.headers.get('XSRF-TOKEN');
        // console.log(xsrfToken);
        // localStorage.setItem('xsrfToken', xsrfToken.trim());

        this._store.dispatch(new user.LoadUserLoginAction(userId));

        // console.log('token');
        // console.log(token);
        if (token) {
          // set token property
          this.token = token;
          // store username and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify({ username: returnUsername, token: token }));
          localStorage.setItem('username', returnUsername);
          localStorage.setItem('departmentId', departmentId);
          localStorage.setItem('userId', userId);
          localStorage.setItem('roles', JSON.stringify(roles_privilege));
          // localStorage.setItem('roles_privilege', JSON.stringify(roles_privilege));
          // localStorage.setItem('roles',"['ACLMNG','ATFAPP','ATFMNG','ATFVIW','DEPMNG','DOCMNG','METDAT','SYSSET','TCKMNG','TSKASM','TSKVIW','TSKRVW','LCNMNG','TSKAPP','USRMNG']")
          // let keyList = ['ACLMNG','ATFAPP','ATFMNG','ATFVIW','DEPMNG','DOCMNG','SYSSET','TCKMNG','TSKASM','TSKVIW','TSKRVW','LCNMNG','TSKAPP','USRMNG']
          // localStorage.setItem('roles',JSON.stringify(keyList))
          // console.log("set roles")
          // return true to indicate successful login
          return true;
        } else {
          // return false to indicate failed login
          return false;
        }
      }).catch(this.handleError);
  }

  private handleError(errorResponse: Response) {
    const body = errorResponse.json();
    const message = body.message ?
      body.message :
      (errorResponse.statusText || 'unknown error');
    return Observable.throw(message);
  }

  logout(): void {
    // clear token remove user from local storage to log user out
    this.token = null;
    localStorage.removeItem('currentUser');
    localStorage.removeItem('username');
    localStorage.removeItem('userGroup');
    localStorage.removeItem('departmentId');
    localStorage.removeItem('userId');
    localStorage.removeItem('roles');
    // localStorage.removeItem('roles_privilege');
  }
  private getHeaders() {
    // I included these headers because otherwise FireFox
    // will request text/html instead of application/json
    const headers = new Headers();
    headers.append('Accept', 'application/json');
    // headers.append('Content-Type', 'application/x-www-form-urlencoded');
    return headers;
  }

}
