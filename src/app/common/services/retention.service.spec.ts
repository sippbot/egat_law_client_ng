import { TestBed, inject } from '@angular/core/testing';

import { RetentionService } from './retention.service';

describe('RetentionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RetentionService]
    });
  });

  it('should be created', inject([RetentionService], (service: RetentionService) => {
    expect(service).toBeTruthy();
  }));
});
