import {Injectable} from '@angular/core';
import {Headers, Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {ApplicationConstant} from '../application-constant';
import {Department} from '../models/department.model';
import {Post} from '../models/post.model';
import 'rxjs/add/operator/map';

@Injectable()
export class PostService {
    baseUrl: string = ApplicationConstant.APP_BASE_ENDPOINT_URL;

    constructor(private http: Http) {
    }

    private getHeaders() {
        // I included these headers because otherwise FireFox
        // will request text/html instead of application/json
        let headers = new Headers();
        headers.append('Accept', 'application/json');
        return headers;
    }

    getPosts(dep: number): Observable<Post[]> {
        return this.http
            .get(`${this.baseUrl}/post/${dep}`, {headers: this.getHeaders()})
            .map(response => this.extractData(response))
            .catch(this.handleError);
    }

    postUser(post: Post): Observable<Post> {
        console.log("service postUser")
        return this.http
            .post(`${this.baseUrl}/post`, post, {headers: this.getHeaders()})
            .map(response => this.extractData(response))
            .catch(this.handleError);
    }
    removeUser(post: Post): Observable<Post> {
        console.log("service removeUser")
        return this.http
            .delete(`${this.baseUrl}/post/${post.id}`, { headers: this.getHeaders() })
            .map(response => this.extractData(response))
            .catch(this.handleError);
    }

    private extractData(response: Response) {
        if (response.status < 200 || response.status >= 300) {
            throw new Error('Bad response status: ' + response.status);
        }
        // console.log("response",response.json())
        try{
            return response.json();
        }catch(e){
            return {}
        }
    }

    private handleError(errorResponse: Response) {
        let body = errorResponse.json();
        let message = body.message ?
            body.message :
            (errorResponse.statusText || 'unknown error');
        return Observable.throw(message);
    }


}

//
// [{"id":9520020,"name":"วิศวกรควบคุมความปลอดภัย","aliasName":"วคภม.","email":"9524040@egat.go.th","posts":[{"post":"นายสมชาย ใจดี (294357)","id":1}]},
// {"id":9524040,"name":"สำนักสารสนเทศโรงไฟฟ้าแม่เมาะ","aliasName":"สท-ฟม.","email":"9520020@egat.go.th","posts":[{"post":"นายสมภพ สามารถ (346659)","id":2
