import { Injectable } from '@angular/core';
import { User } from '../models/user.model';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { ApplicationConstant } from '../application-constant';

@Injectable()
export class UserService {
  baseUrl: string = ApplicationConstant.APP_BASE_ENDPOINT_URL;
  constructor(private http: Http) { }

  private getHeaders() {
    // I included these headers because otherwise FireFox
    // will request text/html instead of application/json
    const headers = new Headers();
    headers.append('Accept', 'application/json');
    return headers;
  }

  getAllUsers(): Observable<any> {
    return this.http
      .get(`${this.baseUrl}/user`, { headers: this.getHeaders() })
      .map(response => this.extractData(response))
      .catch(this.handleError);
  }

  getUser(id: number): Observable<User> {
    return this.http
      .get(`${this.baseUrl}/user/${id}`, { headers: this.getHeaders() })
      .map(response => {
        if (response.json().statusCode > 400) {
          return false;
        }
        console.log(response.json() && response.json())
        const groupId = response.json() && response.json().groupId;
        localStorage.setItem('userGroup',groupId)
        return this.extractData(response)
      })
      .catch(this.handleError);
  }

  updateUser(us: User): Observable<User> {
    const id = us.id;
    const url = `${this.baseUrl}/user/${id}`;
    return this.http
      .put(url, us, { headers: this.getHeaders() }) // because it update objct value so we use PUT method
      .map(response => this.extractData(response))
      .catch(this.handleError);
  }

  createUser(us: User): Observable<User> {
    return this.http
      .post(`${this.baseUrl}/user`, us, { headers: this.getHeaders() })
      .map(response => this.extractData(response))
      .catch(this.handleError);
  }

  deleteUser(userId: number): Observable<number> {
    return this.http
      .delete(`${this.baseUrl}/user/${userId}`, { headers: this.getHeaders() })
      .map(response => this.extractData(response))
      .catch(this.handleError);
  }

  private extractData(response: Response) {
    if (response.status < 200 || response.status >= 300) {
      throw new Error('Bad response status: ' + response.status);
    }
    try{
      return response.json();
    }catch(e){
      return {}
    }
  }

  private handleError(errorResponse: Response) {
    const body = errorResponse.json();
    const message = body.message ?
      body.message :
      (errorResponse.statusText || 'unknown error');
    return Observable.throw(message);
  }
}
