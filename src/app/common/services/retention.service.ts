import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { Retention } from '../models/retention.model';
import { ApplicationConstant } from '../application-constant';


@Injectable()
export class RetentionService {
  baseUrl: string = ApplicationConstant.APP_BASE_ENDPOINT_URL;
  constructor(private http: Http) { }

  private getHeaders() {
    // I included these headers because otherwise FireFox
    // will request text/html instead of application/json
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    return headers;
  }

  //----------------------------------------------------
  //         new version
  //----------------------------------------------------

  getRetentionArtifactsNew(): Observable<Retention[]> {
    let metadata$ = this.http
      .get(`${this.baseUrl}/artifactsummary?Status=untrack`, { headers: this.getHeaders() })
      .map(response => this.extractData(response));
    return metadata$;
  }

  getRetentionArtifactsFollowUp(): Observable<Retention[]> {
    let metadata$ = this.http
      .get(`${this.baseUrl}/artifactsummary?Status=track`, { headers: this.getHeaders() })
      .map(response => this.extractData(response));
    return metadata$;
  }

  getRetentionArtifacts(id: number): Observable<any> {
    let url = `${this.baseUrl}/artifact/${id}`;
    let artifact$ = this.http
      .get(url, { headers: this.getHeaders() })
      .map(response => this.extractData(response));
    return artifact$;
  }


  craeteTrackingPlan(trankingplan: any): Observable<any> {
    let artifact$ = this.http
      .post(`${this.baseUrl}/trackingplan`, trankingplan, { headers: this.getHeaders() })
      .map(response => this.extractData(response));
    return artifact$;
  }


  getTrackingPlan(): Observable<Retention[]> {
    let metadata$ = this.http
      .get(`${this.baseUrl}/trackingplan`, { headers: this.getHeaders() })
      .map(response => this.extractData(response));
    return metadata$;
  }


  getAllTaskDue(): Observable<Retention[]> {
    let metadata$ = this.http
      .get(`${this.baseUrl}/trackingtask`, { headers: this.getHeaders() })
      .map(response => this.extractData(response));
    return metadata$;
  }

  getAllTaskOverDue(): Observable<Retention[]> {
    let metadata$ = this.http
      .get(`${this.baseUrl}/trackingtask?overdue=true`, { headers: this.getHeaders() })
      .map(response => this.extractData(response));
    return metadata$;
  }

  getDepartmentPlan(id: number): Observable<any[]> {
    let metadata$ = this.http
      .get(`${this.baseUrl}/department?artifactId=${id}`, { headers: this.getHeaders() })
      .map(response => this.extractData(response));
    return metadata$;
  }

  getViewTask(id: number): Observable<any> {
    let url = `${this.baseUrl}/.....????..../${id}`;
    let artifact$ = this.http
      .get(url, { headers: this.getHeaders() })
      .map(response => this.extractData(response));
    return artifact$;
  }

  getSelectViewTracking(id: number): Observable<any> {
    let url = `${this.baseUrl}/trackingplan/${id}`;
    let artifact$ = this.http
      .get(url, { headers: this.getHeaders() })
      .map(response => this.extractData(response));
    return artifact$;
  }


  craeteCheckEvaluate(CheckEvaluate: any): Observable<any> {
    let artifact$ = this.http
      .post(`${this.baseUrl}/taskassessment`, CheckEvaluate, { headers: this.getHeaders() })
      .map(response => this.extractData(response));
    return artifact$;
  }


  craeteArtifactreviewAcknowledge(ArtifactreviewAcknowledge: any): Observable<any> {
    let artifact$ = this.http
      .post(`${this.baseUrl}/taskassessmentreview`, ArtifactreviewAcknowledge, { headers: this.getHeaders() })
      .map(response => this.extractData(response));
    return artifact$;
  }

  CreateApproveArtifactAssignment(approveartifactassignment: any): Observable<any> {
    let artifact$ = this.http
      .post(`${this.baseUrl}/taskassessmentproposal`, approveartifactassignment, { headers: this.getHeaders() })
      .map(response => this.extractData(response));
    //.map(this.onSuccess);
    return artifact$;
  }

  SubmitApproveRetention(submitApprove: any): Observable<any> {
    let artifact$ = this.http
      .post(`${this.baseUrl}/taskassessmentapproval`, submitApprove, { headers: this.getHeaders() })
      .map(response => this.extractData(response));
    //.map(this.onSuccess);
    return artifact$;
  }



  getPlanAssessment(id: number): Observable<any> {
    let url = `${this.baseUrl}/taskassessment?planId=${id}`;
    let artifact$ = this.http
      .get(url, { headers: this.getHeaders() })
      .map(response => this.extractData(response));
    return artifact$;
  }

  getSubTaskPlanAssessment(id: number): Observable<any> {


    let url = `${this.baseUrl}/taskassessment?id=${id}`;
    let artifact$ = this.http
      .get(url, { headers: this.getHeaders() })
      .map(response => this.extractData(response));


    return artifact$;
  }

  getRetentionResult(): Observable<any> {
    let metadata$ = this.http
      .get(`${this.baseUrl}/taskassessment`, { headers: this.getHeaders() })
      .map(response => this.extractData(response));
    return metadata$;
  }

  getApproveRetentionAssessment(id: number): Observable<any> {
    let url = `${this.baseUrl}/taskassessment?planId=${id}&&submitted=true`;
    let artifact$ = this.http
      .get(url, { headers: this.getHeaders() })
      .map(response => this.extractData(response));
    return artifact$;
  }


  private extractData(response: Response) {
    if (response.status < 200 || response.status >= 300) {
      throw new Error('Bad response status: ' + response.status);
    }
    try{
      return response.json();
    }catch(e){
      return {}
    }
  }

  private handleError(errorResponse: Response) {
    let body = errorResponse.json();
    let message = body.message ?
      body.message :
      (errorResponse.statusText || 'unknown error');
    return Observable.throw(message);
  }

  private extractDataBlock(blockName: string, response: Response) {
    if (response.status < 200 || response.status >= 300) {
      console.log('Erro : Bad response');
      throw new Error('Bad response status: ' + response.status);
    }
    return response.json()[blockName];
  }




}
