import { Ticket } from '../models/ticket.model';
import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { ApplicationConstant } from '../application-constant';

@Injectable()
export class TicketService {
  baseUrl: string = ApplicationConstant.APP_BASE_ENDPOINT_URL;
  constructor(private http: Http) { }

  private getHeaders() {
    // I included these headers because otherwise FireFox
    // will request text/html instead of application/json
    const headers = new Headers();
    headers.append('Accept', 'application/json');
    return headers;
  }

  getAllTicket(payload: any): Observable<Ticket[]> {
    return this.http
      .get(`${this.baseUrl}/ticket?departmentId=${payload.departmentId}&status=${payload.status}`, { headers: this.getHeaders() })
      .map(response => this.extractData(response))
      .catch(this.handleError);
  }

  getTicketById(id: number): Observable<Ticket> {
    return this.http
      .get(`${this.baseUrl}/ticket/${id}`, { headers: this.getHeaders() })
      .map(response => this.extractData(response))
      .catch(this.handleError);
  }

  closeTicket(req: any): Observable<Ticket> {
    return this.http
      .post(`${this.baseUrl}/taskassignment/closeticket`, req, { headers: this.getHeaders() })
      .map(response => this.extractData(response))
      .catch(this.handleError);
  }

  private extractData(response: Response) {
    if (response.status < 200 || response.status >= 300) {
      throw new Error('Bad response status: ' + response.status);
    }
    try{
      return response.json();
    }catch(e){
      return {}
    }
    
  }

  private handleError(errorResponse: Response) {
    
    const body = errorResponse.json();
    const message = body.message ?
      body.message :
      (errorResponse.statusText || 'unknown error');
    console.log("handleError",message)
    return Observable.throw(message);
  }


}