export class ApplicationConstant {
  static readonly APP_BASE_ENDPOINT_URL : string = 'http://10.249.99.238/api';
  static readonly APP_LOGIN_ENDPOINT_URL : string = 'http://10.249.99.238';
  // static readonly APP_BASE_ENDPOINT_URL : string = 'http://localhost:8080/api';
  // static readonly APP_LOGIN_ENDPOINT_URL : string = 'http://localhost:8080';
  // static readonly APP_BASE_ENDPOINT_URL : string = 'http://localhost:49899/api';
  // static readonly APP_BASE_ENDPOINT_URL : string = 'http://localhost:8888/api';
  // static readonly APP_BASE_ENDPOINT_URL = 'http://45.76.8.242/api';
  // static readonly APP_LOGIN_ENDPOINT_URL = 'http://45.76.8.242';
  // static readonly APP_BASE_ENDPOINT_URL = 'http://localhost:55735/api';
  // static readonly APP_LOGIN_ENDPOINT_URL = 'http://localhost:55735';
  static readonly DATATABLE_GLOBAL_LANGUAGE_CONFIG = {
    'lengthMenu': 'แสดง _MENU_ รายการต่อหน้า',
    'zeroRecords': 'ไม่พบรายการที่ท่านค้นหา',
    'info': 'หน้าที่ _PAGE_ จาก _PAGES_ หน้า',
    'infoEmpty': 'ไม่มีข้อมูลสำหรับแสดงผล',
    'infoFiltered': '(filtered from _MAX_ total records)',
    'search': 'ค้นหา',
    'paginate': {
      'first': 'หน้าแรก',
      'last': 'หน้าสุดท้าย',
      'next': 'ถัดไป',
      'previous': 'ก่อนหน้า'
    }
  };
  static print_flag :boolean=true;
}
