export interface IMap<T> {
    [id: number]: T;
}
