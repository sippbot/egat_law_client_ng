export class ACL {
    roles: Array<string>;
    constructor() {
        // console.log("hhh",localStorage.getItem('roles'))
        this.roles = localStorage.getItem('roles') != null ? JSON.parse(localStorage.getItem('roles')) : [];
    }

    hasRole(key: string): boolean {
        // if ((localStorage.getItem('roles')!=null?JSON.parse(localStorage.getItem('roles')):[]).filter(item => item.toLowerCase() === key.toLowerCase()).length !== 0) {
        if ((localStorage.getItem('roles') != null ? JSON.parse(localStorage.getItem('roles')) : []).filter(item => item.key.toLowerCase() === key.toLowerCase()).length !== 0) {
            return true;
        }
        return false;
    }
    isReadOnly(key: string): boolean {
        // console.log("this.roles",key,localStorage.getItem('roles')!=null?JSON.parse(localStorage.getItem('roles')):[])
        if ((localStorage.getItem('roles') != null ? JSON.parse(localStorage.getItem('roles')) : []).filter(item => item.key.toLowerCase() === key.toLowerCase()).length !== 0) {
            // if (this.roles.filter(item => item.toLowerCase() === key.toLowerCase()).length !== 0) {
            // console.log((localStorage.getItem('roles')!=null?JSON.parse(localStorage.getItem('roles')):[]).filter(item => item.key.toLowerCase() === key.toLowerCase()))
            return (localStorage.getItem('roles') != null ? JSON.parse(localStorage.getItem('roles')) : []).filter(item => item.key.toLowerCase() === key.toLowerCase())[0].isReadOnly;
        }
        return true;
    }
}
