import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import * as usergroups from '../actions/usergroups.action';
import { MetadataService } from '../services/metadata.service';
import {
  LoadUserGroupSuccessAction, LoadUserGroupFailAction,
  LoadUserGroupPermissionSuccessAction, LoadUserGroupPermissionFailAction,
  UpdateUsergroupSuccessAction, UpdateUsergroupFailAction,
  CreateUsergroupSuccessAction, CreateUsergroupFailAction,
  DeleteUserGroupSuccessAction, DeleteUserGroupFailAction
} from '../actions/usergroups.action';
import { UserGroup } from '../models/usergroup.model';

@Injectable()
export class UserGroupEffects {
  /* The effects for different states are singletons that 'intercept'
  dispatched actions that are being sent to the reducer. */
  @Effect() loadUserGroups$ = this._actions.ofType(usergroups.ActionTypes.LOAD_USERGROUPS)
    .switchMap(() => this._metadataService.getUserGroup()
      .map((groups) => new LoadUserGroupSuccessAction(groups))
      .catch(() => Observable.of(new LoadUserGroupFailAction({}))));
  
  @Effect() loadUserGroupsPermission$ = this._actions.ofType(usergroups.ActionTypes.LOAD_USERGROUPS_PERMISSION)
    .switchMap(() => this._metadataService.getUserGroupPermission()
      .map((groups) => new LoadUserGroupPermissionSuccessAction(groups))
      .catch(() => Observable.of(new LoadUserGroupPermissionFailAction({}))));

  @Effect() updateUserGroup$ = this._actions.ofType(usergroups.ActionTypes.UPDATE_USERGROUPS)
    .map(action => action.payload)
    .switchMap((cat) => this._metadataService.updateUserGroup(cat)
      .map((category) => new UpdateUsergroupSuccessAction(category))
      .catch(() => Observable.of(new UpdateUsergroupFailAction({})))).share();

  @Effect() craeteUserGroup$ = this._actions.ofType(usergroups.ActionTypes.CREATE_USERGROUPS)
    .map(action => action.payload)
    .switchMap((userGroup) => this._metadataService.createUserGroup(userGroup)
      .map((category) => new CreateUsergroupSuccessAction(category))
      .catch(() => Observable.of(new CreateUsergroupFailAction({})))).share();

  @Effect() deleteUserGroup$ = this._actions.ofType(usergroups.ActionTypes.DELETE_USERGROUPS)
    .map(action => action.payload)
    .switchMap((userGroup) => this._metadataService.deleteUserGroup(userGroup)
      .map((id) => new DeleteUserGroupSuccessAction(id))
      .catch(() => Observable.of(new DeleteUserGroupFailAction({})))).share();

  constructor(private _actions: Actions,
    private _metadataService: MetadataService) { }
}
