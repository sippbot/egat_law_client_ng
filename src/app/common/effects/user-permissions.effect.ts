import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import * as userpermission from '../actions/user-permissions.action';
import { MetadataService } from '../services/metadata.service';
import {
  LoadRoleSuccessAction, LoadRoleFailAction,
  UpdateRoleSuccessAction, UpdateRoleFailAction,
  CreateRoleSuccessAction, CreateRoleFailAction,
  DeleteRoleSuccessAction, DeleteRoleFailAction
} from '../actions/user-permissions.action';
import { UserPermission } from '../models/user-permissions.model';
import { Role } from '../models/role.model';

@Injectable()
export class UserPermissionEffects {
  /* The effects for different states are singletons that 'intercept'
  dispatched actions that are being sent to the reducer. */
  @Effect() loadRole$ = this._actions.ofType(userpermission.ActionTypes.LOAD_ROLE)
    .switchMap(() => this._metadataService.getRole()
      .map((roles) => new LoadRoleSuccessAction(roles))
      .catch(() => Observable.of(new LoadRoleFailAction({}))));
  
  @Effect() updateRole$ = this._actions.ofType(userpermission.ActionTypes.UPDATE_ROLE)
    .map(action => action.payload)
    .switchMap((role) => this._metadataService.updateRole(role)
      .map((role) => new UpdateRoleSuccessAction(role))
      .catch(() => Observable.of(new UpdateRoleFailAction({})))).share();

  @Effect() createRole$ = this._actions.ofType(userpermission.ActionTypes.CREATE_ROLE)
    .map(action => action.payload)
    .switchMap((role) => this._metadataService.createRole(role)
      .map((role) => new CreateRoleSuccessAction(role))
      .catch(() => Observable.of(new CreateRoleFailAction({})))).share();

  @Effect() deleteRole$ = this._actions.ofType(userpermission.ActionTypes.DELETE_ROLE)
    .map(action => action.payload)
    .switchMap((role) => this._metadataService.deleteRole(role)
      .map((id) => new DeleteRoleSuccessAction(id))
      .catch(() => Observable.of(new DeleteRoleFailAction({})))).share();

  constructor(private _actions: Actions,
    private _metadataService: MetadataService) { }
}
