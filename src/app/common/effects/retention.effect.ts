import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/toArray';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/skip';
import 'rxjs/add/operator/takeUntil';
import 'rxjs/add/observable/of';
import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import * as retentions from '../actions/retention.action';
import { RetentionService } from "../services/retention.service";
import {
  LoadRetentionArtifactNewSuccessAction, LoadRetentionArtifactNewFailAction,
  LoadRetentionArtifactFollowUpSuccessAction, LoadRetentionArtifactFollowUpFailAction,
  LoadRetentionArtifactSuccessAction, LoadRetentionArtifactFailAction,
  CreateTrackingPlanSuccessAction, CreateTrackingPlanFailAction,
  LoadTrackingPlanSuccessAction, LoadTrackingPlanFailAction,
  LoadAllTaskDueSuccessAction, LoadAllTaskDueFailAction,
  LoadAllTaskOverDueSuccessAction, LoadAllTaskOverDueFailAction,
  LoadDepartPlanSuccessAction, LoadDepartPlanFailAction,
  LoadSelectTaskSuccessAction, LoadSelectTaskFailAction,
  LoadSelectViewTrackingFailAction, LoadSelectViewTrackingSuccessAction,
  CreateCheckEvaluateSuccessAction, CreateCheckEvaluateFailAction,
  CreateArtifactReviewKnowledgeSuccessAction, CreateArtifactReviewKnowledgeFailAction,
  CreateApproveArtifactAssignmentSuccessAction, CreateApproveArtifactAssignmentFailAction,
  SubmitApproveRetentionSuccessAction, SubmitApproveRetentionFailAction,
  LoadSelectPlanAssesmentSuccessAction, LoadSelectPlanAssesmentFailAction
  , LoadSelectedSubTaskAssessmentFailAction, LoadSelectedSubTaskAssessmentSuccessAction,
  LoadRetentionResultSuccessAction, LoadRetentionResultNewFailAction,
  LoadApproveRetentionAssessmentSuccessAction, LoadApproveRetentionAssessmentFailAction

} from "../actions/retention.action";
import { Retention } from '../models/retention.model';

@Injectable()
export class RetentionEffects {
  constructor(private _actions: Actions,
    private _retentionService: RetentionService) { }
  /* The effects for different states are singletons that 'intercept'
  dispatched actions that are being sent to the reducer. */


  @Effect() loadRetentionArtifactNew$ = this._actions.ofType(retentions.ActionTypes.LOAD_RETENTION_ARTIFACT_NEW)
    .switchMap(() => this._retentionService.getRetentionArtifactsNew()
      .map((category) => new LoadRetentionArtifactNewSuccessAction(category))
      .catch(() => Observable.of(new LoadRetentionArtifactNewFailAction({}))));

  @Effect() loadRetentionArtifactFollowUp$ = this._actions.ofType(retentions.ActionTypes.LOAD_RETENTION_ARTIFACT_FOLLOWUP)
    .switchMap(() => this._retentionService.getRetentionArtifactsFollowUp()
      .map((category) => new LoadRetentionArtifactFollowUpSuccessAction(category))
      .catch(() => Observable.of(new LoadRetentionArtifactFollowUpFailAction({}))));

  @Effect() loadRetentionArtifact$ = this._actions.ofType(retentions.ActionTypes.SELECT_RETENTION_ARTIFACT)
    .map(action => action.payload)
    .switchMap((id) => this._retentionService.getRetentionArtifacts(id)
      .map((art) => new LoadRetentionArtifactSuccessAction(art))
      .catch(() => Observable.of(new LoadRetentionArtifactFailAction({}))));

  @Effect() craeteTrackingPlan$ = this._actions.ofType(retentions.ActionTypes.CREATE_TRACKING_PLAN)
    .map(action => action.payload)
    .switchMap((artifact) => this._retentionService.craeteTrackingPlan(artifact)
      .map((summary) => new CreateTrackingPlanSuccessAction({}))
      .catch(() => Observable.of(new CreateTrackingPlanFailAction({}))));

  @Effect() loadTrackingPlan$ = this._actions.ofType(retentions.ActionTypes.LOAD_TRACKING_PLAN)
    .switchMap(() => this._retentionService.getTrackingPlan()
      .map((category) => new LoadTrackingPlanSuccessAction(category))
      .catch(() => Observable.of(new LoadTrackingPlanFailAction({}))));

  @Effect() loadAllTaskDue$ = this._actions.ofType(retentions.ActionTypes.LOAD_ALL_TASK_DUE)
    .switchMap(() => this._retentionService.getAllTaskDue()
      .map((category) => new LoadAllTaskDueSuccessAction(category))
      .catch(() => Observable.of(new LoadAllTaskDueFailAction({}))));

  @Effect() loadAllTaskOverDue$ = this._actions.ofType(retentions.ActionTypes.LOAD_ALL_TASK_OVER_DUE)
    .switchMap(() => this._retentionService.getAllTaskOverDue()
      .map((category) => new LoadAllTaskOverDueSuccessAction(category))
      .catch(() => Observable.of(new LoadAllTaskOverDueFailAction({}))));


  @Effect() loadDepartmentPlan$ = this._actions.ofType(retentions.ActionTypes.LOAD_DEPARTMENT_PLAN)
    .map(action => action.payload)
    .switchMap((id) => this._retentionService.getDepartmentPlan(id)
      .map((art) => new LoadDepartPlanSuccessAction(art))
      .catch(() => Observable.of(new LoadDepartPlanFailAction({}))));

  @Effect() SelectTask$ = this._actions.ofType(retentions.ActionTypes.SELECT_TASK)
    .map(action => action.payload)
    .switchMap((id) => this._retentionService.getViewTask(id)
      .map((art) => new LoadSelectTaskSuccessAction(art))
      .catch(() => Observable.of(new LoadSelectTaskFailAction({}))));

  @Effect() SelectTrackingPlan$ = this._actions.ofType(retentions.ActionTypes.SELECT_TRACKING_PLAN)
    .map(action => action.payload)
    .switchMap((id) => this._retentionService.getSelectViewTracking(id)
      .map((art) => new LoadSelectViewTrackingSuccessAction(art))
      .catch(() => Observable.of(new LoadSelectViewTrackingFailAction({}))));


  @Effect() craeteCheckEvaluate$ = this._actions.ofType(retentions.ActionTypes.CREATE_CHECK_EVALUATE)
    .map(action => action.payload)
    .switchMap((task) => this._retentionService.craeteCheckEvaluate(task)
      .map((summary) => new CreateCheckEvaluateSuccessAction({}))
      .catch(() => Observable.of(new CreateCheckEvaluateFailAction({}))));

  @Effect() craeteArtifactreviewacknowledge$ = this._actions.ofType(retentions.ActionTypes.CREATE_ARTIFACTREVIEWKNOWLEDGE)
    .map(action => action.payload)
    .switchMap((task) => this._retentionService.craeteArtifactreviewAcknowledge(task)
      .map((summary) => new CreateArtifactReviewKnowledgeSuccessAction({}))
      .catch(() => Observable.of(new CreateArtifactReviewKnowledgeFailAction({}))));

  @Effect() craeteApproveArtifactAssignment$ = this._actions.ofType(retentions.ActionTypes.CREATE_APPROVEARTIFACTASSIGNMENT)
    .map(action => action.payload)
    .switchMap((artifact) => this._retentionService.CreateApproveArtifactAssignment(artifact)
      .map((summary) => new CreateApproveArtifactAssignmentSuccessAction({}))
      .catch(() => Observable.of(new CreateApproveArtifactAssignmentFailAction({}))));

  @Effect() submitapprove$ = this._actions.ofType(retentions.ActionTypes.SUBMIT_APPROVE_RETENTION)
    .map(action => action.payload)
    .switchMap((artifact) => this._retentionService.SubmitApproveRetention(artifact)
      .map((summary) => new SubmitApproveRetentionSuccessAction({}))
      .catch(() => Observable.of(new SubmitApproveRetentionFailAction({}))));

  @Effect() selectPlanAssessment$ = this._actions.ofType(retentions.ActionTypes.SELECT_PLAN_ASSESSMENT)
    .map(action => action.payload)
    .switchMap((id) => this._retentionService.getPlanAssessment(id)
      .map((art) => new LoadSelectPlanAssesmentSuccessAction(art))
      .catch(() => Observable.of(new LoadSelectPlanAssesmentFailAction({}))));

  @Effect() getSubTaskPlanAssessment$ = this._actions.ofType(retentions.ActionTypes.SELECT_SUB_TASK_ASSESSMENT)
    .map(action => action.payload)
    .switchMap((id) => this._retentionService.getSubTaskPlanAssessment(id)
      .map((art) => new LoadSelectedSubTaskAssessmentSuccessAction(art))
      .catch(() => Observable.of(new LoadSelectedSubTaskAssessmentFailAction({}))));



  @Effect() loadRetentionResult$ = this._actions.ofType(retentions.ActionTypes.LOAD_RETENTION_RESULT)
    .switchMap(() => this._retentionService.getRetentionResult()
      .map((category) => new LoadRetentionResultSuccessAction(category))
      .catch(() => Observable.of(new LoadRetentionResultNewFailAction({}))));

  @Effect() loadApproveRetentionAssessment$ = this._actions.ofType(retentions.ActionTypes.SELECT_APPROVE_RETENTION_ASSESSMENT)
    .map(action => action.payload)
    .switchMap((id) => this._retentionService.getApproveRetentionAssessment(id)
      .map((art) => new LoadApproveRetentionAssessmentSuccessAction(art))
      .catch(() => Observable.of(new LoadApproveRetentionAssessmentFailAction({}))));

}
