import { Store } from '@ngrx/store';
import { TicketService } from '../services/ticket.service';
import {
  LoadTicketsSuccessAction, LoadTicketsFailAction,
  CloseTicketSuccessAction, CloseTicketFailAction,
  SelectTicketAction,
  LoadTicketEntryAction,
  LoadTicketEntrySuccessAction, LoadTicketEntryFailAction
} from '../actions/ticket.action';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import * as reducer from '../reducers/reducer';
import * as ticket from '../actions/ticket.action';

@Injectable()
export class TicketEffects {
  /* The effects for different states are singletons that 'intercept'
  dispatched actions that are being sent to the reducer. */
  @Effect() loadTickets$ = this._actions.ofType(ticket.ActionTypes.LOAD_TICKETS)
    .map(action => action.payload)
    .switchMap((payload) => this._ticketService.getAllTicket(payload)
      .map((tickets) => new LoadTicketsSuccessAction(tickets))
      .catch(() => Observable.of(new LoadTicketsFailAction({})))).share();

  @Effect() loadTicketEntry$ = this._actions.ofType(ticket.ActionTypes.LOAD_TICKET_ENTRY)
    .map(action => action.payload)
    .switchMap((id) => this._ticketService.getTicketById(id)
      .map((tickets) => new LoadTicketEntrySuccessAction(tickets))
      .catch(() => Observable.of(new LoadTicketEntryFailAction({}))));

  @Effect() closeTicket$ = this._actions.ofType(ticket.ActionTypes.CLOSE_TICKET)
    .map(action => action.payload)
    .switchMap((request) => this._ticketService.closeTicket(request)
      .map((ticket) => new CloseTicketSuccessAction(ticket))
      .catch(() => Observable.of(new CloseTicketFailAction({})))).share();

  // SelectOrLoadArtifactAction
  @Effect({ dispatch: false }) selectORLoadTicket$ = this._actions.ofType(ticket.ActionTypes.SELECT_OR_LOAD_TICKET)
    .withLatestFrom(this.store$)
    .map(([action, storeState]) => {
      const _artifact = storeState.ticket.ticketEntities.find(item => item.id === action.payload);
      if (_artifact !== undefined) {
        this.store$.next(new SelectTicketAction(_artifact.id));
      } else {
        this.store$.next(new LoadTicketEntryAction(action.payload));
      }
    });

  constructor(private _actions: Actions,
    private _ticketService: TicketService,
    private store$: Store<reducer.State>) { }
}
