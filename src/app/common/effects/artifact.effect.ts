import { DeleteArtifactAttachmentSuccessAction, DeleteArtifactAttachmentFailAction } from '../actions/artifact.action';
import { Store } from '@ngrx/store';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/toArray';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/skip';
import 'rxjs/add/operator/takeUntil';
import 'rxjs/add/observable/of';
import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import * as artifact from '../actions/artifact.action';
import * as reducer from '../reducers/reducer';
import { ArtifactService } from '../services/artifact.service';
import {
  LoadArtifactSuccessAction,
  LoadArtifactFailAction,
  UpdateArtifactSuccessAction,
  UpdateArtifactFailAction,
  UpdateArtifactNodeSuccessAction,
  UpdateArtifactNodeFailAction,
  CreateArtifactSuccessAction,
  CreateArtifactFailAction,
  DeleteArtifactSuccessAction,
  DeleteArtifactFailAction,
  CreateArtifactNodeGroupSuccessAction,
  CreateArtifactNodeGroupFailAction,
  DeleteArtifactNodeGroupSuccessAction,
  DeleteArtifactNodeGroupFailAction,
  DeleteArtifactNodeSuccessAction,
  DeleteArtifactNodeFailAction,
  CreateArtifactNodeSuccessAction,
  CreateArtifactNodeFailAction,
  LoadArtifactAction,
  CreateArtifactAssignmentSuccessAction,
  CreateArtifactAssignmentFailAction,
  LoadArtifactSummaryAction,
  LoadArtifactSummarySuccessAction,
  LoadArtifactSummaryFailAction,
  LoadCanceledArtifactSummaryAction,
  LoadCanceledArtifactSummarySuccessAction,
  LoadCanceledArtifactSummaryFailAction
} from '../actions/artifact.action';

@Injectable()
export class ArtifactEffects {
  @Effect() loadArtifact$ = this._actions.ofType(artifact.ActionTypes.LOAD_ARTIFACTS)
    .map(action => action.payload)
    .switchMap(obj => this._artifactService.getArtifact(obj.artifactId, obj.isActive)
      .map((artifact) => new LoadArtifactSuccessAction(artifact))
      .catch(() => Observable.of(new LoadArtifactFailAction({}))));

  @Effect() loadArtifactSummary$ = this._actions.ofType(artifact.ActionTypes.LOAD_ARTIFACTS_SUMMARY)
    .map(action => action.payload)
    .switchMap(departmentId => this._artifactService.getArtifactSummary(departmentId)
      .map((category) => new LoadArtifactSummarySuccessAction(category))
      .catch(() => Observable.of(new LoadArtifactSummaryFailAction({})))).share();

  @Effect() loadCanceledArtifactSummary$ = this._actions.ofType(artifact.ActionTypes.LOAD_CANCELED_ARTIFACTS_SUMMARY)
    .map(action => action.payload)
    .switchMap(departmentId => this._artifactService.getCanceledArtifactSummary(departmentId)
      .map((category) => new LoadCanceledArtifactSummarySuccessAction(category))
      .catch(() => Observable.of(new LoadCanceledArtifactSummaryFailAction({})))).share();

  // SelectOrLoadArtifactAction
  @Effect({ dispatch: false }) selectORLoadArtifact$ = this._actions.ofType(artifact.ActionTypes.SELECT_OR_LOAD_ARTIFACT)
    .withLatestFrom(this.store$)
    .map(([action, storeState]) => {
      // const _artifact = storeState.artifacts.artifactEntities.find(item => item.id === action.payload.artifactId);
      // if (_artifact !== undefined) {
      //   this.store$.next(new SelectArtifactAction(_artifact.id));
      // } else {
      this.store$.next(new LoadArtifactAction({ artifactId: action.payload.artifactId, isActive: action.payload.isActive }));
      // }
    });

  @Effect({ dispatch: false }) selectORLoadArtifactSummary$ = this._actions.ofType(artifact.ActionTypes.SELECT_OR_LOAD_ARTIFACT_SUMMARY)
    .withLatestFrom(this.store$)
    .map(([action, storeState]) => {
      const _artifact = storeState.artifacts.artifactSummaryEntities;
      // if (_artifact.length !== 0) {
      //   this.store$.next(new SelectArtifactSummaryAction({
      //     status: action.payload.status,
      //     taskAssignmentType: action.payload.taskAssignmentType
      //   }));
      // } else {
      if (action.payload.status == "canceled") {
        this.store$.next(new LoadCanceledArtifactSummaryAction(action.payload.departmentId));
      } else {
        this.store$.next(new LoadArtifactSummaryAction(action.payload.departmentId));
      }

      // }
    });

  @Effect() craeteArtifact$ = this._actions.ofType(artifact.ActionTypes.CREATE_ARTIFACT)
    .map(action => action.payload)
    .switchMap((artifact) => this._artifactService.createArtifact(artifact)
      .map(updatedArtifact => new CreateArtifactSuccessAction(updatedArtifact))
      .catch(() => Observable.of(new CreateArtifactFailAction({})))).share();

  @Effect() createArtifactNode$ = this._actions.ofType(artifact.ActionTypes.CREATE_ARTIFACT_NODE)
    .map(action => action.payload)
    .switchMap(artifactNode => this._artifactService.createArtifactNode(artifactNode)
      .map(artifact => new CreateArtifactNodeSuccessAction(artifact))
      .catch(() => Observable.of(new CreateArtifactNodeFailAction({}))));

  @Effect() createArtifactNodeGroup$ = this._actions.ofType(artifact.ActionTypes.CREATE_ARTIFACT_NODE_GROUP)
    .map(action => action.payload)
    .switchMap(artifactNode => this._artifactService.createArtifactNodeGroup(artifactNode)
      .map(artifact => new CreateArtifactNodeGroupSuccessAction(artifact))
      .catch(() => Observable.of(new CreateArtifactNodeGroupFailAction({}))));

  @Effect() deleteArtifactNodeGroup$ = this._actions.ofType(artifact.ActionTypes.DELETE_ARTIFACT_NODE_GROUP)
    .map(action => action.payload)
    .switchMap(artifactNode => this._artifactService.deleteArtifactNodeGroup(artifactNode)
      .map(artifact => new DeleteArtifactNodeGroupSuccessAction(artifact))
      .catch(() => Observable.of(new DeleteArtifactNodeGroupFailAction({}))));

  @Effect() deleteArtifactNode$ = this._actions.ofType(artifact.ActionTypes.DELETE_ARTIFACT_NODE)
    .map(action => action.payload)
    .switchMap(artifactNode => this._artifactService.deleteArtifactNode(artifactNode)
      .map(artifact => new DeleteArtifactNodeSuccessAction(artifact))
      .catch(() => Observable.of(new DeleteArtifactNodeFailAction({}))));

  @Effect() updateArtifact$ = this._actions.ofType(artifact.ActionTypes.UPDATE_ARTIFACT)
    .map(action => action.payload)
    .switchMap((artifact) => this._artifactService.updateArtifact(artifact.id, artifact)
      .map(updatedArtifact => new UpdateArtifactSuccessAction(updatedArtifact))
      .catch(() => Observable.of(new UpdateArtifactFailAction({})))).share();

  @Effect() deleteArtifact$ = this._actions.ofType(artifact.ActionTypes.DELETE_ARTIFACT)
    .map(action => action.payload)
    .switchMap(artifactId => this._artifactService.deleteArtifact(artifactId)
      .map(artifact => new DeleteArtifactSuccessAction(artifact))
      .catch(() => Observable.of(new DeleteArtifactFailAction({}))));

  @Effect() updateArtifactNode$ = this._actions.ofType(artifact.ActionTypes.UPDATE_ARTIFACT_NODE)
    .map(action => action.payload)
    .switchMap(artifactNode => this._artifactService.updateArtifactNode(artifactNode.id, artifactNode)
      .map(artifact => new UpdateArtifactNodeSuccessAction(artifact))
      .catch(() => Observable.of(new UpdateArtifactNodeFailAction({}))));

  @Effect() updateArtifactNodeGroup$ = this._actions.ofType(artifact.ActionTypes.UPDATE_ARTIFACT_NODE_GROUP)
    .map(action => action.payload)
    .switchMap(artifactNodeGroup => this._artifactService.updateArtifactNodeGroup(artifactNodeGroup.id, artifactNodeGroup)
      .map(artifact => new UpdateArtifactNodeSuccessAction(artifact))
      .catch(() => Observable.of(new UpdateArtifactNodeFailAction({}))));

  @Effect() craeteArtifactAssignment$ = this._actions.ofType(artifact.ActionTypes.CREATE_ARTIFACT_ASSIGNMENT)
    .map(action => action.payload)
    .switchMap((artifact) => this._artifactService.createArtifactAssignment(artifact)
      .map((summary) => new CreateArtifactAssignmentSuccessAction({}))
      .catch(() => Observable.of(new CreateArtifactAssignmentFailAction({})))).share();

  @Effect() deleteArtifactAttachment$ = this._actions.ofType(artifact.ActionTypes.DELETE_ARTIFACT_ATTACHMENT)
    .map(action => action.payload)
    .switchMap((artifact) => this._artifactService.deleteArtifactAttachment(artifact.artifactId, artifact.documentId)
      .map((summary) => new DeleteArtifactAttachmentSuccessAction(summary))
      .catch(() => Observable.of(new DeleteArtifactAttachmentFailAction({})))).share();

  // We don't use effect for select action
  /*
  @Effect() loadViewArtifact$ = this._actions.ofType(artifact.ActionTypes.SELECT_ARTIFACT)
    .map(action => action.payload)
    .switchMap((id) => this._metadataService.getArtifact(id)
      .map(art => new LoadArtifactSuccessAction(art))
      .catch(() => Observable.of(new LoadArtifactFailAction({}))));
  */

  constructor(private _actions: Actions, private store$: Store<reducer.State>,
    private _artifactService: ArtifactService) { }

}
