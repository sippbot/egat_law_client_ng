import { Store } from '@ngrx/store';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/switchMap';
import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import * as taskAssignment from '../actions/task-assignment.action';
import * as reducer from '../reducers/reducer';
import { TaskAssignmentService } from '../services/task-assignment.service';
import {
  LoadTaskAssignmentsAction,
  LoadTaskAssignmentsSuccessAction,
  LoadTaskAssignmentsFailAction,
  LoadTaskAssignmentFullDetailSuccessAction,
  LoadTaskAssignmentFullDetailFailAction,
  CreateTaskAssessmentSuccessAction,
  CreateTaskAssessmentFailAction,
  CreateTaskAssessmentReviewSuccessAction,
  CreateTaskAssessmentReviewFailAction,
  LoadTaskAssignmentSummarySuccessAction,
  LoadTaskAssignmentSummaryFailAction,
  CreateTaskAssignmentProposalSuccessAction,
  CreateTaskAssignmentProposalFailAction,
  CreateTaskAssignmentApprovalSuccessAction,
  CreateTaskAssignmentApprovalFailAction,
  CreateTaskCommentSuccessAction,
  CreateTaskCommentFailAction,
  CreateTaskAssignmentAttachmentSuccessAction,
  CreateTaskAssignmentAttachmentFailAction,
  DeleteTaskAssignmentAttachmentSuccessAction,
  DeleteTaskAssignmentAttachmentFailAction
} from '../actions/task-assignment.action';

@Injectable()
export class TaskAssignmentEffects {
  @Effect() loadTaskAssignments$ = this._actions.ofType(taskAssignment.ActionTypes.LOAD_TASKASSIGNMENTS)
    .switchMap((action) => this._taskAssignmentService.getInprogressTaskAssignment(action.payload.departmentId, action.payload.artifactId)
      .map((assignments) => new LoadTaskAssignmentsSuccessAction(assignments))
      .catch(() => Observable.of(new LoadTaskAssignmentsFailAction({}))));

  @Effect() loadTaskAssignmentFullDetail$ = this._actions.ofType(taskAssignment.ActionTypes.LOAD_TASKASSIGNMENT_FULLDETAIL)
    .switchMap((action) => this._taskAssignmentService.getTaskAssignmentById(action.payload.taskAssignmentId)
      .map((assignments) => new LoadTaskAssignmentFullDetailSuccessAction(assignments))
      .catch(() => Observable.of(new LoadTaskAssignmentFullDetailFailAction({}))));

  @Effect() createTaskAssessment$ = this._actions.ofType(taskAssignment.ActionTypes.CREATE_TASKASSESSMENT)
    .switchMap((action) => this._taskAssignmentService.createTaskAssessment(action.payload)
      .map((assignments) => new CreateTaskAssessmentSuccessAction(assignments))
      .catch(() => Observable.of(new CreateTaskAssessmentFailAction({})))).share();

  @Effect() createTaskAssessmentReview$ = this._actions.ofType(taskAssignment.ActionTypes.CREATE_TASKASSESSMENT_REVIEW)
    .map(action => action.payload)
    .switchMap((review) => this._taskAssignmentService.createTaskAssessmentReview(review)
      .map((assignments) => new CreateTaskAssessmentReviewSuccessAction(assignments))
      .catch(() => Observable.of(new CreateTaskAssessmentReviewFailAction({})))).share();

  @Effect() loadTaskAssignmentSummary$ = this._actions.ofType(taskAssignment.ActionTypes.LOAD_TASKASSIGNMENT_SUMMARY)
    .switchMap((action) => this._taskAssignmentService.getTaskAssignmentSummaryByArtifactId(action.payload)
      .map((summary) => new LoadTaskAssignmentSummarySuccessAction(summary))
      .catch(() => Observable.of(new LoadTaskAssignmentSummaryFailAction({}))));

  @Effect() createTaskAssignmentProposal$ = this._actions.ofType(taskAssignment.ActionTypes.CREATE_TASKASSIGNMENT_PROPOSAL)
    .switchMap((action) => this._taskAssignmentService.createTaskAssignmentProposal(action.payload)
      .map((assignments) => new CreateTaskAssignmentProposalSuccessAction(assignments))
      .catch(() => Observable.of(new CreateTaskAssignmentProposalFailAction({})))).share();

  @Effect() createTaskComment$ = this._actions.ofType(taskAssignment.ActionTypes.CREATE_TASKCOMMENT)
    .switchMap((action) => this._taskAssignmentService.createTaskComment(action.payload)
      .map((assignments) => new CreateTaskCommentSuccessAction(assignments))
      .catch(() => Observable.of(new CreateTaskCommentFailAction({}))));

  @Effect() createTaskAttachment$ = this._actions.ofType(taskAssignment.ActionTypes.CREATE_TASKASSIGNMENT_ATTACHMENT)
    .switchMap((action) => this._taskAssignmentService.createTaskAttachment(action.payload)
      .map((assignments) => new CreateTaskAssignmentAttachmentSuccessAction(assignments))
      .catch(() => Observable.of(new CreateTaskAssignmentAttachmentFailAction({})))).share();

  @Effect() createTaskAssessmentApproval$ = this._actions.ofType(taskAssignment.ActionTypes.CREATE_TASKASSIGNMENT_APPROVAL)
    .switchMap((action) => this._taskAssignmentService.approveTaskAssignmentProposal(action.payload)
      .map((assignments) => new CreateTaskAssignmentApprovalSuccessAction(assignments))
      .catch(() => Observable.of(new CreateTaskAssignmentApprovalFailAction({})))).share();

  @Effect() deleteTaskAssignmentAttachment$ = this._actions.ofType(taskAssignment.ActionTypes.DELETE_TASKASSIGNMENT_ATTACHMENT)
    .switchMap((action) => this._taskAssignmentService.deleteTaskAttachment(action.payload.taskAssignmentId,action.payload.documentId)
      .map((assignments) => new DeleteTaskAssignmentAttachmentSuccessAction(assignments))
      .catch(() => Observable.of(new DeleteTaskAssignmentAttachmentFailAction({}))));

  constructor(private _actions: Actions,
    private _taskAssignmentService: TaskAssignmentService,
    private store$: Store<reducer.State>) { }
}
