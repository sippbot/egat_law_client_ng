import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import * as notification from '../actions/notification.action';

import {
    LoadNotificationSuccessAction,
    LoadNotificationFailAction,
    ResendNotificationSuccessAction,
    ResendNotificationFailAction

} from '../actions/notification.action';
import { NotificationService } from '../services/notification.service';


@Injectable()
export class NotificationEffects {
    /* The effects for different states are singletons that 'intercept'
    dispatched actions that are being sent to the reducer. */
    @Effect() loadNotification$ = this._actions.ofType(notification.ActionTypes.LOAD_NOTIFICATION)
        .map(action => action.payload)
        .switchMap((userId) => this._notificationService.getNotifications(userId)
            .map((notifications) => new LoadNotificationSuccessAction(notifications))
            .catch(() => Observable.of(new LoadNotificationFailAction({})))).share();

    @Effect() resendNotification$ = this._actions.ofType(notification.ActionTypes.RESEND_NOTIFICATION)
        .map(action => action.payload)
        .switchMap((messageId) => this._notificationService.resendNotification(messageId)
            .map((ret) => new ResendNotificationSuccessAction(ret))
            .catch(() => Observable.of(new ResendNotificationFailAction({}))));

    constructor(private _actions: Actions,
        private _notificationService: NotificationService) { }
}
