import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import * as license from '../actions/license.action';
import { LicenseService } from '../services/license.service';
import {
    LoadLicensesSuccessAction,
    LoadLicensesFailAction,
    CreateLicenseSuccessAction,
    CreateLicenseFailAction,
    RenewLicenseSuccessAction,
    RenewLicenseFailAction

} from '../actions/license.action';

@Injectable()
export class LicenseEffects {
    /* The effects for different states are singletons that 'intercept'
    dispatched actions that are being sent to the reducer. */
    @Effect() loadLicense$ = this._actions.ofType(license.ActionTypes.LOAD_LICENSES)
        .map(action => action.payload)
        .switchMap((userId) => this._LicenseService.getAllLicense(userId)
            .map((depr) => new LoadLicensesSuccessAction(depr))
            .catch(() => Observable.of(new LoadLicensesFailAction({}))));

    @Effect() craeteLicense$ = this._actions.ofType(license.ActionTypes.CREATE_LICENSE)
        .map(action => action.payload)
        .switchMap((license) => this._LicenseService.createLicense(license)
            .map((category) => new CreateLicenseSuccessAction(category))
            .catch(() => Observable.of(new CreateLicenseFailAction({})))).share();

    @Effect() renewLicense$ = this._actions.ofType(license.ActionTypes.RENEW_LICENSE)
    .map(action => action.payload)
    .switchMap((license) => this._LicenseService.renewLicense(license)
        .map((category) => new RenewLicenseSuccessAction(category))
        .catch(() => Observable.of(new RenewLicenseFailAction({})))).share();
    

    constructor(private _actions: Actions,
        private _LicenseService: LicenseService) { }
}
