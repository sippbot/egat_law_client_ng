import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/toArray';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/skip';
import 'rxjs/add/operator/takeUntil';
import 'rxjs/add/observable/of';
import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import * as departments from '../actions/departments.action';
import { DepartmentService } from '../services/department.service';
import * as reducer from '../reducers/reducer';
import {
    CreateDepartmentFailAction,
    CreateDepartmentSuccessAction,
    DeleteDepartmentFailAction,
    DeleteDepartmentSuccessAction,
    LoadDepartmentFailAction,
    LoadDepartmentSuccessAction,
    LoadDepartmentsFailAction,
    LoadDepartmentsSuccessAction,
    LoadActiveDepartmentsSuccessAction,
    LoadActiveDepartmentsFailAction,
    UpdateDepartmentFailAction,
    UpdateDepartmentSuccessAction,
    SelectDepartmentAction,
    LoadDepartmentAction
} from '../actions/departments.action';

@Injectable()
export class DepartmentEffects {
    /* The effects for different states are singletons that 'intercept'
    dispatched actions that are being sent to the reducer. */
    @Effect() loadDepartment$ = this._actions.ofType(departments.ActionTypes.LOAD_DEPARTMENT)
        .map(action => action.payload)
        .switchMap((departmentId) => this._departmentService.getDepartment(departmentId)
            .map((department) => new LoadDepartmentSuccessAction(department))
            .catch(() => Observable.of(new LoadDepartmentFailAction({}))));

    @Effect() loadDepartments$ = this._actions.ofType(departments.ActionTypes.LOAD_DEPARTMENTS)
        .switchMap(() => this._departmentService.getAllDepartment()
            .map((depr) => new LoadDepartmentsSuccessAction(depr))
            .catch(() => Observable.of(new LoadDepartmentsFailAction({}))));
    
    @Effect() loadActiveDepartments$ = this._actions.ofType(departments.ActionTypes.LOAD_ACTIVE_DEPARTMENTS)
        .switchMap(() => this._departmentService.getAllActiveDepartment()
            .map((depr) => new LoadActiveDepartmentsSuccessAction(depr))
            .catch(() => Observable.of(new LoadActiveDepartmentsFailAction({}))));

    @Effect() updateDepartments$ = this._actions.ofType(departments.ActionTypes.UPDATE_DEPARTMENT)
        .map(action => action.payload)
        .switchMap((dap) => this._departmentService.updateDepartment(dap)
            .map((department) => new UpdateDepartmentSuccessAction(department))
            .catch(() => Observable.of(new UpdateDepartmentFailAction({})))).share();

    @Effect() craeteDepartments$ = this._actions.ofType(departments.ActionTypes.CREATE_DEPARTMENT)
        .map(action => action.payload)
        .switchMap((cat) => this._departmentService.createDepartment(cat)
            .map((category) => new CreateDepartmentSuccessAction(category))
            .catch(() => Observable.of(new CreateDepartmentFailAction({})))).share();
    
    @Effect() deleteDepartments$ = this._actions.ofType(departments.ActionTypes.DELETE_DEPARTMENT)
        .map(action => action.payload)
        .switchMap((cat) => this._departmentService.deleteDepartment(cat)
            .map((id) => new DeleteDepartmentSuccessAction(id))
            .catch(() => Observable.of(new DeleteDepartmentFailAction({})))).share();
    
    @Effect({ dispatch: false }) selectORLoadDepartment$ = this._actions.ofType(departments.ActionTypes.SELECT_OR_LOAD_DEPARTMENT)
        .withLatestFrom(this.store$)
        .map(([action, storeState]) => {
            const _department = storeState.departments.departmentEntities.find(item => item.id === action.payload);
            if (_department !== undefined) {
                this.store$.next(new SelectDepartmentAction(action.payload));
            } else {
                this.store$.next(new LoadDepartmentAction(action.payload));
            }
        });

    constructor(private _actions: Actions,private store$: Store<reducer.State>,
        private _departmentService: DepartmentService) {
    }
}
