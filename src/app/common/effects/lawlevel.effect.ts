import { DeleteLawlevelSuccessAction, DeleteLawlevelFailAction } from '../actions/lawlevel.action';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/switchMap';
import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import * as lawlevels from '../actions/lawlevel.action';
import { MetadataService } from '../services/metadata.service';
import {
  LoadLawlevelSuccessAction, LoadLawlevelFailAction,
  UpdateLawlevelSuccessAction, UpdateLawlevelFailAction,
  CreateLawlevelSuccessAction, CreateLawlevelFailAction
} from '../actions/lawlevel.action';

@Injectable()
export class LawlevelEffects {
  /* The effects for different states are singletons that 'intercept'
  dispatched actions that are being sent to the reducer. */
  @Effect() loadLawlevels$ = this._actions.ofType(lawlevels.ActionTypes.LOAD_LAWLEVEL)
    .switchMap(() => this._metadataService.getAllLevel()
      .map((lawlevel) => new LoadLawlevelSuccessAction(lawlevel))
      .catch(() => Observable.of(new LoadLawlevelFailAction({}))))

  @Effect() updateLawlevel$ = this._actions.ofType(lawlevels.ActionTypes.UPDATE_LAWLEVEL)
    .map(action => action.payload)
    .switchMap((level) => this._metadataService.updateLawlevel(level)
      .map((lawlevel) => new UpdateLawlevelSuccessAction(lawlevel))
      .catch(() => Observable.of(new UpdateLawlevelFailAction({})))).share();

  @Effect() craeteLawlevel$ = this._actions.ofType(lawlevels.ActionTypes.CREATE_LAWLEVEL)
    .map(action => action.payload)
    .switchMap((level) => this._metadataService.createLawlevel(level)
      .map((lawlevel) => new CreateLawlevelSuccessAction(lawlevel))
      .catch(() => Observable.of(new CreateLawlevelFailAction({})))).share();

  @Effect() deleteLawlevel$ = this._actions.ofType(lawlevels.ActionTypes.DELETE_LAWLEVEL)
    .map(action => action.payload)
    .switchMap((id) => this._metadataService.deleteLevel(id)
      .map((levelId) => new DeleteLawlevelSuccessAction(levelId))
      .catch(() => Observable.of(new DeleteLawlevelFailAction({})))).share();

  constructor(private _actions: Actions,
    private _metadataService: MetadataService) { }
}
