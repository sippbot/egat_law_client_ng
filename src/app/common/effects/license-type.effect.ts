import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import * as licensetypes from '../actions/license-type.action';
import { LicenseTypeService } from '../services/license-type.service';
import {
  LoadLicenseTypeSuccessAction,
  LoadLicenseTypeFailAction,
  CreateLicenseTypeSuccessAction,
  CreateLicenseTypeFailAction,
  UpdateLicenseTypeSuccessAction,
  UpdateLicenseTypeFailAction,
  DeleteLicenseTypeSuccessAction,
  DeleteLicenseTypeFailAction
} from '../actions/license-type.action';
import { LicenseType } from '../models/license-type.model';

@Injectable()
export class LicensetypeEffects {
  /* The effects for different states are singletons that 'intercept'
  dispatched actions that are being sent to the reducer. */
  @Effect() loadLicensetype$ = this._actions.ofType(licensetypes.ActionTypes.LOAD_LICENSETYPE)
    .switchMap(() => this._licenseTypeService.getAllLicensetype()
      .map((depr) => new LoadLicenseTypeSuccessAction(depr))
      .catch(() => Observable.of(new LoadLicenseTypeFailAction({}))));

  @Effect() craeteLicensetype$ = this._actions.ofType(licensetypes.ActionTypes.CREATE_LICENSETYPE)
    .map(action => action.payload)
    .switchMap((cat) => this._licenseTypeService.createLicensetype(cat)
      .map((category) => new CreateLicenseTypeSuccessAction(category))
      .catch(() => Observable.of(new CreateLicenseTypeFailAction({})))).share();

  @Effect() updateLicensetype$ = this._actions.ofType(licensetypes.ActionTypes.UPDATE_LICENSETYPE)
    .map(action => action.payload)
    .switchMap((licenseType) => this._licenseTypeService.updateLicensetype(licenseType)
      .map((category) => new UpdateLicenseTypeSuccessAction(category))
      .catch(() => Observable.of(new UpdateLicenseTypeFailAction({})))).share();

  @Effect() deleteLicensetype$ = this._actions.ofType(licensetypes.ActionTypes.DELETE_LICENSETYPE)
    .map(action => action.payload)
    .switchMap((licenseTypeId) => this._licenseTypeService.deleteLicensetype(licenseTypeId)
      .map((id) => new DeleteLicenseTypeSuccessAction(id))
      .catch(() => Observable.of(new DeleteLicenseTypeFailAction({})))).share();

  constructor(private _actions: Actions,
    private _licenseTypeService: LicenseTypeService) { }
}
