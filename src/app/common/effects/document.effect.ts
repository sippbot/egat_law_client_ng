import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import * as documents from '../actions/document.action';
import { DocumentService } from '../services/document.service';
import {
  LoadDocumentSuccessAction,
  LoadDocumentFailAction,
  UpdateDocumentSuccessAction,
  UpdateDocumentFailAction,

} from '../actions/document.action';
import { Document } from '../models/document.model';

@Injectable()
export class DocumentEffects {

  /* The effects for different states are singletons that 'intercept'
  dispatched actions that are being sent to the reducer. */
  @Effect() loadDocuments$ = this._actions.ofType(documents.ActionTypes.LOAD_DOCUMENT)
    .map(action => action.payload)
    .switchMap((userId) => this._documentService.getAlldocument(userId)
      .map((docs) => new LoadDocumentSuccessAction(docs))
      .catch(() => Observable.of(new LoadDocumentFailAction({}))));

  @Effect() updateDocuments$ = this._actions.ofType(documents.ActionTypes.UPDATE_DOCUMENT)
    .map(action => action.payload)
    .switchMap((request) => this._documentService.updateDocument(request)
      .map((docs) => new UpdateDocumentSuccessAction(docs))
      .catch(() => Observable.of(new UpdateDocumentFailAction({})))).share();
  constructor(private _actions: Actions,
    private _documentService: DocumentService) { }
}
