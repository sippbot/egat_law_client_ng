import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import { Injectable, group } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import * as category from '../actions/category.action';
import { MetadataService } from '../services/metadata.service';
import {
  LoadCategorySuccessAction, LoadCategoryFailAction,
  UpdateCategorySuccessAction, UpdateCategoryFailAction,
  CreateCategorySuccessAction, CreateCategoryFailAction,
  DeleteCategoryFailAction, DeleteCategorySuccessAction
} from '../actions/category.action';

@Injectable()
export class CategoryEffects {
  /* The effects for different states are singletons that 'intercept'
  dispatched actions that are being sent to the reducer. */
  @Effect() loadCategory$ = this._actions.ofType(category.ActionTypes.LOAD_CATEGORIES)
    .switchMap(() => this._metadataService.getAllCategory()
      .map((category) => new LoadCategorySuccessAction(category))
      .catch(() => Observable.of(new LoadCategoryFailAction({}))))

  @Effect() updateCategory$ = this._actions.ofType(category.ActionTypes.UPDATE_CATEGORY)
    .map(action => action.payload)
    .switchMap((cat) => this._metadataService.updateCategory(cat)
      .map((category) => new UpdateCategorySuccessAction(category))
      .catch(() => Observable.of(new UpdateCategoryFailAction({})))).share();

  @Effect() craeteCategory$ = this._actions.ofType(category.ActionTypes.CREATE_CATEGORY)
    .map(action => action.payload)
    .switchMap((cat) => this._metadataService.createCategory(cat)
      .map((category) => new CreateCategorySuccessAction(category))
      .catch(() => Observable.of(new CreateCategoryFailAction({})))).share();

  @Effect() deleteCategory$ = this._actions.ofType(category.ActionTypes.DELETE_CATEGORY)
    .map(action => action.payload)
    .switchMap((id) => this._metadataService.deleteCategory(id)
      .map((group) => new DeleteCategorySuccessAction(group))
      .catch(() => Observable.of(new DeleteCategoryFailAction({})))).share();

  constructor(private _actions: Actions,
    private _metadataService: MetadataService) { }
}
