// import rxjs lib
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/toArray';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/skip';
import 'rxjs/add/operator/takeUntil';
import 'rxjs/add/observable/of';

// import ngrx lib
import {Injectable} from '@angular/core';
import {Actions, Effect} from '@ngrx/effects';
import * as alert from '../actions/alert.action';
import {ShowAlertAction} from '../actions/alert.action';

@Injectable()
export class AlertEffects {
    /* The effects for different states are singletons that 'intercept'
    dispatched actions that are being sent to the reducer. */
    @Effect({dispatch: false}) showAlert$ = this._actions.ofType(alert.ActionTypes.ADD_ALERT)
        .do(action => {new ShowAlertAction(action.payload)});

    constructor(private _actions: Actions) {
    }
}
