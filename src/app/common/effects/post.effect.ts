import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/toArray';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/skip';
import 'rxjs/add/operator/takeUntil';
import 'rxjs/add/observable/of';
import {Injectable} from '@angular/core';
import {Actions, Effect} from '@ngrx/effects';
import {Observable} from 'rxjs/Observable';
import * as posts from '../actions/post.action';
import {
    CreatePostFailAction,
    CreatePostSuccessAction,
    RemovePostFailAction,
    RemovePostSuccessAction,
    LoadPostSuccessAction,
    LoadPostFailAction
} from '../actions/post.action';
import {PostService} from '../services/post.service';

@Injectable()
export class PostEffects {
    constructor(private _actions: Actions,
                private _postService: PostService) {
    }

    /* The effects for different states are singletons that 'intercept'
    dispatched actions that are being sent to the reducer. */
    @Effect() loadPost$ = this._actions.ofType(posts.ActionTypes.LOAD_POST)
        .map(action => action.payload)
        .switchMap((departmentId) => this._postService.getPosts(departmentId)
            .map((postdata) => new LoadPostSuccessAction(postdata))
            .catch(() => Observable.of(new LoadPostFailAction({}))));

    @Effect() createPost$ = this._actions.ofType(posts.ActionTypes.CREATE_POST)
        .map(action => action.payload)
        .switchMap((post) => this._postService.postUser(post)
            .map((postdata) => new CreatePostSuccessAction(postdata))
            .catch(() => Observable.of(new CreatePostFailAction({}))));
    
    @Effect() removePost$ = this._actions.ofType(posts.ActionTypes.REMOVE_POST)
        .map(action => action.payload)
        .switchMap((post) => this._postService.removeUser(post)
            .map((postdata) => new RemovePostSuccessAction(postdata))
            .catch(() => Observable.of(new RemovePostFailAction({}))));
}
