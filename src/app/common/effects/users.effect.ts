import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/switchMap';
import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import * as users from '../actions/users.action';
import { UserService } from '../services/user.service';
import {
    CreateUserFailAction,
    CreateUserSuccessAction,
    LoadUsersFailAction,
    LoadUsersSuccessAction,
    UpdateUserFailAction,
    UpdateUserSuccessAction,
    LoadUserLoginSuccessAction,
    LoadUserLoginFailAction,
    DeleteUserSuccessAction,
    DeleteUserFailAction
} from '../actions/users.action';

@Injectable()
export class UserEffects {
    /* The effects for different states are singletons that 'intercept'
    dispatched actions that are being sent to the reducer. */
    @Effect() loadUsers$ = this._actions.ofType(users.ActionTypes.LOAD_USERS)
        .switchMap(() => this._userService.getAllUsers()
            .map((userslist) => new LoadUsersSuccessAction(userslist))
            .catch(() => Observable.of(new LoadUsersFailAction({}))));

    @Effect() loadLoginUser$ = this._actions.ofType(users.ActionTypes.LOAD_USER_LOGIN)
        .map(action => action.payload)
        .switchMap((userId) => this._userService.getUser(userId)
            .map((user) => new LoadUserLoginSuccessAction(user))
            .catch(() => Observable.of(new LoadUserLoginFailAction({}))));

    @Effect() updateUsers$ = this._actions.ofType(users.ActionTypes.UPDATE_USERS)
        .map(action => action.payload)
        .switchMap((user) => this._userService.updateUser(user)
            .map((userdata) => new UpdateUserSuccessAction(userdata))
            .catch(() => Observable.of(new UpdateUserFailAction({})))).share();

    @Effect() craeteUsers$ = this._actions.ofType(users.ActionTypes.CREATE_USERS)
        .map(action => action.payload)
        .switchMap((user) => this._userService.createUser(user)
            .map((userdata) => new CreateUserSuccessAction(userdata))
            .catch(() => Observable.of(new CreateUserFailAction({})))).share();

    @Effect() deleteUsers$ = this._actions.ofType(users.ActionTypes.DELETE_USERS)
        .map(action => action.payload)
        .switchMap((user) => this._userService.deleteUser(user)
            .map((userdata) => new DeleteUserSuccessAction(userdata))
            .catch(() => Observable.of(new DeleteUserFailAction({})))).share();
    constructor(private _actions: Actions,
        private _userService: UserService) {
    }
}
