import { User } from '../../common/models/user.model';
import { Store } from '@ngrx/store';
import { DialogService } from 'ng2-bootstrap-modal';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
declare var $: any;
import { AuthenticationService } from '../../common/services/authentication.service';
import { ProfileComponent } from '../profile/profile.component';
import * as reducer from '../../common/reducers/reducer';
import * as user from '../../common/actions/users.action';

@Component({
  selector: 'cat-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.cleanui.scss']
})
export class TopBarComponent implements OnInit {
  userId: number;
  user: User;
  constructor(private authenticationService: AuthenticationService,
    private router: Router,
    private dialogService: DialogService,
    private _store: Store<reducer.State>) {
    this.user = new User();
    this.userId = +localStorage.getItem('userId');
    this._store.let(reducer.getLoginUser).subscribe(
      user => {
        if (user != null) {
          this.user = user;
        }
      }
    );
  }
  ngOnInit() {
    if(this.userId!=0){
      this._store.dispatch(new user.LoadUserLoginAction(this.userId));
    }
    // this.username = localStorage.getItem('username');
    $(function () {
      $('[data-toggle=tooltip]').tooltip();
    });
  }

  logout() {
    if($('body').hasClass('cat__menu-left--visible')||$('body').hasClass('cat__menu-left--visible--mobile')){
      $('.cat__menu-left__pin-button').click();
    }
    this.authenticationService.logout();
    this.router.navigate(['/login/index']);
  }

  showProfile() {
    const disposable = this.dialogService.addDialog(ProfileComponent, {
      user: this.user
    }).subscribe((isSuccess) => {
      // We get dialog result
      if (isSuccess) {
        console.log(this.user)
        // alert('success');
      } else {
        // alert('declined');
      }
    });
  }
}
