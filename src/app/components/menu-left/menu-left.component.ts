import { ACL } from '../../common/util/acl.util';
import { Component, OnInit } from '@angular/core';
declare var $: any;
declare var jQuery: any;

@Component({
  selector: 'cat-menu-left',
  templateUrl: './menu-left-vertical.component.html',
  styleUrls: ['./menu-left.cleanui.scss']
})
export class MenuLeftComponent implements OnInit {
  // userGroup: string;

  // userGroup: string;
  count:number;
  acl: ACL;
  constructor() {
    this.acl = new ACL();
  }

  role(role: string): boolean {
    return this.acl.hasRole(role);
  }
  ngOnInit() {
    this.setMenuEvent();
  }
  SubMenuEvent(){
      
      // $($(".cat__menu-left__list")[1]).css({"height":"auto","opacity":1,"display":"block"})
      if ($('body').hasClass('cat__config--vertical') || $('body').width() < 768) {
        if($('body').hasClass('cat__menu-left--visible')||$('body').hasClass('cat__menu-left--visible--mobile')){
          var parent = $(this).parent(),
            opened = $('.cat__menu-left__submenu--toggled');
            $(parent).find(".cat__menu-left__list").css({"height":"auto","opacity":1})
          if (!parent.hasClass('cat__menu-left__submenu--toggled') && !parent.parent().closest('.cat__menu-left__submenu').length)
            opened.removeClass('cat__menu-left__submenu--toggled').find('> .cat__menu-left__list').slideUp(200);

            // $.each($(".cat__menu-left__list"),function(i,ele){
            //   $(ele).css({"height":"auto","opacity":1,"display":"block"})
            // });

          parent.toggleClass('cat__menu-left__submenu--toggled');
          parent.find('> .cat__menu-left__list').slideToggle(200);
        }
      }
  }
  setSubMenuEvent(){
    
    $('.cat__menu-left__submenu > a').unbind('click', this.SubMenuEvent)
    $('.cat__menu-left__submenu > a').on('click', this.SubMenuEvent);
    $('.cat__menu-left__pin-button').unbind('click', this.SubMenuEvent)
    $('.cat__menu-left__pin-button').on('click', this.SubMenuEvent);
  }
  setMenuEvent(){
    let thisMenuLeftComponent = this;
    $(function () {
      // scripts for "menu-left" module

      /////////////////////////////////////////////////////////////////////////////////////////
      // add backdrop

      $('.cat__menu-left').after('<div class="cat__menu-left__backdrop cat__menu-left__action--backdrop-toggle"><!-- --></div>');

      /////////////////////////////////////////////////////////////////////////////////////////
      // submenu
      thisMenuLeftComponent.setSubMenuEvent();

      // remove submenu toggle class when viewport back to full view
     
      $(window).on('resize', function () {
        // if ($('body').hasClass('cat__config--horizontal') || $('body').width() > 768) {
        //   $('.cat__menu-left__submenu--toggled').removeClass('cat__menu-left__submenu--toggled').find('> .cat__menu-left__list').attr('style', '');
        // }
      });

      $('.cat__menu-left').hover(function () {
        /* $('body').toggleClass('menu_hover'); */
      });




      /////////////////////////////////////////////////////////////////////////////////////////
      // custom scroll init

      if ($('body').hasClass('cat__config--vertical')) {
        if (!(/Mobi/.test(navigator.userAgent)) && jQuery().jScrollPane) {
          $('.cat__menu-left__inner').each(function () {
            $(this).jScrollPane({
              contentWidth: '0px',
              autoReinitialise: true,
              autoReinitialiseDelay: 100
            });
            var api = $(this).data('jsp'),
              throttleTimeout;
            $(window).bind('resize', function () {
              if (!throttleTimeout) {
                throttleTimeout = setTimeout(function () {
                  api.reinitialise();
                  throttleTimeout = null;
                }, 50);
              }
            });
          });
        }
      }


      /////////////////////////////////////////////////////////////////////////////////////////
      // toggle menu

      $('.cat__menu-left__action--menu-toggle').on('click', function () {
        thisMenuLeftComponent.setSubMenuEvent();

        if ($('body').width() < 768) {
          $('body').toggleClass('cat__menu-left--visible--mobile');
        } else {
          $('body').toggleClass('cat__menu-left--visible');
        }

        if ($('body').hasClass('cat__menu-left--visible')) {

          $('.cat__menu-left__submenu .cat__menu-left__list').css('height', 'auto');
          $('.cat__menu-left__submenu .cat__menu-left__list').css('opacity', '1');
        } else {
          $('.cat__menu-left__submenu .cat__menu-left__list').css('height', '0');
          $('.cat__menu-left__submenu .cat__menu-left__list').css('opacity', '0');
        }

      })


      $('.cat__menu-left__action--backdrop-toggle').on('click', function () {


        $('body').removeClass('cat__menu-left--visible--mobile');


      })


      $('.cat__menu-left--visible .cat__menu-left__action--menu-toggle').on('click', function () {

        $('.cat__menu-left__list').css('display', 'none');
        $('.cat__menu-left__submenu').removeClass('cat__menu-left__submenu--toggled');

      })



      /////////////////////////////////////////////////////////////////////////////////////////
      // colorful menu

      var colorfulClasses = 'cat__menu-left--colorful--primary cat__menu-left--colorful--secondary cat__menu-left--colorful--primary cat__menu-left--colorful--default cat__menu-left--colorful--info cat__menu-left--colorful--success cat__menu-left--colorful--warning cat__menu-left--colorful--danger cat__menu-left--colorful--yellow',
        colorfulClassesArray = colorfulClasses.split(' ');

      function setColorfulClasses() {
        $('.cat__menu-left__list--root > .cat__menu-left__item').each(function () {
          var randomClass = colorfulClassesArray[Math.floor(Math.random() * colorfulClassesArray.length)];
          $(this).addClass(randomClass);
        })
      }

      function removeColorfulClasses() {
        $('.cat__menu-left__list--root > .cat__menu-left__item').removeClass(colorfulClasses);
      }

      if ($('body').hasClass('cat__menu-left--colorful')) {
        setColorfulClasses();
      }

      $('body').on('setColorfulClasses', function () {
        setColorfulClasses();
      });

      $('body').on('removeColorfulClasses', function () {
        removeColorfulClasses();
      });


    });

  }
}
