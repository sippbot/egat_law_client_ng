import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import * as reducer from '../../common/reducers/reducer';
import {Alert} from '../../common/models/alert.model';

@Component({
    selector: 'cat-alert',
    templateUrl: './alert.component.html',
    styleUrls: ['./alert.cleanui.scss']
})
export class AlertComponent implements OnInit {
    alerts: Array<Alert>;

    constructor(private _store: Store<reducer.State>) {
        this.alerts = [new Alert('', '', '')];
        this._store.let(reducer.getAlerts).subscribe(alerts => this.alerts = alerts);
    }
    ngOnInit() {
        console.log("sdsd")
    }
}
