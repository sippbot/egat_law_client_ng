import { Component } from '@angular/core';

@Component({
  selector: 'cat-footer',
  templateUrl: './footer.component.html',
  styleUrls:['./footer.cleanui.scss']
})
export class FooterComponent {}
