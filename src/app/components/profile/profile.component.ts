import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { User } from '../../common/models/user.model';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import * as reducer from '../../common/reducers/reducer';

export interface ProfileModel {
  user: User;
}

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html'
})
export class ProfileComponent extends DialogComponent<ProfileModel, boolean> implements ProfileModel, OnInit {
  user: User;
  constructor(public dialogService: DialogService) {
    super(dialogService);
    this.user = new User();
  }

  ngOnInit() {
  }

}
