import { ACL } from '../common/util/acl.util';
import { LawlevelEffects } from '../common/effects/lawlevel.effect';
import { Component, OnInit, Input , ViewChild} from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Lawlevel } from '../common/models/lawlevel.model';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import * as lawlevel from '../common/actions/lawlevel.action';
import * as reducer from '../common/reducers/reducer';
import { SwalComponent } from '@toverux/ngsweetalert2';

@Component({
  selector: 'cat-page',
  templateUrl: './lawclass-edit.html'
})

export class GeneralinfoLawclassEdit implements OnInit {
  lawclassEditForm: FormGroup;
  private lawlevelId: number;
  @ViewChild('validation_dialog') private validationDialog: SwalComponent;
  @Input()
  private level: Lawlevel;
  private subscription$: Subscription;
  acl: ACL;

  constructor(private _route: ActivatedRoute,
    private _store: Store<reducer.State>,
    private fb: FormBuilder,
    private router: Router,
    private lawlevelEffects: LawlevelEffects) {
    this.acl = new ACL();
    // handle url string
    this._route.params.subscribe(params => {
      this.lawlevelId = +params['id'];
    });
    // dispatch select categoryId
    this._store.dispatch(new lawlevel.SelectLawlevelAction(this.lawlevelId));
    // initial form
    this.lawclassEditForm = this.fb.group({
      id: ['', [Validators.required]],
      name: ['', [Validators.required]],
      aliasName: ['', [Validators.required]]
    });

    this._store.let(reducer.getSelectedLawlevel).subscribe(lawlevel => {
      if (lawlevel !== null) {
        this.level = lawlevel;
        this.setFormValue(lawlevel);
      }
    });
    this.lawlevelEffects.updateLawlevel$.filter(
      action => action.type === lawlevel.ActionTypes.UPDATE_LAWLEVEL_SUCCESS
    ).subscribe(() => this.redirectToIndex());
  }

  ngOnInit(): void {
  }
  onSubmit(): void {
    if(this.lawclassEditForm.valid){
      this.level = this.lawclassEditForm.value;
      
      this._store.dispatch(new lawlevel.UpdateLawlevelAction(this.level));
    }else{
      this.validationDialog.show();
    }
  }
  setFormValue(lawlevel: Lawlevel): void {
    this.lawclassEditForm.setValue(lawlevel);
  }
  redirectToIndex(): void {
    this.router.navigate(['/generalinfo/lawclass-index']);
  }
  role(role: string): boolean {
    return this.acl.hasRole(role);
  }
  isReadOnly(role_list: Array<string>): boolean {
    let result = false;
    let thisobj = this;
    $.each(role_list,function(i,role){ 
      result = result||(thisobj.acl.isReadOnly(role)===false)
    });
    return !result;
  }

}
