import { ACL } from '../common/util/acl.util';
import { CategoryEffects } from '../common/effects/category.effect';
import { Component, OnInit, Input  ,ViewChild} from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Category } from '../common/models/category.model';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import * as category from '../common/actions/category.action';
import * as reducer from '../common/reducers/reducer';
import { SwalComponent } from '@toverux/ngsweetalert2';

@Component({
  selector: 'cat-page',
  templateUrl: './classification-edit.html'
})

export class GeneralinfoClassificationEditComponent implements OnInit {
  catEditForm: FormGroup;
  @ViewChild('validation_dialog') private validationDialog: SwalComponent;
  private categoryId: number;
  @Input()
  acl: ACL;
  private cat: Category;

  constructor(private _route: ActivatedRoute,
    private _store: Store<reducer.State>,
    private fb: FormBuilder,
    private router: Router,
    private categoryEffects: CategoryEffects) {
    this.acl = new ACL();
    // handle url string
    this._route.params.subscribe(params => {
      this.categoryId = +params['id'];
    });
    // dispatch select categoryId
    this._store.dispatch(new category.SelectCategoryAction(this.categoryId));
    // initial form
    this.catEditForm = this.fb.group({
      id: ['', [Validators.required]],
      name: ['', [Validators.required]],
      aliasName: ['', [Validators.required]]
    });

    this._store.let(reducer.getSelectedCategory).subscribe(category => {
      if (category !== null) {
        this.cat = category;
        this.setFormValue(category);
      }
    });
    this.categoryEffects.updateCategory$.filter(
      action => action.type === category.ActionTypes.UPDATE_CATEGORY_SUCCESS
    ).subscribe(() => this.redirectToIndex());
  }

  ngOnInit(): void {
  }
  onSubmit(): void {
    if(this.catEditForm.valid){
      this.cat = this.catEditForm.value;
      this._store.dispatch(new category.UpdateCategoryAction(this.cat));
    }else {
      this.validationDialog.show();
    }
  }
  setFormValue(category: Category): void {
    this.catEditForm.setValue(category);
  }
  redirectToIndex(): void {
    this.router.navigate(['/generalinfo/classification-index']);
  }
  role(role: string): boolean {
    return this.acl.hasRole(role);
  }
  isReadOnly(role_list: Array<string>): boolean {
    let result = false;
    let thisobj = this;
    $.each(role_list,function(i,role){ 
      result = result||(thisobj.acl.isReadOnly(role)===false)
    });
    return !result;
  }

}
