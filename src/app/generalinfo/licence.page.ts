import { ACL } from '../common/util/acl.util';
import { OnInit, Component, ViewChild } from '@angular/core';
import * as reducer from '../common/reducers/reducer';
import { Department } from '../common/models/department.model';
import { ApplicationConstant } from '../common/application-constant';
import { Store } from "@ngrx/store";
import { Subscription, Observable } from "rxjs";
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { DepartmentEffects } from '../common/effects/department.effect';
import * as departments from  '../common/actions/departments.action';
declare var $: any;

@Component({
  selector: 'cat-page',
  templateUrl: './licence.html',
})
export class GeneralLicenceIndex implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  //departments: Department[];
  private subscription: Subscription;
  dtOptions: DataTables.Settings = {
    "columnDefs": [
      { "width": "10%", "targets": 0 },
      { "width": "15%", "targets": 1 },
      { "width": "10%", "targets": 2 },
      { "width": "25%", "targets": 3 },
      { "width": "20%", "targets": 4 },
      { "width": "10%", "targets": 5 },
      { "width": "10%", "targets": 6 },

    ],
    "language": ApplicationConstant.DATATABLE_GLOBAL_LANGUAGE_CONFIG
  };
  // We use this trigger because fetching the list of persons can be quite long,
  // thus we ensure the data is fetched before rendering
  dtTrigger: Subject<any> = new Subject();
  public departments: Array<Department>;
  public department$: Observable<Array<Department>>;
  acl: ACL;
  constructor(
    private departmentEffects:DepartmentEffects,
    private _store: Store<reducer.State>) {
    this.acl = new ACL();
    this._store.let(reducer.getDepartments).subscribe(xxx => {
        this.departments = xxx;
        this.rerender();
        return this.departments
      }
    );
    this.departmentEffects.loadActiveDepartments$.filter(
      action => action.type === departments.ActionTypes.LOAD_ACTIVE_DEPARTMENTS_SUCCESS
    ).subscribe(() => {
      this.rerender()
    });
  }
  ngOnInit() {
    this._store.dispatch(new departments.LoadActiveDepartmentsAction({}));

    //this.dep = this.departmentNewForm.value;
    //this._store.dispatch(new licensetypes.CreateLicenseTypeAction(this.dep));
  }
  ngOnDestroy() { }
  // ngAfterViewInit(): void {
  //   this.dtTrigger.next();
  // }
  rerender(): void {
    if (this.dtElement) {
      if(this.dtElement.dtInstance!=undefined){
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          // Destroy the table first
          dtInstance.destroy();
          // Call the dtTrigger to rerender again
          this.dtTrigger.next();
        });
      }else{
        this.dtTrigger.next();
      }
    }
  }
  role(role: string): boolean {
    return this.acl.hasRole(role);
  }
  isReadOnly(role_list: Array<string>): boolean {
    let result = false;
    let thisobj = this;
    $.each(role_list,function(i,role){ 
      result = result||(thisobj.acl.isReadOnly(role)===false)
    });
    return !result;
  }
}
