import { ACL } from '../common/util/acl.util';
import { CategoryEffects } from '../common/effects/category.effect';
import { Component, OnInit, Input ,ViewChild} from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Category } from '../common/models/category.model';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import * as category from '../common/actions/category.action';
import * as reducer from '../common/reducers/reducer';
import { SwalComponent } from '@toverux/ngsweetalert2';

@Component({
  selector: 'cat-page',
  templateUrl: './classification-new.html',

})

export class GeneralinfoClassificationNew implements OnInit {
  catNewForm: FormGroup;
  @Input()
  @ViewChild('validation_dialog') private validationDialog: SwalComponent;
  acl: ACL;
  private cat: Category;

  constructor(private _route: ActivatedRoute,
    private _store: Store<reducer.State>,
    private fb: FormBuilder,
    private router: Router,
    private categoryEffects: CategoryEffects) {
    this.acl = new ACL();
    // initial form
    this.catNewForm = this.fb.group({
      name: ['', [Validators.required]],
      aliasName: ['', [Validators.required]],
      // email: ['', [Validators.required]]
    });

    this.categoryEffects.craeteCategory$.filter(
      action => action.type === category.ActionTypes.CREATE_CATEGORY_SUCCESS
    ).subscribe(() => this.redirectToIndex());

  }

  ngOnInit(): void {
  }
  onSubmit(): void {
    if(this.catNewForm.valid){
      this.cat = this.catNewForm.value;
      this._store.dispatch(new category.CreateCategoryAction(this.cat));
    }else {
      this.validationDialog.show();
    }
  }

  redirectToIndex(): void {
    this.router.navigate(['/generalinfo/classification-index']);
  }
  role(role: string): boolean {
    return this.acl.hasRole(role);
  }
  isReadOnly(role_list: Array<string>): boolean {
    let result = false;
    let thisobj = this;
    $.each(role_list,function(i,role){ 
      result = result||(thisobj.acl.isReadOnly(role)===false)
    });
    return !result;
  }

}
