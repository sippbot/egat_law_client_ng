import { SweetAlert2Module } from '@toverux/ngsweetalert2';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { GeneralinfoDepartmentIndex } from './department-index.page';
import { GeneralinfoDepartmentNew } from './department-new.page';
import { GeneralinfoDepartmentEdit } from './department-edit.page';
import { GeneralinfoLawclassIndexComponent } from './lawclass-index.page';
import { GeneralinfoLawclassNew } from './lawclass-new.page';
import { GeneralinfoLawclassEdit } from './lawclass-edit.page';
import { GeneralinfoClassificationIndexComponent } from './classification-index.page';
import { GeneralinfoClassificationNew } from './classification-new.page';
import { GeneralinfoClassificationEditComponent } from './classification-edit.page';
import { GeneralLicenceIndex } from './licence.page';
import { GeneralLicenceNew } from './licence-new.page';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { StoreModule } from '@ngrx/store';
import { reducer } from '../common/reducers/reducer';
import { EffectsModule } from '@ngrx/effects';
import { DepartmentService } from '../common/services/department.service';
import { DepartmentEffects } from '../common/effects/department.effect';
import { MetadataService } from '../common/services/metadata.service';
import { LawlevelEffects } from '../common/effects/lawlevel.effect';
import { AuthGuard } from '../guard/auth.guard';

// import { reducer } from '../common/reducers/reducer';
export const routes: Routes = [
  { path: 'generalinfo/department-index', component: GeneralinfoDepartmentIndex, canActivate: [AuthGuard] },
  { path: 'generalinfo/department-new', component: GeneralinfoDepartmentNew, canActivate: [AuthGuard] },
  { path: 'generalinfo/department-edit/:id', component: GeneralinfoDepartmentEdit, canActivate: [AuthGuard] },

  { path: 'generalinfo/lawclass-index', component: GeneralinfoLawclassIndexComponent, canActivate: [AuthGuard] },
  { path: 'generalinfo/lawclass-new', component: GeneralinfoLawclassNew, canActivate: [AuthGuard] },
  { path: 'generalinfo/lawclass-edit/:id', component: GeneralinfoLawclassEdit, canActivate: [AuthGuard] },

  { path: 'generalinfo/licence', component: GeneralLicenceIndex, canActivate: [AuthGuard] },
  { path: 'generalinfo/licence-new', component: GeneralLicenceNew, canActivate: [AuthGuard] },

  { path: 'generalinfo/classification-index', component: GeneralinfoClassificationIndexComponent, canActivate: [AuthGuard] },
  { path: 'generalinfo/classification-new', component: GeneralinfoClassificationNew, canActivate: [AuthGuard] },
  { path: 'generalinfo/classification-edit/:id', component: GeneralinfoClassificationEditComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    DataTablesModule,
    SweetAlert2Module
  ],
  declarations: [
    GeneralinfoDepartmentIndex,
    GeneralinfoDepartmentNew,
    GeneralinfoDepartmentEdit,

    GeneralinfoLawclassIndexComponent,
    GeneralinfoLawclassNew,
    GeneralinfoLawclassEdit,

    GeneralLicenceIndex,
    GeneralLicenceNew,

    GeneralinfoClassificationIndexComponent,
    GeneralinfoClassificationNew,
    GeneralinfoClassificationEditComponent
  ],
  providers: [
    DepartmentService,
    MetadataService
  ]
})
export class GeneralinfoModule { }
