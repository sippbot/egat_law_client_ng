import { ACL } from '../common/util/acl.util';
import { DepartmentEffects } from '../common/effects/department.effect';
import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Department } from '../common/models/department.model';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { ActivatedRoute } from '@angular/router';
import * as department from '../common/actions/departments.action';
import * as reducer from '../common/reducers/reducer';
import { SwalComponent } from '@toverux/ngsweetalert2';

declare var $: any;
@Component({
  selector: 'cat-page',
  templateUrl: './department-new.html'
})

export class GeneralinfoDepartmentNew implements OnInit {
  departmentNewForm: FormGroup;
  @Input()
  @ViewChild('validation_dialog') private validationDialog: SwalComponent;
  acl: ACL;
  private dep: Department;

  constructor(private _route: ActivatedRoute,
    private _store: Store<reducer.State>,
    private fb: FormBuilder,
    private router: Router,
    private departmentEffects: DepartmentEffects) {
    this.acl = new ACL();
    // initial form
    this.departmentNewForm = this.fb.group({
      id: ['', [Validators.required]],
      name: ['', [Validators.required]],
      aliasName: ['', [Validators.required]],
      email: ['', [Validators.required]]
    });

    this.departmentEffects.craeteDepartments$.filter(
      action => action.type === department.ActionTypes.CREATE_DEPARTMENT_SUCCESS)
      .subscribe(() => this.redirectToIndex());

  }

  ngOnInit(): void {

    $(function () {
      $('.select2').select2();
      $('.select2-tags').select2({
        tags: true,
        tokenSeparators: [',', ' ']
      });

      $('.add-administrator').click(function () {

        $('.select2-selection__rendered li.select2-selection__choice').each(function () {
          const title = $(this).attr('title');
          const content = '<tr>' +
            '<td>' +
            '<input name="administrator[firstname][]"' +
            'type="text"' +
            'class="form-control"' +
            'value="' + title + '" disabled>' +
            '</td>' +
            '<td>' +
            '<input name="administrator[lastname][]"' +
            'type="text"' +
            'class="form-control"' +
            'value="" disabled>' +
            '</td>' +
            ' <td>' +
            '<select name="administrator[role][]"' +
            'class="form-control select"' +
            'data-validation="[NOTEMPTY]">' +
            '<option value="ผู้ใช้ทั่วไป">ผู้ใช้ทั่วไป</option>' +
            '<option value="ผู้ใช้หลัก">ผู้ใช้หลัก</option>' +
            '</select>' +
            '</td>' +
            '</tr>';
          $('table tbody').append(content);
        });

      });
    });
  }
  onSubmit(): void {
    if (this.departmentNewForm.valid) {
      this.dep = this.departmentNewForm.value;
      this._store.dispatch(new department.CreateDepartmentAction(this.dep));
    } else {
      this.validationDialog.show();
    }
  }

  redirectToIndex(): void {
    this.router.navigate(['/generalinfo/department-index']);
  }
  role(role: string): boolean {
    return this.acl.hasRole(role);
  }
  isReadOnly(role_list: Array<string>): boolean {
    let result = false;
    let thisobj = this;
    $.each(role_list, function (i, role) {
      result = result || (thisobj.acl.isReadOnly(role) === false)
    });
    return !result;
  }
}

