import { ACL } from '../common/util/acl.util';
import { ShowAlertAction } from '../common/actions/alert.action';
import { CategoryEffects } from '../common/effects/category.effect';
import { OnInit, Component, ViewChild } from '@angular/core';
import { Category } from '../common/models/category.model';
import { ApplicationConstant } from '../common/application-constant';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { DataTableDirective } from 'angular-datatables';
import * as reducer from '../common/reducers/reducer';
import * as category from '../common/actions/category.action';
import { Subject } from 'rxjs';
import { Alert } from '../common/models/alert.model';
declare var $: any;
declare var NProgress: any;

@Component({
  selector: 'cat-page',
  templateUrl: './classification-index.html'
})
export class GeneralinfoClassificationIndexComponent implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  categories: Array<Category>;
  acl: ACL;
  /*
  'columnDefs': [
        { 'width': '10%', 'targets': 0 },
        { 'width': '35%', 'targets': 1 },
        { 'width': '35%', 'targets': 2 },
        { 'width': '20%', 'targets': 3 }
      ],
  */
  dtTrigger: Subject<any> = new Subject();
  dtOptions: DataTables.Settings = {

    'pageLength': 30,
    'language': ApplicationConstant.DATATABLE_GLOBAL_LANGUAGE_CONFIG,
    'autoWidth': true,
    'dom': '<\'row\'<\'col-sm-12\'B>>' +
      '<\'row\'<\'col-sm-6\'l><\'col-sm-6\'f>>' +
      '<\'row\'<\'col-sm-12\'tr>>' +
      '<\'row\'<\'col-sm-5\'i><\'col-sm-7\'p>>',
    'buttons': [
      {
        extend: 'excelHtml5',
        text: '<i class="fa fa-file-excel-o fa-x5"></i> ดาวน์โหลด Excel',
        className: 'btn btn-primary btn-sm glyphicon glyphicon-list-alt pull-right',
        exportOptions:{
          columns:[0,1,2]
        }
      },
      {
        extend: 'print',
        text: '<i class="fa fa-print fa-x5"></i> พิมพ์รายงาน',
        className: 'btn btn-primary btn-sm btn-print glyphicon glyphicon-print pull-right',
        pageSize: 'A4',
        title: 'รายการเอกสาร',
        exportOptions:{
          columns:[0,1,2]
        }
      }
    ]
  };
  // We use this trigger because fetching the list of persons can be quite long,
  // thus we ensure the data is fetched before rendering

  constructor(private _store: Store<reducer.State>,
    private router: Router,
    private categoryEffects: CategoryEffects) {
    this.acl = new ACL();
    this.categories = new Array<Category>();
    this._store.let(reducer.getCategories).subscribe(categories => {
      this.categories = categories;
      this.rerender();
      return this.categories
    });
    this.categoryEffects.deleteCategory$.filter(
      action => action.type === category.ActionTypes.DELETE_CATEGORY_SUCCESS
    ).subscribe(() => this.pushSuccessNotification());
    this.categoryEffects.deleteCategory$.filter(
      action => action.type === category.ActionTypes.DELETE_CATEGORY_FAIL
    ).subscribe(() => this.pushFailNotification());
    this.categoryEffects.loadCategory$.filter(
      action => action.type === category.ActionTypes.LOAD_CATEGORIES_SUCCESS
    ).subscribe(() => this.rerender());
  }
  rerender(): void {
    if (this.dtElement) {
      if(this.dtElement.dtInstance!=undefined){
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          // Destroy the table first
          dtInstance.destroy();
          // Call the dtTrigger to rerender again
          this.dtTrigger.next();
        });
      }else{
        this.dtTrigger.next();
      }
    }
  }
  ngOnInit() {
    NProgress.start();
    this._store.dispatch(new category.LoadCategoryAction({}));
  }

  selectCategory(id: number): void {
    this.router.navigate(['/generalinfo/classification-edit']);
  }

  deleteCategory(id: number) {
    
    this._store.dispatch(new category.DeleteCategoryAction(id));
  }

  pushSuccessNotification() {
    this._store.dispatch(new ShowAlertAction(new Alert('ดำเนินการลบหมวดหมู่กฎหมายสำเร็จ', 'สำเร็จ!', 'success')));
  }
  pushFailNotification() {
    this._store.dispatch(new ShowAlertAction(
      new Alert('ไม่สามารถลบหมวดหมู่กฎหมายได้เนื่องจาก มีผู้ใช้อยู่ในกลุ่มนี้', 'ล้มเหลว!', 'fail')));
  }
  role(role: string): boolean {
    return this.acl.hasRole(role);
  }
  isReadOnly(role_list: Array<string>): boolean {
    let result = false;
    let thisobj = this;
    $.each(role_list,function(i,role){ 
      result = result||(thisobj.acl.isReadOnly(role)===false)
    });
    return !result;
  }
}
