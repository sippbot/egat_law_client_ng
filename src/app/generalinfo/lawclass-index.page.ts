import { ACL } from '../common/util/acl.util';
import { ShowAlertAction } from '../common/actions/alert.action';
import { LawlevelEffects } from '../common/effects/lawlevel.effect';
import { OnInit, Component, ViewChild } from '@angular/core';
import * as reducer from '../common/reducers/reducer';
import { Lawlevel } from '../common/models/lawlevel.model';
import { ApplicationConstant } from '../common/application-constant';
import { Store } from '@ngrx/store';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import * as lawlevels from '../common/actions/lawlevel.action';
import { Alert } from '../common/models/alert.model';
declare var $: any;

@Component({
  selector: 'cat-page',
  templateUrl: './lawclass-index.html'
})

export class GeneralinfoLawclassIndexComponent implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtTrigger: Subject<any> = new Subject();
  dtOptions: DataTables.Settings = {

    'pageLength': 30,
    'language': ApplicationConstant.DATATABLE_GLOBAL_LANGUAGE_CONFIG,
    'autoWidth': true,
    'dom': '<\'row\'<\'col-sm-12\'B>>' +
      '<\'row\'<\'col-sm-6\'l><\'col-sm-6\'f>>' +
      '<\'row\'<\'col-sm-12\'tr>>' +
      '<\'row\'<\'col-sm-5\'i><\'col-sm-7\'p>>',
    'buttons': [
      {
        extend: 'excelHtml5',
        text: '<i class="fa fa-file-excel-o fa-x5"></i> ดาวน์โหลด Excel',
        className: 'btn btn-primary btn-sm glyphicon glyphicon-list-alt pull-right',
        exportOptions:{
          columns:[0,1,2]
        }
      },
      {
        extend: 'print',
        text: '<i class="fa fa-print fa-x5"></i> พิมพ์รายงาน',
        className: 'btn btn-primary btn-sm btn-print glyphicon glyphicon-print pull-right',
        pageSize: 'A4',
        title: 'รายการเอกสาร',
        exportOptions:{
          columns:[0,1,2]
        }
      }
    ]
  };
  acl: ACL;
  lawlevels: Array<Lawlevel>;

  constructor(private _store: Store<reducer.State>,
    private LawlevelEffects: LawlevelEffects) {
    this.acl = new ACL();
    this.lawlevels = new Array<Lawlevel>();
    this._store.let(reducer.getLawlevels).subscribe(lawLevel => 
      {
        this.lawlevels = lawLevel
        this.rerender();
        return this.lawlevels
      }
    );
    this.LawlevelEffects.loadLawlevels$.filter(
      action => action.type === lawlevels.ActionTypes.LOAD_LAWLEVEL_SUCCESS
    ).subscribe(() => this.rerender());
    this.LawlevelEffects.deleteLawlevel$.filter(
      action => action.type === lawlevels.ActionTypes.DELETE_LAWLEVEL_SUCCESS
    ).subscribe(() => this.pushSuccessNotification());
    // this.LawlevelEffects.deleteLawlevel$.filter(
    //   action => action.type === lawlevels.ActionTypes.DELETE_LAWLEVEL_FAIL
    // ).subscribe(() => this.pushFailNotification());
  }
  rerender(): void {
    if (this.dtElement) {
      if(this.dtElement.dtInstance!=undefined){
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          // Destroy the table first
          dtInstance.destroy();
          // Call the dtTrigger to rerender again
          this.dtTrigger.next();
        });
      }else{
        this.dtTrigger.next();
      }
    }
  }
  ngOnInit() {
    this._store.dispatch(new lawlevels.LoadLawlevelAction({}));
  }

  deleteLawLevel(id: number) {
    
    this._store.dispatch(new lawlevels.DeleteLawlevelAction(id));
  }

  pushSuccessNotification() {
    this._store.dispatch(new ShowAlertAction(new Alert('ดำเนินการลบประเภทกฎหมายสำเร็จ', 'สำเร็จ!', 'success')));
    this._store.dispatch(new lawlevels.LoadLawlevelAction({}));
  }
  pushFailNotification() {
    this._store.dispatch(new ShowAlertAction(
      new Alert('ไม่สามารถลบประเภทกฎหมายได้เนื่องจาก มีกฎหมายอยู่ในประเภทนี้', 'ล้มเหลว!', 'fail')));
  }
  role(role: string): boolean {
    return this.acl.hasRole(role);
  }
  isReadOnly(role_list: Array<string>): boolean {
    let result = false;
    let thisobj = this;
    $.each(role_list,function(i,role){ 
      result = result||(thisobj.acl.isReadOnly(role)===false)
    });
    return !result;
  }
}
