import { ACL } from '../common/util/acl.util';
import { Alert } from '../common/models/alert.model';
import { ShowAlertAction } from '../common/actions/alert.action';
import { OnInit, Component, ViewChild } from '@angular/core';
import * as reducer from '../common/reducers/reducer';
import { DepartmentEffects } from '../common/effects/department.effect';
import { Department } from '../common/models/department.model';
import { ApplicationConstant } from '../common/application-constant';
import { Store } from '@ngrx/store';
import { DataTableDirective } from 'angular-datatables';
import * as departments from '../common/actions/departments.action';
declare var $: any;

@Component({
  selector: 'cat-page',
  templateUrl: './department-index.html',
})
export class GeneralinfoDepartmentIndex implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  // departments: Department[];
  /*
  'columnDefs': [
      { 'width': '10%', 'targets': 0 },
      { 'width': '35%', 'targets': 1 },
      { 'width': '10%', 'targets': 2 },
      { 'width': '25%', 'targets': 3 },
      { 'width': '20%', 'targets': 4 }
    ],
  */
  dtOptions: DataTables.Settings = {

    'pageLength': 30,
    'language': ApplicationConstant.DATATABLE_GLOBAL_LANGUAGE_CONFIG,
    'autoWidth': true,
    'dom': '<\'row\'<\'col-sm-12\'B>>' +
      '<\'row\'<\'col-sm-6\'l><\'col-sm-6\'f>>' +
      '<\'row\'<\'col-sm-12\'tr>>' +
      '<\'row\'<\'col-sm-5\'i><\'col-sm-7\'p>>',
    'buttons': [
      {
        extend: 'excelHtml5',
        text: '<i class="fa fa-file-excel-o fa-x5"></i> ดาวน์โหลด Excel',
        className: 'btn btn-primary btn-sm glyphicon glyphicon-list-alt pull-right',
        exportOptions: {
          columns: [0, 1, 2, 3, 4]
        }
      },
      {
        extend: 'print',
        text: '<i class="fa fa-print fa-x5"></i> พิมพ์รายงาน',
        className: 'btn btn-primary btn-sm btn-print glyphicon glyphicon-print pull-right',
        pageSize: 'A4',
        title: 'รายการเอกสาร',
        exportOptions: {
          columns: [0, 1, 2, 3, 4]
        }
      }
    ]
  };
  public departments: Array<Department>;
  acl: ACL;

  constructor(private _store: Store<reducer.State>, private departmentEffects: DepartmentEffects) {
    this.acl = new ACL();
    this._store.let(reducer.getDepartments).subscribe(departments => {
      this.departments = departments;
    });
    this.departmentEffects.deleteDepartments$.filter(
      action => action.type === departments.ActionTypes.DELETE_DEPARTMENT_SUCCESS
    ).subscribe(() => this.pushSuccessNotification());
    this.departmentEffects.deleteDepartments$.filter(
      action => action.type === departments.ActionTypes.DELETE_DEPARTMENT_FAIL
    ).subscribe(() => this.pushFailNotification());
  }
  ngOnInit() {
    this._store.dispatch(new departments.LoadDepartmentsAction({}));
  }
  deleteDepartment(departmentId: number) {

    this._store.dispatch(new departments.DeleteDepartmentAction(departmentId));
  }
  pushSuccessNotification() {
    this._store.dispatch(new departments.LoadDepartmentsAction({}));
    this._store.dispatch(new ShowAlertAction(new Alert('ดำเนินการลบหน่วยงานสำเร็จ', 'สำเร็จ!', 'success')));
  }
  pushFailNotification() {
    this._store.dispatch(new ShowAlertAction(
      new Alert('ไม่สามารถลบหน่วยงานได้', 'ล้มเหลว!', 'fail')));
  }
  role(role: string): boolean {
    return this.acl.hasRole(role);
  }
  isReadOnly(role_list: Array<string>): boolean {
    let result = false;
    let thisobj = this;
    $.each(role_list, function (i, role) {
      result = result || (thisobj.acl.isReadOnly(role) === false)
    });
    return !result;
  }
}
