import { ACL } from '../common/util/acl.util';
import { ShowAlertAction } from '../common/actions/alert.action';
import { DepartmentEffects } from '../common/effects/department.effect';
import { Component, OnInit, AfterViewInit, Input, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Department } from '../common/models/department.model';
import { User } from '../common/models/user.model';
import { Post } from '../common/models/post.model';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import * as department from '../common/actions/departments.action';
import * as users from '../common/actions/users.action';
import * as post from '../common/actions/post.action';
import * as reducer from '../common/reducers/reducer';
import { SwalComponent } from '@toverux/ngsweetalert2';
import { Alert } from '../common/models/alert.model';
import { PostEffects } from '../common/effects/post.effect';

declare var $: any;
declare var jQuery: any;

@Component({
    selector: 'cat-page',
    templateUrl: './department-edit.html'
})

export class GeneralinfoDepartmentEdit implements OnInit, AfterViewInit {
    departmentEditForm: FormGroup;
    postForm: FormGroup;
    @ViewChild('dialog') private swalDialog: SwalComponent;
    @ViewChild('validation_dialog') private validationDialog: SwalComponent;
    @ViewChild('removeUserList_dialog') private removeUserListDialog: SwalComponent;
    @Input()
    acl: ACL;
    isPrimaryArr = [
        { position: 'หัวหน้ากอง/แผนก', status: true },
        { position: 'ผู้แทน', status: false }
    ];
    departmentId: number;
    private dep: Department;
    private postObject: Post;
    users: User[];
    postArr: Post[];
    private subscription$: Subscription;
    selectedUserId = 0;

    constructor(private _route: ActivatedRoute,
        private _store: Store<reducer.State>,
        private fb: FormBuilder,
        private router: Router,
        private departmentEffects: DepartmentEffects,
        private postEffects:PostEffects) {
            this.acl = new ACL();
        // handle url string
        this._route.params.subscribe(params => {
            this.departmentId = +params['id'];
            if(this.departmentId>0){
                this._store.dispatch(new department.SelectOrLoadDepartmentAction(this.departmentId));
            }
        });

        
        // dispatch  all users
        this._store.dispatch(new users.LoadUsersAction({}));
        // dispatch get post from department ID
        this._store.dispatch(new post.LoadPostAction(this.departmentId));

        // initial departmentEditForm
        this.departmentEditForm = this.fb.group({
            id: ['', [Validators.required]],
            name: ['', [Validators.required]],
            aliasName: ['', [Validators.required]],
            email: ['', [Validators.required]],
            isActive: [false, [Validators.required]]
        });

        // initial postForm
        this.postForm = this.fb.group({
            isPrimary: [true, [Validators.required]],
            userId: [null, [Validators.required]],
            departmentId: [this.departmentId, null],
        });

        // get users data for set to departmentEditForm
        this._store.let(reducer.getUsers).subscribe(users => {
            if (users !== null) {
                this.users = users;
            }
        });

        // get post array data for set to departmentEditForm
        this._store.let(reducer.getPostById).subscribe(post => {
            // Following code should check whether post is not null or empty because of post is originally an array.
            if (post.length !== 0) {
                // console.log(post)
                this.postArr = post;
                // this.postForm.patchValue({departmentId: post[0].departmentId});
            }
        });

        // get department object data for set to departmentEditForm
        this._store.let(reducer.getSelectedDepartment).subscribe(department => {
            if (department !== null) {
                // console.log(department)
                this.dep = department;
                this.setFormValue(department);
            }
        });
        this.departmentEffects.updateDepartments$.filter(
            action => action.type === department.ActionTypes.UPDATE_DEPARTMENT_SUCCESS
        ).subscribe(() => this.swalDialog.show());

        this.departmentEffects.updateDepartments$.filter(
            action => action.type === department.ActionTypes.UPDATE_DEPARTMENT_FAIL
        ).subscribe(() => this._store.dispatch(new ShowAlertAction(
            new Alert('ไม่สามารถลบหน่วยงานนี้ได้เนื่องจาก มีผู้ใช้อยู่ในกลุ่มนี้', 'ล้มเหลว!', 'fail'))));
        
    }

    ngAfterViewInit() {
        jQuery('.department-post').on(
            'change',
            (e) => {
                this.selectedUserId = jQuery(e.target).val();
            }
        );
    }

    ngOnInit(): void {
        $(function () {
            // $('.select2').select2();
            $('.select2-tags').select2({
                tags: true,
                tokenSeparators: [',', ' ']
            });

            $('.add-administrator').click(function () {

                $('.select2-selection__rendered li.select2-selection__choice').each(function () {
                    const title = $(this).attr('title');
                    const content = '<tr>' +
                        '<td>' +
                        '<input name="administrator[firstname][]"' +
                        'type="text"' +
                        'class="form-control"' +
                        'value="' + title + '" disabled>' +
                        '</td>' +
                        '<td>' +
                        '<input name="administrator[lastname][]"' +
                        'type="text"' +
                        'class="form-control"' +
                        'value="" disabled>' +
                        '</td>' +
                        ' <td>' +
                        '<select name="administrator[role][]"' +
                        'class="form-control select"' +
                        'data-validation="[NOTEMPTY]">' +
                        '<option value="????????????">????????????</option>' +
                        '<option value="??????????">??????????</option>' +
                        '</select>' +
                        '</td>' +
                        '</tr>';
                    $('table tbody').append(content);
                });

            });
        });
    }
    deleteUser(user){
        console.log("remove",user,this.postEffects.removePost$)
        
        this._store.dispatch(new post.RemovePostAction(user));
        
    }

    onSubmit(): void {
        if(this.departmentEditForm.valid){
            

            this.dep = this.departmentEditForm.value;
            this._store.dispatch(new department.UpdateDepartmentAction(this.dep));
        }else{
            this.validationDialog.show();
        }
    }
    stayOnPage() {
    }

    onPostSubmit(): void {
        console.log(this.postForm.value)
        this.postObject = this.postForm.value;
        // if(this.selectedUserId!=null){
            // Also, assign hidden field of form here
            this.postObject.departmentId = this.departmentId;
            this.postObject.userId = this.selectedUserId;
            
            this._store.dispatch(new post.CreatePostAction(this.postObject));
        // }else{
        //     this.validationDialog.show();
        // }
        
        
    }

    setFormValue(department: Department): void {
        if(department!=undefined){
            this.departmentEditForm.setValue({
                id: department.id,
                name: department.name,
                aliasName: department.aliasName,
                email: department.email,
                isActive: department.isActive
            });
        }
    }

    redirectToIndex(): void {
        this.router.navigate(['/generalinfo/department-index']);
    }
    role(role: string): boolean {
        return this.acl.hasRole(role);
    }
    isReadOnly(role_list: Array<string>): boolean {
        let result = false;
        let thisobj = this;
        $.each(role_list,function(i,role){ 
            result = result||(thisobj.acl.isReadOnly(role)===false)
        });
        return !result;
    }
}
