import { ACL } from '../common/util/acl.util';
import { LawlevelEffects } from '../common/effects/lawlevel.effect';
import { Component, OnInit, Input, ViewChild} from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Lawlevel } from '../common/models/lawlevel.model';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import * as lawlevel from '../common/actions/lawlevel.action';
import * as reducer from '../common/reducers/reducer';
import { SwalComponent } from '@toverux/ngsweetalert2';

@Component({
  selector: 'cat-page',
  templateUrl: './lawclass-new.html'
})

export class GeneralinfoLawclassNew implements OnInit {
  lawclassForm: FormGroup;
  @Input()
  @ViewChild('validation_dialog') private validationDialog: SwalComponent;
  private level: Lawlevel;
  acl: ACL;

  constructor(private _store: Store<reducer.State>,
    private fb: FormBuilder,
    private router: Router,
    private lawlevelEffects: LawlevelEffects) {
    this.acl = new ACL();
    // initial form
    this.lawclassForm = this.fb.group({
      name: ['', [Validators.required]],
      aliasName: ['', [Validators.required]]
    });

    this.lawlevelEffects.craeteLawlevel$.filter(
      action => action.type === lawlevel.ActionTypes.CREATE_LAWLEVEL_SUCCESS
    ).subscribe(() => this.redirectToIndex());

  }

  ngOnInit(): void {
  }
  onSubmit(): void {
    if(this.lawclassForm.valid){
      this.level = this.lawclassForm.value;
      console.log(this.level);
      this._store.dispatch(new lawlevel.CreateLawlevelAction(this.level));
    }else{
      this.validationDialog.show();
    }
  }

  redirectToIndex(): void {
    this.router.navigate(['/generalinfo/lawclass-index']);
  }
  role(role: string): boolean {
    return this.acl.hasRole(role);
  }
  isReadOnly(role_list: Array<string>): boolean {
    let result = false;
    let thisobj = this;
    $.each(role_list,function(i,role){ 
      result = result||(thisobj.acl.isReadOnly(role)===false)
    });
    return !result;
  }
}
