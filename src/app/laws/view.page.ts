import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'cat-page',
  templateUrl: './view.html',
})

export class LawsView implements OnInit {
  artifactId: number;

  constructor(private _route: ActivatedRoute) {
    this._route.params.subscribe(params => {
      this.artifactId = +params['id'];
    });
  }

  ngOnInit() {
    // this._store.dispatch(new artifact.SelectViewArtifactAction(this.artifactId));
  }
}
