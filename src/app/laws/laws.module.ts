import { ArtifactModule } from '../artifact/artifact.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LawsView } from './view.page';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { ArtifactService } from '../common/services/artifact.service';
import { AuthGuard } from '../guard/auth.guard';
import { MyDatePickerTHModule } from 'mydatepicker-th';

export const routes: Routes = [
//  { path: 'laws/index', component: LawsIndex, canActivate: [AuthGuard] },
//  { path: 'laws/new', component: LawsNew, canActivate: [AuthGuard] },
//  { path: 'laws/draft', component: LawsDraft, canActivate: [AuthGuard] },
 // { path: 'laws/cancel', component: LawsCancel, canActivate: [AuthGuard] },
 // { path: 'laws/edit/:id', component: LawsEdit, canActivate: [AuthGuard] },
////  { path: 'laws/addnew', component: LawsAddnew, canActivate: [AuthGuard] },
  // { path: 'laws/in-progress', component: LawsInProgress, canActivate: [AuthGuard] },
//  { path: 'laws/artifact-assesment/:id', component: LawsArtifactAssesment, canActivate: [AuthGuard] },
//  { path: 'laws/artifact-assesment/:id/:departmentId', component: LawsArtifactAssesment, canActivate: [AuthGuard] },
//  { path: 'laws/review-artifact-assesment/:id', component: LawsReviewArtifactAssesment, canActivate: [AuthGuard] },
//  { path: 'laws/approve-artifact-assesment/:id', component: LawsApproveArtifactAssesment, canActivate: [AuthGuard] },
  { path: 'laws/view/:id', component: LawsView, canActivate: [AuthGuard] },
 // { path: 'laws/view-review-artifact/:id', component: ViewReviewArtifact, canActivate: [AuthGuard] },
 // { path: 'laws/view-approve-artifact/:id', component: ViewApproveArtifact, canActivate: [AuthGuard] }

];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    DataTablesModule,
    BrowserModule,
    FroalaEditorModule,
    FroalaViewModule,
    MyDatePickerTHModule,
    ArtifactModule
  ],
  declarations: [
    LawsView
  ],
  providers: [
    ArtifactService,
    AuthGuard
  ]

})

export class LawsModule { }
