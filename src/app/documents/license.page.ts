import { ACL } from '../common/util/acl.util';
import { DataTableDirective } from 'angular-datatables';
import { ApplicationConstant } from '../common/application-constant';
import { Store } from '@ngrx/store';
import { License } from '../common/models/license.model';
import { DialogService } from 'ng2-bootstrap-modal';
import { LicenseUploadComponent } from '../license/upload/upload.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import * as reducer from '../common/reducers/reducer';
import * as license from '../common/actions/license.action';
declare var $: any;

@Component({
  selector: 'cat-page',
  templateUrl: './license.html'
})
export class DocumentLicenseComponent implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  userId: number;
  userGroup: string;
  licenses: Array<License>;
  acl: ACL;
  dtOptions: DataTables.Settings = {
    'columnDefs': [
      { 'width': '20%', 'targets': 0 },
      { 'width': '10%', 'targets': 1 },
      { 'width': '10%', 'targets': 2 },
      { 'width': '10%', 'targets': 3 },
      { 'width': '10%', 'targets': 4 },
      { 'width': '10%', 'targets': 5 },
      { 'width': '10%', 'targets': 6 },
      { 'width': '10%', 'targets': 7 },
      { 'width': '10%', 'targets': 8 }
    ],
    'pageLength': 30,
    'language': ApplicationConstant.DATATABLE_GLOBAL_LANGUAGE_CONFIG
  };

  constructor(private _store: Store<reducer.State>,
    private dialogService: DialogService) {
    this.acl = new ACL();
    this.userId = +localStorage.getItem('userId');
    this.userGroup = localStorage.getItem('userGroup');
    this._store.let(reducer.getLicenses).subscribe(licenses => {
      this.licenses = licenses;
    });
  }
  ngOnInit() {
    if (this.userGroup === '2' || this.userGroup === '5') {
      this._store.dispatch(new license.LoadLicensesAction(0));
    } else {
      this._store.dispatch(new license.LoadLicensesAction(this.userId));
    }

  }

  uploadLicense() {
    const disposable = this.dialogService.addDialog(LicenseUploadComponent, {
      taskAssignmentId: 0,
      userId: this.userId
    }).subscribe((isSuccess) => {
      // We get dialog result
      if (isSuccess) {
        // alert('success');
      } else {
        // alert('declined');
      }
    });
  }
  role(role: string): boolean {
    return this.acl.hasRole(role);
  }
  isReadOnly(role_list: Array<string>): boolean {
    let result = false;
    let thisobj = this;
    $.each(role_list,function(i,role){ 
      result = result||(thisobj.acl.isReadOnly(role)===false)
    });
    return !result;
  }

}
