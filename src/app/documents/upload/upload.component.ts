import { DocumentEffects } from '../../common/effects/document.effect';
import { Subscription } from 'rxjs';
import { ShowAlertAction } from '../../common/actions/alert.action';
import { Store } from '@ngrx/store';
import { Document } from '../../common/models/document.model';
import { Component, OnInit, Input } from '@angular/core';
import * as reducer from '../../common/reducers/reducer';
import * as taskAssignment from '../../common/actions/task-assignment.action';
import * as document from '../../common/actions/document.action';
import { Alert } from '../../common/models/alert.model';
import { Http, Headers, RequestOptions } from '@angular/http';
import { ApplicationConstant } from '../../common/application-constant';
import { TaskAssignmentEffects } from '../../common/effects/task-assignment.effect';

declare var $: any;
declare var NProgress: any;

@Component({
  selector: 'app-document-upload',
  templateUrl: './upload.component.html'
})
export class DocumentUploadComponent implements OnInit {
  @Input() userId: number;
  @Input() taskAssignmentId: number;
  private apiEndPoint: string = ApplicationConstant.APP_BASE_ENDPOINT_URL + '/fileupload';
  // private fileEndpoint: string = ApplicationConstant.APP_BASE_ENDPOINT_URL + '/document/';
  count = 0;
  // fileDatas: FileData[];
  documents: Document[];
  fileName: string;
  isHasFileUpload = false;
  formSuccess: Subscription;
  formError: Subscription;
  isReadyToSubmit = false;

  constructor(private _store: Store<reducer.State>,
    private http: Http,
    private documentEffects: DocumentEffects,
    private taskAssignmentEffects: TaskAssignmentEffects) {
    this.documents = [];
    this.fileName = '';
    this.taskAssignmentEffects.createTaskAttachment$.filter(
      action => action.type === taskAssignment.ActionTypes.CREATE_TASKASSIGNMENT_ATTACHMENT_SUCCESS
    ).subscribe(() => {
      this._store.dispatch(new ShowAlertAction(new Alert('บันทึกไฟล์ของท่านเรียบร้อยแล้ว', 'อัพโหลดสำเร็จ!', 'success')));
      this.closeModal();
    });
    this.documentEffects.updateDocuments$.filter(
      action => action.type === document.ActionTypes.UPDATE_DOCUMENT_SUCCESS
    ).subscribe(() => {
      this._store.dispatch(new ShowAlertAction(new Alert('บันทึกไฟล์ของท่านเรียบร้อยแล้ว', 'อัพโหลดสำเร็จ!', 'success')));
      this.closeModal();
    });
  }

  ngOnInit() {
  }

  fileChange(event) {
    const fileList: FileList = event.target.files;
    this.isHasFileUpload = true;
    for (let i = 0; i < fileList.length; i++) {
      const file: File = fileList[i];
      const formData: FormData = new FormData();
      if (this.fileName === '') {
        this.fileName = this.fileName;
      }
      formData.append('uploadFile', file, file.name);
      formData.append('ownerId', localStorage.getItem('userId'));
      formData.append('departmentId', localStorage.getItem('departmentId'));
      // let xxsrfToken = localStorage.getItem('xsrfToken');
      const headers = new Headers();
      headers.append('Accept', 'application/json');
      // headers.append('X-XSRF-TOKEN', xxsrfToken);
      const options = new RequestOptions({ headers: headers });
      NProgress.start();
      this.http.post(`${this.apiEndPoint}`, formData, options)
        .map(res => res.json())
        // .catch(error => Observable.throw(error))
        .subscribe(
          data => this.handleFileUploadSuccess(data),
          error => console.log(error.json())
        );
    }
  }

  handleFileUploadSuccess(data: any) {
    NProgress.done();
    this.isReadyToSubmit = true;
    const doc: Document = new Document();
    doc.id = data.id;
    doc.title = data.filename;
    this.documents.push(doc);
  }

  onSubmit() {
    if (this.taskAssignmentId != null) {
      const doc = this.documents.pop();
      const req = {
        userId: this.userId,
        taskAssignmentId: this.taskAssignmentId,
        documentId: doc.id,
        title: this.fileName
      };

      this._store.dispatch(new taskAssignment.CreateTaskAssignmentAttachmentAction(req));
    } else {
      const doc = this.documents.pop();
      const req = {
        title: this.fileName,
        id: doc.id
      };

      this._store.dispatch(new document.UpdateDocumentAction(req));
    }
  }

  closeModal() {
    $('.modal-backdrop').remove();
    $('.modal.fade').remove();
    $('body').removeClass('modal-open');
  }
}
