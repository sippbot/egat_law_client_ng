import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Department } from '../common/models/department.model';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import * as department from '../common/actions/departments.action';
import * as reducer from '../common/reducers/reducer';
import { User } from '../common/models/user.model';
declare var $: any;

@Component({
  selector: 'cat-page',
  templateUrl: './document-new.html'
})

export class DocumentNew implements OnInit {
  departmentNewForm: FormGroup;
  @Input()
  users: User[];
  private dep: Department;
  private count: number;

  constructor(private _store: Store<reducer.State>,
    private fb: FormBuilder,
    private router: Router) {
    // initial form
    this.departmentNewForm = this.fb.group({
      id: ['', [Validators.required]],
      name: ['', [Validators.required]],
      aliasName: ['', [Validators.required]],
      email: ['', [Validators.required]]
    });

    this._store.let(reducer.getDepartments).subscribe(departments => {
      if (this.count == null) {
        this.count = departments.length;
      } else {
        if (this.count < departments.length) {
          this.redirectToIndex();
        }
      }
    }
    );
  }

  ngOnInit(): void {

    $(function () {
      $('.select2').select2();
      $('.select2-tags').select2({
        tags: true,
        tokenSeparators: [',', ' ']
      });
      $('.add-administrator').click(function () {
        $('.select2-selection__rendered li.select2-selection__choice').each(function () {
          const title = $(this).attr('title');
          const content = '<tr>' +
            '<td>' +
            '<input name="administrator[firstname][]"' +
            'type="text"' +
            'class="form-control"' +
            'value="' + title + '" disabled>' +
            '</td>' +
            '<td>' +
            '<input name="administrator[lastname][]"' +
            'type="text"' +
            'class="form-control"' +
            'value="" disabled>' +
            '</td>' +
            ' <td>' +
            '<select name="administrator[role][]"' +
            'class="form-control select"' +
            'data-validation="[NOTEMPTY]">' +
            '<option value="ผู้ใช้ทั่วไป">ผู้ใช้ทั่วไป</option>' +
            '<option value="ผู้ใช้หลัก">ผู้ใช้หลัก</option>' +
            '</select>' +
            '</td>' +
            '</tr>';
          $('table tbody').append(content);
        });

      });
    });
  }
  onSubmit(): void {
    this.dep = this.departmentNewForm.value;
    this._store.dispatch(new department.CreateDepartmentAction(this.dep));
  }

  redirectToIndex(): void {
    this.router.navigate(['/generalinfo/department-index']);
  }

}
