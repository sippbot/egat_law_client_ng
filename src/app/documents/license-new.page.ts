import { ACL } from '../common/util/acl.util';
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Department } from '../common/models/department.model';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import * as department from '../common/actions/departments.action';
import * as reducer from '../common/reducers/reducer';
import { IMyOptions } from 'mydatepicker-th';
import { User } from '../common/models/user.model';

declare var $: any;

@Component({
    selector: 'cat-page',
    templateUrl: './license-new.html'
})

export class DocumentLicenseNew implements OnInit {
    departmentNewForm: FormGroup;
    @Input()
    users: User[];
    private dep: Department;
    private count: number;
    acl: ACL;
    placeholder = 'dd/mm/yyyy';
    myDatePickerOptions: IMyOptions = {
        dateFormat: 'dd/mm/yyyy',
        editableDateField: false,
        openSelectorOnInputClick: true
    };

    constructor(private _store: Store<reducer.State>,
        private fb: FormBuilder,
        private router: Router) {
        this.acl = new ACL();
        // initial form
        this.departmentNewForm = this.fb.group({
            id: ['', [Validators.required]],
            name: ['', [Validators.required]],
            aliasName: ['', [Validators.required]],
            license_renewal_date: ['', [Validators.required]],
            expired_date: ['', [Validators.required]],
            allow_date: ['', [Validators.required]],
            email: ['', [Validators.required]]
        });

        this._store.let(reducer.getDepartments).subscribe(departments => {
            if (this.count == null) {
                this.count = departments.length;
            } else {
                if (this.count < departments.length) {
                    this.redirectToIndex();
                }
            }
        }
        );
    }

    ngOnInit(): void {
        $(function () {
            $('.select2').select2();
            $('.select2-tags').select2({
                tags: true,
                tokenSeparators: [',', ' ']
            });

            $('.add-administrator').click(function () {
                $('.select2-selection__rendered li.select2-selection__choice').each(function () {
                    const title = $(this).attr('title');
                    const content = '<tr>' +
                        '<td>' +
                        '<input name="administrator[firstname][]"' +
                        'type="text"' +
                        'class="form-control"' +
                        'value="' + title + '" disabled>' +
                        '</td>' +
                        '<td>' +
                        '<input name="administrator[lastname][]"' +
                        'type="text"' +
                        'class="form-control"' +
                        'value="" disabled>' +
                        '</td>' +
                        ' <td>' +
                        '<select name="administrator[role][]"' +
                        'class="form-control select"' +
                        'data-validation="[NOTEMPTY]">' +
                        '<option value="ผู้ใช้ทั่วไป">ผู้ใช้ทั่วไป</option>' +
                        '<option value="ผู้ใช้หลัก">ผู้ใช้หลัก</option>' +
                        '</select>' +
                        '</td>' +
                        '</tr>';
                    $('table tbody').append(content);
                });
            });
        });
    }

    onSubmit(): void {
        this.dep = this.departmentNewForm.value;
        this._store.dispatch(new department.CreateDepartmentAction(this.dep));
    }

    redirectToIndex(): void {
        this.router.navigate(['/generalinfo/department-index']);
    }
    role(role: string): boolean {
        return this.acl.hasRole(role);
    }
    isReadOnly(role_list: Array<string>): boolean {
        let result = false;
        let thisobj = this;
        $.each(role_list, function (i, role) {
            result = result || (thisobj.acl.isReadOnly(role) === false)
        });
        return !result;
    }
}


/* export class GeneralinfoDepartmentNew implements OnInit{
  ngOnInit(){
    $(function () {
      $('.select2').select2();
      $('.select2-tags').select2({
          tags: true,
          tokenSeparators: [',', ' ']
      });

    $(".add-administrator").click(function() {

          $( ".select2-selection__rendered li.select2-selection__choice" ).each(function() {
            var title = $(this).attr('title');
            var content = '<tr>'+
                                '<td>'+
                                   '<input name="administrator[firstname][]"'+
                                         'type="text"'+
                                         'class="form-control"'+
                                         'value="'+title+'" disabled>'+
                                '</td>'+
                                '<td>'+
                                  '<input name="administrator[lastname][]"'+
                                      'type="text"'+
                                      'class="form-control"'+
                                      'value="" disabled>'+
                                    '</td>'+
                              ' <td>'+
                                  '<select name="administrator[role][]"'+
                                  'class="form-control select"'+
                                  'data-validation="[NOTEMPTY]">'+
                                    '<option value="ผู้ใช้ทั่วไป">ผู้ใช้ทั่วไป</option>'+
                                    '<option value="ผู้ใช้หลัก">ผู้ใช้หลัก</option>'+
                                  '</select>'+
                                '</td>'+
                              '</tr>';
             $("table tbody").append(content);
          });

        });
    });

  }

} */
