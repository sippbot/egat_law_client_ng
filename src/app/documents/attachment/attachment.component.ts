import { TaskAssignmentEffects } from '../../common/effects/task-assignment.effect';
import { ShowAlertAction } from '../../common/actions/alert.action';
import { Actions } from '@ngrx/effects';
import { DataTableDirective } from 'angular-datatables';
import { ApplicationConstant } from '../../common/application-constant';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { License } from '../../common/models/license.model';
import { Store } from '@ngrx/store';
import { Component, OnInit } from '@angular/core';
import * as reducer from '../../common/reducers/reducer';
import * as documents from '../../common/actions/document.action';
import * as license from '../../common/actions/license.action';
import * as taskAssignment from '../../common/actions/task-assignment.action';
import { Document } from '../../common/models/models';
import { Alert } from '../../common/models/alert.model';
declare var $: any;

@Component({
  selector: 'app-attachment',
  templateUrl: './attachment.component.html'
})
export class AttachmentComponent implements OnInit {
  dtElement: DataTableDirective;
  documents: Array<Document>;
  licenses: Array<License>;
  userId: number;
  taskAssignmentId: number;
  mode: string;
/*
'columnDefs': [
      { 'width': '30%', 'targets': 0 },
      { 'width': '15%', 'targets': 1 },
      { 'width': '15%', 'targets': 2 },
      { 'width': '10%', 'targets': 3 },
      { 'width': '10%', 'targets': 4 },
      { 'width': '20%', 'targets': 5 }
    ],
*/
  dtOptionsLicense: DataTables.Settings = {
    'autoWidth': true,
    'pageLength': 30,
    'language': ApplicationConstant.DATATABLE_GLOBAL_LANGUAGE_CONFIG
  };
/*
    'columnDefs': [
      { 'width': '70%', 'targets': 0 },
      { 'width': '30%', 'targets': 1 }
    ],
*/
  dtOptionsDocument: DataTables.Settings = {
    'autoWidth': true,
    'pageLength': 30,
    'language': ApplicationConstant.DATATABLE_GLOBAL_LANGUAGE_CONFIG
  };

  constructor(public bsModalRef: BsModalRef,
    private _store: Store<reducer.State>,
    private taskAssignmentEffects: TaskAssignmentEffects) {
    this.mode = 'documents';
    this.documents = new Array<Document>();
    this._store.let(reducer.getDocuments).subscribe(documents => {
      this.documents = documents;
    }
    );
    this._store.let(reducer.getLicenses).subscribe(licenses => {
      this.licenses = licenses;
    }
    );
    this.taskAssignmentEffects.createTaskAttachment$.filter(
      action => action.type === taskAssignment.ActionTypes.CREATE_TASKASSIGNMENT_ATTACHMENT_SUCCESS
    ).subscribe(() => {
      this._store.dispatch(new ShowAlertAction(new Alert('แนบไฟล์เรียบร้อยแล้ว', 'แนบไฟล์สำเร็จ!', 'success')));
      this.closeModal();
      // $('.modal-dialog').hide();
      // this.bsModalRef.hide();
    });
  }

  ngOnInit() {
    if (this.mode === 'documents') {
      this._store.dispatch(new documents.LoadDocumentAction(this.userId));
    } else {
      this._store.dispatch(new license.LoadLicensesAction(this.userId));
    }

  }

  onSubmit(documentId: number) {
    console.log("submit")
    const req = {
      userId: this.userId,
      taskAssignmentId: this.taskAssignmentId,
      documentId: documentId
    };

   
    this._store.dispatch(new taskAssignment.CreateTaskAssignmentAttachmentAction(req));
  }

  closeModal(){
    $(".modal-backdrop").remove();
    $(".modal.fade").remove();
    $("body").removeClass("modal-open")
  }

}
