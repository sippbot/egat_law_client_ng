import { ACL } from '../common/util/acl.util';
import { Store } from '@ngrx/store';
import * as reducer from '../common/reducers/reducer';
import { Document } from '../common/models/document.model';
import { OnInit, Component, ViewChild } from '@angular/core';
import { ApplicationConstant } from '../common/application-constant';
import { DataTableDirective } from 'angular-datatables';
import * as documents from '../common/actions/document.action';
import { DocumentUploadComponent } from './upload/upload.component';
import { BsModalService } from 'ngx-bootstrap/modal';
import { DocumentEffects } from '../common/effects/document.effect';
import * as document from '../common/actions/document.action';

import { Subject } from 'rxjs';
declare var $: any;

@Component({
  selector: 'cat-page',
  templateUrl: './manage-document.html'
})

export class DocumentManage implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  public documents_list: Array<Document>;
  // public document$: Observable<Array<Document>>;
  public userId: number;
  acl: ACL;
  dtTrigger: Subject<any> = new Subject();
  /*
      'columnDefs': [
        { 'width': '40%', 'targets': 0 },
        { 'width': '20%', 'targets': 1 },
        { 'width': '20%', 'targets': 2 },
        { 'width': '20%', 'targets': 3 }
      ],
  */
  dtOptions: DataTables.Settings = {
    'autoWidth': true,
    'pageLength': 30,
    'language': ApplicationConstant.DATATABLE_GLOBAL_LANGUAGE_CONFIG,
    'dom': '<\'row\'<\'col-sm-12\'B>>' +
      '<\'row\'<\'col-sm-6\'l><\'col-sm-6\'f>>' +
      '<\'row\'<\'col-sm-12\'tr>>' +
      '<\'row\'<\'col-sm-5\'i><\'col-sm-7\'p>>',
    'buttons': [
      {
        extend: 'excelHtml5',
        text: '<i class="fa fa-file-excel-o fa-x5"></i> ดาวน์โหลด Excel',
        className: 'btn btn-primary btn-sm glyphicon glyphicon-list-alt pull-right',
        exportOptions:{
          columns:[0,1,2,3]
        }
      },
      {
        extend: 'print',
        text: '<i class="fa fa-print fa-x5"></i> พิมพ์รายงาน',
        className: 'btn btn-primary btn-sm btn-print glyphicon glyphicon-print pull-right',
        pageSize: 'A4',
        title: 'รายการเอกสาร',
        exportOptions:{
          columns:[0,1,2,3]
        }
      }
    ]
  };

  constructor(private _store: Store<reducer.State>,
    private documentEffects:DocumentEffects,
    private modalService: BsModalService) {
    this.acl = new ACL();
    this.documents_list = new Array<Document>();
    this.userId = +localStorage.getItem('userId');
    this._store.let(reducer.getDocuments).subscribe(
      documents => {
        this.documents_list = documents
        this.rerender()
        return this.documents_list
      }
    );
    this.documentEffects.loadDocuments$.filter(
      action => action.type === document.ActionTypes.LOAD_DOCUMENT_SUCCESS
    ).subscribe(() => {
      this.rerender()
    });
  }
  rerender(): void {
    if (this.dtElement) {
      if(this.dtElement.dtInstance!=undefined){
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          // Destroy the table first
          dtInstance.destroy();
          // Call the dtTrigger to rerender again
          this.dtTrigger.next();
        });
      }else{
        this.dtTrigger.next();
      }
    }
  }
  ngOnInit() {
    this._store.dispatch(new documents.LoadDocumentAction(this.userId));
  }
  uploadDocument(){
    const initialState = {
      userId: this.userId,
      mode: 'upload_document'
    };
    this.modalService.show(DocumentUploadComponent, {initialState});
  }
  refresh() {
    this._store.dispatch(new documents.LoadDocumentAction(this.userId));
  }
  role(role: string): boolean {
    return this.acl.hasRole(role);
  }
  isReadOnly(role_list: Array<string>): boolean {
    let result = false;
    let thisobj = this;
    $.each(role_list,function(i,role){ 
      result = result||(thisobj.acl.isReadOnly(role)===false)
    });
    return !result;
  }
}
