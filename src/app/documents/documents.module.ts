import { SweetAlert2Module } from '@toverux/ngsweetalert2';
import { DataTablesModule } from 'angular-datatables';
import { DocumentUploadComponent } from './upload/upload.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DocumentManage } from './manage-document.page';
import { DocumentLicenseComponent } from './license.page';
import { DocumentLicenseNew } from './license-new.page';
import { DocumentNew } from './document-new.page';
import { DocumentService } from '../common/services/document.service';
import { AuthGuard } from '../guard/auth.guard';
import { MyDatePickerTHModule } from 'mydatepicker-th';
import { AttachmentComponent } from './attachment/attachment.component';
import { LicenseModule } from '../license/license.module';

export const routes: Routes = [
  { path: 'documentation/manage-document', component: DocumentManage, canActivate: [AuthGuard] },
  { path: 'documentation/license', component: DocumentLicenseComponent, canActivate: [AuthGuard] },
  { path: 'documentation/license-new', component: DocumentLicenseNew, canActivate: [AuthGuard] },
  { path: 'documentation/document-new', component: DocumentNew, canActivate: [AuthGuard] },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MyDatePickerTHModule,
    DataTablesModule,
    SweetAlert2Module,
    LicenseModule
  ],
  declarations: [
    DocumentManage,
    DocumentLicenseComponent,
    DocumentLicenseNew,
    DocumentNew,
    DocumentUploadComponent,
    AttachmentComponent
  ],
  exports: [
    DocumentUploadComponent,
    AttachmentComponent
  ],
  entryComponents: [
    AttachmentComponent,
    DocumentUploadComponent
  ],
  providers: [DocumentService]

})

export class DocumentsModule { }
